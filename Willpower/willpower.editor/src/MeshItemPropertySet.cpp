#include "willpower/editor/MeshItemPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		using namespace std;
		using namespace WP_NAMESPACE;

		void MeshItemPropertySet::addEntry(MeshItemProperty const& entry)
		{
			mEntries.push_back(entry);
		}

		int MeshItemPropertySet::getNumEntries() const
		{
			return (int)mEntries.size();
		}

		MeshItemProperty const& MeshItemPropertySet::getEntry(int index) const
		{
			return mEntries[index];
		}

		MeshItemProperty& MeshItemPropertySet::getEntry(int index)
		{
			return mEntries[index];
		}

		/*
		void MeshItemPropertySet::testValue(MeshItemPropertyEntry& entry)
		{
			entry.addValidator([](MeshItemPropertyEntry const& entry) -> MeshItemPropertyEntry::ValidatorResult {

				uint32_t value = entry.getValue<uint32_t>();
				if (value > 1000)
				{
					return MeshItemPropertyEntry::ValidatorResult(true, "Validation successful");
				}
				else
				{
					return MeshItemPropertyEntry::ValidatorResult(false, "Value too small");
				}
			});

			entry.setValue<uint32_t>(1500);
		}
		*/

	} // editor
} // WP_NAMESPACE
