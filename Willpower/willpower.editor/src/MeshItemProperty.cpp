#include "willpower/editor/MeshItemProperty.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		using namespace std;
		using namespace WP_NAMESPACE;

		MeshItemProperty::MeshItemProperty()
			: mValue(false)
			, mRanged(false)
			, mName("")
			, mWidget(EditorWidget::None)
		{
		}

		MeshItemProperty::MeshItemProperty(EntryType const& value)
			: mValue(value)
			, mRanged(false)
			, mName("")
			, mWidget(EditorWidget::None)
		{
		}

		MeshItemProperty::MeshItemProperty(EntryType const& value, EntryType const& minValue, EntryType const& maxValue)
			: mValue(value)
			, mMinimumValue(minValue)
			, mMaximumValue(maxValue)
			, mRanged(true)
			, mName("")
			, mWidget(EditorWidget::None)
		{

		}

		void MeshItemProperty::setName(string const& name)
		{
			mName = name;
		}

		string const& MeshItemProperty::getName() const
		{
			return mName;
		}

		bool MeshItemProperty::isRanged() const
		{
			return mRanged;
		}

		void MeshItemProperty::setWidget(EditorWidget widget)
		{
			mWidget = widget;
		}

		MeshItemProperty::EditorWidget MeshItemProperty::getWidget() const
		{
			return mWidget;
		}

		MeshItemProperty::DataType MeshItemProperty::getType() const
		{
			return (MeshItemProperty::DataType)mValue.index();
		}

		void MeshItemProperty::addValidator(MeshItemProperty::Validator validator)
		{
			mValidators.push_back(validator);
		}

	} // editor
} // WP_NAMESPACE
