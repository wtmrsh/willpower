#include "utils/StringUtils.h"

#include "willpower/editor/Colour.h"

#include "willpower/serialization/SerializationUtils.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		using namespace std;
		using namespace utils;

		Colour::Colour(float r, float g, float b, float a)
			: red(r)
			, green(g)
			, blue(b)
			, alpha(a)
		{
		}

		Colour::Colour(istream& fp)
		{
			red = serialization::SerializationUtils::readReal32(fp);
			green = serialization::SerializationUtils::readReal32(fp);
			blue = serialization::SerializationUtils::readReal32(fp);
			alpha = serialization::SerializationUtils::readReal32(fp);
		}

		string Colour::toString() const
		{
			return STR_FORMAT("{},{},{},{}", red, green, blue, alpha);
		}

		void Colour::write(ostream& fp) const
		{
			serialization::SerializationUtils::writeReal32(fp, red);
			serialization::SerializationUtils::writeReal32(fp, green);
			serialization::SerializationUtils::writeReal32(fp, blue);
			serialization::SerializationUtils::writeReal32(fp, alpha);
		}

		ostream& operator<<(ostream& os, Colour const& c)
		{
			os << c.toString();
			return os;
		}

	} // editor
} // WP_NAMESPACE
