#include "willpower/editor/VertexPropertyCollection.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		using namespace std;
		using namespace WP_NAMESPACE;

		VertexPropertyCollection::VertexPropertyCollection(string const& name)
			: geometry::MeshPropertyCollection<VertexPropertySet>(name, "VertexProperties", "Vertex Properties")
		{
		}

		VertexPropertyCollection::VertexPropertyCollection(string const& name, InitialiseVertexPropertySetFunction function)
			: geometry::MeshPropertyCollection<VertexPropertySet>(name, "VertexProperties", "Vertex Properties")
		{
			setInitialiseVertexPropertySetFunction(function);
		}

		VertexPropertyCollection::VertexPropertyCollection(VertexPropertyCollection const& other)
			: MeshPropertyCollection(other)
		{
			mInitialiseVertexPropertySet = other.mInitialiseVertexPropertySet;
		}

		void VertexPropertyCollection::setInitialiseVertexPropertySetFunction(InitialiseVertexPropertySetFunction function)
		{
			mInitialiseVertexPropertySet = function;
		}

		VertexPropertySet* VertexPropertyCollection::createItem(VertexPropertySet const* prototype)
		{
			auto propSet = new VertexPropertySet();
			
			mInitialiseVertexPropertySet(propSet, prototype);
			return propSet;
		}

		void VertexPropertyCollection::writeItemBinary(VertexPropertySet const* item, ostream& fp)
		{
			int numEntries = item->getNumEntries();
			
			serialization::SerializationUtils::writeUint32(fp, numEntries);
			
			for (int i = 0; i < numEntries; ++i)
			{
				auto const& entry = item->getEntry(i);

				uint32_t entryType = (uint32_t)entry.getType();
				
				serialization::SerializationUtils::writeUint32(fp, entryType);
				serialization::SerializationUtils::writeString(fp, entry.getName());
				
				switch (entryType)
				{
				case MeshItemProperty::DataType::Boolean:
					serialization::SerializationUtils::writeInt32(fp, entry.getValue<bool>() ? 1 : 0);
					break;

				case MeshItemProperty::DataType::SignedInt8:
					serialization::SerializationUtils::writeInt8(fp, entry.getValue<int8_t>());
					break;

				case MeshItemProperty::DataType::SignedInt16:
					serialization::SerializationUtils::writeInt16(fp, entry.getValue<int16_t>());
					break;

				case MeshItemProperty::DataType::SignedInt32:
					serialization::SerializationUtils::writeInt32(fp, entry.getValue<int32_t>()); 
					break;

				case MeshItemProperty::DataType::SignedInt64:
					serialization::SerializationUtils::writeInt64(fp, entry.getValue<int64_t>()); 
					break;

				case MeshItemProperty::DataType::UnsignedInt8:
					serialization::SerializationUtils::writeUint8(fp, entry.getValue<uint8_t>());
					break;

				case MeshItemProperty::DataType::UnsignedInt16:
					serialization::SerializationUtils::writeUint16(fp, entry.getValue<uint16_t>()); 
					break;

				case MeshItemProperty::DataType::UnsignedInt32:
					serialization::SerializationUtils::writeUint32(fp, entry.getValue<uint32_t>()); 
					break;

				case MeshItemProperty::DataType::UnsignedInt64:
					serialization::SerializationUtils::writeUint64(fp, entry.getValue<uint64_t>()); 
					break;

				case MeshItemProperty::DataType::Real32:
					serialization::SerializationUtils::writeReal32(fp, entry.getValue<float>()); 
					break;

				case MeshItemProperty::DataType::Real64:
					serialization::SerializationUtils::writeReal64(fp, entry.getValue<double>());
					break;

				case MeshItemProperty::DataType::String:
					serialization::SerializationUtils::writeString(fp, entry.getValue<string>());
					break;

				case MeshItemProperty::DataType::Colour:
					entry.getValue<Colour>().write(fp);
					break;
				}
			}
		}

		VertexPropertySet* VertexPropertyCollection::readItemBinary(istream& fp)
		{
			auto propSet = new VertexPropertySet();
			mInitialiseVertexPropertySet(propSet, nullptr);

			uint32_t numEntries = serialization::SerializationUtils::readUint32(fp);

			for (uint32_t i = 0; i < numEntries; ++i)
			{
				uint32_t entryType = serialization::SerializationUtils::readUint32(fp);
				string entryName = serialization::SerializationUtils::readString(fp);

				auto& entry = propSet->getEntry(i);

				ASSERT_TRACE(entry.getName() == entryName && "VertexPropertyCollection::readItemBinary() name mismatch.");
				ASSERT_TRACE((uint32_t)entry.getType() == entryType && "VertexPropertyCollection::readItemBinary() type mismatch.");

				switch (entryType)
				{
				case MeshItemProperty::DataType::Boolean:
					entry.setValue<bool>(serialization::SerializationUtils::readInt32(fp) ? true : false);
					break;

				case MeshItemProperty::DataType::SignedInt8:
					entry.setValue<int8_t>(serialization::SerializationUtils::readInt8(fp));
					break;

				case MeshItemProperty::DataType::SignedInt16:
					entry.setValue<int16_t>(serialization::SerializationUtils::readInt16(fp));
					break;

				case MeshItemProperty::DataType::SignedInt32:
					entry.setValue<int32_t>(serialization::SerializationUtils::readInt32(fp));
					break;

				case MeshItemProperty::DataType::SignedInt64:
					entry.setValue<int64_t>(serialization::SerializationUtils::readInt64(fp));
					break;

				case MeshItemProperty::DataType::UnsignedInt8:
					entry.setValue<uint8_t>(serialization::SerializationUtils::readUint8(fp));
					break;

				case MeshItemProperty::DataType::UnsignedInt16:
					entry.setValue<uint16_t>(serialization::SerializationUtils::readUint16(fp));
					break;

				case MeshItemProperty::DataType::UnsignedInt32:
					entry.setValue<uint32_t>(serialization::SerializationUtils::readUint32(fp));
					break;

				case MeshItemProperty::DataType::UnsignedInt64:
					entry.setValue<uint64_t>(serialization::SerializationUtils::readUint64(fp));
					break;

				case MeshItemProperty::DataType::Real32:
					entry.setValue<float>(serialization::SerializationUtils::readReal32(fp));
					break;

				case MeshItemProperty::DataType::Real64:
					entry.setValue<double>(serialization::SerializationUtils::readReal64(fp));
					break;

				case MeshItemProperty::DataType::String:
					entry.setValue<string>(serialization::SerializationUtils::readString(fp));
					break;

				case MeshItemProperty::DataType::Colour:
					entry.setValue<Colour>(Colour(fp));
					break;
				}
			}

			return propSet;
		}

		void VertexPropertyCollection::writeItemText(VertexPropertySet const* item, ostream& fp)
		{
			int numEntries = item->getNumEntries();

			fp << "# " << numEntries << " entries." << endl;

			for (int i = 0; i < numEntries; ++i)
			{
				auto const& entry = item->getEntry(i);
				uint32_t entryType = (uint32_t)entry.getType();

				fp << entryType << ": " << entry.getName() << ": ";

				switch (entryType)
				{
				case MeshItemProperty::DataType::Boolean:
					fp << entry.getValue<bool>();
					break;
				case MeshItemProperty::DataType::Colour:
				{
					auto c = entry.getValue<editor::Colour>();
					fp << "(" << c.red << "," << c.green << "," << c.blue << "," << c.alpha << ")";
				}
				break;
				case MeshItemProperty::DataType::Real32:
					fp << entry.getValue<float>();
					break;
				case MeshItemProperty::DataType::Real64:
					fp << entry.getValue<double>();
					break;
				case MeshItemProperty::DataType::SignedInt8:
					fp << entry.getValue<int8_t>();
					break;
				case MeshItemProperty::DataType::SignedInt16:
					fp << entry.getValue<int16_t>();
					break;
				case MeshItemProperty::DataType::SignedInt32:
					fp << entry.getValue<int32_t>();
					break;
				case MeshItemProperty::DataType::SignedInt64:
					fp << entry.getValue<int64_t>();
					break;
				case MeshItemProperty::DataType::UnsignedInt8:
					fp << entry.getValue<uint8_t>();
					break;
				case MeshItemProperty::DataType::UnsignedInt16:
					fp << entry.getValue<uint16_t>();
					break;
				case MeshItemProperty::DataType::UnsignedInt32:
					fp << entry.getValue<uint32_t>();
					break;
				case MeshItemProperty::DataType::UnsignedInt64:
					fp << entry.getValue<uint64_t>();
					break;
				case MeshItemProperty::DataType::String:
					fp << entry.getValue<bool>();
					break;
				default:
					throw exception("Unknown MeshItemProperty::DataType");
				}

				fp << "\n";
			}
		}

		VertexPropertySet* VertexPropertyCollection::readItemText(istream& fp)
		{
			WP_UNUSED(fp);

			return nullptr;
		}

	} // editor
} // WP_NAMESPACE
