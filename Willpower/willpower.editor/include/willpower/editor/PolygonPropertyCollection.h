#pragma once

#include <vector>
#include <functional>


#include "willpower/geometry/MeshPropertyCollection.h"

#include "willpower/editor/Platform.h"
#include "willpower/editor/PolygonPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API PolygonPropertyCollection : public geometry::MeshPropertyCollection<PolygonPropertySet>
		{
		public:

			typedef std::function<void(PolygonPropertySet*, PolygonPropertySet const*)> InitialisePolygonPropertySetFunction;

		private:

			InitialisePolygonPropertySetFunction mInitialisePolygonPropertySet;

		private:

			PolygonPropertySet* createItem(PolygonPropertySet const* prototype);

			void writeItemBinary(PolygonPropertySet const* item, std::ostream& fp);

			PolygonPropertySet* readItemBinary(std::istream& fp);

			void writeItemText(PolygonPropertySet const* item, std::ostream& fp);

			PolygonPropertySet* readItemText(std::istream& fp);

		public:

			PolygonPropertyCollection(std::string const& name);

			PolygonPropertyCollection(std::string const& name, InitialisePolygonPropertySetFunction function);

			PolygonPropertyCollection(PolygonPropertyCollection const& other);

			void setInitialisePolygonPropertySetFunction(InitialisePolygonPropertySetFunction function);
		};

	} // editor
} // WP_NAMESPACE
