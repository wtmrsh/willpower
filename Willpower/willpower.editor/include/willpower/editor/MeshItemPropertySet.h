#pragma once

#include <vector>


#include "willpower/editor/Platform.h"
#include "willpower/editor/MeshItemProperty.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API MeshItemPropertySet
		{
			std::vector<MeshItemProperty> mEntries;

		public:

			void addEntry(MeshItemProperty const& entry);

			int getNumEntries() const;

			MeshItemProperty const& getEntry(int index) const;

			MeshItemProperty& getEntry(int index);

			//void testValue(MeshItemPropertyEntry& entry);
		};

	} // editor
} // WP_NAMESPACE
