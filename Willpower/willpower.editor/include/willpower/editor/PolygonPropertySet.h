#pragma once


#include "willpower/editor/Platform.h"
#include "willpower/editor/MeshItemPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API PolygonPropertySet : public MeshItemPropertySet
		{
		public:

			PolygonPropertySet() = default;

			virtual ~PolygonPropertySet() = default;
		};

		class PolygonPropertySetFactory
		{
		public:

			virtual PolygonPropertySet* createPolygonPropertySet() = 0;
		};

	} // editor
} // WP_NAMESPACE
