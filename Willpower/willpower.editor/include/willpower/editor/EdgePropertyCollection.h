#pragma once

#include <vector>
#include <functional>


#include "willpower/geometry/MeshPropertyCollection.h"

#include "willpower/editor/Platform.h"
#include "willpower/editor/EdgePropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API EdgePropertyCollection : public geometry::MeshPropertyCollection<EdgePropertySet>
		{
		public:

			typedef std::function<void(EdgePropertySet*, EdgePropertySet const*)> InitialiseEdgePropertySetFunction;

		private:

			InitialiseEdgePropertySetFunction mInitialiseEdgePropertySet;

		private:

			EdgePropertySet* createItem(EdgePropertySet const* prototype);

			void writeItemBinary(EdgePropertySet const* item, std::ostream& fp);

			EdgePropertySet* readItemBinary(std::istream& fp);

			void writeItemText(EdgePropertySet const* item, std::ostream& fp);

			EdgePropertySet* readItemText(std::istream& fp);

		public:

			EdgePropertyCollection(std::string const& name);

			EdgePropertyCollection(std::string const& name, InitialiseEdgePropertySetFunction function);

			EdgePropertyCollection(EdgePropertyCollection const& other);

			void setInitialiseEdgePropertySetFunction(InitialiseEdgePropertySetFunction function);
		};

	} // editor
} // WP_NAMESPACE
