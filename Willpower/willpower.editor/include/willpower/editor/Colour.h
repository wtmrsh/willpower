#pragma once

#include <ostream>
#include <istream>

#include <utils/StringUtils.h>


#include "willpower/editor/Platform.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		struct WP_EDITOR_API Colour
		{
			float red, green, blue, alpha;

		public:

			Colour(float r, float g, float b, float a = 1.0f);

			explicit Colour(std::istream& fp);

			std::string toString() const;

			void write(std::ostream& fp) const;
		};

		std::ostream& operator << (std::ostream& os, Colour const& c);

	} // editor
} // WP_NAMESPACE
