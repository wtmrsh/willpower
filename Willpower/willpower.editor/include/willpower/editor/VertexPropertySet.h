#pragma once


#include "willpower/editor/Platform.h"
#include "willpower/editor/MeshItemPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API VertexPropertySet : public MeshItemPropertySet
		{
		public:
			
			VertexPropertySet() = default;

			virtual ~VertexPropertySet() = default;
		};

		class VertexPropertySetFactory
		{
		public:

			virtual VertexPropertySet* createVertexPropertySet() = 0;
		};

	} // editor
} // WP_NAMESPACE
