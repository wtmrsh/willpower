#pragma once

#include <vector>
#include <functional>


#include "willpower/geometry/MeshPropertyCollection.h"

#include "willpower/editor/Platform.h"
#include "willpower/editor/VertexPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API VertexPropertyCollection : public geometry::MeshPropertyCollection<VertexPropertySet>
		{
		public:

			typedef std::function<void(VertexPropertySet*, VertexPropertySet const*)> InitialiseVertexPropertySetFunction;

		private:

			InitialiseVertexPropertySetFunction mInitialiseVertexPropertySet;

		private:

			VertexPropertySet* createItem(VertexPropertySet const* prototype);

			void writeItemBinary(VertexPropertySet const* item, std::ostream& fp);

			VertexPropertySet* readItemBinary(std::istream& fp);

			void writeItemText(VertexPropertySet const* item, std::ostream& fp);

			VertexPropertySet* readItemText(std::istream& fp);

		public:

			VertexPropertyCollection(std::string const& name);

			VertexPropertyCollection(std::string const& name, InitialiseVertexPropertySetFunction function);

			VertexPropertyCollection(VertexPropertyCollection const& other);

			void setInitialiseVertexPropertySetFunction(InitialiseVertexPropertySetFunction function);
		};

	} // editor
} // WP_NAMESPACE
