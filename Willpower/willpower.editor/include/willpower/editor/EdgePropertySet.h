#pragma once


#include "willpower/editor/Platform.h"
#include "willpower/editor/MeshItemPropertySet.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API EdgePropertySet : public MeshItemPropertySet
		{
		public:

			EdgePropertySet() = default;

			virtual ~EdgePropertySet() = default;
		};

		class EdgePropertySetFactory
		{
		public:

			virtual EdgePropertySet* createEdgePropertySet() = 0;
		};

	} // editor
} // WP_NAMESPACE
