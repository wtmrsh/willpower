#pragma once

#include <vector>
#include <functional>
#include <variant>

#include "willpower/editor/Platform.h"
#include "willpower/editor/Colour.h"

namespace WP_NAMESPACE
{
	namespace editor
	{

		class WP_EDITOR_API MeshItemProperty
		{
		public:

			enum DataType
			{
				Boolean,
				SignedInt8,
				SignedInt16,
				SignedInt32,
				SignedInt64,
				UnsignedInt8,
				UnsignedInt16,
				UnsignedInt32,
				UnsignedInt64,
				Real32,
				Real64,
				String,
				Colour
			};

			enum EditorWidget
			{
				None,
				Checkbox,
				Spinbox,
				Slider,
				Radioboxes,
				LineEdit,
				Combobox,
				ColourPicker
			};

		public:

			typedef std::pair<bool, std::string> ValidatorResult;

			typedef std::function<ValidatorResult(MeshItemProperty const&)> Validator;

			// This declaration must match the order of DataType enum
			typedef std::variant<
				bool,
				int8_t,
				int16_t,
				int32_t,
				int64_t,
				uint8_t,
				uint16_t,
				uint32_t,
				uint64_t,
				float,
				double,
				std::string,
				editor::Colour
			> EntryType;

		private:

			EntryType mValue, mMinimumValue, mMaximumValue;

			bool mRanged;

			std::string mName;

			EditorWidget mWidget;

			std::vector<Validator> mValidators;

		public:

			MeshItemProperty();

			MeshItemProperty(EntryType const& value);

			MeshItemProperty(EntryType const& value, EntryType const& minValue, EntryType const& maxValue);

			void setName(std::string const& name);

			std::string const& getName() const;

			bool isRanged() const;

			void setWidget(EditorWidget widget);

			EditorWidget getWidget() const;

			DataType getType() const;
			
			void addValidator(Validator validator);

			template<typename T>
			ValidatorResult setValue(T const& value)
			{
				/*
				if (mRanged && (value < mMinimumValue || value > mMaximumValue))
				{
					return ValidatorResult(false, "Value out of range");
				}
				*/

				for (auto const validator: mValidators)
				{
					MeshItemProperty e(value);
					auto result = validator(e);
					if (!result.first)
					{
						return result;
					}
				}

				mValue = value;
				return ValidatorResult(true, "Validation successful");
			}

			template<typename T>
			void setRange(T const& minValue, T const& maxValue)
			{
				mMinimumValue = minValue;
				mMaximumValue = maxValue;
				mRanged = true;
			}

			EntryType const& getValue() const
			{
				return mValue;
			}

			template<typename T>
			T const& getValue() const
			{
				return std::get<T>(mValue);
			}

			template<typename T>
			T const& getMinimumValue() const
			{
				return std::get<T>(mMinimumValue);
			}

			template<typename T>
			T const& getMaximumValue() const
			{
				return std::get<T>(mMaximumValue);
			}
		};

	} // editor
} // WP_NAMESPACE
