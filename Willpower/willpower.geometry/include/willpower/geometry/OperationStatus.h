#pragma once

#include "willpower/geometry/Platform.h"

namespace WP_NAMESPACE
{
	namespace geometry
	{

		enum OperationStatus
		{
			Success,
			GenericFailure,
			InvalidEdge,
		};

	} // geometry
} // WP_NAMESPACE
