
#include <list>
#include <vector>
#include <set>

#include "willpower/geometry/Platform.h"

namespace WP_NAMESPACE
{
	namespace geometry
	{

		typedef std::list<uint32_t> IndexList;
		typedef std::vector<uint32_t> IndexVector;
		typedef std::set<uint32_t> IndexSet;

	} // geometry
} // WP_NAMESPACE
#pragma once
