#include "willpower/common/Exceptions.h"

#include "willpower/application/resourcesystem/ShaderResourceDefinitionFactory.h"

namespace WP_NAMESPACE
{
	namespace application
	{
		namespace resourcesystem
		{
			using namespace std;

			ShaderResourceDefinitionFactory::ShaderResourceDefinitionFactory(string const& factoryType)
				: ResourceDefinitionFactory("Shader", factoryType)
			{
			}

		} // resourcesystem
	} // application
} // WP_NAMESPACE