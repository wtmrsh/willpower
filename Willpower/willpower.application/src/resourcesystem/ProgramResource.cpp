#include <utils/StringUtils.h>

#include <mpp/ProgrammaticProgramStream.h>
#include <mpp/program/Parser.h>

#include "willpower/common/Exceptions.h"

#include "willpower/application/resourcesystem/ProgramResource.h"
#include "willpower/application/resourcesystem/ShaderResource.h"

namespace WP_NAMESPACE
{
	namespace application
	{
		namespace resourcesystem
		{
			using namespace std;
			using namespace utils;

			ProgramResource::ProgramResource(string const& name, string const& namesp, string const& source, map<string, string> const& tags, ResourceLocation* location)
				: Resource(name, namesp, "Program", source, tags, location)
			{
			}

			bool ProgramResource::load(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
			{
				WP_UNUSED(renderSystem);

				auto vertShader = static_cast<ShaderResource*>(getDependentResource("Vertex").get());
				auto fragShader = static_cast<ShaderResource*>(getDependentResource("Fragment").get());

				auto parser = make_shared<mpp::program::Parser>();

				parser->setMeshSpecification(mSpecification);
				parser->setVertexSource(vertShader->getText());
				parser->setFragmentSource(fragShader->getText());

				auto ps = make_shared<mpp::ProgrammaticProgramStream>(resourceMgr);
				ps->setParser(parser);

				mMppResource = resourceMgr->declareResource(getQualifiedName(), ps).first;
				mMppResource->acquire(this);
				return true;
			}

			bool ProgramResource::unload(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
			{
				WP_UNUSED(renderSystem);
				WP_UNUSED(resourceMgr);

				mMppResource->release(this);
				return true;
			}

		} // resourcesystem
	} // application
} // WP_NAMESPACE