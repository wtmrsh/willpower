#pragma once


#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Types.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API PathDatabase
		{
			friend class ConvexPolygonisation;

		public:

			struct Entry
			{
				EdgeIndex* base;
				uint32_t length;

			public:

				Entry()
					: base(nullptr)
					, length(0)
				{
				}
			};

		private:

			EdgeIndex* mData;

			EdgeIndex* mpHead;

			Entry* mEntries;

			std::vector<bool> mIsCalculated;

			uint32_t mNumPolygons;

		private:

			inline uint32_t triangular(uint32_t n) const
			{
				return n * (n + 1) / 2;
			}

			inline uint32_t getIndex(EdgeIndex source, EdgeIndex target) const
			{
				uint32_t source1 = source + 1;
				return triangular(mNumPolygons - 1) - triangular(mNumPolygons - source1) + (target - source1);
			}

			void createEntry(PolygonIndex source, PolygonIndex target, EdgeIndex* base, uint32_t length);

			EdgeIndex* getHead() const;

			EdgeIndex* getDataAtOffset(uint32_t offset);

		public:

			PathDatabase();

			~PathDatabase();

			void setSize(int numPolygons);

			uint32_t getFreeSpace() const;

			Entry const& getEntry(PolygonIndex source, PolygonIndex target) const;

			void setCalculated(PolygonIndex target);

			bool isCalculated(PolygonIndex target) const;
		};

	} // wayfinder
} // WP_NAMESPACE

