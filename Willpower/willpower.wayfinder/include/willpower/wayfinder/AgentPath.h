#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Types.h"
#include "willpower/wayfinder/AgentTarget.h"
#include "willpower/wayfinder/ConvexPolygonisation.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API AgentPath
		{
			friend class AgentPathIterator;

		private:

			// Targetting
			AgentTarget const* mTarget;

			ConvexPolygonisation const* mPolygons;

			// Path
			EdgeIndex const* mFirstPathNode;

			EdgeIndex const* mCurPathNode;

			EdgeIndex const* mLastPathNode;

			int mPathIncrement;

			Vector2 mNodeVertices[2];

			// Optional time before we start pathing
			float mStartTimer;

			// Pathing helpers
			wp::MathsUtils::Side mNextEdgeSign;

			PossiblePolygonIndex mTargetLastSeenPolygon;

		private:

			bool inFinalPolygon() const;

			bool movedToNewNode(Vector2 const& position) const;

			void calculateNextEdge(Vector2 const& position, EdgeIndex node);

			Vector2 getDirectionToTarget(Vector2 const& position) const;

			Vector2 getDirectionToNextNode(Vector2 const& position) const;

		public:

			class WP_WAYFINDER_API Iterator
			{
				friend class AgentPath;

			private:

				AgentPath const* mPath;

				EdgeIndex const* mFirstPathNode;

				int mPosition;

			private:

				Iterator();

				Iterator(AgentPath const* path, int position = 0);

			public:

				Iterator const& operator++();

				Iterator operator++(int);

				EdgeIndex operator*();

				bool operator==(Iterator const& other) const;

				bool operator!=(Iterator const& other) const;
			};

		public:

			AgentPath();

			AgentPath(Vector2 const& position, EdgeIndex const* pathStart, EdgeIndex const* pathEnd, int pathInc, ConvexPolygonisation const* polygons, float startTimer = 0.0f);

			void clear();
			
			void setTarget(AgentTarget const* target);

			Iterator begin() const;

			Iterator end() const;

			Vector2 update(float frameTime, Vector2 const& position);
		};

	} // wayfinder
} // WP_NAMESPACE

