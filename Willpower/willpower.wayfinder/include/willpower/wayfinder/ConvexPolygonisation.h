#pragma once

#include "willpower/common/Vector2.h"
#include "willpower/common/MathsUtils.h"
#include "willpower/common/AccelerationGrid.h"
#include "willpower/common/Logger.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Types.h"
#include "willpower/wayfinder/FloorLine.h"
#include "willpower/wayfinder/PathDatabase.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{
		class Floor;

		class WP_WAYFINDER_API ConvexPolygonisation
		{
		public:

			static const unsigned int MaxPolygonDegree = 16;

		public:

			struct Edge
			{
				FloorLine line;
				PossibleVertexIndex v[2]; // index into vertex list
				PossiblePolygonIndex p[2]; // index into polygon list

				Edge()
				{
					v[0] = v[1] = p[0] = p[1] = NoIndex;
				}
			};

		private:

			struct Polygon
			{
				VertexIndex vertices[MaxPolygonDegree];
				PolygonIndex neighbours[MaxPolygonDegree];
				uint32_t degree, numNeighbours;

				Polygon() : numNeighbours(0) {}

				Vector2 getCentre(ConvexPolygonisation const* cv) const
				{
					Vector2 centre(0.0f, 0.0f);
					for (uint32_t i = 0; i < degree; ++i)
					{
						centre += cv->mVertices[vertices[i]];
					}

					return centre / (float)degree;
				}
			};

			struct GraphEdge
			{
				PolygonIndex target;
				uint32_t distance;
			};

		private:

			std::vector<Vector2> mVertices;

			std::vector<Edge> mEdges;

			std::map<uint32_t, EdgeIndex> mVertexToEdgeLookup;

			std::map<uint32_t, EdgeIndex> mPolygonToEdgeLookup;

			std::vector<Polygon> mPolygons;

			std::vector<std::vector<GraphEdge>> mPathGraph;

			BoundingBox mBounds;

			AccelerationGrid* mPolygonAccelerationGrid;

		private:

			VertexIndex addVertex(float x, float y);

			uint32_t addTriangle(VertexIndex vertices[3]);

			std::vector<Vector2> processLoop(std::vector<Vector2> const& points);

			void polygonise1(std::vector<Vector2> const& border, std::vector<std::vector<Vector2>> const& holes);

			void polygonise2(std::vector<Vector2> const& border, std::vector<std::vector<Vector2>> const& holes);

		public:

			ConvexPolygonisation();

			~ConvexPolygonisation();

			void addArea(std::vector<Vector2> const& border, std::vector<std::vector<Vector2>> const& holes);

			void buildPathGraph();

			void createAccelerationGrids(int dimX, int dimY);

			void calculatePaths(PolygonIndex target, PathDatabase* database) const;

			std::vector<int> calculatePathLengths(PolygonIndex target) const;

			void composeConvex(unsigned int maxDegree = MaxPolygonDegree);

			Vector2 const& getVertex(VertexIndex index) const;

			Edge const& getEdge(EdgeIndex index) const;

			std::vector<Polygon> const& getPolygons() const;

			uint32_t getNumPolygons() const;

			int32_t getContainingPolygon(float x, float y) const;

			int32_t getContainingPolygon(Vector2 const& position) const;
		};

	} // wayfinder
} // WP_NAMESPACE
