#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Agent.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API FlockingAgent : public Agent
		{
			void getNeighbourForces(Vector2& separation, Vector2& alignment);

			void getMapForces(Vector2& wallAvoidance);

			Vector2 clampTurn(Vector2 const& newDirection, float frameTime);

		public:

			FlockingAgent(int radius, Vector2 const& position, float maxSpeed);

			~FlockingAgent();

			void update(float frameTime);
		};

	} // wayfinder
} // WP_NAMESPACE

