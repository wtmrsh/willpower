#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/wayfinder/Platform.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API AgentTarget
		{
		protected:

			Vector2 mPosition;

		public:

			explicit AgentTarget(Vector2 const& position);

			virtual ~AgentTarget();

			void setPosition(Vector2 const& position);

			Vector2 const& getPosition() const;
		};

	} // wayfinder
} // WP_NAMESPACE

