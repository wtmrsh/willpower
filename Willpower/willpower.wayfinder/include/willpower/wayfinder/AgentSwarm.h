#pragma once

#include <map>
#include <unordered_set>

#include "willpower/common/Vector2.h"
#include "willpower/common/AccelerationGrid.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Types.h"
#include "willpower/wayfinder/Agent.h"
#include "willpower/wayfinder/Mesh.h"
#include "willpower/wayfinder/PathDatabase.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API AgentSwarm
		{
			struct AgentData
			{
				// Non-owning
				Agent* agent;
				Sector* sector;
				Floor* floor;
				AgentTarget* target;

				AgentData()
					: agent(nullptr)
					, sector(nullptr)
					, floor(nullptr)
					, target(nullptr)
				{
				}
			};

			struct AgentTargetData
			{
				struct SectorData
				{
					PossibleSectorIndex sector;
					PossiblePolygonIndex polygon;
					uint32_t refCount;
					bool moved;

					SectorData()
						: sector(NoIndex)
						, polygon(NoIndex)
						, refCount(1)
						, moved(true)
					{
					}
				};

				// First in pair is count, second is polygon id for that Floor.
				std::map<int, SectorData> sizeCounts;

				uint32_t refCount;

				AgentTargetData()
					: refCount(1)
				{
				}
			};

		private:

			// Mesh data
			Mesh* mwMesh;

			// Agent data
			std::vector<AgentData> mAgents;

			uint32_t mAgentCount;

			std::vector<Agent*> mwAddedAgents;

			// Lookup grid for agents
			AccelerationGrid* mAgentGrid;

			// Agent target data
			std::map<AgentTarget const*, AgentTargetData> mTargets;

			// Flocking params
			float mSeekingWeight;

			float mSeparationWeight;

			float mAlignmentWeight;

			float mWallAvoidanceWeight;

			float mNeighbourSensorRange;

			float mWallSensorRange;

			float mAgentPadding;

		private:

			void addNewAgents();

			void calculateTargetPaths();

			void updateAgents(float frameTime);

		public:

			AgentSwarm(Mesh* mesh, int initialCapacity);

			~AgentSwarm();

			void addAgent(Agent* agent);

			void removeAgent(Agent* agent);

			uint32_t getNumAgents() const;
			
			Agent const* getAgent(uint32_t index) const;

			Agent* getAgent(uint32_t index);

			AccelerationGrid const* getAccelerationGrid() const;

			std::set<uint32_t> getAgentIdsWithinRadius(Vector2 const& position, float radius) const;

			void setAgentTarget(Agent* agent, AgentTarget* target);

			void setAgentFlockSeekingWeight(float weight);

			float getAgentFlockSeekingWeight() const;

			void setAgentFlockSeparationWeight(float weight);

			float getAgentFlockSeparationWeight() const;

			void setAgentFlockAlignmentWeight(float weight);

			float getAgentFlockAlignmentWeight() const;

			void setAgentFlockWallAvoidanceWeight(float weight);

			float getAgentFlockWallAvoidanceWeight() const;

			void setAgentFlockNeighbourSensorRange(float range);

			float getAgentFlockNeighbourSensorRange() const;

			void setAgentFlockWallSensorRange(float range);

			float getAgentFlockWallSensorRange() const;

			void setAgentFlockPadding(float padding);

			float getAgentFlockPadding() const;

			void update(float frameTime);

		};

	} // wayfinder
} // WP_NAMESPACE

