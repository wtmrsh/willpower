#pragma once

#include "willpower/common/Vector2.h"
#include "willpower/common/MathsUtils.h"
#include "willpower/common/AccelerationGrid.h"
#include "willpower/common/Logger.h"

#include "willpower/geometry/Mesh.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Sector.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API Mesh
		{
			std::vector<Sector*> mSectors;

			BoundingBox mBounds;

		private:

			void createSectors(geometry::Mesh const* geometryMesh);

			void setBounds(geometry::Mesh const* geometryMesh);

		public:

			Mesh(geometry::Mesh const* geometryMesh, int insetSize);

			Mesh(geometry::Mesh const* geometryMesh, std::vector<int> const& insetSizes);

			~Mesh();

			void composeConvex(int maxDegree = ConvexPolygonisation::MaxPolygonDegree);

			uint32_t getNumSectors() const;

			Sector* getSector(uint32_t index);

			Sector* getContainingSector(int insetSize, float x, float y);

			Sector* getContainingSector(int insetSize, Vector2 const& position);

			BoundingBox const& getBounds() const;
		};

	} // wayfinder
} // WP_NAMESPACE
