#pragma once

#include "willpower/common/Vector2.h"
#include "willpower/common/MathsUtils.h"
#include "willpower/common/AccelerationGrid.h"
#include "willpower/common/Logger.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Floor.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API Sector
		{
			int32_t mId;

			std::vector<Vector2> mBorder;

			std::vector<std::vector<Vector2>> mHoles;

			BoundingBox mBounds;

			std::map<int, Floor*> mFloors;

			// Edge lookup
			std::vector<Vector2> mEdgeList;

			AccelerationGrid* mEdgesGrid;

		private:

			void updateBounds(std::vector<Vector2> const& points);

		public:

			Sector();

			explicit Sector(std::vector<Vector2> const& border);

			Sector(std::vector<Vector2> const& border, std::vector<std::vector<Vector2>> const& holes);

			~Sector();

			void setId(int32_t id);

			int32_t getId() const;
			
			void setBorder(std::vector<Vector2> const& border);

			void addHole(std::vector<Vector2> const& hole);

			void addHoles(std::vector<std::vector<Vector2>> const& holes);

			std::vector<Vector2> const& getBorder() const;

			uint32_t getNumHoles() const;

			std::vector<Vector2> const& getHole(uint32_t index) const;

			std::vector<Vector2> getEdgeVerticesWithinRadius(Vector2 const& position, float radius) const;

			void createAccelerationGrid();

			Floor* createFloor(int insetSize);

			std::vector<int> getFloorInsetSizes() const;

			bool hasFloor(int insetSize) const;

			Floor* getFloor(int insetSize);

			bool containsPoint(int insetSize, float x, float y) const;

			void composeConvex(int maxDegree = ConvexPolygonisation::MaxPolygonDegree);
		};

	} // wayfinder
} // WP_NAMESPACE
