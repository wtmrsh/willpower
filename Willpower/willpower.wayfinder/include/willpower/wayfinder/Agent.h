#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/AgentTarget.h"
#include "willpower/wayfinder/AgentPath.h"
#include "willpower/wayfinder/Sector.h"
#include "willpower/wayfinder/ConvexPolygonisation.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API Agent : public AgentTarget
		{
			friend class AgentSwarm;

		private:

			uint32_t mId;

			// Properties
			Vector2 mDirection;

			int mRadius;

			float mMaxSpeed;

			float mTurnRate;
			
			AgentPath mPath;

			// Targetting
			AgentTarget* mTarget;

		protected:

			AgentSwarm* mSwarm;

			Sector* mSector;

		private:

			void setSwarm(AgentSwarm* swarm);

			void setAgentId(uint32_t id);

			void setTarget(AgentTarget* target);

			void setPath(AgentPath const& path);

			void clearPath();

			void setSector(Sector* sector);

		protected:

			uint32_t getAgentId() const;

			Sector* getSector();

			Vector2 updatePath(float frameTime);

			void setDirection(Vector2 const& direction);

			void move(float distance);

		public:

			Agent(int radius, Vector2 const& position, float maxSpeed);

			~Agent();

			Vector2 const& getPosition() const;

			Vector2 const& getDirection() const;

			int getRadius() const;

			void setMaxSpeed(float maxSpeed);
			
			float getMaxSpeed() const;

			void setTurnRate(float turnRate);

			float getTurnRate() const;

			AgentTarget* getTarget();

			virtual void update(float frameTime) = 0;
		};

	} // wayfinder
} // WP_NAMESPACE

