#pragma once

#include "willpower/common/Vector2.h"
#include "willpower/common/MathsUtils.h"
#include "willpower/common/AccelerationGrid.h"
#include "willpower/common/Logger.h"

#include "willpower/wayfinder/Platform.h"
#include "willpower/wayfinder/Types.h"
#include "willpower/wayfinder/ConvexPolygonisation.h"
#include "willpower/wayfinder/PathDatabase.h"
#include "willpower/wayfinder/AgentPath.h"

namespace WP_NAMESPACE
{
	namespace wayfinder
	{

		class WP_WAYFINDER_API Floor
		{
			struct IsolatedArea
			{
				std::vector<Vector2> border;
				std::vector<std::vector<Vector2>> holes;
			};

		private:

			int mInset;

			std::vector<Vector2> mBorder;

			std::vector<std::vector<Vector2>> mHoles;

			ConvexPolygonisation* mPolygons;

			PathDatabase* mPathDatabase;

			void calculatePaths(PolygonIndex target, bool recalculate) const;
			
		public:

			Floor();

			explicit Floor(std::vector<Vector2> const& border);

			Floor(std::vector<Vector2> const& border, std::vector<std::vector<Vector2>> const& holes);

			~Floor();

			void setBorder(std::vector<Vector2> const& border);

			void addHole(std::vector<Vector2> const& hole);

			void addHoles(std::vector<std::vector<Vector2>> const& holes);

			std::vector<Vector2> const& getBorder() const;

			uint32_t getNumHoles() const;

			std::vector<Vector2> const& getHole(uint32_t index) const;

			void triangulate(int inset);

			int getInset() const;

			void composeConvex(int maxDegree = ConvexPolygonisation::MaxPolygonDegree);

			void calculatePaths(PolygonIndex target);

			void recalculatePaths(PolygonIndex target);

			std::vector<int> calculatePathLengths(PolygonIndex target) const;

			AgentPath getPath(Vector2 const& position, PolygonIndex source, PolygonIndex target, float startTimer = 0.0f) const;

			ConvexPolygonisation const* getPolygonisation() const;

			uint32_t getNumPolygons() const;

			int32_t getContainingPolygon(float x, float y) const;

			int32_t getContainingPolygon(Vector2 const& position) const;
		};

	} // wayfinder
} // WP_NAMESPACE
