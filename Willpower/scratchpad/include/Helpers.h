#pragma once

#include <willpower/geometry/Mesh.h>

wp::geometry::Mesh* createQuad(float sizeX, float sizeY);
