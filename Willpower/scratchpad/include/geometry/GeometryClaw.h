#pragma once

#include "willpower/geometry/Mesh.h"

#include "willpower/collide/Simulation.h"
#include "willpower/collide/Collider.h"

#include "willpower/viz/GeometryMeshRenderer.h"
#include "willpower/viz/AccelerationGridRenderer.h"
#include "willpower/viz/DynamicLineRenderer.h"
#include "willpower/viz/DynamicQuadRenderer.h"
#include "willpower/viz/DynamicTriangleRenderer.h"

#include "geometry/UserAttributes.h"
#include "geometry/ViewportLineBatchDataProvider.h"
#include "geometry/EntityQuadBatchDataProvider.h"
#include "geometry/TriangleBatchDataProvider.h"

#include "Claw.h"

struct RegularPolygon
{
	std::vector<uint32_t> edgeIds;

	uint32_t polygonId;
};

struct ArcPolgon
{
	uint32_t startEdgeId{ ~0u };
	uint32_t endEdgeId{ ~0u };

	std::vector<uint32_t> leftEdgeIds, rightEdgeIds;

	uint32_t polygonId;
};

class GeometryClaw : public Claw
{
public:

	typedef std::function<float(float)> InterpolationFunction;

private:

	// Viewport debugging
	std::shared_ptr<ViewportLineBatchDataProvider> mViewportDataProvider;

	std::shared_ptr<wp::viz::DynamicLineRenderer> mViewportRenderer;

	// Entity rendering
	std::shared_ptr<EntityQuadBatchDataProvider> mEntityDataProvider;

	std::shared_ptr<wp::viz::DynamicQuadRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>> mEntityRenderer;

	// Geometry
	std::shared_ptr<wp::geometry::Mesh> mMesh;

	VertexAttributeFactory* mVertexAttributeFactory;

	EdgeAttributeFactory* mEdgeAttributeFactory;

	PolygonAttributeFactory* mPolygonAttributeFactory;

	PolygonVertexAttributeFactory* mPolygonVertexAttributeFactory;

	// Geometry rendering
	std::shared_ptr<wp::viz::GeometryMeshRenderer> mMeshRenderer;

	std::shared_ptr<wp::viz::AccelerationGridRenderer> mMeshGridRenderer;

	// Test triangle rendering
	std::shared_ptr<TriangleBatchDataProvider> mTriangleDataProvider;

	std::shared_ptr<wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>> mTriangleRenderer;


	// Collision simulation
	wp::collide::Simulation* mSimulation;

	wp::collide::Collider* mPlayer;

	wp::Vector2 mPlayerVelocity;

	float mPlayerSpeed;

	bool mMovingLeft, mMovingRight, mMovingUp, mMovingDown;

	wp::Vector2 mMousePosition;

	bool mHitLine;

	wp::collide::SweepResult mLineResult;

	int mMouseX, mMouseY;

private:

	void setupImpl(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr) override;

	uint32_t createPolygon(float x, float y, float w, float h);

	RegularPolygon createRectangle(float x, float y, float width, float height, float offsetAngle);

	RegularPolygon createRegularPolygon(float centreX, float centreY, float radius, size_t sides, float offsetAngle);

	ArcPolgon createArc(float centreX, float centreY, float radius, float startAngle, float endAngle, float width1, float width2, InterpolationFunction widthFunc, size_t resolution);

	void createCollideSimulation();

public:

	GeometryClaw(WP_NAMESPACE::Logger* logger, mpp::RenderSystem* renderSystem);

	~GeometryClaw();

	void handleInput(InputManager* inputMgr, float frameTime) override;

	void update(float frameTime) override;

};
