#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include <mpp/helper/TriangleBatchDataProvider.h>

class TriangleBatchDataProvider : public mpp::helper::TriangleBatch2DDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>
{
	mpp::RenderSystem* mRenderSystem{ nullptr };

	bool mDirty{ true };

	std::vector<float> mVertexData;

	float mTotalTime;

public:

	TriangleBatchDataProvider(mpp::RenderSystem* renderSystem)
		: mRenderSystem(renderSystem)
		, mTotalTime(0.0f)
	{
		setNumPrimitives(0);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin.x = bMin.y = bMin.z = -1e10f;
		bMax.x = bMax.y = bMax.z = 1e10f;
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1, float& x2, float& y2) override
	{
		x0 = 0;
		y0 = 0;
		x1 = 50;
		y1 = 50;
		x2 = 50;
		y2 = 0;
	}

	void texcoords(uint32_t index, float& u0, float& v0, float& u1, float& v1, float& u2, float& v2) override
	{
		u0 = 0;
		v0 = 0;
		u1 = 1;
		v1 = 1;
		u2 = 1;
		v2 = 0;
	}

	void colour(uint32_t index, float& red, typename float& green, typename float& blue, typename float& alpha) override
	{
		red = 1;
		green = 0;
		blue = 1;
		alpha = 1;
	}

	mpp::Colour diffuse() override
	{
		return mpp::Colour::White;
	}

	void setDirty()
	{
		mDirty = true;
	}

	bool update(float frameTime, wp::Vector2 const& position, wp::Vector2 const& dir)
	{
		mTotalTime += frameTime;

		setNumPrimitives(1);

		return true;
	}
};