#pragma once

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include <mpp/helper/LineBatchDataProvider.h>

class ViewportLineBatchDataProvider : public mpp::helper::LineBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeUnsignedByte>
{
	glm::vec3 mBounds[2];

public:

	ViewportLineBatchDataProvider()
	{
		update(wp::BoundingBox(), 0.0f);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin = mBounds[0];
		bMax = mBounds[1];
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1)
	{
		switch (index)
		{
		case 0:
			x0 = mBounds[0].x;
			y0 = mBounds[0].y;
			x1 = mBounds[1].x;
			y1 = mBounds[0].y;
			break;
		case 1:
			x0 = mBounds[1].x;
			y0 = mBounds[0].y;
			x1 = mBounds[1].x;
			y1 = mBounds[1].y;
			break;
		case 2:
			x0 = mBounds[1].x;
			y0 = mBounds[1].y;
			x1 = mBounds[0].x;
			y1 = mBounds[1].y;
			break;
		case 3:
			x0 = mBounds[0].x;
			y0 = mBounds[1].y;
			x1 = mBounds[0].x;
			y1 = mBounds[0].y;
			break;
		default:
			break;
		}
	}

	void colour(uint32_t index, uint8_t& red, uint8_t& green, uint8_t& blue, uint8_t& alpha)
	{
		red = 255;
		green = 255;
		blue = 255;
		alpha = 255;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::White;
	}

	bool update(wp::BoundingBox const& viewBounds, float frameTime)
	{
		setNumPrimitives(4);

		auto minExtent = viewBounds.getMinExtent();
		auto maxExtent = viewBounds.getMaxExtent();

		mBounds[0] = glm::vec3(minExtent.x, minExtent.y, 1e10f);
		mBounds[1] = glm::vec3(maxExtent.x, maxExtent.y, -1e10f);

		return true;
	}
};
