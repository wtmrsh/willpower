#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include <mpp/helper/QuadBatchDataProvider.h>

class EntityQuadBatchDataProvider : public mpp::helper::QuadBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>
{
	mpp::RenderSystem* mRenderSystem{ nullptr };

	bool mDirty{ true };

	std::vector<float> mVertexData;

	float mTotalTime;

	float mRadius;

	wp::Vector2 mPosition;

	float mAngle;

public:

	EntityQuadBatchDataProvider(mpp::RenderSystem* renderSystem, float radius)
		: mRenderSystem(renderSystem)
		, mRadius(radius)
		, mTotalTime(0.0f)
		, mAngle(0.0f)
	{
		setNumPrimitives(0);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin.x = bMin.y = bMin.z = -1e10f;
		bMax.x = bMax.y = bMax.z = 1e10f;
	}

	void position(uint32_t index, float& x, float& y)
	{
		x = mPosition.x;
		y = mPosition.y;
	}

	void angle(uint32_t index, float& angle)
	{
		angle = WP_DEGTORAD(mAngle);
	}

	void direction(uint32_t index, float& x, float& y)
	{
		float a;
		angle(index, a);

		x = sinf(a * 3.14159f / 180);
		y = cosf(a * 3.14159f / 180);
	}

	void textureAtlasTexcoords(uint32_t index, float& u0, float& v0, float& u1, float& v1)
	{
		float animTime = 2.0f;
		float d = fmod(mTotalTime, animTime);
		float dt = d / animTime;
		int frame = (int)(dt * 8);
		float u = (frame % 4) / 4.0f;
		float v = (frame / 4) / 2.0f;

		u0 = u;
		v0 = v;
		u1 = u + 0.25f;
		v1 = v + 0.5f;
	}

	void dimensions(uint32_t index, float& halfWidth, float& halfHeight)
	{
		halfWidth = mRadius;
		halfHeight = mRadius;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::White;
	}

	void setDirty()
	{
		mDirty = true;
	}

	bool update(float frameTime, wp::Vector2 const& position, wp::Vector2 const& dir)
	{
		mTotalTime += frameTime;
		mPosition = position;
		mAngle = wp::Vector2::UNIT_Y.anticlockwiseAngleTo(dir);

		setNumPrimitives(1);

		return true;
	}
};