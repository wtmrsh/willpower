#pragma once

#include <string>

#include "willpower/common/Vector2.h"

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

enum CollisionObjectType
{
	// Do not change this order as it is used for render order
	Point,
	Circle,
	AABB,
	Triangle,
	ConvexPolygon
};

class CollisionObject
{
	CollisionObjectType mType;

	std::string mName;

	wp::Vector2 mPosition;

	bool mHovered, mSelected, mColliding;

public:

	CollisionObject(CollisionObjectType type, std::string const& name);

	virtual ~CollisionObject() = default;

	CollisionObjectType getType() const;

	std::string const& getName() const;

	virtual void setPosition(wp::Vector2 const& pos);

	wp::Vector2 const& getPosition() const;

	virtual void move(wp::Vector2 const& distance);

	virtual void setHover(bool hovered);

	bool isHovered() const;

	virtual void setSelected(bool selected);

	bool isSelected() const;

	virtual void setColliding(bool colliding);

	bool isColliding() const;

	virtual void scale(float amt) {}

	virtual void rotate(float angle) {}
	
	virtual bool pointInObject(float x, float y) const = 0;

	virtual void update(float frameTime) {}

	virtual void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr) {}
	
	virtual void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr) {}
};