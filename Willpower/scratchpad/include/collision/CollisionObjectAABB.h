#pragma once

#include "willpower/common/BoundingBox.h"

#include "CollisionObject.h"

class CollisionObjectAABB : public CollisionObject
{
	wp::BoundingBox mBox;

	mpp::Colour mColour;

public:

	CollisionObjectAABB(std::string const& name, mpp::Colour const& colour);

	void setPosition(wp::Vector2 const& pos);

	void move(wp::Vector2 const& distance);

	void setSize(wp::Vector2 const& size);

	void getExtents(wp::Vector2& minExtent, wp::Vector2& maxExtent) const;

	void scale(float scale);

	bool pointInObject(float x, float y) const;

	void update(float frameTime);

	void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};