#pragma once

#include <vector>
#include <functional>

#include "Claw.h"
#include "CollisionObject.h"
#include "IntersectionLine.h"

class CollisionClaw : public Claw
{
	enum DragType
	{
		None,
		Move,
		Scale,
		Rotate
	};

public:

	typedef std::function<bool(CollisionObject const*, CollisionObject const*)> CollisionFunction;

	typedef std::pair<CollisionObjectType, CollisionObjectType> CollisionTypePair;

private:

	DragType mDragType;

	std::vector<CollisionObject*> mObjects;

	CollisionObject* mHoveredObject, *mSelectedObject;

	std::vector<IntersectionLine> mIntersectionLines;

	std::vector<IntersectionLineHit> mIntersectionLineHits;

	std::vector<IntersectionLineIntersection> mIntersectionLineIntersections;

	std::map<CollisionTypePair, CollisionFunction> mCollisionFunctions;

public:

	CollisionClaw(WP_NAMESPACE::Logger* logger, mpp::RenderSystem* renderSystem);

	~CollisionClaw();

	void setup(mpp::ResourceManager* resourceMgr);

	void addCollisionFunction(CollisionObjectType type1, CollisionObjectType type2, CollisionFunction func);

	CollisionObject* addObject(CollisionObjectType type, std::string const& name, mpp::Colour const& colour);

	void removeAllObjects();
	
	void handleInput(InputManager* inputMgr, float frameTime);

	void update(float frameTime);

};