#pragma once

#include "willpower/common/BoundingCircle.h"

#include "CollisionObject.h"

class CollisionObjectCircle : public CollisionObject
{
	wp::BoundingCircle mCircle;

	mpp::Colour mColour;

public:

	CollisionObjectCircle(std::string const& name, mpp::Colour const& colour);

	void setPosition(wp::Vector2 const& pos);

	void move(wp::Vector2 const& distance);

	void setRadius(float radius);

	float getRadius() const;

	void scale(float amt);

	bool pointInObject(float x, float y) const;

	void update(float frameTime);

	void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};