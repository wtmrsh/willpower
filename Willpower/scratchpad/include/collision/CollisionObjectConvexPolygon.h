#pragma once

#include <vector>

#include "CollisionObject.h"

class CollisionObjectConvexPolygon : public CollisionObject
{
	std::vector<wp::Vector2> mVertices;

	wp::Vector2 mCentre;

	mpp::Colour mColour;

private:

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr, mpp::Colour const& colour, float inset);

public:

	CollisionObjectConvexPolygon(std::string const& name, mpp::Colour const& colour);

	void addVertex(float x, float y);

	void scale(float amt);

	void rotate(float angle);

	bool pointInObject(float x, float y) const;

	bool boxIntersects(wp::Vector2 const& boxMin, wp::Vector2 const& boxMax) const;

	bool circleIntersects(wp::Vector2 const& circleCentre, float circleRadius) const;

	bool triangleIntersects(wp::Vector2 const& v0, wp::Vector2 const& v1, wp::Vector2 const& v2) const;

	bool convexPolygonIntersects(CollisionObjectConvexPolygon const* other) const;

	void update(float frameTime);

	void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};