#pragma once

#include <vector>

#include "CollisionObject.h"

class CollisionObjectTriangle : public CollisionObject
{
	wp::Vector2 mVertices[3];

	wp::Vector2 mCentre;

	mpp::Colour mColour;

private:

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr, mpp::Colour const& colour, float inset);

public:

	CollisionObjectTriangle(std::string const& name, mpp::Colour const& colour);

	void setVertices(float x0, float y0, float x1, float y1, float x2, float y2);

	wp::Vector2 const* getVertices() const;

	void scale(float amt);

	void rotate(float angle);

	bool pointInObject(float x, float y) const;

	void update(float frameTime);

	void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};