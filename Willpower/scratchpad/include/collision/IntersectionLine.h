#pragma once

#include "CollisionObjectPoint.h"

struct IntersectionLine
{
	CollisionObjectPoint* start;
	CollisionObjectPoint* end;
};

struct IntersectionLineHit
{
	wp::Vector2 start, end;
	wp::Vector2 normal0, normal1;
};

struct IntersectionLineIntersection
{
	wp::Vector2 position;
};