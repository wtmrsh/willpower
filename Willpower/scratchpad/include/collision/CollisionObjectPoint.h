#pragma once

#include "willpower/common/BoundingBox.h"

#include "CollisionObject.h"

class CollisionObjectPoint : public CollisionObject
{
	wp::BoundingBox mBox;

	mpp::Colour mColour;

public:

	CollisionObjectPoint(std::string const& name, mpp::Colour const& colour);

	void setPosition(wp::Vector2 const& pos);

	void move(wp::Vector2 const& distance);

	bool pointInObject(float x, float y) const;

	void update(float frameTime);

	void renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};