#pragma once

#include <string>
#include <vector>
#include <functional>

#include "willpower/wayfinder/Mesh.h"
#include "willpower/wayfinder/AgentSwarm.h"
#include "willpower/wayfinder/AgentTarget.h"
#include "willpower/wayfinder/FlockingAgent.h"

#include "Claw.h"

class MouseTarget : public wp::wayfinder::AgentTarget
{
public:

	MouseTarget(wp::Vector2 const& position)
		: wp::wayfinder::AgentTarget(position)
	{
	}
};


class NavMeshClaw : public Claw
{
	wp::wayfinder::Mesh* mMesh;

	wp::wayfinder::AgentSwarm* mSwarm;

	float mMouseX, mMouseY;

	MouseTarget* mMouseTarget;

	wp::wayfinder::Sector const* mHoveredSector;

	int32_t mHoveredPolygon;

	std::vector<wp::wayfinder::AgentTarget*> mTargets;

	// UI
	int mAgentSize;

	int mHoverAgent, mSelectedAgent;

	int mHoverTarget, mSelectedTarget;
	
private:

	void loadMesh(std::string const& filename);

	void getHoverItem(float x, float y, int* hoverAgent = nullptr, int* hoverTarget = nullptr) const;

public:

	NavMeshClaw(WP_NAMESPACE::Logger* logger, mpp::RenderSystem* renderSystem);

	~NavMeshClaw();

	void setup(mpp::ResourceManager* resourceMgr);

	void handleInput(InputManager* inputMgr, float frameTime);

	void update(float frameTime);
};