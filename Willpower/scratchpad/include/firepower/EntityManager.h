#pragma once

#include <vector>

#include <mpp/QuadBatch.h>
#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include "willpower/firepower/ObjectCollisionManager.h"

#include "firepower/Entity.h"

class EntityManager
{
	std::vector<Entity*> mEntities;

	std::string mTexture;

	mpp::QuadBatch* mBatch;

	int mTextureWidth, mTextureHeight;

public:

	EntityManager(int initialSize, std::string const& texture);

	EntityManager();

	void addEntity(Entity* entity);

	void update(float frameTime, wp::firepower::ObjectCollisionManager* objectMgr);

	void render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);
};
