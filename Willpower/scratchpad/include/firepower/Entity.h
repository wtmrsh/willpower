#pragma once

#include "willpower/common/Vector2.h"
#include "willpower/common/Types.h"
#include "willpower/common/SplinePath.h"

class Entity
{
	wp::Vector2 mPosition, mDirection;

	wp::SplinePath const* mPath;

	float mDistance, mSpeed;

	wp::uint32_t mId;

public:

	Entity(wp::SplinePath const* path, float distance, float speed);

	wp::Vector2 const& getPosition() const;

	wp::Vector2 const& getDirection() const;

	wp::BoundingBox getBounds() const;

	void setId(wp::uint32_t id);

	wp::uint32_t getId() const;

	void update(float frameTime);
};
