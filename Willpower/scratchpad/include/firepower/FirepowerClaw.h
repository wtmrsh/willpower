#pragma once

#undef min
#undef max

#include <half/half.hpp>

#include <mpp/RenderSystem.h>

#include "willpower/Common/SplinePath.h"
#include "willpower/Common/DynamicAccelerationGrid.h"

#include "willpower/firepower/BulletManager.h"
//#include "willpower/firepower/BombManager.h"
#include "willpower/firepower/BeamManager.h"
#include "willpower/firepower/ObjectCollisionManager.h"

#include "willpower/Geometry/Mesh.h"

#include "willpower/viz/GeometryMeshRenderer_old.h"
#include "willpower/viz/GeometryMeshRenderParams.h"

#include "willpower/viz/FirepowerMeshCollisionManagerRenderer_old.h"
#include "willpower/viz/FirepowerMeshCollisionManagerRenderParams.h"

#include "willpower/viz/SplineRenderer_old.h"
#include "willpower/viz/DynamicSplineRenderer_old.h"
#include "willpower/viz/SplineRenderParams.h"

#include "willpower/viz/AccelerationGridRenderer_old.h"
#include "willpower/viz/AccelerationGridRenderParams.h"

#include "willpower/viz/BoundingAreaRenderer_old.h"
#include "willpower/viz/BoundingAreaRenderParams.h"

#include "firepower/Entity.h"
#include "firepower/EntityManager.h"
#include "Claw.h"

class FirepowerClaw : public Claw
{
	mpp::RenderSystem* mRenderSystem;

	// Bullets
	wp::firepower::BulletManager<float, float, uint8_t, 4>* mBulletMgr;

	// Bombs
	//wp::firepower::BombManager<float, float, float, 4>* mBombMgr;

	// Beams
	wp::firepower::BeamManager<float, float, float, 4>* mBeamMgr;

	// Managers
	wp::firepower::ObjectCollisionManager* mObjectMgr;
	
	// Mesh
	wp::geometry::Mesh* mMesh;

	wp::viz::FirepowerMeshCollisionManagerRenderer_old* mMeshRenderer;

	wp::viz::FirepowerMeshCollisionManagerRenderParams mMeshRenderParams;

	wp::firepower::MeshCollisionManager* mMeshCollisionMgr;

	// Splines
	std::vector<wp::SplinePath*> mSplines;

	wp::viz::SplineRenderer_old* mSplineRenderer;

	wp::viz::SplineRenderParams mSplineRenderParams;

	// Entities
	EntityManager* mEntityMgr;

	// Viz
	wp::viz::AccelerationGridRenderer_old<wp::DynamicAccelerationGrid>* mGridRenderer;

	wp::viz::AccelerationGridRenderParams mGridRenderParams;

	wp::viz::BoundingAreaRenderer_old* mBoundsRenderer;

	wp::viz::BoundingAreaRenderParams mBoundsRenderParams;

	//mpp::IndexedTriangleBatch* mTriBatch;

	int mMouseX, mMouseY;

private:

	uint32_t createPolygon(float x, float y, std::vector<wp::Vector2> const& vertexPositions);

public:

	FirepowerClaw(wp::Logger* logger, mpp::RenderSystem* renderSystem);

	~FirepowerClaw();

	std::vector<std::string> getText() const;

	void setup(mpp::ResourceManager* resourceMgr);

	void handleInput(InputManager* inputMgr, float frameTime);

	void update(float frameTime);

};
