#pragma once

#include <exception>

class FlowControlException : public std::exception
{
public:

	FlowControlException() = default;

	virtual ~FlowControlException() = default;
};

class FlowControlQuitException : public FlowControlException
{
	int mExitCode;

public:

	explicit FlowControlQuitException(int exitCode)
		: mExitCode(exitCode)
	{
	}

	int getExitCode() const
	{
		return mExitCode;
	}
};