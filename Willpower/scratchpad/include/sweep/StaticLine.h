#pragma once

#include "willpower/common/Vector2.h"

class StaticLine
{
	wp::Vector2 mVertices[2];

public:

	StaticLine() = default;

	StaticLine(wp::Vector2 const& v0, wp::Vector2 const& v1);

	void setVertex(int index, float x, float y);

	void setVertex(int index, wp::Vector2 const& v);

	wp::Vector2 const& getVertex(int index) const;

	void getVertices(wp::Vector2& v0, wp::Vector2& v1) const;
};