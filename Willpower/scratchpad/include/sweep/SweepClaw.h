#pragma once

#include <string>
#include <vector>
#include <functional>

#include "willpower/collide/Simulation.h"
#include "willpower/collide/Collider.h"

#include "willpower/geometry/Mesh.h"

#include "willpower/viz/GeometryMeshRenderer_old.h"
#include "willpower/viz/GeometryMeshRenderParams.h"

#include "Claw.h"

#include "sweep/StaticLine.h"

class SweepClaw : public Claw
{
	wp::geometry::Mesh* mMesh;

	wp::viz::GeometryMeshRenderer_old* mMeshRenderer;

	// Collision simulation
	wp::collide::Simulation* mSimulation;

	std::vector<wp::collide::Collider*> mColliders;

	// Old collision stuff
	std::vector<StaticLine> mLines;

	// UI
	wp::Vector2 mMousePosition, mPrevMousePosition;

	wp::collide::Collider *mHoveredCollider, *mSelectedCollider;

	StaticLine mSweepLine;

	bool mDragging, mSweeping;

	wp::Vector2 mSweptPosition;

	// Movement
	float mMoveSpeed;

	// Debug options
	bool mRenderSimulationDebug;

	int mSimulationDebugCellX, mSimulationDebugCellY;

	int mNumSweepChecks;

private:

	void sweepColliderWithMouse(wp::collide::Collider* collider);

	void renderDebug(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

public:

	SweepClaw(WP_NAMESPACE::Logger* logger, mpp::RenderSystem* renderSystem);

	~SweepClaw();

	void loadMesh(std::string const& filename);

	void setup(mpp::ResourceManager* resourceMgr);

	void handleInput(InputManager* inputMgr, float frameTime);

	void update(float frameTime);
};