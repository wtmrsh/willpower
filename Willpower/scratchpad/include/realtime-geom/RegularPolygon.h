#pragma once

#include "realtime-geom/Primitive.h"


class RegularPolygon : public Primitive 
{
public:

	RegularPolygon(Operation operation, FillRule fillType, uint32_t numSides);
};
