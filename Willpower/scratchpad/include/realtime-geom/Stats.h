#pragma once

#include <cstdint>

struct Stats
{
	uint32_t primitivesProcessed;

	uint32_t polygonsGenerated;

	uint32_t verticesGenerated;

	int64_t clipNs;
	
	int64_t triangulateNs;
};
