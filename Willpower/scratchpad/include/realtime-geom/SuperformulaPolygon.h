#pragma once

#include "realtime-geom/Primitive.h"


class SuperformulaPolygon : public Primitive
{
	float mValues[6];

private:

	float r(float theta) const;

	wp::Vector2 calculate(float theta) const;

public:

	SuperformulaPolygon(Operation operation, FillRule fillType, float values[6]);
};
