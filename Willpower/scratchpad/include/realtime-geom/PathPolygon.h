#pragma once

#include <vector>

#include <willpower/common/Vector2.h>

#include "realtime-geom/Primitive.h"


class PathPolygon : public Primitive
{
public:

	enum struct JoinType
	{
		Bevel,
		Mitre,
		Round,
		Square
	};

	enum struct EndType
	{
		Butt,
		Joined,
		Polygon,
		Round,
		Square
	};

public:

	PathPolygon(Operation operation, FillRule fillType, std::vector<wp::Vector2> const& points, float width, JoinType joinType = JoinType::Mitre, EndType endType = EndType::Square);
};
