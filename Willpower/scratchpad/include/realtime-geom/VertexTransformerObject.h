#pragma once

#include <vector>

#include <willpower/common/Vector2.h>

#include "realtime-geom/VertexTransformer.h"


class VertexTransformerObject
{
	VertexTransformer mVertexTransformer;

	wp::Vector2 mInfluenceEye;

	float mInfluenceOn, mInfluenceOff;

protected:

	VertexTransformer* getVertexTransformer()
	{
		return &mVertexTransformer;
	}

	wp::Vector2 transformVertex(wp::Vector2 const& v) const
	{
		return mVertexTransformer.transform(v);
	}

public:

	VertexTransformerObject()
		: mInfluenceEye({ 0, 0 })
		, mInfluenceOn(-1.0f)
		, mInfluenceOff(-1.0f)
	{
	}

	virtual ~VertexTransformerObject() = default;

	void setTransformerParent(VertexTransformer* transformer)
	{
		mVertexTransformer.setParent(transformer);
		invalidatePostTransform();
	}

	void setInfluence(wp::Vector2 const& pos, float onDist, float offDist)
	{
		mInfluenceEye = pos;
		mInfluenceOn = onDist;
		mInfluenceOff = offDist;
	}

	float getInfluence(wp::Vector2 const& pos) const
	{
		auto d = pos.distanceTo(mInfluenceEye);
		
		if (d <= mInfluenceOn && d >= mInfluenceOff)
		{
			return (d - mInfluenceOff) / (mInfluenceOn - mInfluenceOff);
		}
		else
		{
			return 0.0f;
		}
	}

	void setOrigin(wp::Vector2 const& origin)
	{
		mVertexTransformer.setOrigin(origin);
	}

	void setOffsets(std::vector<std::pair<float, wp::Vector2>> const& offsets)
	{
		mVertexTransformer.setOffsets(offsets);
		invalidatePostTransform();
	}

	void setScales(std::vector<std::pair<float, float>> const& scales)
	{
		mVertexTransformer.setScales(scales);
		invalidatePostTransform();
	}

	void setAngles(std::vector<std::pair<float, float>> const& angles)
	{
		mVertexTransformer.setAngles(angles);
		invalidatePostTransform();
	}

	void setOrbitAngles(std::vector<std::pair<float, float>> const& orbitAngles)
	{
		mVertexTransformer.setOrbitAngles(orbitAngles);
		invalidatePostTransform();
	}

	void setOrbitDistances(std::vector<std::pair<float, float>> const& orbitDistances)
	{
		mVertexTransformer.setOrbitDistances(orbitDistances);
		invalidatePostTransform();
	}

	void setFollowOrbitAngle(bool follow)
	{
		mVertexTransformer.setFollowOrbitAngle(follow);
		invalidatePostTransform();
	}

	void deltaT(VertexTransformer::Key key, float delta)
	{
		mVertexTransformer.deltaT(key, delta);
		invalidatePostTransform();
	}

	void setT(VertexTransformer::Key key, float value)
	{
		mVertexTransformer.setT(key, value);
		invalidatePostTransform();
	}

	float getT(VertexTransformer::Key key) const
	{
		return mVertexTransformer.getT(key);
	}

	virtual void invalidatePostTransform() = 0;

};
