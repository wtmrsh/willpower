#pragma once

#include "realtime-geom/Primitive.h"


class RectanglePolygon : public Primitive
{
public:

	RectanglePolygon(Operation operation, FillRule fillType, float xyRatio);
};
