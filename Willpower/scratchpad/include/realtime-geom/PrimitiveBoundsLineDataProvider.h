#pragma once

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include <mpp/helper/LineBatchDataProvider.h>

#include <willpower/common/AccelerationGrid.h>
#include <willpower/common/BoundingBox.h>

#include "World.h"


class PrimitiveBoundsLineBatchDataProvider : public mpp::helper::LineBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeUnsignedByte>
{
	struct Line
	{
		float x0, y0, x1, y1;
		uint8_t r, g, b, a;
	};

private:

	glm::vec3 mBounds[2];

	World* mWorld;

	std::shared_ptr<wp::AccelerationGrid> mGrid;

	std::vector<Line> mLines;

public:

	PrimitiveBoundsLineBatchDataProvider(World* world, std::shared_ptr<wp::AccelerationGrid> grid)
		: mWorld(world)
		, mGrid(grid)
	{
		mLines.reserve(4096);
	}

	void setWorld(World* world)
	{
		mWorld = world;
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin = mBounds[0];
		bMax = mBounds[1];
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1)
	{
		auto const& line = mLines[index];

		x0 = line.x0;
		y0 = line.y0;
		x1 = line.x1;
		y1 = line.y1;
	}

	void colour(uint32_t index, uint8_t& red, uint8_t& green, uint8_t& blue, uint8_t& alpha)
	{
		auto const& line = mLines[index];

		red = line.r;
		green = line.g;
		blue = line.b;
		alpha = line.a;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::White;
	}

	bool update(wp::BoundingBox const& viewBounds)
	{
		mLines.clear();

		auto primitiveIndices = mGrid->getCandidateItemsInBoundingArea(viewBounds);

		for (auto index : primitiveIndices)
		{
			auto primitive = mWorld->getPrimitive(index);
			auto const& bounds = primitive->getBounds();

			wp::Vector2 minExtent, maxExtent;

			bounds.getExtents(minExtent, maxExtent);

			mLines.push_back(Line(
				minExtent.x,
				minExtent.y,
				minExtent.x,
				maxExtent.y,
				255,
				0,
				255,
				255
			));

			mLines.push_back(Line(
				minExtent.x,
				maxExtent.y,
				maxExtent.x,
				maxExtent.y,
				255,
				0,
				255,
				255
			));

			mLines.push_back(Line(
				maxExtent.x,
				maxExtent.y,
				maxExtent.x,
				minExtent.y,
				255,
				0,
				255,
				255
			));

			mLines.push_back(Line(
				maxExtent.x,
				minExtent.y,
				minExtent.x,
				minExtent.y,
				255,
				0,
				255,
				255
			));
		}

		setNumPrimitives(mLines.size());

		// Extents
		auto minExtent = viewBounds.getMinExtent();
		auto maxExtent = viewBounds.getMaxExtent();

		mBounds[0] = glm::vec3(minExtent.x, minExtent.y, 1e10f);
		mBounds[1] = glm::vec3(maxExtent.x, maxExtent.y, -1e10f);

		return true;
	}
};
