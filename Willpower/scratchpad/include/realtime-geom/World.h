#pragma once

#include <vector>
#include <set>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>

#include "realtime-geom/Primitive.h"
#include "realtime-geom/PrimitiveGroup.h"
#include "realtime-geom/Clipper.h"
#include "realtime-geom/Stats.h"

class World
{
	std::vector<Primitive*> mPrimitives;

	PrimitiveGroup mGroup;

	mutable int64_t mClipNs, mTriangulateNs;

	mutable uint32_t mNumPrimitivesClipped, mNumPolygonsGenerated, mNumVerticesGenerated;

public:

	World();

	virtual ~World();

	uint32_t addPrimitive(Primitive* primitive, bool addToGroup);

	Primitive* getPrimitive(uint32_t index);

	Primitive* findPrimitive(wp::Vector2 const& worldPos) const;

	std::vector<Primitive*> const& getPrimitives() const;

	void newFrame();

	std::vector<ClippedPolygon> calculatePolygons(wp::BoundingBox const& extents, std::set<uint32_t> const& primitiveIndices) const;

	std::vector <wp::Vector2> triangulate(wp::BoundingBox const& extents, std::set<uint32_t> const& primitiveIndices) const;

	Stats getStats() const;
};
