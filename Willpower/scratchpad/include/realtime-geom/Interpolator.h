#pragma once

#include <array>
#include <cstdint>
#include <exception>

#include <willpower/common/Globals.h>
#include <willpower/common/BezierSpline.h>

#include "realtime-geom/Easing.h"


template<typename T>
class Interpolator
{
public:

	typedef std::pair<float, T> Point;

	static const uint32_t MaxPoints = 16;

private:

	std::array<Point, MaxPoints> mPoints;

	uint32_t mNumPoints;

	Easing mEasing;

private:

	T getValueInSegment(uint32_t segment, float time) const
	{
		auto const& s1 = mPoints[segment];
		auto const& s2 = mPoints[segment + 1];

		auto tt = s2.first - s1.first;
		float dt = (time - s1.first) / tt;

		return s1.second + (s2.second - s1.second) * dt;
	}

public:

	Interpolator()
		: mNumPoints(0)
		, mEasing(Easing::Linear)
	{
	}

	explicit Interpolator(T value)
		: mNumPoints(1)
		, mEasing(Easing::Linear)
	{
		mPoints[0] = std::make_pair(0.0f, value);
	}

	explicit Interpolator(std::vector<Point> const& points)
		: mNumPoints((uint32_t)points.size())
		, mEasing(Easing::Linear)
	{
		if (mNumPoints > MaxPoints)
		{
			throw std::exception("Too many points");
		}

		for (uint32_t i = 0; i < mNumPoints; ++i)
		{
			if (i > 0 && points[i].first <= points[i - 1].first)
			{
				throw std::exception("Interpolator points not ascending in time");
			}
			
			mPoints[i] = points[i];
		}
	}

	virtual ~Interpolator() = default;

	T getValue(float time) const
	{
		if (mNumPoints == 1)
		{
			return mPoints[0].second;
		}

		time = ease(mEasing, time);

		for (uint32_t i = 0; i < mNumPoints - 1; ++i)
		{
			if (mPoints[i].first <= time && mPoints[i + 1].first > time)
			{
				return getValueInSegment(i, time);
			}
		}

		auto const& point = mPoints.back();
		return point.second;
	}

	void setEasing(Easing easing)
	{
		mEasing = easing;
	}

	Easing getEasing() const
	{
		return mEasing;
	}

	void setPoints(std::vector<std::pair<float, T>> const& values)
	{
		mNumPoints = (uint32_t)values.size();

		if (mNumPoints > MaxPoints)
		{
			throw std::exception("Too many points");
		}
		
		for (uint32_t i = 0; i < mNumPoints; ++i)
		{
			mPoints[i] = values[i];
		}
	}

	void addPoint(float time, T value)
	{
		time = std::min(std::max(0.0f, time), 1.0f);

		if (mNumPoints == 0)
		{
			mPoints[mNumPoints++] = std::make_pair(0.0f, value);
		}
		else if (mNumPoints < MaxPoints)
		{
			uint32_t i = 0;
			for (; i < mNumPoints; ++i)
			{
				auto const& point = mPoints[i];

				if (point.first == time)
				{
					throw std::exception("Point exists at same time");
				}
				else if (point.first > time)
				{
					break;
				}
			}

			// Shift points up
			mNumPoints++;

			for (uint32_t j = i + 1; j < mNumPoints; ++j)
			{
				mPoints[j] = mPoints[j - 1];
			}

			mPoints[i] = std::make_pair(time, value);
		}
		else
		{
			throw std::exception("Too many points");
		}
	}

	void removePoint(uint32_t index)
	{
		// Shift points down
		mNumPoints--;
	
		for (uint32_t i = index; i < mNumPoints; ++i)
		{
			mPoints[i] = mPoints[i + 1];
		}
	}
};

template<>
class Interpolator<wp::Vector2>
{
public:

	typedef std::pair<float, wp::Vector2> Point;

	static const uint32_t MaxPoints = 16;

	enum struct InterpolationType
	{
		Linear,
		Circle,
		Bezier
	};

private:

	std::array<Point, MaxPoints> mPoints;

	uint32_t mNumPoints;

	Easing mEasing;

	InterpolationType mType;

	wp::BezierSpline* mSpline;

private:

	wp::Vector2 getValueInSegment(uint32_t segment, float time) const
	{
		auto const& s1 = mPoints[segment];
		auto const& s2 = mPoints[segment + 1];

		auto tt = s2.first - s1.first;
		float dt = (time - s1.first) / tt;

		return s1.second + (s2.second - s1.second) * dt;
	}

	wp::Vector2 getLinearValue(uint32_t segment, float time) const
	{
		return getValueInSegment(segment, time);
	}

	wp::Vector2 getCircleValue(uint32_t segment, float time) const
	{
		// Use x-coord of points as radius
		auto const& s1 = mPoints[segment];
		auto const& s2 = mPoints[segment + 1];

		auto tt = s2.first - s1.first;
		float dt = (time - s1.first) / tt;

		float radius = s1.second.x + (s2.second.x - s1.second.x) * dt;
		return wp::Vector2::UNIT_Y.rotatedAnticlockwiseCopy(time * 360.0f) * radius;
	}

	wp::Vector2 getBezierValue(uint32_t segment, float time) const
	{
		return mSpline->getPositionAtT(time);
	}

	void createSpline()
	{
		if (mPoints.size() < 4)
		{
			throw std::exception("Too few points for spline");
		}

		if (mSpline)
		{
			delete mSpline;
		}

		std::vector<wp::Vector2> points;

		for (auto const& point : mPoints)
		{
			points.push_back(point.second);
		}

		mSpline = new wp::BezierSpline(points);
	}

public:

	Interpolator()
		: mNumPoints(0)
		, mEasing(Easing::Linear)
		, mType(InterpolationType::Linear)
		, mSpline(nullptr)
	{
	}

	explicit Interpolator(wp::Vector2 const& value)
		: mNumPoints(1)
		, mEasing(Easing::Linear)
		, mType(InterpolationType::Linear)
		, mSpline(nullptr)
	{
		mPoints[0] = std::make_pair(0.0f, value);
	}

	explicit Interpolator(std::vector<Point> const& points)
		: mNumPoints((uint32_t)points.size())
		, mEasing(Easing::Linear)
		, mType(InterpolationType::Linear)
		, mSpline(nullptr)
	{
		if (mNumPoints > MaxPoints)
		{
			throw std::exception("Too many points");
		}

		for (uint32_t i = 0; i < mNumPoints; ++i)
		{
			if (i > 0 && points[i].first <= points[i - 1].first)
			{
				throw std::exception("Interpolator points not ascending in time");
			}

			mPoints[i] = points[i];
		}
	}

	virtual ~Interpolator()
	{
		if (mSpline)
		{
			delete mSpline;
		}
	}
	
	wp::Vector2 getValue(float time) const
	{
		if (mNumPoints == 1)
		{
			return mPoints[0].second;
		}

		time = ease(mEasing, time);

		for (uint32_t i = 0; i < mNumPoints - 1; ++i)
		{
			if (mPoints[i].first <= time && mPoints[i + 1].first > time)
			{
				switch (mType)
				{
				case InterpolationType::Linear:
					return getLinearValue(i, time);

				case InterpolationType::Circle:
					return getCircleValue(i, time);

				case InterpolationType::Bezier:
					return getBezierValue(i, time);

				default:
					throw std::exception("Unknown interpolation type");
				}
			}
		}

		auto const& point = mPoints.back();
		return point.second;
	}

	void setType(InterpolationType type)
	{
		if (mType == InterpolationType::Bezier)
		{
			delete mSpline;
			mSpline = nullptr;
		}

		mType = type;

		if (mType == InterpolationType::Bezier)
		{
			createSpline();
		}
	}

	InterpolationType getType() const
	{
		return mType;
	}

	void setEasing(Easing easing)
	{
		mEasing = easing;
	}

	Easing getEasing() const
	{
		return mEasing;
	}

	void setPoints(std::vector<std::pair<float, wp::Vector2>> const& values)
	{
		mNumPoints = (uint32_t)values.size();

		if (mNumPoints > MaxPoints)
		{
			throw std::exception("Too many points");
		}

		for (uint32_t i = 0; i < mNumPoints; ++i)
		{
			mPoints[i] = values[i];
		}

		if (mType == InterpolationType::Bezier)
		{
			createSpline();
		}
	}

	void addPoint(float time, wp::Vector2 const& value)
	{
		time = std::min(std::max(0.0f, time), 1.0f);

		if (mNumPoints == 0)
		{
			mPoints[mNumPoints++] = std::make_pair(0.0f, value);
		}
		else if (mNumPoints < MaxPoints)
		{
			uint32_t i = 0;
			for (; i < mNumPoints; ++i)
			{
				auto const& point = mPoints[i];

				if (point.first == time)
				{
					throw std::exception("Point exists at same time");
				}
				else if (point.first > time)
				{
					break;
				}
			}

			// Shift points up
			mNumPoints++;

			for (uint32_t j = i + 1; j < mNumPoints; ++j)
			{
				mPoints[j] = mPoints[j - 1];
			}

			mPoints[i] = std::make_pair(time, value);
		}
		else
		{
			throw std::exception("Too many points");
		}

		if (mType == InterpolationType::Bezier)
		{
			createSpline();
		}
	}

	void removePoint(uint32_t index)
	{
		// Shift points down
		mNumPoints--;

		for (uint32_t i = index; i < mNumPoints; ++i)
		{
			mPoints[i] = mPoints[i + 1];
		}

		if (mType == InterpolationType::Bezier)
		{
			createSpline();
		}
	}
};
