#pragma once

#include <vector>

#include "realtime-geom/Primitive.h"
#include "realtime-geom/VertexTransformerObject.h"


class PrimitiveGroup : public VertexTransformerObject
{
	std::vector<Primitive*> mPrimitives;

public:

	PrimitiveGroup();

	explicit PrimitiveGroup(std::vector<Primitive*> primitives);

	void addPrimitive(Primitive* primitive);

	void removePrimitive(Primitive* primitive);

	void invalidatePostTransform() override;
};