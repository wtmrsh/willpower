#pragma once

#include <vector>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>

#include "realtime-geom/Primitive.h"

struct ClippedPolygon
{
	bool isHole;
	std::vector<wp::Vector2> vertices;
	wp::BoundingBox bounds;
};

class Clipper
{
public:

	virtual std::vector<ClippedPolygon> clip(std::vector<Primitive*> const& primitives, wp::BoundingBox const& extents) = 0;
};
