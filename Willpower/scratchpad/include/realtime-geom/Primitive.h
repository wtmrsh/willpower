#pragma once

#include <vector>
#include <array>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>

#include "realtime-geom/VertexTransformerObject.h"


struct PrimitiveVertex
{
	wp::Vector2 p;
	int64_t z;
};


class Primitive : public VertexTransformerObject
{
public:

	enum struct Operation
	{
		Union,
		Intersection,
		Difference,
		XOR
	};

	enum struct FillRule
	{
		NonZero,
		EvenOdd
	};

protected:

	enum struct Index
	{
		Current = 0,
		Original,
		Min,
		Max
	};

private:

	Operation mOperation;

	FillRule mFillRule;

	wp::BoundingBox mBounds;

	mutable bool mCacheInvalid;

	mutable std::vector<PrimitiveVertex> mCachedVertices;

protected:

	std::vector<PrimitiveVertex> mVertices;

private:

	void invalidate();

	virtual std::vector<PrimitiveVertex> generateTransformedVertices(wp::Vector2* minExtent = nullptr, wp::Vector2* maxExtent = nullptr) const;

	void calculateBounds();

protected:

	void setVertices(std::vector<PrimitiveVertex> vertices);

public:

	Primitive(Operation operation, FillRule fillType);

	virtual ~Primitive() = default;

	void setOperation(Operation operation);

	Operation getOperation() const;

	void setFillRule(FillRule fillRule);

	FillRule getFillRule() const;

	wp::BoundingBox const& getBounds() const;

	virtual std::vector<PrimitiveVertex> getVertices() const;
	
	virtual uint32_t getNumVertices() const;

	void invalidatePostTransform() override;
};
