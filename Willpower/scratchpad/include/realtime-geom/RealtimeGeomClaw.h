#pragma once

#include <vector>

#include <willpower/common/AccelerationGrid.h>
#include <willpower/viz/DynamicLineRenderer.h>
#include <willpower/viz/DynamicTriangleRenderer.h>
#include <willpower/viz/AccelerationGridRenderer.h>

#include "Claw.h"

#include "realtime-geom/Primitive.h"
#include "realtime-geom/World.h"
#include "realtime-geom/Stats.h"
#include "realtime-geom/PolygonBorderLineDataProvider.h"
#include "realtime-geom/PolygonTriangleDataProvider.h"
#include "realtime-geom/PrimitiveLineDataProvider.h"
#include "realtime-geom/PrimitiveBoundsLineDataProvider.h"


class RealtimeGeomClaw : public Claw
{
	int mMouseX, mMouseY;

	bool mMovingLeft, mMovingRight, mMovingUp, mMovingDown;

	wp::Vector2 mMousePosition;

	World* mWorld;

	std::shared_ptr<wp::AccelerationGrid> mAccelGrid;

	std::shared_ptr<wp::viz::AccelerationGridRenderer> mGridRenderer;

	std::shared_ptr<PolygonBorderLineBatchDataProvider> mPolygonBorderLineDataProvider;

	std::shared_ptr<PolygonTriangleDataProvider> mPolygonTriangleDataProvider;

	std::shared_ptr<PrimitiveLineBatchDataProvider> mPrimitiveLineDataProvider;

	std::shared_ptr<PrimitiveBoundsLineBatchDataProvider> mPrimitiveBoundsLineDataProvider;

	std::shared_ptr<wp::viz::DynamicLineRenderer> mPolygonBorderLineRenderer, mPrimitiveLineRenderer, mPrimitiveBoundsLineRenderer;

	std::shared_ptr<wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>> mPolygonTriangleRenderer;

private:

	void setupImpl(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr) override;

	void setScene(int scene);

	Primitive* findPrimitive(wp::Vector2 const& worldPos) const;

public:

	RealtimeGeomClaw(WP_NAMESPACE::Logger* logger, mpp::RenderSystem* renderSystem);

	~RealtimeGeomClaw();

	std::vector<std::string> getText() const override;

	void handleInput(InputManager* inputMgr, float frameTime) override;

	void update(float frameTime) override;

};
