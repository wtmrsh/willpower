#pragma once

#include "realtime-geom/Clipper.h"

class Clipper2Clipper : public Clipper
{
public:

	std::vector<ClippedPolygon> clip(std::vector<Primitive*> const& primitives, wp::BoundingBox const& extents) override;
};
