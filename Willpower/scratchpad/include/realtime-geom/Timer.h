#pragma once

#include <cstdlib>
#include <string>
#include <chrono> 
#include <iomanip>
#include <sstream>

class Timer 
{
    std::chrono::high_resolution_clock::time_point mTimeStarted;

    std::chrono::high_resolution_clock::duration mDuration = {};

    bool mPaused;

public:

    Timer(bool startPaused = false) 
        : mPaused(startPaused)
    {
        if (!mPaused)
        {
            mTimeStarted = std::chrono::high_resolution_clock::now();
        }
    }

    void restart()
    {
        mPaused = false;
        mDuration = {};
        mTimeStarted = std::chrono::high_resolution_clock::now();
    }

    void resume()
    {
        if (!mPaused)
        {
            return;
        }

        mPaused = false;
        mTimeStarted = std::chrono::high_resolution_clock::now();
    }

    void pause()
    {
        if (mPaused)
        {
            return;
        }

        std::chrono::high_resolution_clock::time_point now =
            std::chrono::high_resolution_clock::now();

        mDuration += (now - mTimeStarted);
        mPaused = true;
    }

    int64_t elapsedNanoseconds()
    {
        if (!mPaused)
        {
            std::chrono::high_resolution_clock::time_point now =
                std::chrono::high_resolution_clock::now();

            mDuration += (now - mTimeStarted);
        }

        return std::chrono::duration_cast<std::chrono::nanoseconds>(mDuration).count();
    }

    static std::string nsToString(int64_t ns)
    {
        int nsecs_log10 = static_cast<int>(std::log10(ns));

        std::ostringstream os{};
        os.precision(static_cast<uint8_t>(2.0 - (nsecs_log10 % 3)));

        os << std::fixed;
        if (nsecs_log10 < 6)
            os << ns * 1.0e-3 << " us";
        else if (nsecs_log10 < 9)
            os << ns * 1.0e-6 << " ms";
        else
            os << ns * 1.0e-9 << " s";

        return os.str();
    }

    std::string elapsedStr()
    {
        return nsToString(elapsedNanoseconds());
    }
};
