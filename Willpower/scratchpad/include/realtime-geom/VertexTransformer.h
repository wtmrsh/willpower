#pragma once

#include <willpower/common/Vector2.h>
#include <willpower/common/MathsUtils.h>

#include "realtime-geom/Interpolator.h"


class VertexTransformer
{
public:

	enum LoopBehaviour
	{
		Stop,
		PingPong,
		Loop
	};

	enum Key
	{
		Scale,
		Angle,
		Offset,
		OrbitAngle,
		OrbitDistance
	};

private:

	struct TValue
	{
		float value;
		LoopBehaviour loop;
		int dir;
	};

private:

	VertexTransformer* mParent;

	wp::Vector2 mOrigin;

	Interpolator<wp::Vector2> mOffset;

	Interpolator<float> mScale;

	Interpolator<float> mAngle;

	Interpolator<float> mOrbitDistance;

	Interpolator<float> mOrbitAngle;
	
	bool mFollowOrbitAngle;

	TValue mT[5];

private:

	wp::Vector2 calculateOffset() const
	{
		auto offset = mOrigin + mOffset.getValue(mT[Key::Offset].value);

		if (mParent)
		{
			offset += mParent->calculateOffset();
		}

		return offset;
	}

public:

	explicit VertexTransformer(VertexTransformer* parent = nullptr)
		: mParent(parent)
		, mOrigin(wp::Vector2::ZERO)
		, mOffset(wp::Vector2::ZERO)
		, mScale(1.0f)
		, mAngle(0.0f)
		, mOrbitDistance(0.0f)
		, mOrbitAngle(0.0f)
		, mFollowOrbitAngle(false)
	{
		for (int i = 0; i < 5; ++i)
		{
			mT[i].value = 0.0f;
			mT[i].loop = LoopBehaviour::Stop;
			mT[i].dir = 1;
		}
	}

	void setParent(VertexTransformer* parent)
	{
		mParent = parent;
	}

	void deltaT(Key key, float delta)
	{
		auto& t = mT[key];

		auto newValue = t.value + t.dir * delta;

		switch (t.loop)
		{
		case LoopBehaviour::Stop:
			newValue = std::min(std::max(0.0f, newValue), 1.0f);
			break;

		case LoopBehaviour::PingPong:
			if (newValue > 1.0f)
			{
				newValue = 2.0f - newValue;
				t.dir = -t.dir;
			}
			else if (newValue < 0.0f)
			{
				newValue = -newValue;
				t.dir = -t.dir;
			}
			break;

		case LoopBehaviour::Loop:
			while (newValue > 1.0f)
			{
				newValue -= 1.0f;
			}
			while (newValue < 0.0f)
			{
				newValue += 1.0f;
			}
			break;
		}

		t.value = newValue;
	}

	void setT(Key key, float value)
	{
		mT[key].value = std::min(std::max(0.0f, value), 1.0f);
	}

	float getT(Key key) const
	{
		return mT[key].value;
	}

	void setLoopBehaviour(Key key, LoopBehaviour behaviour)
	{
		mT[key].loop = behaviour;
	}

	LoopBehaviour getLoopBehaviour(Key key) const
	{
		return mT[key].loop;
	}

	void setEasing(Key key, Easing easing)
	{
		switch (key)
		{
		case Key::Offset:
			mOffset.setEasing(easing);
			break;

		case Key::Scale:
			mScale.setEasing(easing);
			break;

		case Key::Angle:
			mAngle.setEasing(easing);
			break;

		case Key::OrbitAngle:
			mOrbitAngle.setEasing(easing);
			break;

		case Key::OrbitDistance:
			mOrbitDistance.setEasing(easing);
			break;

		default:
			throw std::exception("Unknown key");
		}
	}

	Easing getEasing(Key key) const
	{
		switch (key)
		{
		case Key::Offset:
			return mOffset.getEasing();

		case Key::Scale:
			return mScale.getEasing();

		case Key::Angle:
			return mAngle.getEasing();

		case Key::OrbitAngle:
			return mOrbitAngle.getEasing();

		case Key::OrbitDistance:
			return mOrbitDistance.getEasing();

		default:
			throw std::exception("Unknown key");
		}
	}

	void setOffsetInterpolationType(Interpolator<wp::Vector2>::InterpolationType type)
	{
		mOffset.setType(type);
	}

	void setOrigin(wp::Vector2 const& origin)
	{
		mOrigin = origin;
	}

	void setOffsets(std::vector<std::pair<float, wp::Vector2>> const& offsets)
	{
		mOffset.setPoints(offsets);
	}

	void setScales(std::vector<std::pair<float, float>> const& scales)
	{
		mScale.setPoints(scales);
	}

	void setAngles(std::vector<std::pair<float, float>> const& angles)
	{
		mAngle.setPoints(angles);
	}

	void setOrbitAngles(std::vector<std::pair<float, float>> const& orbitAngles)
	{
		mOrbitAngle.setPoints(orbitAngles);
	}

	void setOrbitDistances(std::vector<std::pair<float, float>> const& orbitDistances)
	{
		mOrbitDistance.setPoints(orbitDistances);
	}

	void setFollowOrbitAngle(bool follow)
	{
		mFollowOrbitAngle = follow;
	}

	bool getFollowOrbitAngle() const
	{
		return mFollowOrbitAngle;
	}

	wp::Vector2 transform(wp::Vector2 const& vertex) const
	{
		auto vt = vertex;

		// Get orbit angle first as we're using it twice
		auto orbitAngle = mOrbitAngle.getValue(mT[Key::OrbitAngle].value);

		// Scale
		vt *= mScale.getValue(mT[Key::Scale].value) * 0.5f;

		// Local rotation
		auto localAngle = mAngle.getValue(mT[Key::Angle].value);
		
		if (mFollowOrbitAngle)
		{
			localAngle += orbitAngle;
		}

		vt.rotateAnticlockwise(localAngle);

		// Translate
		vt += calculateOffset();

		// Orbit
		auto orbitDir = wp::Vector2::UNIT_Y.rotatedAnticlockwiseCopy(orbitAngle);

		auto orbitDist = mOrbitDistance.getValue(mT[Key::OrbitDistance].value);
		vt += orbitDir * orbitDist;

		return vt;
	}
};
