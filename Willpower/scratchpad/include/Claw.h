#pragma once

#include <string>

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>
#include <mpp/Scene.h>

#include "willpower/common/Logger.h"
#include "willpower/common/Vector2.h"

#include "InputManager.h"

class Claw
{
	std::string mTitle;

protected:
	
	WP_NAMESPACE::Logger* mLogger;

	int mWindowSizeX, mWindowSizeY;

	mpp::CameraPtr mCamera;

	mpp::ScenePtr mScene;

private:

	virtual void setupImpl(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr) {}

public:

	Claw(WP_NAMESPACE::Logger* logger, std::string const& title, mpp::RenderSystem* renderSystem);

	virtual ~Claw() = default;

	std::string const& getTitle() const;

	virtual std::vector<std::string> getText() const { return {}; }

	mpp::ScenePtr getScene();

	mpp::CameraPtr getCamera();

	wp::Vector2 getViewPosition() const;

	wp::Vector2 getMouseWorldPosition(int mouseX, int mouseY) const;

	void setup(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr);

	virtual void handleInput(InputManager* inputMgr, float frameTime) {}

	virtual void update(float frameTime) {}
};