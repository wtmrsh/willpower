SET VSVER=%1
SET PLATFORM=%2
SET CONFIGURATION=%3
SET BASEDIR=%~dp0
SET PROJECTDIR=%BASEDIR%\%VSVER%\
SET SRCDIR=%VSVER%\bin\%PLATFORM%\%CONFIGURATION%
SET TDPDIR=bin\%VSVER%\%PLATFORM%\%CONFIGURATION%
SET OUTDIR=%PROJECTDIR%bin\%PLATFORM%\%CONFIGURATION%

echo Copying binaries to %OUTDIR%

copy /Y "%PROJECTDIR%..\..\3rd party\bin\vs2017\%PLATFORM%\%CONFIGURATION%\*.*" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.common\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.application\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.application\3rd party\%TDPDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.collide\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.editor\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.firepower\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.firepower\3rd party\%TDPDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.geometry\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.geometry.renderer\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.serialization\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\Willpower\willpower.wayfinder\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\MassivePolyPusher\mpp\build\%SRCDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\MassivePolyPusher\mpp\3rd party\%TDPDIR%\*.dll" "%OUTDIR%"
copy /Y "%PROJECTDIR%..\..\..\..\MassivePolyPusher\mpp-mesh\build\%SRCDIR%\*.dll" "%OUTDIR%"