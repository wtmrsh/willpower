#include <mpp/helper/OrthoCamera.h>

#include "Claw.h"

using namespace std;

Claw::Claw(wp::Logger* logger, string const& title, mpp::RenderSystem* renderSystem)
	: mLogger(logger)
	, mTitle(title)
	, mWindowSizeX((int)renderSystem->getWindowWidth())
	, mWindowSizeY((int)renderSystem->getWindowHeight())
{
	mScene = renderSystem->createScene("Default");
	mCamera = make_shared<mpp::helper::OrthoCamera>(glm::vec2(0.0f, 0.0f), renderSystem->getWindowWidth(), renderSystem->getWindowHeight());
}

string const& Claw::getTitle() const
{
	return mTitle;
}

mpp::ScenePtr Claw::getScene()
{
	return mScene;
}

mpp::CameraPtr Claw::getCamera()
{
	return mCamera;
}

wp::Vector2 Claw::getViewPosition() const
{
	auto const& cameraPos = mCamera->getPosition();
	return wp::Vector2(cameraPos.x, cameraPos.z);
}

wp::Vector2 Claw::getMouseWorldPosition(int mouseX, int mouseY) const
{
	wp::Vector2 cursorWorldPos((float)mouseX, (float)mouseY);
	
	cursorWorldPos.x -= mWindowSizeX / 2;
	cursorWorldPos.y -= mWindowSizeY / 2;
	cursorWorldPos.y = -cursorWorldPos.y;

	cursorWorldPos += getViewPosition();

	return cursorWorldPos;
}

void Claw::setup(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	setupImpl(renderSystem, resourceMgr);

	// Load all MPP resources
	resourceMgr->loadAllResources();
}