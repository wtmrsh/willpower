#include <utils/StringUtils.h>

#include "sdl/WindowSDL.h"

#include "FlowControlException.h"

using namespace std;
using namespace utils;

WindowSDL::WindowSDL() :
	mWindow(nullptr)
{
}

void WindowSDL::checkErrors(int line)
{
	char const* error = SDL_GetError();

	if (*error != '\0')
	{
		string errMsg = STR_FORMAT("{}: {}", error, line);
		
		SDL_ClearError();
		throw exception(errMsg.c_str());
	}
}

void WindowSDL::create(int width, int height, bool fullScreen, bool vsync)
{
	mWidth = width;
	mHeight = height;
	mFullscreen = fullScreen;

	// Use OpenGL 3.2
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	checkErrors(__LINE__);

	// Create window
	unsigned int windowFlags = SDL_WINDOW_OPENGL;

	if (fullScreen)
	{
		mWindow = SDL_CreateWindow("ScratchPad", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
	}
	else
	{
		mWindow = SDL_CreateWindow("ScratchPad", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	}

	if (!mWindow)
	{
		string err = SDL_GetError();
		throw exception(("Could not create SDL window: " + err).c_str());
	}

	checkErrors(__LINE__);

#ifdef _DEBUG
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

	mContextGL = SDL_GL_CreateContext(mWindow);
	checkErrors(__LINE__);

	if (SDL_GL_MakeCurrent(mWindow, mContextGL) != 0)
	{
		throw exception("Could not set the OpenGL context.");
	}

	checkErrors(__LINE__);

	// Create renderer
	unsigned int rendererFlags = SDL_RENDERER_ACCELERATED;

	if (vsync)
	{
		rendererFlags |= SDL_RENDERER_PRESENTVSYNC;
	}

//	SDL_ShowCursor(SDL_DISABLE);
}

void WindowSDL::destroy()
{
	SDL_GL_DeleteContext(mContextGL);
	SDL_DestroyWindow(mWindow);
}
	
void WindowSDL::setFullscreen(bool fullscreen)
{
	if (fullscreen)
	{
		SDL_SetWindowFullscreen(mWindow, SDL_WINDOW_FULLSCREEN);
	}
	else
	{
		SDL_SetWindowFullscreen(mWindow, 0);
	}

	mFullscreen = fullscreen;
}

void WindowSDL::setSize(int width, int height)
{
	// if we're fullscreen, then go to windowed first, to stop other windows in background being resized.
	if (mFullscreen)
	{
		setFullscreen(false);
		SDL_SetWindowSize(mWindow, width, height);
		setFullscreen(true);
	}
	else
	{
		SDL_SetWindowSize(mWindow, width, height);
	}
}

void WindowSDL::show()
{
	SDL_GL_SwapWindow(mWindow);
}

void WindowSDL::processEvents(InputManager* inputMgr)
{
	SDL_Event evt;
	while (SDL_PollEvent(&evt))
	{
		//
		// Input events
		//
		InputEvent ie;
		switch (evt.type)
		{
		case SDL_KEYDOWN:
			ie.type = IET_KeyPressed;
			ie.code = evt.key.keysym.sym;
			inputMgr->addEvent(ie);
			break;

		case SDL_KEYUP:
			ie.type = IET_KeyReleased;
			ie.code = evt.key.keysym.sym;
			inputMgr->addEvent(ie);
			break;

		case SDL_MOUSEBUTTONDOWN:
			ie.type = IET_ButtonPressed;
			ie.code = evt.button.button;
			inputMgr->addEvent(ie);
			break;

		case SDL_MOUSEBUTTONUP:
			ie.type = IET_ButtonReleased;
			ie.code = evt.button.button; 
			inputMgr->addEvent(ie);
			break;

		case SDL_MOUSEMOTION:
			ie.type = IET_MouseMotion;
			ie.code = evt.motion.xrel;
			inputMgr->addEvent(ie);
			break;

		case SDL_QUIT:
			throw FlowControlQuitException(0);
		}

		//
		// Window events
		//
		switch (evt.window.event)
		{
		case SDL_WINDOWEVENT_CLOSE:
			throw FlowControlQuitException(0);
		}
	}
}
