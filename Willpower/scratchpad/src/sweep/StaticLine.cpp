#include "sweep/StaticLine.h"

using namespace wp;

StaticLine::StaticLine(Vector2 const& v0, Vector2 const& v1)
{
	mVertices[0] = v0;
	mVertices[1] = v1;
}

void StaticLine::setVertex(int index, float x, float y)
{
	mVertices[index].set(x, y);
}

void StaticLine::setVertex(int index, Vector2 const& v)
{
	setVertex(index, v.x, v.y);
}

Vector2 const& StaticLine::getVertex(int index) const
{
	return mVertices[index];
}

void StaticLine::getVertices(Vector2& v0, Vector2& v1) const
{
	v0 = mVertices[0];
	v1 = mVertices[1];
}
