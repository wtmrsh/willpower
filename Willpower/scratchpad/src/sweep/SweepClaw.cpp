#include <algorithm>

#include <utils/StringUtils.h>

#include "willpower/common/MathsUtils.h"

#include "willpower/collide/ColliderCircle.h"
#include "willpower/collide/ColliderAABB.h"
#include "willpower/collide/SweepResult.h"

#include "willpower/geometry/SerializerMeshChunk.h"

#include "willpower/serialization/BinarySerializer.h"

#include "sweep/SweepClaw.h"

#undef max

using namespace std;
using namespace wp;
using namespace utils;

bool test_sweep_callback(collide::Collider* collider, Logger* logger, collide::SweepResult const* result)
{
	return true;
}

SweepClaw::SweepClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "Sweep tests", renderSystem)
	, mMesh(nullptr)
	, mSimulation(nullptr)
	, mHoveredCollider(nullptr)
	, mSelectedCollider(nullptr)
	, mDragging(false)
	, mSweeping(false)
	, mMoveSpeed(120)
	, mRenderSimulationDebug(false)
	, mSimulationDebugCellX(-1)
	, mSimulationDebugCellY(-1)
{
	//mMeshRenderer = new geometry::renderer::GeometryRenderer();
}

SweepClaw::~SweepClaw()
{
	delete mSimulation;
	delete mMesh;
	delete mMeshRenderer;
}

void SweepClaw::loadMesh(string const& filename)
{
	if (mMesh)
	{
		delete mMesh;
	}

	serialization::BinarySerializer ser;

	ser.addChunkFactory("Mesh",	[](string name, string desc) -> wp::serialization::SerializerChunk* { return new geometry::SerializerMeshChunk(name, desc); });
	ser.deserialize(filename);

	// Load in mesh data
	auto meshChunk = (wp::geometry::SerializerMeshChunk*)ser.getChunkByIndex(0);
	mMesh = new geometry::Mesh();

	auto const& vertices = meshChunk->getVertices();
	auto const& edges = meshChunk->getEdges();
	auto const& polygons = meshChunk->getPolygons();

	for (auto const& vertex: vertices)
	{
		mMesh->addVertex(vertex, -1);
	}

	for (auto const& edge: edges)
	{
		mMesh->addEdge(edge, -1);
	}

	for (auto const& polygon: polygons)
	{
		mMesh->addPolygon(polygon, -1);
	}
}

void SweepClaw::setup(mpp::ResourceManager* resourceMgr)
{
	// Load mesh
	loadMesh("test3.ged");

	//
	// Create simulation and add objects to it
	//
	wp::Vector2 minExtent, maxExtent;
	mMesh->getExtents(minExtent, maxExtent);
	
	mSimulation = new collide::Simulation(minExtent, maxExtent, 100, 100, nullptr);

	// Add circles
	collide::ColliderCircle* cc = new collide::ColliderCircle(0, 0, 15.0f);
	cc->setConsumeMovement(true);

	mColliders.push_back(cc);
	mSimulation->addCollider(cc);

	// Add boxes
	collide::ColliderAABB* ca = new collide::ColliderAABB(-50, 30, 20, 30);
	ca->setConsumeMovement(true);

	mColliders.push_back(ca);
	mSimulation->addCollider(ca);

	// Add mesh lines
	wp::uint32_t polygonIndex = mMesh->getFirstPolygonIndex();
	while (!mMesh->polygonIndexIterationFinished(polygonIndex))
	{
		geometry::Polygon const& polygon = mMesh->getPolygon(polygonIndex);

		auto edgeIt = polygon.getFirstEdge();
		while (edgeIt != polygon.getEndEdge())
		{
			auto const& polyEdge = *edgeIt;

			geometry::Edge const& edge = mMesh->getEdge(polyEdge.index);

			if (edge.getConnectivity() == geometry::Edge::Connectivity::External)
			{
				Vector2 v0 = mMesh->getVertex(polyEdge.v0).getPosition();
				Vector2 v1 = mMesh->getVertex(polyEdge.v1).getPosition();

				mSimulation->addStaticLine(v0, v1);
			}

			++edgeIt;
		}

		// Next polygon
		polygonIndex = mMesh->getNextEdgeIndex(polygonIndex);
	}
}

void SweepClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	// Get cursor position and highlight
	int cursorPosX, cursorPosY;
	inputMgr->getMousePosition(&cursorPosX, &cursorPosY);
	cursorPosY = mWindowSizeY - cursorPosY;

	// Translate cursor position to world coordinates
	cursorPosX -= mWindowSizeX / 2;
	cursorPosY -= mWindowSizeY / 2;

	mPrevMousePosition = mMousePosition;
	mMousePosition.set(cursorPosX, cursorPosY);

	// Get hovered object, if we're not sweeping or dragging
	if (!mSweeping && !mDragging)
	{
		mHoveredCollider = nullptr;

		// Check in reverse render order
		for (wp::int32_t i = mColliders.size() - 1; i >= 0; --i)
		{
			auto* collider = mColliders[i];

			if (collider->pointInside(mMousePosition))
			{
				// Hover
				mHoveredCollider = collider;
			}
		}
	}

	// Check sweeping
	if (mHoveredCollider)
	{
		// Check dragging
		if (!mSweeping && inputMgr->buttonPressed(MouseDefinition::Mouse_Left))
		{
			mDragging = true;
		}
	}
	
	if (inputMgr->buttonReleased(MouseDefinition::Mouse_Left))
	{
		if (mHoveredCollider)
		{
			mSelectedCollider = mHoveredCollider;
		}

		mDragging = false;
	}

	// Check sweeping
	if (!mDragging && inputMgr->buttonPressed(MouseDefinition::Mouse_Right))
	{
		mSelectedCollider = nullptr;
		if (mHoveredCollider)
		{
			mSweeping = true;
		
			mSweepLine.setVertex(0, mHoveredCollider->getCentre());
			mSweepLine.setVertex(1, mMousePosition);
		}
	}

	if (inputMgr->buttonReleased(MouseDefinition::Mouse_Right))
	{
		mSweeping = false;
	}

	// Movement
	Vector2 selectedMovement = Vector2::ZERO;
	if (inputMgr->keyDown(KeyDefinition::Key_UpArrow))
	{
		selectedMovement += Vector2::UNIT_Y;
	}
	if (inputMgr->keyDown(KeyDefinition::Key_DownArrow))
	{
		selectedMovement -= Vector2::UNIT_Y;
	}
	if (inputMgr->keyDown(KeyDefinition::Key_LeftArrow))
	{
		selectedMovement -= Vector2::UNIT_X;
	}
	if (inputMgr->keyDown(KeyDefinition::Key_RightArrow))
	{
		selectedMovement += Vector2::UNIT_X;
	}

	selectedMovement.normalise();
	selectedMovement *= mMoveSpeed * frameTime;

	// Move the selected object
	if (mSelectedCollider)
	{
		mSelectedCollider->setMovement(selectedMovement);
	}

	// Debug
	if (inputMgr->keyPressed(KeyDefinition::Key_F12))
	{
		mRenderSimulationDebug = !mRenderSimulationDebug;
	}

	if (mRenderSimulationDebug)
	{
		// Get the cell the mouse is in
		mSimulation->getStaticLinesGrid()->getContainingCell(true,
			mMousePosition.x, mMousePosition.y,
			mSimulationDebugCellX, mSimulationDebugCellY
		);
	}
}

void SweepClaw::sweepColliderWithMouse(collide::Collider* collider)
{
	Vector2 desiredMovement = Vector2::ZERO;
	if (mDragging)
	{
		desiredMovement = mMousePosition - mPrevMousePosition;
	}
	else if (mSweeping)
	{
		desiredMovement = mSweepLine.getVertex(1) - collider->getCentre();
	}

	if (mDragging)
	{
		collider->setMovement(desiredMovement);
	}
	else
	{
		wp::collide::SweepResult result;
		mSimulation->projectCollider(collider, desiredMovement, &result);
		mSweptPosition = result.newPosition;
	}
}

void SweepClaw::update(float frameTime)
{
	mSimulation->update(frameTime);
	mNumSweepChecks = mSimulation->getNumSweepChecks();

	mSweepLine.setVertex(1, mMousePosition);

	// Sweep the selected object
	if (mHoveredCollider)
	{
		sweepColliderWithMouse(mHoveredCollider);
	}
}

void SweepClaw::renderDebug(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	/*
	AccelerationGrid const* staticLinesGrid = mSimulation->getStaticLinesGrid();

	int cellDimX = staticLinesGrid->getCellDimensionX();
	int cellDimY = staticLinesGrid->getCellDimensionY();
	Vector2 cellOffset = staticLinesGrid->getOffset();
	Vector2 cellSize = staticLinesGrid->getCellSize();

	renderSystem->pushModelMatrix2d();
	renderSystem->translateTransform2d(glm::vec2(cellOffset.x, cellOffset.y));

	// Render lines
	for (int x = 0; x <= cellDimX; ++x)
	{
		//renderSystem->renderLine2d(cellOffset.x + x * cellSize.x, cellOffset.y + 0, cellOffset.x + x * cellSize.x, cellOffset.y + cellDimY * cellSize.y, mpp::Colour::Yellow);
		renderSystem->renderLine2d(x * cellSize.x, 0, x * cellSize.x, cellDimY * cellSize.y, mpp::Colour::Yellow);
	}

	for (int y = 0; y <= cellDimY; ++y)
	{
		//renderSystem->renderLine2d(cellOffset.x + 0, cellOffset.y + y * cellSize.y, cellOffset.x + cellDimX * cellSize.x, cellOffset.y + y * cellSize.y, mpp::Colour::Yellow);
		renderSystem->renderLine2d(0, y * cellSize.y, cellDimX * cellSize.x, y * cellSize.y, mpp::Colour::Yellow);
	}

	renderSystem->popModelMatrix2d();

	// Render objects
	if (mSimulationDebugCellX >= 0 && mSimulationDebugCellY >= 0)
	{
		auto const& staticLineIndices = staticLinesGrid->_getItemsInCellRange(
			mSimulationDebugCellX, mSimulationDebugCellY,
			mSimulationDebugCellX, mSimulationDebugCellY
			);

		for (wp::uint32_t staticLineIndex : staticLineIndices)
		{
			auto const& staticLine = mSimulation->getStaticLine(staticLineIndex);

			Vector2 slVerts[2];
			staticLine.getVertices(slVerts[0], slVerts[1]);

			renderSystem->renderLine2d(slVerts[0].x, slVerts[0].y, slVerts[1].x, slVerts[1].y, mpp::Colour(1.0f, 0.4f, 0.0f));
		}
	}
	*/
}
