#pragma comment(lib, "ws2_32.lib")

#if _MSC_VER < 1930
#  include <vld.h> // Memory tracking
#endif

#include <iostream>
#include <cstdarg>
#include <winsock2.h>

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>
#include <mpp/Logger.h>

#include <utils/StringUtils.h>

#include <willpower/application/resourcesystem/ResourceManager.h>

#include <willpower/common/MathsUtils.h>

#include <willpower/Firepower/BeamResource.h>
#include <willpower/Firepower/BeamDefaultDefinitionFactory.h>

#include "FlowControlException.h"
#include "DirectoryResourceLocation.h"

// Platform
#include "sdl/WindowSDL.h"
#include "sdl/TimerSDL.h"
#include "sdl/InputManagerSDL.h"

#include "Claw.h"

// Claws
#include "geometry/GeometryClaw.h"
#include "realtime-geom/RealtimeGeomClaw.h"

using namespace std;
using namespace utils; 
using namespace wp;

wp::Logger* gLogger = nullptr;
mpp::Logger* gMppLogger = nullptr;
wp::application::resourcesystem::ResourceManager* gResMgr = nullptr;
mpp::RenderSystem* gRenderSystem = nullptr;
mpp::ResourceManager* gResourceManager = nullptr;

Window* gWindow = nullptr;
Timer* gTimer = nullptr;
InputManager* gInputMgr = nullptr;
Claw* gClaw = nullptr;

string getHostNameLowercase()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 0), &wsaData);

	char hostname[256];
	if (gethostname(hostname, sizeof(hostname)) != 0)
	{
		throw exception("Could not determine hostname!");
	}

	WSACleanup();

	for (size_t i = 0; i < strlen(hostname); ++i)
	{
		hostname[i] = tolower(hostname[i]);
	}

	return string(hostname);
}

class BeamResourceFactory : public wp::application::resourcesystem::ResourceFactory
{
public:

	BeamResourceFactory()
		: ResourceFactory("Beam")
	{
	}

	wp::application::resourcesystem::Resource* createResource(string const& name, string const& namesp, string const& source, map<string, string> const& tags, wp::application::resourcesystem::ResourceLocation* location) override
	{
		return new firepower::BeamResource(name, namesp, source, tags, location);
	}
};


void startup()
{
	gLogger = new Logger();
	if (!gLogger->open("ScratchPadLog.html"))
	{
		throw exception("Could not create Willpower logger!");
	}

	gMppLogger = new mpp::Logger();
	if (!gMppLogger->initialise("mpp.log", mpp::Logger::Level::Debug))
	{
		throw exception("Could not create MPP logger!");
	}

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		throw exception("Could not initialise SDL video!");

	gWindow = new WindowSDL();

	string hostname = getHostNameLowercase();
	if (hostname == "ygernainferna")
	{
		gWindow->create(1600, 900, false, true);
	}
	else if (hostname == "ygernasaturna")
	{
		gWindow->create(800, 600, false, true);
	}
	else if (hostname == "ygernaburning")
	{
		gWindow->create(1024, 768, false, true);
	}
	else
	{
		gWindow->create(800, 600, false, true);
	}

	gRenderSystem = new mpp::RenderSystem(gWindow->getWidth(), gWindow->getHeight(), gMppLogger);
	gResourceManager = new mpp::ResourceManager(gRenderSystem, gMppLogger);
	//gResourceManager->setImageLoadFunction(loadImage);
	gRenderSystem->createCoreResources(gResourceManager);

	gResMgr = new application::resourcesystem::ResourceManager(gRenderSystem, gResourceManager, gLogger);

	gResMgr->addResourceLocationFactory("Directory", [](string const& location, string const& definitionFile)
		-> application::resourcesystem::ResourceLocation*
	{
		return new DirectoryResourceLocation(gLogger, location, definitionFile);
	});

	gResMgr->addResourceFactory(new BeamResourceFactory);

	gResMgr->addResourceDefinitionFactory(new firepower::BeamDefaultDefinitionFactory);

	gResMgr->addResourceLocation("Directory", "../../../../../Resources", "Resources.xml");
	gResMgr->scanLocations();
	gResMgr->loadAllResources(true);

	gInputMgr = new InputManagerSDL();
	gTimer = new TimerSDL();

	//gClaw = new CollisionClaw(gLogger, gRenderSystem);
	//gClaw = new SweepClaw(gLogger, gRenderSystem);
	//gClaw = new NavMeshClaw(gLogger, gRenderSystem);
	//gClaw = new GeometryClaw(gLogger, gRenderSystem);
	//gClaw = new FirepowerClaw(gLogger, gRenderSystem);
	gClaw = new RealtimeGeomClaw(gLogger, gRenderSystem);

	gRenderSystem->getOrCreateRenderPipeline("Default");
}

void shutdown()
{
	delete gClaw;
	delete gTimer;
	delete gInputMgr;
	delete gResourceManager;
	delete gRenderSystem;
	delete gMppLogger;
	delete gResMgr;

	if (gWindow)
	{
		gWindow->destroy();
		delete gWindow;
	}

	SDL_Quit();
	delete gLogger;
}

#undef main // SDL_main is included via SDL.h
int main(int argc, char** argv)
{
#if _MSC_VER < 1930
	//VLD_OPT_UNICODE_REPORT
	VLDSetReportOptions(VLD_OPT_REPORT_TO_FILE, L"memdump.log");
#endif

	int exitCode = 0;

	try
	{
		//
		// Initialisation
		//
		startup();

		if (gClaw)
		{
			gClaw->setup(gRenderSystem, gResourceManager);
		}

		//
		// Main loop
		//
		float accum = 0.0f;
		const float updateFreq = 1.0f / 60.0f;

		float fpsTimer = 0.0f, fps = 0.0f;
		int frameCount = 0;

		gTimer->reset();
		bool running = true;
		while (running)
		{
			// Get frame time
			float frameTime = gTimer->getDeltaTime();
			accum += frameTime;

			// Calculate FPS
			frameCount++;
			fpsTimer += frameTime;
			if (fpsTimer >= 1.0f)
			{
				fpsTimer -= 1.0f;
				fps = (float)frameCount;
				frameCount = 0;
			}

			// Process window messages
			gWindow->processEvents(gInputMgr);

			// Update input
			gInputMgr->update();

			if (gInputMgr->keyPressed(Key_Escape))
			{
				running = false;
			}
			if (gInputMgr->keyPressed(Key_F12))
			{
				gRenderSystem->showDebugPanel(!gRenderSystem->isDebugPanelShown(), mpp::RenderSystem::TimeUnit::Milliseconds, mpp::RenderSystem::SizeUnit::Megabytes);
			}

			int cursorPosX, cursorPosY;
			gInputMgr->getMousePosition(&cursorPosX, &cursorPosY);
			cursorPosY = gWindow->getHeight() - cursorPosY;

			if (gClaw)
			{
				gClaw->handleInput(gInputMgr, updateFreq);
			}

			// Update current state
			while (accum >= updateFreq)
			{
				accum -= updateFreq;

				if (gClaw)
				{
					gClaw->update(updateFreq);
				}
			}

			// Render
			gRenderSystem->startStatsCollection();

			auto viewPos = gClaw->getViewPosition();

			gRenderSystem->renderScene(
				gClaw->getScene(), 
				gClaw->getCamera(), 
				glm::vec2(
					viewPos.x - gRenderSystem->getWindowWidth() / 2, 
					viewPos.y - gRenderSystem->getWindowHeight() / 2),
				"Default");

			auto ri = gRenderSystem->finishStatsCollection();

			vector<string> lines;

			Vector2 cursorWorldPos = gClaw->getMouseWorldPosition(cursorPosX, cursorPosY);

			lines.push_back(STR_FORMAT("{},{}", cursorWorldPos.x, cursorWorldPos.y));

			lines.push_back(STR_FORMAT("FPS: {}", fps));
			
			auto clawText = gClaw->getText();
			if (!clawText.empty())
			{
				lines.push_back("");
				for (auto const& text : clawText)
				{
					lines.push_back(text);
				}
			}

			gRenderSystem->renderText(lines, (int)gRenderSystem->getWindowWidth() - 160, (int)(gRenderSystem->getWindowHeight() - (lines.size() + 1)) * 16, mpp::Colour::White);

			gWindow->show();
		}
	}
	catch (FlowControlQuitException const& e)
	{
		exitCode = e.getExitCode();
	}
	catch (exception const& e)
	{
		cout << e.what() << endl;
		gLogger->error(e.what());
		exitCode = 1;

#ifdef _DEBUG
		char const* msg = e.what();

		size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), 0, 0);
		wstring ret(reqLength, L'\0');

		::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), &ret[0], (int)ret.length());
		OutputDebugString(ret.c_str());
#endif
	}

	shutdown();
	return exitCode;
}