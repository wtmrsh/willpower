#include "firepower/EntityManager.h"

using namespace wp;

EntityManager::EntityManager(int initialSize, std::string const& texture)
	: mBatch(nullptr)
	, mTexture(texture)
	, mTextureWidth(0)
	, mTextureHeight(0)
{
	mEntities.reserve(initialSize);
}

EntityManager::EntityManager()
{
	for (auto entity: mEntities)
	{
		delete entity;
	}

	delete mBatch;
}

void EntityManager::addEntity(Entity* entity)
{
	mEntities.push_back(entity);
}

void EntityManager::update(float frameTime, firepower::ObjectCollisionManager* objectMgr)
{
	for (auto entity: mEntities)
	{
		entity->update(frameTime);
		
		auto bounds = entity->getBounds();
		objectMgr->onObjectMoved(entity->getId(), bounds.getMinExtent(), bounds.getMaxExtent());
	}
}

void EntityManager::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	if (!mBatch)
	{
		auto entityAtlas = resourceMgr->getResource(mTexture);
		auto const& entityResource = ((mpp::Texture const&)*entityAtlas);

		mTextureWidth = entityResource.getWidth();
		mTextureHeight = entityResource.getHeight();

		mBatch = new mpp::QuadBatch(
			"Entities",
			{
				mpp::QuadBatchOptions::PrimitiveOptions::Auto,
				mpp::mesh::Vertex::DataType::Float,
				{ mpp::mesh::Vertex::DataType::Float, true },
				{ mpp::mesh::Vertex::DataType::UnsignedByte, false },
				false,
				false,
				16,
				16,
				32
			},
			true,
			entityAtlas,
			256,
			renderSystem,
			resourceMgr);

		mBatch->load();
	}
	/*
	mBatch->startUpdate(mEntities.size());

	float* posBuffer = (float*)mBatch->getPositionData();
	float* texBuffer = (float*)mBatch->getTexCoordData();

	for (uint32_t offset = 0, i = 0; i < mEntities.size(); ++i)
	{
		// Entity data
		Entity* entity = mEntities[i];

		Vector2 const& pos = entity->getPosition();
		Vector2 const& dir = entity->getDirection();
		float spriteIndex = (float)0;

		// Position/rotation data
		posBuffer[offset + 0] = pos.x;
		posBuffer[offset + 1] = pos.y;
		posBuffer[offset + 2] = dir.x;
		posBuffer[offset + 3] = dir.y;

		// Texture data
		float texRatio = float(mTextureHeight) / mTextureWidth;
		texBuffer[offset + 0] = spriteIndex * texRatio;
		texBuffer[offset + 1] = 0.0f;
		texBuffer[offset + 2] = (spriteIndex + 1) * texRatio;
		texBuffer[offset + 3] = 1.0f;

		offset += 4;
	}

	mBatch->finishUpdate(mEntities.size(), true);
	*/
	auto mi = renderSystem->renderModelBatched(*mBatch, true, nullptr, mBatch->getPrimitiveCount(mBatch->getCount()));
	//mi->getMeshInstance("Entities")->setUniform("DIFFUSE", glm::vec4(1, 1, 1, 1));
}