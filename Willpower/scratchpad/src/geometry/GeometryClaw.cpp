#include <mpp/helper/OrthoCamera.h>

#include "willpower/common/CubicBSpline.h"
#include "willpower/common/ExtentsCalculator.h"

#include "willpower/geometry/Polygon.h"
#include "willpower/geometry/MeshOperations.h"

#include "willpower/collide/ColliderCircle.h"
#include "willpower/collide/ColliderAABB.h"

#include "geometry/GeometryClaw.h"

using namespace std;
using namespace wp;
using namespace wp::geometry;

GeometryClaw::GeometryClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "Geometry", renderSystem)
	, mVertexAttributeFactory(nullptr)
	, mEdgeAttributeFactory(nullptr)
	, mPolygonAttributeFactory(nullptr)
	, mPolygonVertexAttributeFactory(nullptr)
	, mSimulation(nullptr)
	, mPlayer(nullptr)
{
}

GeometryClaw::~GeometryClaw()
{
	delete mVertexAttributeFactory;
	delete mEdgeAttributeFactory;
	delete mPolygonAttributeFactory;
	delete mPolygonVertexAttributeFactory;
	delete mSimulation;
}

RegularPolygon GeometryClaw::createRectangle(float x, float y, float width, float height, float offsetAngle)
{
	// Generate points
	auto w2 = width / 2.0f;
	auto h2 = height / 2.0f;

	vector<wp::Vector2> points{
		{ -w2, -h2 },
		{ w2, -h2 },
		{ w2, h2 },
		{ -w2, h2 }
	};
	
	float cx = x + width / 2.0f;
	float cy = y + height / 2.0f;

	for (auto& point: points)
	{
		wp::MathsUtils::rotatePoint(point, offsetAngle);
		point.x += cx;
		point.y += cy;
	}

	// Create polygon
	vector<uint32_t> vertexIds;
	for (auto const& point: points)
	{
		vertexIds.push_back(mMesh->addVertex(Vertex(point)));
	}

	// Set vertex data
	for (auto vertexId: vertexIds)
	{
		auto const& vertex = mMesh->getVertex(vertexId);
		Vector2 const& vertexPos = vertex.getPosition();

		VertexAttributes va
		{
			1.0f, 1.0f, 1.0f
		};

		mMesh->setVertexUserData(vertexId, &va);
	}

	RegularPolygon regPolygon;
	vector<uint32_t> edgeDefs;
	for (size_t i = 0; i < vertexIds.size(); ++i)
	{
		size_t j = (i + 1) % vertexIds.size();
		auto edgeId = mMesh->addEdge(Edge(vertexIds[i], vertexIds[j]));

		EdgeAttributes ea
		{
			1.0f, 1.0f, 1.0f
		};

		mMesh->setEdgeUserData(edgeId, &ea);

		regPolygon.edgeIds.push_back(edgeId);

		edgeDefs.push_back(vertexIds[i]);
		edgeDefs.push_back(vertexIds[j]);
		edgeDefs.push_back(edgeId);
	}

	wp::geometry::Polygon polygon(edgeDefs);
	regPolygon.polygonId = mMesh->addPolygon(polygon);

	// Set polygon attribs
	PolygonAttributes pa
	{
		1.0f, 1.0f, 1.0f,
		wp::geometry::UserAttributePolygonColourType::Polygon,
		{ "CloudsTexture", "TestTexture" }
	};

	if (regPolygon.polygonId & 1)
	{
		pa.textures[1] = "Floor1";
	}

	mMesh->setPolygonUserData(regPolygon.polygonId, &pa);

	// Set polygon vertex attribs
	for (size_t i = 0; i < vertexIds.size(); ++i)
	{
		auto vertexId = vertexIds[i];
		auto vp = mMesh->getVertex(vertexId).getPosition() / 256;

		vp.rotateAnticlockwise(offsetAngle);

		PolygonVertexAttributes pva
		{
			vp.x, 
			vp.y, 
			vp.x, 
			vp.y, 
			0.7f,
			0.3f, 
			1.0f, 
			1.0f, 
			1.0f
		};

		mMesh->setPolygonVertexUserData(regPolygon.polygonId, vertexId, &pva);
	}

	return regPolygon;
}

ArcPolgon GeometryClaw::createArc(float centreX, float centreY, float radius, float startAngle, float endAngle, float width1, float width2, InterpolationFunction widthFunc, size_t resolution)
{
	// Work out vertices required based on arc length and width
	/*
	while (startAngle >= 360.0f)
	{
		startAngle -= 360.0f;
	}

	while (endAngle >= 360.0f)
	{
		endAngle -= 360.0f;
	}
	*/

	if (startAngle > endAngle)
	{
		swap(startAngle, endAngle);
	}

	if (endAngle - startAngle > 360.0f)
	{
		throw exception();
	}

	float dAngle = endAngle - startAngle;

	float d1_1 = radius - width1 / 2.0f;
	float d2_1 = radius + width1 / 2.0f;
	float d1_2 = radius - width2 / 2.0f;
	float d2_2 = radius + width2 / 2.0f;

	// Generate points
	vector<wp::Vector2> points;

	auto res2 = (size_t)(dAngle * ((d2_1 + d2_2) * 0.5f) / resolution);
	for (size_t i = 0; i < res2; ++i)
	{
		auto inc = (i / (float)(res2 - 1));

		Vector2 point(0, d2_1 + (d2_2 - d2_1) * widthFunc(inc));
		float angle = startAngle + dAngle * inc;

		wp::MathsUtils::rotatePoint(point, angle);

		point.x += centreX;
		point.y += centreY;
		points.push_back(point);
	}

	auto res1 = (size_t)(dAngle * ((d1_1 + d1_2) * 0.5f) / resolution);
	for (size_t i = 0; i < res1; ++i)
	{
		auto inc = (i / (float)(res1 - 1));

		Vector2 point(0, d1_2 - (d1_2 - d1_1) * widthFunc(inc));
		float angle = endAngle - dAngle * inc;

		wp::MathsUtils::rotatePoint(point, angle);

		point.x += centreX;
		point.y += centreY;
		points.push_back(point);
	}

	// Create polygon
	vector<uint32_t> vertexIds;
	for (auto const& point: points)
	{
		vertexIds.push_back(mMesh->addVertex(Vertex(point)));
	}

	// Set vertex data
	for (auto vertexId: vertexIds)
	{
		auto const& vertex = mMesh->getVertex(vertexId);
		Vector2 const& vertexPos = vertex.getPosition();
	}

	ArcPolgon arcPolygon;
	vector<uint32_t> edgeDefs;
	for (size_t i = 0; i < vertexIds.size(); ++i)
	{
		size_t j = (i + 1) % vertexIds.size();
		auto edgeId = mMesh->addEdge(Edge(vertexIds[i], vertexIds[j]));

		if (j == 0)
		{
			arcPolygon.startEdgeId = edgeId;
		}
		else if (i == res2)
		{
			arcPolygon.endEdgeId = edgeId;
		}
		else if (i < res2)
		{
			arcPolygon.leftEdgeIds.push_back(edgeId);
		}
		else
		{
			arcPolygon.rightEdgeIds.push_back(edgeId);
		}

		edgeDefs.push_back(vertexIds[i]);
		edgeDefs.push_back(vertexIds[j]);
		edgeDefs.push_back(edgeId);
	}

	wp::geometry::Polygon polygon(edgeDefs);
	arcPolygon.polygonId = mMesh->addPolygon(polygon);

	// Set vertex attribs
	PolygonVertexAttributes pva
	{
		0.0f, 0.0f, 1.0f, 1.0f, 1.0f
	};

	for (auto const& vertexId : vertexIds)
	{
		mMesh->setPolygonVertexUserData(arcPolygon.polygonId, vertexId, &pva);
	}

	return arcPolygon;
}

void GeometryClaw::createCollideSimulation()
{
	Vector2 minExtent, maxExtent;
	mMesh->getExtents(minExtent, maxExtent);

	ExtentsCalculator extents(minExtent, maxExtent, 1);
	mSimulation = new collide::Simulation(extents, 8, 8, mLogger);

	// Add mesh lines
	uint32_t polygonIndex = mMesh->getFirstPolygonIndex();
	while (!mMesh->polygonIndexIterationFinished(polygonIndex))
	{
		geometry::Polygon const& polygon = mMesh->getPolygon(polygonIndex);

		auto edgeIt = polygon.getFirstEdge();
		while (edgeIt != polygon.getEndEdge())
		{
			auto const& polyEdge = *edgeIt;

			geometry::Edge const& edge = mMesh->getEdge(polyEdge.index);

			if (edge.getConnectivity() == geometry::Edge::Connectivity::External)
			{
				Vector2 v0 = mMesh->getVertex(polyEdge.v0).getPosition();
				Vector2 v1 = mMesh->getVertex(polyEdge.v1).getPosition();

				mSimulation->addStaticLine(v0, v1);
			}

			++edgeIt;
		}

		// Next polygon
		polygonIndex = mMesh->getNextPolygonIndex(polygonIndex);
	}
}

void extrudeVertexCallback(geometry::Mesh* mesh, geometry::ExtrudeVertexResult const& result)
{
	auto polygonId = result.affectedPolygon;

	// Vertices
	for (size_t i = 0; i < result.newVertexIndices.size(); ++i)
	{
		mesh->setVertexUserData(result.newVertexIndices[i], mesh->getVertexUserData(result.vertexIndex));
	}

	// Edges
	auto refEdgeData = mesh->getEdgeUserData(result.newEdgeIndices[0]);
	for (size_t i = 1; i < result.newEdgeIndices.size(); ++i)
	{
		mesh->setEdgeUserData(result.newEdgeIndices[i], refEdgeData);
	}

	// Polygon vertices
	auto const* pva = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, result.vertexIndex));
	PolygonVertexAttributes pva2 = *pva;

	for (auto vertexIndex : result.newVertexIndices)
	{
		auto const& vp = mesh->getVertex(vertexIndex).getPosition() / 256;

		pva2.u0 = vp.x;
		pva2.v0 = vp.y;
		pva2.u1 = vp.x;
		pva2.v1 = vp.y;

		mesh->setPolygonVertexUserData(polygonId, vertexIndex, &pva2);
	}
}

void splitEdgeCallback(geometry::Mesh* mesh, geometry::SplitEdgeResult const& result)
{
	auto polygonId = result.affectedPolygons[0];
	int numEdges = (int)result.newEdgeIndices.size();

	// Vertices
	auto const* va0 = static_cast<VertexAttributes const*>(mesh->getVertexUserData(result.splitEdgeVertexIndices[0]));
	auto const* va1 = static_cast<VertexAttributes const*>(mesh->getVertexUserData(result.splitEdgeVertexIndices[1]));

	for (size_t i = 0; i < result.newVertexIndices.size(); ++i)
	{
		auto va = va0->lerp(*va1, (i + 1) / (float)numEdges);
		mesh->setVertexUserData(result.newVertexIndices[i], &va);
	}

	// Edges
	auto refEdgeData = mesh->getEdgeUserData(result.newEdgeIndices[0]);
	for (size_t i = 1; i < result.newEdgeIndices.size(); ++i)
	{
		mesh->setEdgeUserData(result.newEdgeIndices[i], refEdgeData);
	}

	// Polygon vertices
	auto const* pva0 = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, result.splitEdgeVertexIndices[0]));
	auto const* pva1 = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, result.splitEdgeVertexIndices[1]));

	for (size_t i = 0; i < result.newVertexIndices.size(); ++i)
	{
		auto pva = pva0->lerp(*pva1, (i + 1) / (float)numEdges);
		mesh->setPolygonVertexUserData(polygonId, result.newVertexIndices[i], &pva);
	}
}

void chamferVertexCallback(geometry::Mesh* mesh, geometry::ChamferVertexResult const& result)
{
	auto polygonId = result.affectedPolygon;

	// Vertices
	for (size_t i = 0; i < result.newVertexIndices.size(); ++i)
	{
		mesh->setVertexUserData(result.newVertexIndices[i], mesh->getVertexUserData(result.vertexIndex));
	}

	// Edges
	auto refEdgeData = mesh->getEdgeUserData(result.newEdgeIndices[0]);
	for (size_t i = 1; i < result.newEdgeIndices.size(); ++i)
	{
		mesh->setEdgeUserData(result.newEdgeIndices[i], refEdgeData);
	}

	// Polygon vertices
	auto const* pva = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, result.vertexIndex));
	PolygonVertexAttributes pva2 = *pva;

	for (auto vertexIndex: result.newVertexIndices)
	{
		auto const& vp = mesh->getVertex(vertexIndex).getPosition() / 256;

		pva2.u0 = vp.x;
		pva2.v0 = vp.y;
		pva2.u1 = vp.x;
		pva2.v1 = vp.y;

		mesh->setPolygonVertexUserData(polygonId, vertexIndex, &pva2);
	}
}

void snipVertexCallback(geometry::Mesh* mesh, geometry::SnipVertexResult const& result)
{
	auto polygonId = result.affectedPolygon;
	auto const& edge = mesh->getEdge(result.newEdgeIndex);

	// First vertex
	auto vertexId = edge.getFirstVertex();
	auto vp = mesh->getVertex(vertexId).getPosition();
	auto const* pva = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, vertexId));

	PolygonVertexAttributes pva2 = *pva;
	pva2.u0 = vp.x / 256;
	pva2.v0 = vp.y / 256;
	pva2.u1 = vp.x / 256;
	pva2.v1 = vp.y / 256;

	mesh->setPolygonVertexUserData(polygonId, vertexId, &pva2);

	// Second vertex
	vertexId = edge.getSecondVertex();
	vp = mesh->getVertex(vertexId).getPosition();

	pva2.u0 = vp.x / 256;
	pva2.v0 = vp.y / 256;
	pva2.u1 = vp.x / 256;
	pva2.v1 = vp.y / 256;
	pva2.w0 = 0.7f;
	pva2.w1 = 0.3f;
	pva2.r = pva2.g = pva2.b = 1.0f;

	mesh->setPolygonVertexUserData(polygonId, vertexId, &pva2);
}

void setEdgeLengthCallback(geometry::Mesh* mesh, geometry::SetEdgeLengthResult const& result)
{
	auto polygonId = result.affectedPolygons[0];

	// Polygon vertices
	for (size_t i = 0; i < 2; ++i)
	{
		auto vertexId = result.splitEdgeVertexIndices[i];
		auto const& vp = mesh->getVertex(vertexId).getPosition();

		auto const* pva = static_cast<PolygonVertexAttributes const*>(mesh->getPolygonVertexUserData(polygonId, vertexId));
		
		PolygonVertexAttributes pva2 = *pva;
		pva2.u0 = vp.x / 256;
		pva2.v0 = vp.y / 256;
		pva2.u1 = vp.x / 256;
		pva2.v1 = vp.y / 256;

		mesh->setPolygonVertexUserData(polygonId, vertexId, &pva2);
	}
}

void bridgeEdgesCallback(geometry::Mesh* mesh, geometry::BridgeEdgesResult const& result)
{
	// Get all created polygons and assign texture
	PolygonAttributes pa
	{
		1.0f, 1.0f, 1.0f,
		wp::geometry::UserAttributePolygonColourType::Polygon,
		{ "CloudsTexture", "TestTexture" }
	};

	for (auto const& p : result.polygons)
	{
		mesh->setPolygonUserData(p.index, &pa);

		auto const& meshPoly = mesh->getPolygon(p.index);
		auto const& meshPolyVertexIndices = meshPoly.getVertexIndexList();

		for (auto vertexIndex : meshPolyVertexIndices)
		{
			auto const& vp = mesh->getVertex(vertexIndex).getPosition();

			PolygonVertexAttributes pva
			{
				vp.x / 256, vp.y / 256, vp.x / 256, vp.y / 256,
				0.7f, 0.3f,
				1.0f, 1.0f, 1.0f
			};

			mesh->setPolygonVertexUserData(p.index, vertexIndex, &pva);
		}
	}
}

void extrudePolygonCallback(geometry::Mesh* mesh, geometry::ExtrudePolygonResult const& result)
{
	// Get all created polygons and assign texture
	PolygonAttributes pa
	{
		1.0f, 1.0f, 1.0f,
		wp::geometry::UserAttributePolygonColourType::Polygon,
		{ "CloudsTexture", "TestTexture" }
	};

	for (auto const& p : result.polygons)
	{
		mesh->setPolygonUserData(p.index, &pa);

		auto const& meshPoly = mesh->getPolygon(p.index);
		auto const& meshPolyVertexIndices = meshPoly.getVertexIndexList();

		for (auto vertexIndex : meshPolyVertexIndices)
		{
			auto const& vp = mesh->getVertex(vertexIndex).getPosition() / 256;

			PolygonVertexAttributes pva
			{
				vp.x, vp.y, vp.x, vp.y,
				0.7f, 0.3f,
				1.0f, 1.0f, 1.0f
			};

			mesh->setPolygonVertexUserData(p.index, vertexIndex, &pva);
		}
	}
}

void slicePolygonCallback(geometry::Mesh* mesh, geometry::SlicePolygonResult const& result)
{
	// Get all created polygons and assign texture
	PolygonAttributes pa
	{
		1.0f, 1.0f, 1.0f,
		wp::geometry::UserAttributePolygonColourType::Polygon,
		{ "CloudsTexture", "TestTexture" }
	};

	if (result.newPolygonIndex != -1)
	{
		mesh->setPolygonUserData(result.newPolygonIndex, &pa);

		auto const& meshPoly = mesh->getPolygon(result.newPolygonIndex);
		auto const& meshPolyVertexIndices = meshPoly.getVertexIndexList();

		for (auto vertexIndex : meshPolyVertexIndices)
		{
			auto const& vp = mesh->getVertex(vertexIndex).getPosition() / 256;

			PolygonVertexAttributes pva
			{
				vp.x, vp.y, vp.x, vp.y,
				0.7f, 0.3f,
				1.0f, 1.0f, 1.0f
			};

			mesh->setPolygonVertexUserData(result.newPolygonIndex, vertexIndex, &pva);
		}
	}
}

void GeometryClaw::setupImpl(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	if (!mVertexAttributeFactory)
	{
		mVertexAttributeFactory = new VertexAttributeFactory();
	}
	if (!mEdgeAttributeFactory)
	{
		mEdgeAttributeFactory = new EdgeAttributeFactory();
	}
	if (!mPolygonAttributeFactory)
	{
		mPolygonAttributeFactory = new PolygonAttributeFactory();
	}
	if (!mPolygonVertexAttributeFactory)
	{
		mPolygonVertexAttributeFactory = new PolygonVertexAttributeFactory();
	}

	// Create mesh
	mMesh = make_shared<geometry::Mesh>(
		mVertexAttributeFactory, 
		mEdgeAttributeFactory, 
		mPolygonAttributeFactory,
		mPolygonVertexAttributeFactory);

	// First room
	auto room1 = createRectangle(0, 0, 384, 256, 0);
	
	geometry::ExtrudeVertexOptions evo1;
	geometry::ExtrudeVertexResult evr1;
	auto extrudeVertexIndex = mMesh->getPolygon(room1.polygonId).getVertexIndexList()[3];
	geometry::MeshOperations::extrudeVertex(mMesh.get(), extrudeVertexIndex, 32, evo1, &evr1);
	extrudeVertexCallback(mMesh.get(), evr1);

	//geometry::SplitEdgeResult ser0;
	//geometry::MeshOperations::splitEdge(mMesh.get(), room1.edgeIds[1], 2, &ser0);
	//splitEdgeCallback(mMesh.get(), ser0);

	geometry::SlicePolygonResult slr1;
	geometry::MeshOperations::slicePolygon(mMesh.get(), room1.polygonId, 1, 3, false, &slr1);
	slicePolygonCallback(mMesh.get(), slr1);

	geometry::ChamferVertexResult cvr1;
	auto chamferVertexIndex = 3;// mMesh->getPolygon(room1.polygonId).getVertexIndexList()[3];
	geometry::MeshOperations::chamferVertex(mMesh.get(), chamferVertexIndex, 64, geometry::MeshOperations::OptimalBezierCurvature, &cvr1);
	chamferVertexCallback(mMesh.get(), cvr1);

	geometry::SplitEdgeResult ser1;
	//geometry::MeshOperations::splitEdge(mMesh.get(), room1.edgeIds[1], 2, &ser1);
	//splitEdgeCallback(mMesh.get(), ser1);

	// Second room
	auto room2 = createRectangle(512, 128, 384, 256, 0);

	geometry::SnipVertexResult svr1;
	auto snipVertexIndex = mMesh->getPolygon(room2.polygonId).getVertexIndexList()[1];
	geometry::MeshOperations::snipVertex(mMesh.get(), snipVertexIndex, 64, &svr1);
	snipVertexCallback(mMesh.get(), svr1);

	geometry::SplitEdgeResult ser2;
	geometry::MeshOperations::splitEdge(mMesh.get(), room2.edgeIds[3], 0.5f, &ser2);
	splitEdgeCallback(mMesh.get(), ser2);

	geometry::SplitEdgeResult ser3;
	geometry::MeshOperations::splitEdge(mMesh.get(), room2.edgeIds[0], 0.5f, &ser3);
	splitEdgeCallback(mMesh.get(), ser3);

	geometry::ExtrudePolygonOptions epo1;
	geometry::ExtrudePolygonResult epr1;
	geometry::MeshOperations::extrudePolygonNormal(mMesh.get(),
		room2.polygonId,
		{ ser3.newEdgeIndices[1] },
		128,
		epo1,
		&epr1);
	extrudePolygonCallback(mMesh.get(), epr1);

	geometry::SetEdgeLengthResult selr;
	geometry::MeshOperations::setEdgeLength(mMesh.get(), epr1.polygons[0].extrudedEdgeIndices[0], 64, &selr);
	setEdgeLengthCallback(mMesh.get(), selr);

	// Connect rooms
	/*
	geometry::BridgeEdgesOptions beo1;
	geometry::BridgeEdgesResult ber1;
	geometry::MeshOperations::bridgeEdges(mMesh.get(),
		ser1.newEdgeIndices[1],
		ser2.newEdgeIndices[1],
		beo1,
		&ber1);
	bridgeEdgesCallback(mMesh.get(), ber1);

	// Room 3
	auto room3 = createRectangle(384, -256, 256, 256, 45);
	
	geometry::SlicePolygonResult slr1;
	geometry::MeshOperations::slicePolygon(mMesh.get(),
		room3.polygonId,
		Vector2(380, -40),
		Vector2(380, -240),
		false,
		&slr1);
	slicePolygonCallback(mMesh.get(), slr1);

	geometry::SnipVertexResult svr2;
	snipVertexIndex = mMesh->getPolygon(room3.polygonId).getVertexIndexList()[0];
	geometry::MeshOperations::snipVertex(mMesh.get(), snipVertexIndex, 64, &svr2);
	snipVertexCallback(mMesh.get(), svr2);

	geometry::BridgeEdgesOptions beo2(
		geometry::BridgeEdgesOptions::Type::Curved,
		geometry::BridgeEdgesOptions::SqueezeType::Curved,
		1.0f,
		5,
		true,
		0.5f
	);

	geometry::BridgeEdgesResult ber2;
	geometry::MeshOperations::bridgeEdges(mMesh.get(),
		epr1.polygons[0].extrudedEdgeIndices[0],
		svr2.newEdgeIndex,
		beo2,
		&ber2);
	bridgeEdgesCallback(mMesh.get(), ber2);
	
	// Room 4
	auto room4 = createRectangle(-384, -128, 256, 256, 45);

	auto const& room4Edges = mMesh->getPolygon(room4.polygonId).getEdgeIndexList();

	geometry::ExtrudePolygonOptions epo2(geometry::Offsetter::CornerType::Arc, 0.0f, false, false, true);
	geometry::ExtrudePolygonResult epr2;
	geometry::MeshOperations::extrudePolygonNormal(mMesh.get(),
		room4.polygonId,
		{ room4Edges[2], room4Edges[3] },
		64,
		epo2,
		&epr2);
	extrudePolygonCallback(mMesh.get(), epr2);

	geometry::ExtrudePolygonOptions epo3;
	geometry::ExtrudePolygonResult epr3;
	geometry::MeshOperations::extrudePolygonDirected(mMesh.get(),
		room4.polygonId,
		{ room4Edges[1] },
		wp::Vector2(-1, 0) * 64,
		epo2,
		&epr2);
	extrudePolygonCallback(mMesh.get(), epr2);
	*/
	// Create mesh renderer
	viz::GeometryMeshRenderer::GridOptions gridOptions
	{
		true,
		256.0f,
		0.5f,
		viz::GeometryMeshRenderer::GridOptions::CellStrategy::Intersects
	};

	mMeshRenderer = make_shared<viz::GeometryMeshRenderer>("Geometry", mMesh, gridOptions, 32, resourceMgr);

	mMeshRenderer->build(renderSystem, resourceMgr);
	mMeshRenderer->addToScene(getScene(), 0);

	// Create grid renderer for mesh
	mMeshGridRenderer = make_shared<viz::AccelerationGridRenderer>("Geometry_Grid", mMeshRenderer->getLookupGrid(), 32, resourceMgr);
	mMeshGridRenderer->build(renderSystem, resourceMgr);
	mMeshGridRenderer->addToScene(getScene(), 1);

	// Collision
	createCollideSimulation();

	mPlayer = new wp::collide::ColliderCircle(192, 128, 32);

	auto cb = [](wp::collide::SweepResult* result, wp::collide::StaticLine const& line, void* user) -> bool
	{
		auto const& normal = line.getNormal();

		// Push the result position away from the line a small amount
		result->newPosition += normal * 0.01f; // * MathsUtils::Epsilon;
		result->movementDone = result->newPosition - result->oldPosition;
		result->distanceMoved = result->movementDone.length();

		// Calculate edge normal to move along
		Winding angleDir;
		float angle = 180 - result->movementDesired.minimumAngleTo(normal, &angleDir);

		Vector2 newDirection = angleDir ==
			Winding::Clockwise ? normal.perpendicular() : -normal.perpendicular();

		result->movementLeft = newDirection;// *sin(WP_DEGTORAD(angle));
		return true;
	};

	mPlayer->setHitLineCallback(cb);

	mSimulation->addCollider(mPlayer);

	mPlayerVelocity = Vector2::ZERO;
	mPlayerSpeed = 200;

	mMovingLeft = mMovingRight = mMovingUp = mMovingDown = false;

	// Player rendering
	mEntityDataProvider = make_shared<EntityQuadBatchDataProvider>(renderSystem, 32.0f);
	viz::QuadRendererOptions entityOptions
	{
		viz::RotationOptions::VerticesByAngle,
		false,
		64,
		64,
		resourceMgr->getResource("ZombieTexture"),
		16
	};

	mEntityRenderer = make_shared<wp::viz::DynamicQuadRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>>("Entities", entityOptions, mEntityDataProvider, resourceMgr);
	mEntityRenderer->build(renderSystem, resourceMgr);
	mEntityRenderer->addToScene(getScene(), 2);

	// Triangle rendering
	mTriangleDataProvider = make_shared<TriangleBatchDataProvider>(renderSystem);
	viz::TriangleRendererOptions triangleOptions
	{
		false,
		false,
		false,
		false,
		resourceMgr->getResource("__mpp_tex_none__")
	};

	mTriangleRenderer = make_shared<wp::viz::DynamicTriangleRenderer< mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>>("TestTris", triangleOptions, mTriangleDataProvider, resourceMgr);
	mTriangleRenderer->build(renderSystem, resourceMgr);
	mTriangleRenderer->addToScene(getScene(), 3);

	// Viewport rendering
	mViewportDataProvider = make_shared<ViewportLineBatchDataProvider>();
	mViewportRenderer = make_shared<wp::viz::DynamicLineRenderer>("Viewport", mViewportDataProvider, resourceMgr);
	mViewportRenderer->build(renderSystem, resourceMgr);
	mViewportRenderer->addToScene(getScene(), 3);
}

void GeometryClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	inputMgr->getMousePosition(&mMouseX, &mMouseY);

	if (inputMgr->keyPressed(Key_LeftArrow))
	{
		mMovingLeft = true;
	}
	if (inputMgr->keyPressed(Key_RightArrow))
	{
		mMovingRight = true;
	}
	if (inputMgr->keyPressed(Key_UpArrow))
	{
		mMovingUp = true;
	}
	if (inputMgr->keyPressed(Key_DownArrow))
	{
		mMovingDown = true;
	}
	if (inputMgr->keyReleased(Key_LeftArrow))
	{
		mMovingLeft = false;
	}
	if (inputMgr->keyReleased(Key_RightArrow))
	{
		mMovingRight = false;
	}
	if (inputMgr->keyReleased(Key_UpArrow))
	{
		mMovingUp = false;
	}
	if (inputMgr->keyReleased(Key_DownArrow))
	{
		mMovingDown = false;
	}

	// Render params
	if (inputMgr->keyPressed(Key_1))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderBackground(!params->getRenderBackground());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_2))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderVertices(!params->getRenderVertices());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_3))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderTriangulation(!params->getRenderTriangulation());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_4))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderPolygonEdges(!params->getRenderPolygonEdges());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_5))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderBorder(!params->getRenderBorder());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_6))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderEdgeDirections(!params->getRenderEdgeDirections());

		mMeshRenderer->updateRenderParams();
	}
	if (inputMgr->keyPressed(Key_7))
	{
		auto params = static_cast<wp::viz::GeometryMeshRenderParams*>(mMeshRenderer->getParams().get());
		params->setRenderEdgeNormals(!params->getRenderEdgeNormals());

		mMeshRenderer->updateRenderParams();
	}

	if (inputMgr->keyPressed(Key_0))
	{
		auto params = static_cast<wp::viz::AccelerationGridRenderParams*>(mMeshGridRenderer->getParams().get());
		params->setRender(!params->getRender());

		mMeshGridRenderer->updateRenderParams();
	}
}

void GeometryClaw::update(float frameTime)
{
	mPlayerVelocity = Vector2::ZERO;
	if (mMovingLeft)
	{
		mPlayerVelocity.x -= mPlayerSpeed;
	}
	if (mMovingRight)
	{
		mPlayerVelocity.x += mPlayerSpeed;
	}
	if (mMovingUp)
	{
		mPlayerVelocity.y += mPlayerSpeed;
	}
	if (mMovingDown)
	{
		mPlayerVelocity.y -= mPlayerSpeed;
	}

	mPlayerVelocity.normalise();
	mPlayer->setMovement(mPlayerVelocity * mPlayerSpeed);

	// Update
	mSimulation->update(frameTime);

	// Move camera
	auto viewPosition = mPlayer->getCentre();
	static_cast<mpp::helper::OrthoCamera*>(mCamera.get())->setPosition(glm::vec2(viewPosition.x, viewPosition.y));

	// Update renderers
	Vector2 cursorWorldPos((float)mMouseX, (float)(mWindowSizeY - mMouseY));
	cursorWorldPos.x -= mWindowSizeX / 2;
	cursorWorldPos.y -= mWindowSizeY / 2;
	cursorWorldPos += viewPosition;

	Vector2 viewSize((float)mWindowSizeX, (float)mWindowSizeY);
	BoundingBox viewBounds(viewPosition - viewSize / 2, viewSize);

	mMeshRenderer->update(viewBounds, frameTime);

	mEntityDataProvider->update(frameTime, viewPosition, (cursorWorldPos - viewPosition).normalisedCopy());
	mEntityRenderer->update(viewBounds, frameTime);
	
	mTriangleDataProvider->update(frameTime, viewPosition, (cursorWorldPos - viewPosition).normalisedCopy());
	mTriangleRenderer->update(viewBounds, frameTime);

	mViewportDataProvider->update(viewBounds, frameTime);
	mViewportRenderer->update(viewBounds, frameTime);
}
