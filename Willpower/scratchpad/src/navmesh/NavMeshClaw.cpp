#include <algorithm>

#include <utils/StringUtils.h>

#include "willpower/common/MathsUtils.h"

#include "willpower/geometry/SerializerMeshChunk.h"
#include "willpower/geometry/MeshUtils.h"

#include "willpower/serialization/BinarySerializer.h"

#include "willpower/wayfinder/ConvexPolygonisation.h"
#include "willpower/wayfinder/FlockingAgent.h"

#include "navmesh/NavMeshClaw.h"

using namespace std;
using namespace wp;
using namespace utils;

#define INSET_SIZE 15

NavMeshClaw::NavMeshClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "NavMesh", renderSystem)
	, mMesh(nullptr)
	, mSwarm(nullptr)
	, mMouseTarget(nullptr)
	, mHoveredSector(nullptr)
	, mHoveredPolygon(-1)
	, mAgentSize(10)
	, mHoverAgent(-1)
	, mSelectedAgent(-1)
	, mHoverTarget(-1)
	, mSelectedTarget(-1)
{
}

NavMeshClaw::~NavMeshClaw()
{
	delete mSwarm;
	delete mMesh;
	delete mMouseTarget;
}

void NavMeshClaw::loadMesh(string const& filename)
{
	geometry::Mesh* gmesh;

	serialization::BinarySerializer ser;

	ser.addChunkFactory("Mesh", [](string name, string desc) -> serialization::SerializerChunk*
	{
		return new geometry::SerializerMeshChunk(name, desc);
	});

	ser.deserialize(filename);

	// Load in mesh data
	auto meshChunk = (geometry::SerializerMeshChunk*)ser.getChunkByIndex(0);
	gmesh = new geometry::Mesh();

	auto const& vertices = meshChunk->getVertices();
	auto const& edges = meshChunk->getEdges();
	auto const& polygons = meshChunk->getPolygons();

	for (auto const& vertex : vertices)
	{
		gmesh->addVertex(vertex, -1);
	}

	for (auto const& edge : edges)
	{
		gmesh->addEdge(edge, -1);
	}

	for (auto const& polygon : polygons)
	{
		gmesh->addPolygon(polygon, -1);
	}

	if (mMesh)
	{
		delete mMesh;
	}

	vector<int> insets;
	insets.push_back(10);
	//insets.push_back(20);
	//insets.push_back(30);
	//insets.push_back(40);
	//insets.push_back(50);

	mMesh = new wayfinder::Mesh(gmesh, insets);
	mMesh->composeConvex(3);
}

void NavMeshClaw::getHoverItem(float x, float y, int* hoverAgent, int* hoverTarget) const
{
	if (hoverAgent)
	{
		*hoverAgent = -1;
	}

	if (hoverTarget)
	{
		*hoverTarget = -1;
	}

	Vector2 pos(x, y);

	// First check we are on the map
	auto sector = mMesh->getContainingSector(mAgentSize, pos);

	if (!sector)
	{
		return;
	}

	// Now check agents and targets
	for (wp::uint32_t i = 0; i < mSwarm->getNumAgents(); ++i)
	{
		auto* agent = mSwarm->getAgent(i);

		int radius = agent->getRadius();
		float d = agent->getPosition().distanceTo(pos);

		if (d <= radius)
		{
			if (hoverAgent)
			{
				*hoverAgent = i;
				break;
			}
		}
	}

	if (*hoverAgent < 0)
	{
		for (wp::uint32_t i = 0; i < mTargets.size(); ++i)
		{
			auto const& pos = mTargets[i]->getPosition();

			if (mMouseX >= (pos.x - 10) && mMouseX <= (pos.x + 10) &&
				mMouseY >= (pos.y - 10) && mMouseY <= (pos.y + 10))
			{
				if (hoverTarget)
				{
					*hoverTarget = i;
					break;
				}
			}
		}
	}
}

void NavMeshClaw::setup(mpp::ResourceManager* resourceMgr)
{
	// Load mesh
	loadMesh("test2.ged");

	mSwarm = new wayfinder::AgentSwarm(mMesh, 256);
	mMouseTarget = new MouseTarget(Vector2(0, 0));
}

void NavMeshClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	int x, y;
	inputMgr->getMousePosition(&x, &y);

	mMouseX = (float)(x - mWindowSizeX / 2);
	mMouseY = -(float)(y - mWindowSizeY / 2);

	Vector2 mousePosition(mMouseX, mMouseY);
	mMouseTarget->setPosition(mousePosition);

	// Agent size
	if (inputMgr->keyPressed(KeyDefinition::Key_1))
	{
		mAgentSize = 10;
	}
	else if (inputMgr->keyPressed(KeyDefinition::Key_2))
	{
		mAgentSize = 20;
	}
	else if (inputMgr->keyPressed(KeyDefinition::Key_3))
	{
		mAgentSize = 30;
	}
	else if (inputMgr->keyPressed(KeyDefinition::Key_4))
	{
		mAgentSize = 40;
	}
	else if (inputMgr->keyPressed(KeyDefinition::Key_5))
	{
		mAgentSize = 50;
	}

	getHoverItem(mMouseX, mMouseY, &mHoverAgent, &mHoverTarget);

	// Create an agent on left-click
	if (inputMgr->buttonPressed(MouseDefinition::Mouse_Left))
	{
		if (mHoverAgent < 0 && mHoverTarget < 0)
		{
			auto* agent = new wayfinder::FlockingAgent(mAgentSize, mousePosition, 60);
			mSwarm->addAgent(agent);
			mSwarm->setAgentTarget(agent, mMouseTarget);
		}
		else if (mHoverAgent >= 0)
		{
			mSelectedAgent = mHoverAgent;
			mSelectedTarget = -1;
		}
		else if (mHoverTarget >= 0)
		{
			mSelectedTarget = mHoverTarget;

			if (mSelectedAgent >= 0)
			{
				auto agent = mSwarm->getAgent(mSelectedAgent);

				if (agent->getTarget() == mTargets[mSelectedTarget])
				{
					mSwarm->setAgentTarget(agent, nullptr);
				}
				else
				{
					mSwarm->setAgentTarget(agent, mTargets[mSelectedTarget]);
				}
			}
			else
			{
				mSelectedAgent = -1;
			}
		}
	}
	if (inputMgr->buttonPressed(MouseDefinition::Mouse_Right))
	{
		if (mHoverAgent < 0 && mHoverTarget < 0)
		{
			bool desel = false;
			if (mSelectedAgent >= 0)
			{
				desel = true;
				mSelectedAgent = -1;
			}
			if (mSelectedTarget >= 0)
			{
				desel = true;
				mSelectedTarget = -1;
			}

			if (!desel)
			{
				auto target = new wayfinder::AgentTarget(Vector2(mMouseX, mMouseY));
				mTargets.push_back(target);
			}
		}
	}
}

void NavMeshClaw::update(float frameTime)
{
	mSwarm->update(frameTime);
}
