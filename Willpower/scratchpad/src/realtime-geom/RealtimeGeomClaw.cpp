#include "realtime-geom/Timer.h"
#include "realtime-geom/RealtimeGeomClaw.h"
#include "realtime-geom/RegularPolygon.h"
#include "realtime-geom/RectanglePolygon.h"
#include "realtime-geom/SuperformulaPolygon.h"
#include "realtime-geom/PathPolygon.h"

using namespace std;
using namespace wp;

void setupVertexTransformerObject(
	VertexTransformerObject* obj,
	vector<pair<float, wp::Vector2>> const& offsets,
	vector<pair<float, float>> const& scales,
	vector<pair<float, float>> const& angles,
	vector<pair<float, float>> const& orbitAngles,
	vector<pair<float, float>> const& orbitDists,
	bool followOrbitAngle)
{
	obj->setOffsets(offsets);
	obj->setScales(scales);
	obj->setAngles(angles);
	obj->setOrbitAngles(orbitAngles);
	obj->setOrbitDistances(orbitDists);
	obj->setFollowOrbitAngle(followOrbitAngle);
}

uint32_t addPrimitive(
	World* world, 
	bool addToGroup,
	uint32_t numSides, 
	vector<pair<float, wp::Vector2>> const& offsets, 
	vector<pair<float, float>> const& scales,
	vector<pair<float, float>> const& angles,
	vector<pair<float, float>> const& orbitAngles,
	vector<pair<float, float>> const& orbitDists,
	bool followOrbitAngle,
	Primitive::Operation op, 
	Primitive::FillRule fillType)
{
	auto p = new RegularPolygon(op, fillType, numSides);
	setupVertexTransformerObject(p, offsets, scales, angles, orbitAngles, orbitDists, followOrbitAngle);
	
	return world->addPrimitive(p, addToGroup);
}

uint32_t addRectangle(
	World* world,
	bool addToGroup,
	float xyRatio, 
	vector<pair<float, wp::Vector2>> const& offsets,
	vector<pair<float, float>> const& scales,
	vector<pair<float, float>> const& angles,
	vector<pair<float, float>> const& orbitAngles,
	vector<pair<float, float>> const& orbitDists,
	bool followOrbitAngle,
	Primitive::Operation op,
	Primitive::FillRule fillType)
{
	auto p = new RectanglePolygon(op, fillType, xyRatio);
	setupVertexTransformerObject(p, offsets, scales, angles, orbitAngles, orbitDists, followOrbitAngle);
	
	return world->addPrimitive(p, addToGroup);
}

uint32_t addSuperformula(
	World* world, 
	bool addToGroup,
	float values[6], 
	vector<pair<float, wp::Vector2>> const& offsets,
	vector<pair<float, float>> const& scales,
	vector<pair<float, float>> const& angles,
	vector<pair<float, float>> const& orbitAngles,
	vector<pair<float, float>> const& orbitDists,
	bool followOrbitAngle,
	Primitive::Operation op, 
	Primitive::FillRule fillType)
{
	auto p = new SuperformulaPolygon(op, fillType, values);
	setupVertexTransformerObject(p, offsets, scales, angles, orbitAngles, orbitDists, followOrbitAngle);

	return world->addPrimitive(p, addToGroup);
}

uint32_t addPath(
	World* world, 
	bool addToGroup,
	vector<wp::Vector2> const& points, 
	float width, 
	vector<pair<float, wp::Vector2>> const& offsets,
	vector<pair<float, float>> const& scales,
	vector<pair<float, float>> const& angles,
	vector<pair<float, float>> const& orbitAngles,
	vector<pair<float, float>> const& orbitDists,
	bool followOrbitAngle,
	Primitive::Operation op, 
	Primitive::FillRule fillType)
{
	auto p = new PathPolygon(op, fillType, points, width);
	setupVertexTransformerObject(p, offsets, scales, angles, orbitAngles, orbitDists, followOrbitAngle);
	
	return world->addPrimitive(p, addToGroup);
}

float randuf()
{
	return rand() / (float)RAND_MAX;
}

float randf()
{
	return randuf() * 2 - 1;
}

void scene1(World* world, vector<uint32_t>& primitiveIndices, int screenWidth, int screenHeight)
{
	primitiveIndices.push_back(addPrimitive(world, true, 4,
		{ { 0.0f, { -256.0f, 192.0f } } },
		{ { 0.0f, 500.0f } },
		{ { 0.0f, 45.0f } },
		{ { 0.0f, 0.0f } },
		{ { 0.0f, 0.0f } },
		false,
		Primitive::Operation::Union, 
		Primitive::FillRule::EvenOdd));

	primitiveIndices.push_back(addPrimitive(world, true, 4,
		{ { 0.0f, { -384.0f, 192.0f } } },
		{ { 0.0f, 100.0f } },
		{ { 0.0f, 45.0f } },
		{ { 0.0f, 0.0f } },
		{ { 0.0f, 0.0f } },
		false,
		Primitive::Operation::Difference, 
		Primitive::FillRule::EvenOdd));

	primitiveIndices.push_back(addPrimitive(world, true, 4,
		{ { 0.0f, { -128.0f, 192.0f } } },
		{ { 0.0f, 100.0f } },
		{ { 0.0f, 45.0f }, { 1.0f, 315.0f } },
		{ { 0.0f, 0.0f } },
		{ { 0.0f, 0.0f } },
		false,
		Primitive::Operation::Difference,
		Primitive::FillRule::EvenOdd));

	primitiveIndices.push_back(addPrimitive(world, true, 4,
		{ { 0.0f, { -354.0f, 162.0f } } },
		{ { 0.0f, 100.0f } },
		{ { 0.0f, 45.0f } },
		{ { 0.0f, 0.0f } },
		{ { 0.0f, 0.0f } },
		false,
		Primitive::Operation::XOR,
		Primitive::FillRule::EvenOdd));

/*
	primitiveIndices.push_back(addRectangle(world, 5.0f, { 0, 0 }, { 0, 384 }, 250, 250, 0.0f, 0.0f,
		Primitive::Operation::Intersection, Primitive::FillRule::NonZero));

	float sfv[6] = { 1, 1, 5, 1, 1, 1 };
	primitiveIndices.push_back(addSuperformula(world, sfv, { 0, -192 }, 180, 0.0f, 
		Primitive::Operation::Union, Primitive::FillRule::EvenOdd));

	vector<wp::Vector2> pp = {
		{ 0, 0 }, {100, 0}, {150, 150}, { 300, 50 }
	};

	primitiveIndices.push_back(addPath(world, pp, 100.0f, { -384, -192 }, 1.0f, 0.0f, 
		Primitive::Operation::Union, Primitive::FillRule::NonZero));
*/
}

void scene2(World* world, vector<uint32_t>& primitiveIndices, int screenWidth, int screenHeight)
{
	/*
	srand(1337);

	for (int i = 0; i < 20; ++i)
	{
		uint32_t index;
		
		wp::Vector2 pos;
		float scale, angle;
		Primitive::Operation op;
		Primitive::FillRule fr;

		pos.set(randf() * screenWidth * 0.5f, randf() * screenHeight * 0.5f);
		scale = randuf() * 100 + 200;
		angle = randuf() * 360.0f;

		switch (rand() % 4)
		{
		case 0:
			op = Primitive::Operation::Union;
			fr = Primitive::FillRule::EvenOdd;
			index = addPrimitive(world, 4, pos, pos, scale, scale, angle, angle, op, fr);
			break;

		case 1:
			op = Primitive::Operation::Union;
			fr = Primitive::FillRule::EvenOdd;
			index = addPrimitive(world, (rand() % 6) + 3, pos, pos, scale, scale, angle, angle, op, fr);
			break;

		case 2:
			op = Primitive::Operation::Union;
			fr = Primitive::FillRule::NonZero;
			index = addRectangle(world, randuf() + 1.0f, pos, pos, scale, scale, angle, angle, op, fr);
			break;

		case 3:
			op = Primitive::Operation::Union;
			fr = Primitive::FillRule::NonZero;
			index = addPrimitive(world, ~0u, pos, pos, scale, scale, angle, angle, op, fr);
			break;
		}

		primitiveIndices.push_back(index);
	}
	*/
}

RealtimeGeomClaw::RealtimeGeomClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "RealtimeGeom", renderSystem)
	, mWorld(nullptr)
{
}

RealtimeGeomClaw::~RealtimeGeomClaw()
{
	delete mWorld;
}

void RealtimeGeomClaw::setupImpl(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	mMovingLeft = false;
	mMovingRight = false;
	mMovingUp = false;
	mMovingDown = false;

	// Acceleration grid
	mAccelGrid = make_shared<wp::AccelerationGrid>(-4096.0f, -4096.0f, 8192.0f, 8192.0f, 16, 16);

	mGridRenderer = make_shared<viz::AccelerationGridRenderer>("Accel_Grid", mAccelGrid, 32, resourceMgr);
	mGridRenderer->build(renderSystem, resourceMgr);
	mGridRenderer->addToScene(getScene(), 1);

	//
	// Rendering
	//

	// Triangles
	mPolygonTriangleDataProvider = make_shared<PolygonTriangleDataProvider>(nullptr, mAccelGrid);

	viz::TriangleRendererOptions triangleOptions
	{
		false,
		false,
		false,
		false,
		resourceMgr->getResource("__mpp_tex_none__")
	};

	mPolygonTriangleRenderer = make_shared<wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>>("PrimitiveTris", triangleOptions, mPolygonTriangleDataProvider, resourceMgr);
	mPolygonTriangleRenderer->build(renderSystem, resourceMgr);
	mPolygonTriangleRenderer->addToScene(getScene(), 2);

	// Lines
	mPolygonBorderLineDataProvider = make_shared<PolygonBorderLineBatchDataProvider>(nullptr, mAccelGrid);

	mPolygonBorderLineRenderer = make_shared<wp::viz::DynamicLineRenderer>("Polygons", mPolygonBorderLineDataProvider, resourceMgr);
	mPolygonBorderLineRenderer->build(renderSystem, resourceMgr);
	mPolygonBorderLineRenderer->addToScene(getScene(), 3);

	mPrimitiveLineDataProvider = make_shared<PrimitiveLineBatchDataProvider>(nullptr, mAccelGrid);

	mPrimitiveLineRenderer = make_shared<wp::viz::DynamicLineRenderer>("Primitives", mPrimitiveLineDataProvider, resourceMgr);
	mPrimitiveLineRenderer->build(renderSystem, resourceMgr);
	mPrimitiveLineRenderer->addToScene(getScene(), 3);
	mPrimitiveLineRenderer->setVisible(false);

	mPrimitiveBoundsLineDataProvider = make_shared<PrimitiveBoundsLineBatchDataProvider>(nullptr, mAccelGrid);

	mPrimitiveBoundsLineRenderer = make_shared<wp::viz::DynamicLineRenderer>("PrimitiveBounds", mPrimitiveBoundsLineDataProvider, resourceMgr);
	mPrimitiveBoundsLineRenderer->build(renderSystem, resourceMgr);
	mPrimitiveBoundsLineRenderer->addToScene(getScene(), 3);
	mPrimitiveBoundsLineRenderer->setVisible(false);

	// Create scene
	setScene(0);
}

void RealtimeGeomClaw::setScene(int scene)
{
	delete mWorld;
	mWorld = new World();

	vector<uint32_t> primitiveIndices;

	switch (scene)
	{
	case 0:
		scene1(mWorld, primitiveIndices, mWindowSizeX, mWindowSizeY);
		break;

	case 1:
		scene2(mWorld, primitiveIndices, mWindowSizeX, mWindowSizeY);
		break;

	default:
		throw exception("Bad scene number");
	}

	mAccelGrid->clear();
	for (auto index : primitiveIndices)
	{
		auto primitive = mWorld->getPrimitive(index);
		mAccelGrid->addItem(index, primitive->getBounds());
	}

	mPolygonTriangleDataProvider->setWorld(mWorld);
	mPolygonBorderLineDataProvider->setWorld(mWorld);
	mPrimitiveLineDataProvider->setWorld(mWorld);
	mPrimitiveBoundsLineDataProvider->setWorld(mWorld);
}

Primitive* RealtimeGeomClaw::findPrimitive(wp::Vector2 const& worldPos) const
{
	return mWorld->findPrimitive(worldPos);
}

vector<std::string> RealtimeGeomClaw::getText() const
{
	auto stats = mWorld->getStats();

	return {
		"Prim clip: " + to_string(stats.primitivesProcessed),
		"Poly gen: " + to_string(stats.polygonsGenerated),
		"Vert gen: " + to_string(stats.verticesGenerated),
		"Clip time: " + Timer::nsToString(stats.clipNs),
		"Trig time: " + Timer::nsToString(stats.triangulateNs)
	};
}

void RealtimeGeomClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	inputMgr->getMousePosition(&mMouseX, &mMouseY);

	if (inputMgr->keyPressed(Key_LeftArrow))
	{
		mMovingLeft = true;
	}
	if (inputMgr->keyPressed(Key_RightArrow))
	{
		mMovingRight = true;
	}
	if (inputMgr->keyPressed(Key_UpArrow))
	{
		mMovingUp = true;
	}
	if (inputMgr->keyPressed(Key_DownArrow))
	{
		mMovingDown = true;
	}
	if (inputMgr->keyReleased(Key_LeftArrow))
	{
		mMovingLeft = false;
	}
	if (inputMgr->keyReleased(Key_RightArrow))
	{
		mMovingRight = false;
	}
	if (inputMgr->keyReleased(Key_UpArrow))
	{
		mMovingUp = false;
	}
	if (inputMgr->keyReleased(Key_DownArrow))
	{
		mMovingDown = false;
	}

	if (inputMgr->keyPressed(Key_1))
	{
		setScene(0);
	}
	if (inputMgr->keyPressed(Key_2))
	{
		setScene(1);
	}

	if (inputMgr->keyPressed(Key_F1))
	{
		mPolygonTriangleRenderer->setVisible(!mPolygonTriangleRenderer->isVisible());
	}
	if (inputMgr->keyPressed(Key_F2))
	{
		mPolygonBorderLineRenderer->setVisible(!mPolygonBorderLineRenderer->isVisible());
	}
	if (inputMgr->keyPressed(Key_F3))
	{
		mPrimitiveLineRenderer->setVisible(!mPrimitiveLineRenderer->isVisible());
	}
	if (inputMgr->keyPressed(Key_F4))
	{
		mPrimitiveBoundsLineRenderer->setVisible(!mPrimitiveBoundsLineRenderer->isVisible());
	}
	if (inputMgr->keyPressed(Key_F11))
	{
		auto params = static_cast<wp::viz::AccelerationGridRenderParams*>(mGridRenderer->getParams().get());
		params->setRender(!params->getRender());

		mGridRenderer->updateRenderParams();
	}

	if (inputMgr->buttonPressed(Mouse_Left))
	{
		auto prim = findPrimitive(getMouseWorldPosition(mMouseX, mMouseY));
		if (prim)
		{
			auto op = prim->getOperation();
			op = (Primitive::Operation)(((int)op + 1) % 4);

			prim->setOperation(op);
			/*
			if (prim->get() == Primitive::FillRule::EvenOdd)
			{
				prim->setFillRule(Primitive::FillRule::NonZero);
			}
			else
			{
				prim->setFillRule(Primitive::FillRule::EvenOdd);
			}
			*/
		}
	}

	// Animate
	float p = mMouseX / (float)mWindowSizeX;
	float q = 1.0f - (mMouseY / (float)mWindowSizeY);
	mWorld->getPrimitive(2)->setT(VertexTransformer::Angle, p);
	//mWorld->getPrimitive(6)->setOriginT(q);
}

void RealtimeGeomClaw::update(float frameTime)
{
	if (mMovingLeft)
	{
	}
	if (mMovingRight)
	{
	}
	if (mMovingUp)
	{
	}
	if (mMovingDown)
	{
	}

	mWorld->newFrame();

	wp::Vector2 viewPosition(0, 0);
	wp::Vector2 viewSize((float)mWindowSizeX, (float)mWindowSizeY);
	wp::BoundingBox viewBounds(viewPosition - viewSize / 2, viewSize);

	if (mPolygonBorderLineRenderer->isVisible())
	{
		mPolygonBorderLineDataProvider->update(viewBounds);
		mPolygonBorderLineRenderer->update(viewBounds, frameTime);
	}

	if (mPrimitiveLineRenderer->isVisible())
	{
		mPrimitiveLineDataProvider->update(viewBounds);
		mPrimitiveLineRenderer->update(viewBounds, frameTime);
	}

	if (mPrimitiveBoundsLineRenderer->isVisible())
	{
		mPrimitiveBoundsLineDataProvider->update(viewBounds);
		mPrimitiveBoundsLineRenderer->update(viewBounds, frameTime);
	}

	if (mPolygonTriangleRenderer->isVisible())
	{
		mPolygonTriangleDataProvider->update(viewBounds);
		mPolygonTriangleRenderer->update(viewBounds, frameTime);
	}
}
