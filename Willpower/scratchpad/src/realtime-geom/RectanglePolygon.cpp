#include "realtime-geom/RectanglePolygon.h"

using namespace std;


RectanglePolygon::RectanglePolygon(Operation operation, FillRule fillType, float xyRatio)
	: Primitive(operation, fillType)
{
	float i1 = 1.0f / xyRatio;

	vector<PrimitiveVertex> vertices = {
		{ wp::Vector2(1.0f, i1), 0 },
		{ wp::Vector2(-1.0f, i1), 0 },
		{ wp::Vector2(-1.0f, -i1), 0 },
		{ wp::Vector2(1.0f, -i1), 0 }
	};

	setVertices(vertices);
}