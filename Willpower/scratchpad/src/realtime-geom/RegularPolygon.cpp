#include "realtime-geom/RegularPolygon.h"

using namespace std;


RegularPolygon::RegularPolygon(Operation operation, FillRule fillType, uint32_t numSides)
	: Primitive(operation, fillType)
{
	// Circle
	if (numSides == ~0u)
	{
		numSides = 64;
	}

	vector<PrimitiveVertex> vertices(numSides);

	for (uint32_t i = 0; i < numSides; ++i)
	{
		float angle = 360.0f * i / (float)numSides;
		vertices[i] = {
			wp::Vector2::UNIT_Y.rotatedClockwiseCopy(angle),
			0
		};
	}

	setVertices(vertices);
}