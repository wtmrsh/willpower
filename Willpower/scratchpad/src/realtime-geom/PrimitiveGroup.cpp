#include "realtime-geom/PrimitiveGroup.h"

using namespace std;


PrimitiveGroup::PrimitiveGroup()
{
}

PrimitiveGroup::PrimitiveGroup(vector<Primitive*> primitives)
{
	mPrimitives = primitives;

	for (auto primitive : mPrimitives)
	{
		primitive->setTransformerParent(getVertexTransformer());
	}
}

void PrimitiveGroup::invalidatePostTransform()
{
	for (auto primitive : mPrimitives)
	{
		primitive->invalidatePostTransform();
	}
}

void PrimitiveGroup::addPrimitive(Primitive* primitive)
{
	mPrimitives.push_back(primitive);

	primitive->setTransformerParent(getVertexTransformer());
}

void PrimitiveGroup::removePrimitive(Primitive* primitive)
{
	auto numPrimitives = (uint32_t)mPrimitives.size();
	for (uint32_t i = 0; i < numPrimitives; ++i)
	{
		if (mPrimitives[i] == primitive)
		{
			mPrimitives[i]->setTransformerParent(nullptr);

			for (uint32_t j = i; j < numPrimitives - 1; ++j)
			{
				mPrimitives[j] = mPrimitives[j + 1];
			}

			return;
		}
	}

	throw exception("Primitive not found in group");
}