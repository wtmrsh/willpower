#include "realtime-geom/Primitive.h"

using namespace std;


Primitive::Primitive(Operation operation, FillRule fillType)
	: mOperation(operation)
	, mFillRule(fillType)
	, mCacheInvalid(true)
{
}

void Primitive::invalidatePostTransform()
{
	invalidate();
}

void Primitive::invalidate()
{
	mCacheInvalid = true;
	calculateBounds();
}

void Primitive::calculateBounds()
{
	wp::Vector2 minExtent, maxExtent;
	auto vertices = generateTransformedVertices(&minExtent, &maxExtent);

	// Inflate very slightly
	minExtent -= 1.0f;
	maxExtent += 1.0f;

	mBounds.setPosition(minExtent);
	mBounds.setSize(maxExtent - minExtent);
}

void Primitive::setOperation(Operation operation)
{
	mOperation = operation;
}

Primitive::Operation Primitive::getOperation() const
{
	return mOperation;
}

void Primitive::setFillRule(FillRule fillRule)
{
	mFillRule = fillRule;
}

Primitive::FillRule Primitive::getFillRule() const
{
	return mFillRule;
}

wp::BoundingBox const& Primitive::getBounds() const
{
	return mBounds;
}

void Primitive::setVertices(vector<PrimitiveVertex> vertices)
{
	mVertices = vertices;
	invalidate();
}

vector<PrimitiveVertex> Primitive::generateTransformedVertices(wp::Vector2* minExtent, wp::Vector2* maxExtent) const
{
	if (minExtent)
	{
		minExtent->set(1e10f, 1e10f);
	}
	if (maxExtent)
	{
		maxExtent->set(-1e10f, -1e10f);
	}

	auto numVertices = getNumVertices();
	vector<PrimitiveVertex> vertices(numVertices);

	for (uint32_t i = 0; i < numVertices; ++i)
	{
		vertices[i].p = transformVertex(mVertices[i].p);
		vertices[i].z = mVertices[i].z;

		if (minExtent)
		{
			if (vertices[i].p.x < minExtent->x)
			{
				minExtent->x = vertices[i].p.x;
			}
			if (vertices[i].p.y < minExtent->y)
			{
				minExtent->y = vertices[i].p.y;
			}
		}
		if (maxExtent)
		{
			if (vertices[i].p.x > maxExtent->x)
			{
				maxExtent->x = vertices[i].p.x;
			}
			if (vertices[i].p.y > maxExtent->y)
			{
				maxExtent->y = vertices[i].p.y;
			}
		}
	}

	return vertices;
}

vector<PrimitiveVertex> Primitive::getVertices() const
{
	if (mCacheInvalid)
	{
		mCachedVertices = generateTransformedVertices();
		mCacheInvalid = false;
	}

	return mCachedVertices;
}

uint32_t Primitive::getNumVertices() const
{
	return (uint32_t)mVertices.size();
}