#define USINGZ

#include <clipper2/clipper.h>

#include "realtime-geom/PathPolygon.h"

using namespace std;


Clipper2Lib::PathsD inflatePaths(Clipper2Lib::PathsD const& paths, float width, PathPolygon::JoinType joinType, PathPolygon::EndType endType)
{
	Clipper2Lib::JoinType jt;
	Clipper2Lib::EndType et;

	switch (joinType)
	{
	case PathPolygon::JoinType::Bevel:
		jt = Clipper2Lib::JoinType::Bevel;
		break;

	case PathPolygon::JoinType::Mitre:
		jt = Clipper2Lib::JoinType::Miter;
		break;

	case PathPolygon::JoinType::Round:
		jt = Clipper2Lib::JoinType::Round;
		break;

	case PathPolygon::JoinType::Square:
		jt = Clipper2Lib::JoinType::Square;
		break;
	}

	switch (endType)
	{
	case PathPolygon::EndType::Butt:
		et = Clipper2Lib::EndType::Butt;
		break;

	case PathPolygon::EndType::Joined:
		et = Clipper2Lib::EndType::Joined;
		break;

	case PathPolygon::EndType::Polygon:
		et = Clipper2Lib::EndType::Polygon;
		break;

	case PathPolygon::EndType::Round:
		et = Clipper2Lib::EndType::Round;
		break;

	case PathPolygon::EndType::Square:
		et = Clipper2Lib::EndType::Square;
		break;
	}

	return Clipper2Lib::InflatePaths(paths, width * 0.5f, jt, et);
}


PathPolygon::PathPolygon(Operation operation, FillRule fillType, vector<wp::Vector2> const& points, float width, JoinType joinType, EndType endType)
	: Primitive(operation, fillType)
{
	Clipper2Lib::PathD polyline;
	
	for (auto const& point : points)
	{
		polyline.push_back({ point.x, point.y, 0 });
	}
	
	auto solution = inflatePaths({ polyline }, width, joinType, endType);

	vector<PrimitiveVertex> vertices;

	for (auto const& point : solution[0])
	{
		vertices.push_back({ { (float)point.x, (float)point.y }, point.z });
	}

	setVertices(vertices);
}