#include <iterator>

#include <mapbox/earcut.hpp>

#include <willpower/common/BezierSpline.h>
#include <willpower/common/MathsUtils.h>

#include "realtime-geom/World.h"
#include "realtime-geom/Clipper2Clipper.h"
#include "realtime-geom/Timer.h"

using namespace std;


World::World()
	: mClipNs(-1)
	, mTriangulateNs(-1)
	, mNumPrimitivesClipped(0)
	, mNumPolygonsGenerated(0)
	, mNumVerticesGenerated(0)
{
	vector<pair<float, wp::Vector2>> offsets = {
		{ 0.0f, { 0.0f, 0.0f } } 
	};

	vector<pair<float, float>> scales = {
		{ 0.0f, 1.0f }
	};

	vector<pair<float, float>> angles = {
		{ 0.0f, 0.0f }
	};

	vector<pair<float, float>> orbitAngles = {
		{ 0.0f, 0.0f }
	};

	vector<pair<float, float>> orbitDistances = {
		{ 0.0f, 0.0f }
	};

	// Set up test group
	mGroup.setOrigin({ 400, 0 });
	mGroup.setOffsets(offsets);
	mGroup.setScales(scales);
	mGroup.setAngles(angles);
	mGroup.setOrbitAngles(orbitAngles);
	mGroup.setOrbitDistances(orbitDistances);
	mGroup.setFollowOrbitAngle(false);
}

World::~World()
{
	for (auto primitive : mPrimitives)
	{
		delete primitive;
	}
}

Stats World::getStats() const
{
	return {
		mNumPrimitivesClipped,
		mNumPolygonsGenerated,
		mNumVerticesGenerated,
		mClipNs,
		mTriangulateNs
	};
}

uint32_t World::addPrimitive(Primitive* primitive, bool addToGroup)
{
	auto index = (uint32_t)mPrimitives.size();

	mPrimitives.push_back(primitive);

	if (addToGroup)
	{
		mGroup.addPrimitive(primitive);
	}

	return index;
}

Primitive* World::getPrimitive(uint32_t index)
{
	return mPrimitives[index];
}

vector<Primitive*> const& World::getPrimitives() const
{
	return mPrimitives;
}

Primitive* World::findPrimitive(wp::Vector2 const& worldPos) const
{
	for (auto primitive : mPrimitives)
	{
		auto const& bounds = primitive->getBounds();
		if (bounds.pointInside(worldPos))
		{
			return primitive;
		}
	}

	return nullptr;
}

void World::newFrame()
{
	mClipNs = 0;
	mTriangulateNs = 0;
	mNumPrimitivesClipped = 0;
	mNumPolygonsGenerated = 0;
	mNumVerticesGenerated = 0;
}

vector<ClippedPolygon> World::calculatePolygons(wp::BoundingBox const& extents, set<uint32_t> const& primitiveIndices) const
{
	if (primitiveIndices.empty())
	{
		return {};
	}

	vector<Primitive*> primitives;
	for (auto index : primitiveIndices)
	{
		primitives.push_back(mPrimitives[index]);
	}

	return Clipper2Clipper().clip(primitives, extents);
}

vector<wp::Vector2> World::triangulate(wp::BoundingBox const& extents, std::set<uint32_t> const& primitiveIndices) const
{
	using Point = array<float, 2>;
	
	vector<wp::Vector2> finalTriangleData;

	mNumPrimitivesClipped = (uint32_t)primitiveIndices.size();

	Timer timer;

	auto clippedPolygons = calculatePolygons(extents, primitiveIndices);

	mClipNs = timer.elapsedNanoseconds();
	mNumPolygonsGenerated = (uint32_t)clippedPolygons.size();
	mNumVerticesGenerated = 0;
	for (auto const& poly : clippedPolygons)
	{
		mNumVerticesGenerated += (uint32_t)poly.vertices.size();
	}
	
	vector<vector<Point>> triangulations;
	vector<wp::Vector2> triVertList;
	
	for (auto const& clippedPolygon : clippedPolygons)
	{
		auto const& polyVerts = clippedPolygon.vertices;

		// Read in points to required format for triangulation
		vector<Point> points(polyVerts.size());

		for (uint32_t i = 0; i < polyVerts.size(); ++i)
		{
			points[i] = { polyVerts[i].x, polyVerts[i].y };
		}
		
		if (!clippedPolygon.isHole)
		{
			// New polygon: time to calculate the current one
			if (!triangulations.empty())
			{
				auto triangleIndices = mapbox::earcut<uint32_t>(triangulations);

				for (auto triIndex : triangleIndices)
				{
					finalTriangleData.push_back(triVertList[triIndex]);
				}

				triVertList.clear();
				triangulations.clear();
			}
		}

		triangulations.push_back(points);

		// Store vertices in contiguous list for later indexing
		copy(polyVerts.begin(), polyVerts.end(), back_inserter(triVertList));
	}

	// Final triangulation
	auto triangleIndices = mapbox::earcut<uint32_t>(triangulations);

	mTriangulateNs = timer.elapsedNanoseconds() - mClipNs;

	for (auto triIndex : triangleIndices)
	{
		finalTriangleData.push_back(triVertList[triIndex]);
	}

	auto totalTime = timer.elapsedNanoseconds();

	return finalTriangleData;
}