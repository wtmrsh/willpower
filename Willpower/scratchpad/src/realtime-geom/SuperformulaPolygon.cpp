#include <cmath>

#include <willpower/common/Globals.h>

#include "realtime-geom/SuperformulaPolygon.h"

using namespace std;


SuperformulaPolygon::SuperformulaPolygon(Operation operation, FillRule fillType, float values[6])
	: Primitive(operation, fillType)
{
	for (int i = 0; i < 6; ++i)
	{
		mValues[i] = values[i];
	}

	vector<PrimitiveVertex> vertices;

	for (float a = 0.0f; a < WP_TWOPI; a += 0.01f)
	{
		vertices.push_back({ calculate(a), 0 });
	}

	setVertices(vertices);
}

float SuperformulaPolygon::r(float theta) const
{
	return (float)pow(
		pow(abs(cos(mValues[2] * theta / 4.0) / mValues[0]), mValues[4]) +
		pow(abs(sin(mValues[2] * theta / 4.0) / mValues[1]), mValues[5]), -1.0 / mValues[3]);
}

wp::Vector2 SuperformulaPolygon::calculate(float theta) const
{
	float rad = r(theta);

	float x = rad * cosf(theta);
	float y = rad * sinf(theta);

	return { x, y };
}