#include "Helpers.h"

using namespace wp;
using namespace wp::geometry;

Mesh* createQuad(float sizeX, float sizeY)
{
    auto mesh = new wp::geometry::Mesh();
    //this->mesh->createAccelerationGrids(-this->sizeX / 2, -this->sizeY / 2, this->sizeX, this->sizeY, this->sizeX / 100, this->sizeX / 100);

    // Create a test polygon
    wp::geometry::Vertex vertex(-100, -100);
    uint32_t v0 = mesh->addVertex(vertex, -1);

    vertex = wp::geometry::Vertex(100, -100);
	uint32_t v1 = mesh->addVertex(vertex, -1);

    vertex = wp::geometry::Vertex(100, 100);
	uint32_t v2 = mesh->addVertex(vertex, -1);

    vertex = wp::geometry::Vertex(-100, 100);
	uint32_t v3 = mesh->addVertex(vertex, -1);

    wp::geometry::Edge edge;

    edge.setVertices(v0, v1);
	uint32_t e0 = mesh->addEdge(edge, -1);

    edge.setVertices(v1, v2);
	uint32_t e1 = mesh->addEdge(edge, -1);

    edge.setVertices(v2, v3);
	uint32_t e2 = mesh->addEdge(edge, -1);

    edge.setVertices(v3, v0);
	uint32_t e3 = mesh->addEdge(edge, -1);

	wp::geometry::Polygon polygon({
		v0, v1, e0,
		v1, v2, e1,
		v2, v3, e2,
		v3, v0, e3
	});

	mesh->addPolygon(polygon, -1);
	return mesh;
}
