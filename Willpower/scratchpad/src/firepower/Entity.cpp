#include "firepower/Entity.h"

using namespace wp;

Entity::Entity(SplinePath const* path, float distance, float speed)
	: mPath(path)
	, mDistance(distance)
	, mSpeed(speed)
	, mId((uint32_t)-1)
{
	mPosition = mPath->getPosition(mDistance);
	mDirection = mPath->getDirection(mDistance);
}

Vector2 const& Entity::getPosition() const
{
	return mPosition;
}

Vector2 const& Entity::getDirection() const
{
	return mDirection;
}

wp::BoundingBox Entity::getBounds() const
{
	return BoundingBox(mPosition.x - 19, mPosition.y - 19, 38, 38);
}

void Entity::setId(uint32_t id)
{
	mId = id;
}

uint32_t Entity::getId() const
{
	return mId;
}

void Entity::update(float frameTime)
{
	mDistance += mSpeed * frameTime;
	
	float pathLength = mPath->getLength();
	while (mDistance > pathLength)
	{
		mDistance -= pathLength;
	}

	mPosition = mPath->getPosition(mDistance);
	mDirection = mPath->getDirection(mDistance).normalisedCopy();
}