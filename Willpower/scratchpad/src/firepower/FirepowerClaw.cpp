#include <algorithm>

#include <utils/StringUtils.h>

#include "willpower/application/resourcesystem/ResourceManager.h"

#include "willpower/common/MathsUtils.h"
#include "willpower/common/CubicBSplineLooping.h"
#include "willpower/common/CentripetalCatmullRomSplineLooping.h"
#include "willpower/common/BezierSpline.h"

#include "firepower/FirepowerClaw.h"

#include "sdl/WindowSDL.h"

using namespace std;
using namespace wp;
using namespace utils;

extern wp::Logger* gLogger;
extern Window* gWindow;
extern application::resourcesystem::ResourceManager* gResMgr;

bool dummyUpdateBullet(firepower::Bullet* bullet, float frameTime)
{
	bullet->setSpeed(200);
	bullet->setTurnSpeed(45.0f);
	return true;
}

bool dummyBulletHitEdge(firepower::Bullet* bullet, Vector2 const& oldPosition, firepower::Edge const& edge)
{
	//edge.normal
	bullet->setAngle(bullet->getAngle() + 180);
	return true;
}

FirepowerClaw::FirepowerClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "NavMesh", renderSystem)
	, mRenderSystem(renderSystem)
	, mBulletMgr(nullptr)
	//, mBombMgr(nullptr)
	, mBeamMgr(nullptr)
	, mObjectMgr(nullptr)
	, mMesh(nullptr)
	, mMeshCollisionMgr(nullptr)
	, mMeshRenderer(nullptr)
	, mSplineRenderer(nullptr)
	, mGridRenderer(nullptr)
	, mBoundsRenderer(nullptr)
	, mEntityMgr(nullptr)
//	, mTriBatch(nullptr)
	, mMouseX(0)
	, mMouseY(0)
{
}

FirepowerClaw::~FirepowerClaw()
{
	delete mBulletMgr;
	
	//delete mBombMgr;
	
	delete mBeamMgr;
	
	delete mObjectMgr;

	delete mMeshRenderer;
	delete mMesh;
	delete mMeshCollisionMgr;

	for (uint32_t i = 0; i < mSplines.size(); ++i)
	{
		delete mSplines[i];
	}

	delete mSplineRenderer;
	delete mGridRenderer;
	delete mBoundsRenderer;
	delete mEntityMgr;
	//delete mTriBatch;
}

vector<string> FirepowerClaw::getText() const
{
#ifdef WP_PROFILE_BUILD
	//string bulletTime = StringUtils::toString(mBulletMgr->getMicroseconds());
	//string bombTime = StringUtils::toString(mBombMgr->getMicroseconds());
#endif
	string bulletCount = StringUtils::toString(mBulletMgr->getNumBullets());
	string bulletCapacity = StringUtils::toString(mBulletMgr->getCapacity());
	
	float bulletRenderUsage = mBulletMgr->getRenderUsage() / (float)mBulletMgr->getRenderCapacity();
	string bulletRenderPct = StringUtils::toString((int)(bulletRenderUsage * 100));

	//string bombCount = StringUtils::toString(mBombMgr->getNumBombs());
	//string arcCount = StringUtils::toString(mBombMgr->getNumArcs());
	//string bombCapacity = StringUtils::toString(mBombMgr->getCapacity());

	//float bombRenderUsage = mBombMgr->getRenderUsage() / (float)mBombMgr->getRenderCapacity();
	//string bombRenderPct = StringUtils::toString((int)(bombRenderUsage * 100));

	return {
		StringUtils::toString(mMouseX) + "," + StringUtils::toString(mMouseY),
		"Bullet count: " + bulletCount + "/" + bulletCapacity,
		"Bullet render usage: " + bulletRenderPct + "%",
		//"Bomb/arc count: " + bombCount + "/" + arcCount,
		//"Bomb render usage: " + bombRenderPct + "%",
#ifdef WP_PROFILE_BUILD
		//"Bullet update: " + bulletTime + "us",
		//"Bomb update: " + bombTime + "us"
#endif
	};
}

uint32_t FirepowerClaw::createPolygon(float x, float y, vector<Vector2> const& vertexPositions)
{
	vector<uint32_t> vertexIndices;
	for (auto const& vertexPos: vertexPositions)
	{
		vertexIndices.push_back(mMesh->addVertex(geometry::Vertex(vertexPos.x + x, vertexPos.y + y)));
	}

	vector<uint32_t> edgeIndices;
	for (uint32_t i = 0; i < vertexIndices.size(); ++i)
	{
		uint32_t v0 = vertexIndices[i];
		uint32_t v1 = vertexIndices[(i + 1) % vertexIndices.size()];

		edgeIndices.push_back(mMesh->addEdge(geometry::Edge(v0, v1)));
	}

	vector<uint32_t> polygonData;
	for (uint32_t edgeIndex: edgeIndices)
	{
		auto const& edge = mMesh->getEdge(edgeIndex);
		polygonData.push_back(edge.getFirstVertex());
		polygonData.push_back(edge.getSecondVertex());
		polygonData.push_back(edgeIndex);
	}

	geometry::Polygon polygon(polygonData);
	return mMesh->addPolygon(polygon);
}

void FirepowerClaw::setup(mpp::ResourceManager* resourceMgr)
{
	//
	// Window extents
	//
	Vector2 windowOrigin = Vector2::ZERO;
	Vector2 windowSize((float)gWindow->getWidth(), (float)gWindow->getHeight());

	//
	// Create mesh
	//
	mMesh = new geometry::Mesh();

	// Main polygon
	uint32_t p0 = createPolygon(0, 0, {
		Vector2(200, 50),
		Vector2(1150, 50),
		Vector2(1150, 200),
		Vector2(1350, 200),
		Vector2(1350, 850),
		Vector2(850, 850),
		Vector2(850, 800),
		Vector2(550, 800),
		Vector2(550, 850),
		Vector2(50, 850),
		Vector2(50, 400),
		Vector2(200, 250)
		});
	
	// Holes
	uint32_t h0 = createPolygon(0, 0, {
		Vector2(400, 200),
		Vector2(400, 350),
		Vector2(800, 350),
		Vector2(800, 450),
		Vector2(850, 450),
		Vector2(950, 350),
		Vector2(950, 200)
		});

	mMesh->addHoleToPolygon(p0, h0);

	uint32_t h1 = createPolygon(0, 0, {
		Vector2(200, 600),
		Vector2(200, 650),
		Vector2(250, 700),
		Vector2(300, 700),
		Vector2(300, 600)
		});

	mMesh->addHoleToPolygon(p0, h1);

	uint32_t h2 = createPolygon(0, 0, {
		Vector2(1050, 550),
		Vector2(1050, 650),
		Vector2(1150, 650),
		Vector2(1150, 550)
		});

	mMesh->addHoleToPolygon(p0, h2);

	//mMesh->chamferVertex(2, 100);
	//mMesh->chamferVertex(4, 200);
	//mMesh->chamferVertex(26, 50);

	// Set mesh as collidable
	mMeshCollisionMgr = new firepower::MeshCollisionManager(windowOrigin, windowSize, 128);
	mMeshCollisionMgr->addMesh(mMesh);

	//
	// Bullet specification
	//
	mpp::QuadBatchSpecification<float, float, uint8_t, 4> bulletSpec(
		mRenderSystem,
		false,
		32,
		true, // Same size dimensions
		32, // max width
		32, // max height
		false);

	mBulletMgr = new firepower::BulletManager<float, float, uint8_t, 4>(
		"Bullets", 
		bulletSpec, 
		mMeshCollisionMgr, 
		gResMgr->getResource("Blasts"), 
		256,
		windowOrigin,
		windowOrigin + windowSize);

	//
	// Bomb specification
	//
	/*
	mBombMgr = new firepower::BombManager<float, float, float, 4>(
		"Bombs",
		mMeshCollisionMgr,
		16,
		windowOrigin,
		windowOrigin + windowSize);
		*/
	//
	// Beam specification
	//
	mBeamMgr = new firepower::BeamManager<float, float, float, 4>(
		"Beams", 
		mMeshCollisionMgr, 
		gResMgr,
		16, 
		windowOrigin, 
		windowOrigin + windowSize);

	//
	// Create object managers
	//

	mObjectMgr = new firepower::ObjectCollisionManager(windowOrigin, windowSize, 80, 256, 64);

	// Mesh render params
	mMeshRenderParams.setEdgeColour(mpp::Colour::Yellow);
	mMeshRenderParams.setRenderEdges(true);
	//mMeshRenderParams.setRenderAccelerationGrids(true);

	// Create mesh renderer
	mMeshRenderer = new viz::FirepowerMeshCollisionManagerRenderer_old("Collision", { mMeshCollisionMgr }, &mMeshRenderParams);

	//
	// Create splines and renderer
	//
	//wp::SplinePath* spline = new BezierSpline({
	wp::SplinePath* spline = new CubicBSplineLooping({
		Vector2(400, 100),
		Vector2(1000, 100),
		Vector2(1100, 200),
		Vector2(1100, 450),
		Vector2(700, 650),
		Vector2(500, 650),
		Vector2(400, 800),
		Vector2(250, 800),
		Vector2(100, 800),
		Vector2(100, 600),
		Vector2(100, 450),
		Vector2(300, 400),
		Vector2(300, 200)
		});

	mSplines.push_back(spline);
	
	/*
	mSplineRenderParams.setSplineColour(mpp::Colour::Red);

	mSplineRenderer = new viz::SplineRenderer("Spline", mSplines, &mSplineRenderParams);
	//mSplineRenderer = new viz::DynamicSplineRenderer("Spline", mSplines, &mSplineRenderParams, windowOrigin, windowSize);
	*/

	// Render grid
	mGridRenderParams.setEdgeColour(mpp::Colour::Blue);
	mGridRenderer = new viz::AccelerationGridRenderer_old<DynamicAccelerationGrid>("Grid", mObjectMgr->getGrid(), &mGridRenderParams);
	
	/*
	// Bounding boxes
	mBoundsRenderer = new viz::BoundingAreaRenderer(&mBoundsRenderParams, windowOrigin, windowSize);

	mBoundsRenderer->addItem(spline->getBounds());
	*/

	//
	// Create entities
	//
	mEntityMgr = new EntityManager(256, "Entities");
	/*
	for (int i = 0; i < 1; ++i)
	{
		Entity* e = new Entity(spline, i * 200, 40.0f);
		BoundingBox bb = e->getBounds();
		uint32_t eId = mObjectMgr->addAABB(bb.getMinExtent(), bb.getMaxExtent());
		e->setId(eId);

		mEntityMgr->addEntity(e);
	}
	*/
}

void FirepowerClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	// Emit bullets at cursor position
	/*
	if (inputMgr->keyPressed(Key_Space))
	{
		int mx, my;
		inputMgr->getMousePosition(&mx, &my);
		Vector2 mouseWorldPos((float)mx, (float)(gWindow->getHeight() - my));

		int bulletType = mBulletMgr->getTypeName("RedBall");
		for (int i = 0; i < 50; ++i)
		{
			float angle = (rand() / 32768.0f) * 360;
			mBulletMgr->addBullet(bulletType, mouseWorldPos, angle, 0.0f, 1e6f, dummyUpdateBullet, dummyBulletHitEdge);
		}
	}
	*/

	inputMgr->getMousePosition(&mMouseX, &mMouseY);
	mMouseY = gWindow->getHeight() - mMouseY;

	Vector2 mousePos((float)mMouseX, (float)mMouseY);

	if (inputMgr->buttonPressed(Mouse_Left))
	{
		int bulletType = mBulletMgr->getTypeName("RedBall");
		for (int i = 0; i < 50; ++i)
		{
			float angle = (rand() / 32768.0f) * 360;
			mBulletMgr->addBullet(bulletType, mousePos, angle, 0.0f, 1e6f, dummyUpdateBullet, dummyBulletHitEdge);
		}
	}

	
	/*
	if (inputMgr->buttonPressed(Mouse_Right))
	{
		//mBombMgr->addBomb(0, Vector2((float)mMouseX, (float)mMouseY), 32, 16, 250.0f);

		vector<firepower::Bomb::Arc> arcs =
		{
			//make_pair(330.0f, 30.0f),
			//make_pair(60.0f, 120.0f),
			//make_pair(150.0f, 210.0f),
			//make_pair(240.0f, 300.0f)
		};

		mBombMgr->addBomb(0, mousePos, 32, 20, true,
			firepower::Bomb::LifeOptions(),
			firepower::ControlSetting(),
			firepower::ControlSetting(300.0f),
			firepower::ControlSetting(),
			arcs);
	}
	*/	

	if (inputMgr->buttonPressed(Mouse_Right))
	{
		mBeamMgr->addBeam(0, mousePos, 0.0f, 64, 200.0f, firepower::ControlSetting(-200.0f), firepower::ControlSetting(1000.0f));
	}
}

void FirepowerClaw::update(float frameTime)
{
	if (mBeamMgr->getNumBeams() > 0)
	{
		Vector2 mousePos((float)mMouseX, (float)mMouseY);

		auto& beam = mBeamMgr->getBeam(0);
		
		auto const& beamPos = beam.getPosition();
		beam.setAngle(225 - beamPos.anticlockwiseAngleTo(beamPos - mousePos));
	}
		
	mObjectMgr->clearFrameCache();

//	mBombMgr->update(frameTime, mObjectMgr);
	mBeamMgr->update(frameTime, mObjectMgr);
	mBulletMgr->update(frameTime, nullptr);

	//mEntityMgr->update(frameTime, mObjectMgr);
}
