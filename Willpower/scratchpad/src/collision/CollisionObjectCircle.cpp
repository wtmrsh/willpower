#include "collision/CollisionObjectCircle.h"

using namespace std;
using namespace wp;

CollisionObjectCircle::CollisionObjectCircle(string const& name, mpp::Colour const& colour)
	: CollisionObject(CollisionObjectType::Circle, name)
	, mColour(colour)
{
}

void CollisionObjectCircle::setPosition(Vector2 const& pos)
{
	CollisionObject::setPosition(pos);
	mCircle.setPosition(getPosition());
}

void CollisionObjectCircle::move(Vector2 const& distance)
{
	CollisionObject::move(distance);
	mCircle.setPosition(getPosition());
}

void CollisionObjectCircle::setRadius(float radius)
{
	mCircle.setRadius(radius);
}

float CollisionObjectCircle::getRadius() const
{
	return mCircle.getRadius();
}

void CollisionObjectCircle::scale(float amt)
{
	mCircle.setRadius(mCircle.getRadius() * amt);
}

bool CollisionObjectCircle::pointInObject(float x, float y) const
{
	return mCircle.pointInside(x, y);
}

void CollisionObjectCircle::update(float frameTime)
{

}

void CollisionObjectCircle::renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	Vector2 pos = mCircle.getPosition();
	float radius = mCircle.getRadius();

	if (isColliding())
	{
//		renderSystem->renderCircle2d(pos.x, pos.y, radius, mpp::Colour(1.0f, 0.5f, 0.0f));
	}
	else if (isHovered() || isSelected())
	{
//		renderSystem->renderCircle2d(pos.x, pos.y, radius, mpp::Colour::White);
	}
}

void CollisionObjectCircle::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	Vector2 pos = mCircle.getPosition();
	float radius = mCircle.getRadius();

	if (isColliding() || isHovered() || isSelected())
	{
		radius -= 5;
	}

//	renderSystem->renderCircle2d(pos.x, pos.y, radius, mColour);
}
