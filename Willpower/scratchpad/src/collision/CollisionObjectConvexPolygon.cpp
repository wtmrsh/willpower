#include "willpower/common/MathsUtils.h"

#include "collision/CollisionObjectConvexPolygon.h"

using namespace std;
using namespace wp;

CollisionObjectConvexPolygon::CollisionObjectConvexPolygon(string const& name, mpp::Colour const& colour)
	: CollisionObject(CollisionObjectType::ConvexPolygon, name)
	, mColour(colour)
{
}

void CollisionObjectConvexPolygon::addVertex(float x, float y)
{
	mVertices.push_back(Vector2(x, y));

	mCentre = Vector2::ZERO;
	for (auto vertex: mVertices)
	{
		mCentre += vertex;
	}

	mCentre /= mVertices.size();
}

void CollisionObjectConvexPolygon::scale(float amt)
{
	for (auto& vertex: mVertices)
	{
		vertex *= amt;
	}
}

void CollisionObjectConvexPolygon::rotate(float angle)
{
	MathsUtils::rotatePoints(mVertices, angle);
}

bool CollisionObjectConvexPolygon::pointInObject(float x, float y) const
{
	auto pos = getPosition();
	Vector2 v0 = pos + mVertices[0];

	for (size_t i = 1; i < mVertices.size() - 1; ++i)
	{
		if (MathsUtils::pointInTriangle(Vector2(x, y), v0, pos + mVertices[i], pos + mVertices[i + 1]))
		{
			return true;
		}
	}

	return false;
}

bool CollisionObjectConvexPolygon::boxIntersects(Vector2 const& boxMin, Vector2 const& boxMax) const
{
	auto pos = getPosition();
	Vector2 v0 = pos + mVertices[0];

	for (size_t i = 1; i < mVertices.size() - 1; ++i)
	{
		if (MathsUtils::boxIntersectsTriangle(boxMin, boxMax, v0, pos + mVertices[i], pos + mVertices[i + 1]))
		{
			return true;
		}
	}

	return false;
}

bool CollisionObjectConvexPolygon::circleIntersects(Vector2 const& circleCentre, float circleRadius) const
{
	auto pos = getPosition();
	Vector2 v0 = pos + mVertices[0];

	for (size_t i = 1; i < mVertices.size() - 1; ++i)
	{
		if (MathsUtils::circleIntersectsTriangle(circleCentre, circleRadius, v0, pos + mVertices[i], pos + mVertices[i + 1]))
		{
			return true;
		}
	}

	return false;
}

bool CollisionObjectConvexPolygon::triangleIntersects(Vector2 const& v0, Vector2 const& v1, Vector2 const& v2) const
{
	vector<Vector2> thisVertices = mVertices;
	auto thisPos = getPosition();

	for (auto& vertex: thisVertices)
	{
		vertex += thisPos;
	}

	return MathsUtils::triangleIntersectsConvexPolygon(v0, v1, v2, thisVertices);
}

bool CollisionObjectConvexPolygon::convexPolygonIntersects(CollisionObjectConvexPolygon const* other) const
{
	vector<Vector2> thisVertices = mVertices;
	vector<Vector2> otherVertices = other->mVertices;

	auto thisPos = getPosition();
	auto otherPos = other->getPosition();

	for (auto& vertex: thisVertices)
	{
		vertex += thisPos;
	}
	for (auto& vertex: otherVertices)
	{
		vertex += otherPos;
	}

	return MathsUtils::convexPolygonIntersectsConvexPolygon(thisVertices, otherVertices);
}

void CollisionObjectConvexPolygon::update(float frameTime)
{

}

void CollisionObjectConvexPolygon::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr, mpp::Colour const& colour, float inset)
{
	auto pos = getPosition();
	float v1dist, v2dist;

	Vector2 v0 = pos + mCentre, v1, v2;

	for (size_t i = 0; i < mVertices.size() - 1; ++i)
	{
		// Scale triangles
		v1dist = mCentre.distanceTo(mVertices[i]) - inset;
		v1 = v0 + (mVertices[i] - mCentre).normalisedCopy() * v1dist;

		v2dist = mCentre.distanceTo(mVertices[i + 1]) - inset;
		v2 = v0 + (mVertices[i + 1] - mCentre).normalisedCopy() * v2dist;

		//renderSystem->renderTriangle2d(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y, colour);
	}

	// Scale triangles
	v1dist = mCentre.distanceTo(mVertices.back()) - inset;
	v1 = v0 + (mVertices.back() - mCentre).normalisedCopy() * v1dist;

	v2dist = mCentre.distanceTo(mVertices.front()) - inset;
	v2 = v0 + (mVertices.front() - mCentre).normalisedCopy() * v2dist;

	//renderSystem->renderTriangle2d(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y, colour);
}

void CollisionObjectConvexPolygon::renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	if (isColliding())
	{
		render(renderSystem, resourceMgr, mpp::Colour(1.0f, 0.5f, 0.0f), 0);
	}
	else if (isHovered() || isSelected())
	{
		render(renderSystem, resourceMgr, mpp::Colour::White, 0);
	}
}

void CollisionObjectConvexPolygon::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	float inset = 0;
	if (isColliding() || isHovered() || isSelected())
	{ 
		inset = 5;
	}

	render(renderSystem, resourceMgr, mColour, inset);
}
