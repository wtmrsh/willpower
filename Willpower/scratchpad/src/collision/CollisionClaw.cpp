#include <algorithm>

#include "willpower/common/MathsUtils.h"

#include "collision/CollisionClaw.h"
#include "collision/CollisionObjectPoint.h"
#include "collision/CollisionObjectAABB.h"
#include "collision/CollisionObjectCircle.h"
#include "collision/CollisionObjectConvexPolygon.h"
#include "collision/CollisionObjectTriangle.h"

using namespace std;
using namespace wp;

CollisionClaw::CollisionClaw(Logger* logger, mpp::RenderSystem* renderSystem)
	: Claw(logger, "Collision tests", renderSystem)
	, mDragType(DragType::None)
	, mHoveredObject(nullptr)
	, mSelectedObject(nullptr)
{
}

CollisionClaw::~CollisionClaw()
{
	removeAllObjects();
}

void CollisionClaw::setup(mpp::ResourceManager* resourceMgr)
{
	// Points
	auto point = (CollisionObjectPoint*)addObject(CollisionObjectType::Point, "Vertex1", mpp::Colour::Cyan);
	point->setPosition(Vector2(340, 480));

	// Lines
	auto lineVert1 = (CollisionObjectPoint*)addObject(CollisionObjectType::Point, "LineVertex1", mpp::Colour::Blue);
	lineVert1->setPosition(Vector2(360, 500));

	auto lineVert2 = (CollisionObjectPoint*)addObject(CollisionObjectType::Point, "LineVertex2", mpp::Colour::Blue);
	lineVert2->setPosition(Vector2(400, 480));

	IntersectionLine intersectionLine;
	intersectionLine.start = lineVert1;
	intersectionLine.end = lineVert2;
	mIntersectionLines.push_back(intersectionLine);

	auto lineVert3 = (CollisionObjectPoint*)addObject(CollisionObjectType::Point, "LineVertex3", mpp::Colour::Green);
	lineVert3->setPosition(Vector2(260, 500));

	auto lineVert4 = (CollisionObjectPoint*)addObject(CollisionObjectType::Point, "LineVertex4", mpp::Colour::Green);
	lineVert4->setPosition(Vector2(300, 480));

	intersectionLine.start = lineVert3;
	intersectionLine.end = lineVert4;
	mIntersectionLines.push_back(intersectionLine);


	// Boxes
	auto box = (CollisionObjectAABB*)addObject(CollisionObjectType::AABB, "GreenBox", mpp::Colour::Green);
	box->setPosition(Vector2(200, 200));
	box->setSize(Vector2(150, 150));

	box = (CollisionObjectAABB*)addObject(CollisionObjectType::AABB, "BlueBox", mpp::Colour::Blue);
	box->setPosition(Vector2(600, 400));
	box->setSize(Vector2(150, 150));

	// Circle
	auto circle = (CollisionObjectCircle*)addObject(CollisionObjectType::Circle, "GreenCircle", mpp::Colour::Green);
	circle->setPosition(Vector2(200, 400));
	circle->setRadius(45);

	circle = (CollisionObjectCircle*)addObject(CollisionObjectType::Circle, "BlueCircle", mpp::Colour::Blue);
	circle->setPosition(Vector2(500, 100));
	circle->setRadius(25);

	// ConvexPolygons
	auto polygon = (CollisionObjectConvexPolygon*)addObject(CollisionObjectType::ConvexPolygon, "YellowPolygon", mpp::Colour::Yellow);
	polygon->addVertex(0, -50);
	polygon->addVertex(40, 0);
	polygon->addVertex(20, 50);
	polygon->addVertex(-35, 20);
	polygon->addVertex(-30, -25);
	polygon->setPosition(Vector2(80, 100));

	polygon = (CollisionObjectConvexPolygon*)addObject(CollisionObjectType::ConvexPolygon, "GreyPolygon", mpp::Colour::Grey50);
	polygon->addVertex(0, -60);
	polygon->addVertex(40, 20);
	polygon->addVertex(40, 50);
	polygon->addVertex(-50, 20);
	polygon->addVertex(-45, -25);
	polygon->setPosition(Vector2(450, 380));

	// Triangles
	auto triangle = (CollisionObjectTriangle*)addObject(CollisionObjectType::Triangle, "BlueTriangle", mpp::Colour::Blue);
	triangle->setVertices(200, 100, 350, 100, 275, 175);

	triangle = (CollisionObjectTriangle*)addObject(CollisionObjectType::Triangle, "BlueTriangle", mpp::Colour::Blue);
	triangle->setVertices(630, 250, 750, 150, 600, 100);

	// Sort objects by type order for rendering
	sort(mObjects.begin(), mObjects.end(), [](CollisionObject const* a, CollisionObject const* b) 
	{
		return a->getType() > b->getType();
	});

	//
	// Collision functions
	//

	// Point-AABB collision function
	addCollisionFunction(CollisionObjectType::Point, CollisionObjectType::AABB, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 obj2Min, obj2Max;
		((CollisionObjectAABB*)(c2))->getExtents(obj2Min, obj2Max);
		return MathsUtils::pointInBox(c1->getPosition(), obj2Min, obj2Max);
	});

	// Point-Circle collision function
	addCollisionFunction(CollisionObjectType::Point, CollisionObjectType::Circle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		float circleRadius = ((CollisionObjectCircle*)(c2))->getRadius();
		return MathsUtils::pointInCircle(c1->getPosition(), c2->getPosition(), circleRadius);
	});

	// Point-ConvexPolygon collision function
	addCollisionFunction(CollisionObjectType::Point, CollisionObjectType::ConvexPolygon, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		auto pos = c1->getPosition();
		return ((CollisionObjectConvexPolygon*)(c2))->pointInObject(pos.x, pos.y);
	});

	// Point-Triangle collision function
	addCollisionFunction(CollisionObjectType::Point, CollisionObjectType::Triangle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		auto pos1 = c1->getPosition();
		auto pos2 = c2->getPosition();
		auto vertices = ((CollisionObjectTriangle*)(c2))->getVertices();
		return MathsUtils::pointInTriangle(pos1, pos2 + vertices[0], pos2 + vertices[1], pos2 + vertices[2]);
	});

	// AABB-AABB collision function
	addCollisionFunction(CollisionObjectType::AABB, CollisionObjectType::AABB, [](CollisionObject const* c1, CollisionObject const* c2)
	{ 
		Vector2 obj1Min, obj1Max, obj2Min, obj2Max;
		((CollisionObjectAABB*)(c1))->getExtents(obj1Min, obj1Max);
		((CollisionObjectAABB*)(c2))->getExtents(obj2Min, obj2Max);
		return MathsUtils::boxIntersectsBox(obj1Min, obj1Max, obj2Min, obj2Max);
	});

	// Circle-circle collision function
	addCollisionFunction(CollisionObjectType::Circle, CollisionObjectType::Circle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		float obj1Radius = ((CollisionObjectCircle*)(c1))->getRadius();
		float obj2Radius = ((CollisionObjectCircle*)(c2))->getRadius();
		return MathsUtils::circleIntersectsCircle(c1->getPosition(), obj1Radius, c2->getPosition(), obj2Radius);
	});

	// AABB-circle collision function
	addCollisionFunction(CollisionObjectType::AABB, CollisionObjectType::Circle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 obj1Min, obj1Max;
		((CollisionObjectAABB*)(c1))->getExtents(obj1Min, obj1Max);
		float obj2Radius = ((CollisionObjectCircle*)(c2))->getRadius();
		return MathsUtils::boxIntersectsCircle(obj1Min, obj1Max, c2->getPosition(), obj2Radius);
	});

	// ConvexPolygon-ConvexPolygon collision function
	addCollisionFunction(CollisionObjectType::ConvexPolygon, CollisionObjectType::ConvexPolygon, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		return ((CollisionObjectConvexPolygon*)(c1))->convexPolygonIntersects((CollisionObjectConvexPolygon*)c2	);
	});

	// ConvexPolygon-AABB collision function
	addCollisionFunction(CollisionObjectType::ConvexPolygon, CollisionObjectType::AABB, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 obj2Min, obj2Max;
		((CollisionObjectAABB*)(c2))->getExtents(obj2Min, obj2Max);
		return ((CollisionObjectConvexPolygon*)(c1))->boxIntersects(obj2Min, obj2Max);
	});

	// ConvexPolygon-Circle collision function
	addCollisionFunction(CollisionObjectType::ConvexPolygon, CollisionObjectType::Circle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		float radius = ((CollisionObjectCircle*)(c2))->getRadius();
		return ((CollisionObjectConvexPolygon*)(c1))->circleIntersects(c2->getPosition(), radius);
	});

	// Triangle-AABB collision function
	addCollisionFunction(CollisionObjectType::Triangle, CollisionObjectType::AABB, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 obj2Min, obj2Max;
		((CollisionObjectAABB*)(c2))->getExtents(obj2Min, obj2Max);

		Vector2 const* vertices = ((CollisionObjectTriangle*)(c1))->getVertices();
		auto triPos = c1->getPosition();
		return MathsUtils::boxIntersectsTriangle(obj2Min, obj2Max, triPos + vertices[0], triPos + vertices[1], triPos + vertices[2]);
	});

	// Triangle-Circle collision function
	addCollisionFunction(CollisionObjectType::Triangle, CollisionObjectType::Circle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		float circleRadius = ((CollisionObjectCircle*)(c2))->getRadius();

		Vector2 const* vertices = ((CollisionObjectTriangle*)(c1))->getVertices();
		auto triPos = c1->getPosition();
		return MathsUtils::circleIntersectsTriangle(c2->getPosition(), circleRadius, triPos + vertices[0], triPos + vertices[1], triPos + vertices[2]);
	});

	// Triangle-Triangle collision function
	addCollisionFunction(CollisionObjectType::Triangle, CollisionObjectType::Triangle, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 const* vertices1 = ((CollisionObjectTriangle*)(c1))->getVertices();
		auto triPos1 = c1->getPosition();
		Vector2 const* vertices2 = ((CollisionObjectTriangle*)(c2))->getVertices();
		auto triPos2 = c2->getPosition();
		return MathsUtils::triangleIntersectsTriangle(triPos1 + vertices1[0], triPos1 + vertices1[1], triPos1 + vertices1[2],
			triPos2 + vertices2[0], triPos2 + vertices2[1], triPos2 + vertices2[2]);
	});

	// Triangle-ConvexPolygon collision function
	addCollisionFunction(CollisionObjectType::Triangle, CollisionObjectType::ConvexPolygon, [](CollisionObject const* c1, CollisionObject const* c2)
	{
		Vector2 const* vertices1 = ((CollisionObjectTriangle*)(c1))->getVertices();
		auto triPos1 = c1->getPosition();
		return ((CollisionObjectConvexPolygon*)(c2))->triangleIntersects(triPos1 + vertices1[0], triPos1 + vertices1[1], triPos1 + vertices1[2]);
	});
}

void CollisionClaw::addCollisionFunction(CollisionObjectType type1, CollisionObjectType type2, CollisionFunction func)
{
	mCollisionFunctions[make_pair(type1, type2)] = func;
}

CollisionObject* CollisionClaw::addObject(CollisionObjectType type, std::string const& name, mpp::Colour const& colour)
{
	CollisionObject* obj = nullptr;

	switch (type)
	{
	case CollisionObjectType::Point:
		obj = new CollisionObjectPoint(name, colour);
		break;

	case CollisionObjectType::AABB:
		obj = new CollisionObjectAABB(name, colour);
		break;
		
	case CollisionObjectType::Circle:
		obj = new CollisionObjectCircle(name, colour);
		break;

	case CollisionObjectType::ConvexPolygon:
		obj = new CollisionObjectConvexPolygon(name, colour);
		break;

	case CollisionObjectType::Triangle:
		obj = new CollisionObjectTriangle(name, colour);
		break;

	default:
		break;
	}

	mObjects.push_back(obj);
	return obj;
}

void CollisionClaw::removeAllObjects()
{
	for (auto object: mObjects)
	{
		delete object;
	}

	mObjects.clear();
}

void CollisionClaw::handleInput(InputManager* inputMgr, float frameTime)
{
	// Get cursor position and highlight
	int cursorPosX, cursorPosY;
	inputMgr->getMousePosition(&cursorPosX, &cursorPosY);
	cursorPosY = mWindowSizeY - cursorPosY;

	mHoveredObject = nullptr;

	// Check in reverse render order
	for (auto it = mObjects.rbegin(); it != mObjects.rend(); ++it)
	{
		auto object = *it;

		// Unhover
		object->setHover(false);

		if (object->pointInObject((float)cursorPosX, (float)cursorPosY))
		{
			// Hover
			if (!mHoveredObject)
			{
				object->setHover(true);
				mHoveredObject = object;
			}

			// Select for moving
			if (inputMgr->buttonPressed(MouseDefinition::Mouse_Left))
			{
				mDragType = DragType::Move;
				if (mSelectedObject)
				{
					mSelectedObject->setSelected(false);
				}

				object->setSelected(true);
				mSelectedObject = object;
			}

			// Select for scaling/rotating
			if (inputMgr->buttonPressed(MouseDefinition::Mouse_Right))
			{
				if (inputMgr->keyDown(KeyDefinition::Key_LeftControl) || inputMgr->keyDown(KeyDefinition::Key_RightControl))
				{
					mDragType = DragType::Rotate;
				}
				else
				{
					mDragType = DragType::Scale;
				}

				if (mSelectedObject)
				{
					mSelectedObject->setSelected(false);
				}

				object->setSelected(true);
				mSelectedObject = object;
			}
		}
	}

	if (inputMgr->buttonReleased(MouseDefinition::Mouse_Left))
	{
		mDragType = DragType::None;
		if (mSelectedObject)
		{
			mSelectedObject->setSelected(false);
		}

		mSelectedObject = false;
	}
	if (inputMgr->buttonReleased(MouseDefinition::Mouse_Right))
	{
		mDragType = DragType::None;
		if (mSelectedObject)
		{
			mSelectedObject->setSelected(false);
		}

		mSelectedObject = false;
	}


	// Check movement
	switch (mDragType)
	{
	case DragType::Move:
		if (mSelectedObject)
		{
			int dragX, dragY;
			inputMgr->getMouseMovement(&dragX, &dragY);
			dragY = -dragY;

			mSelectedObject->move(Vector2((float)dragX, (float)dragY));
		}
		break;

	case DragType::Scale:
		if (mSelectedObject)
		{
			int dragX, dragY;
			inputMgr->getMouseMovement(&dragX, &dragY);
			dragY = -dragY;

			mSelectedObject->scale((100.0f + dragX) / 100.0f);
		}
		break;

	case DragType::Rotate:
		if (mSelectedObject)
		{
			int dragX, dragY;
			inputMgr->getMouseMovement(&dragX, &dragY);
			dragY = -dragY;

			mSelectedObject->rotate((float)dragX);
		}
		break;
	}
}

void CollisionClaw::update(float frameTime)
{
	for (auto object: mObjects)
	{
		object->update(frameTime);
		object->setColliding(false);
	}

	// Check collisions
	for (wp::uint32_t i = 0; i < mObjects.size() - 1; ++i)
	{
		for (wp::uint32_t j = i + 1; j < mObjects.size(); ++j)
		{
			auto type1 = mObjects[i]->getType();
			auto type2 = mObjects[j]->getType();

			auto it = mCollisionFunctions.find(make_pair(type1, type2));
			if (it != mCollisionFunctions.end())
			{
				if (it->second(mObjects[i], mObjects[j]))
				{
					mObjects[i]->setColliding(true);
					mObjects[j]->setColliding(true);
				}
			}
			else
			{
				it = mCollisionFunctions.find(make_pair(type2, type1));
				if (it != mCollisionFunctions.end())
				{
					if (it->second(mObjects[j], mObjects[i]))
					{
						mObjects[i]->setColliding(true);
						mObjects[j]->setColliding(true);
					}
				}
			}
		}
	}

	// Check intersections
	mIntersectionLineHits.clear();
	mIntersectionLineIntersections.clear();
	for (auto const& line: mIntersectionLines)
	{
		for (auto object: mObjects)
		{
			switch (object->getType())
			{
			case CollisionObjectType::AABB:
			{
				Vector2 boxMin, boxMax;
				((CollisionObjectAABB*)(object))->getExtents(boxMin, boxMax);
				LineHit hit1, hit2;

				auto hitRes = MathsUtils::lineBoxIntersection(
					line.start->getPosition(), line.end->getPosition(),
					boxMin, boxMax, &hit1, &hit2);

				if (hitRes == MathsUtils::LineIntersectionType::Intersecting)
				{
					IntersectionLineHit iHit;

					if (hit1.getFlags() & LineHit::Flags::HitEnters)
					{
						iHit.start = hit1.getPosition();
						iHit.end = line.end->getPosition();
						iHit.normal0 = iHit.start;
						iHit.normal1 = iHit.start + hit1.getNormal() * 10;
					}
					else
					{
						iHit.start = line.start->getPosition();
						iHit.end = hit1.getPosition();
						iHit.normal0 = iHit.end;
						iHit.normal1 = iHit.end + hit1.getNormal() * 10;

					}
					mIntersectionLineHits.push_back(iHit);
				}
				else if (hitRes == MathsUtils::LineIntersectionType::DoublyIntersecting)
				{
					IntersectionLineHit iHit;

					iHit.start = hit1.getPosition();
					iHit.end = hit2.getPosition();
					iHit.normal0 = hit1.getPosition();
					iHit.normal1 = hit1.getPosition();
					mIntersectionLineHits.push_back(iHit);
				}
				else if (hitRes == MathsUtils::LineIntersectionType::Inside)
				{
					IntersectionLineHit iHit;

					iHit.start = line.start->getPosition();
					iHit.end = line.end->getPosition();
					iHit.normal0 = line.start->getPosition();
					iHit.normal1 = line.end->getPosition();
					mIntersectionLineHits.push_back(iHit);
				}
			}
			break;

			case CollisionObjectType::Circle:
			{
				float circleRadius = ((CollisionObjectCircle*)(object))->getRadius();
				LineHit hit1, hit2;

				auto hitRes = MathsUtils::lineCircleIntersection(
					line.start->getPosition(), line.end->getPosition(),
					object->getPosition(), circleRadius, &hit1, &hit2);

				if (hitRes == MathsUtils::LineIntersectionType::Intersecting)
				{
					IntersectionLineHit iHit;

					if (hit1.getFlags() & LineHit::Flags::HitEnters)
					{
						iHit.start = hit1.getPosition();
						iHit.end = line.end->getPosition();
						iHit.normal0 = iHit.end;
						iHit.normal1 = iHit.end + hit1.getNormal() * 10;
					}
					else
					{
						iHit.start = line.start->getPosition();
						iHit.end = hit1.getPosition();
						iHit.normal0 = iHit.start;
						iHit.normal1 = iHit.start + hit1.getNormal() * 10;
					}
					mIntersectionLineHits.push_back(iHit);
				}
				else if (hitRes == MathsUtils::LineIntersectionType::DoublyIntersecting)
				{
					IntersectionLineHit iHit;

					iHit.start = hit1.getPosition();
					iHit.end = hit2.getPosition();
					iHit.normal0 = hit1.getPosition();
					iHit.normal1 = hit1.getPosition();
					mIntersectionLineHits.push_back(iHit);
				}
				else if (hitRes == MathsUtils::LineIntersectionType::Inside)
				{
					IntersectionLineHit iHit;

					iHit.start = line.start->getPosition();
					iHit.end = line.end->getPosition();
					iHit.normal0 = line.start->getPosition();
					iHit.normal1 = line.end->getPosition();
					mIntersectionLineHits.push_back(iHit);
				}
			}
			break;
			}
		}

		for (auto const& line2: mIntersectionLines)
		{
			if (line.start == line2.start || line.end == line2.end)
			{
				continue;
			}

			LineHit hit;
			if (MathsUtils::lineLineIntersection(
				line.start->getPosition(), line.end->getPosition(),
				line2.start->getPosition(), line2.end->getPosition(), &hit) == MathsUtils::LineIntersectionType::Intersecting)
			{
				IntersectionLineIntersection iIntersection;
				iIntersection.position = hit.getPosition();

				mIntersectionLineIntersections.push_back(iIntersection);
			}
		}
	}
}

