#include "willpower/common/MathsUtils.h"

#include "collision/CollisionObjectTriangle.h"

using namespace std;
using namespace wp;

CollisionObjectTriangle::CollisionObjectTriangle(string const& name, mpp::Colour const& colour)
	: CollisionObject(CollisionObjectType::Triangle, name)
	, mColour(colour)
{
}

void CollisionObjectTriangle::setVertices(float x0, float y0, float x1, float y1, float x2, float y2)
{
	mVertices[0].set(x0, y0);
	mVertices[1].set(x1, y1);
	mVertices[2].set(x2, y2);

	mCentre = (mVertices[0] + mVertices[1] + mVertices[2]) / 3.0f;
}

Vector2 const* CollisionObjectTriangle::getVertices() const
{
	return mVertices;
}

void CollisionObjectTriangle::scale(float amt)
{
	for (auto& vertex: mVertices)
	{
		vertex *= amt;
	}
}

void CollisionObjectTriangle::rotate(float angle)
{
	MathsUtils::rotatePoint(mVertices[0], angle);
	MathsUtils::rotatePoint(mVertices[1], angle);
	MathsUtils::rotatePoint(mVertices[2], angle);
}

bool CollisionObjectTriangle::pointInObject(float x, float y) const
{
	auto pos = getPosition();
	return MathsUtils::pointInTriangle(Vector2(x, y), pos + mVertices[0], pos + mVertices[1], pos + mVertices[2]);
}

void CollisionObjectTriangle::update(float frameTime)
{

}

void CollisionObjectTriangle::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr, mpp::Colour const& colour, float inset)
{
	auto c = mCentre + getPosition();
	Vector2 v[3];

	for (int i = 0; i < 3; ++i)
	{
		float vdist = mCentre.distanceTo(mVertices[i]) - inset;
		v[i] = c + (mVertices[i] - mCentre).normalisedCopy() * vdist;
	}

//	renderSystem->renderTriangle2d(c.x, c.y, v[0].x, v[0].y, v[1].x, v[1].y, colour);
//	renderSystem->renderTriangle2d(c.x, c.y, v[1].x, v[1].y, v[2].x, v[2].y, colour);
//	renderSystem->renderTriangle2d(c.x, c.y, v[2].x, v[2].y, v[0].x, v[0].y, colour);
}

void CollisionObjectTriangle::renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	if (isColliding())
	{
		render(renderSystem, resourceMgr, mpp::Colour(1.0f, 0.5f, 0.0f), 0);
	}
	else if (isHovered() || isSelected())
	{
		render(renderSystem, resourceMgr, mpp::Colour::White, 0);
	}
}

void CollisionObjectTriangle::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	float inset = 0;
	if (isColliding() || isHovered() || isSelected())
	{
		inset = 5;
	}

	render(renderSystem, resourceMgr, mColour, inset);
}
