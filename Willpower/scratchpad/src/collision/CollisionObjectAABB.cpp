#include "collision/CollisionObjectAABB.h"

using namespace std;
using namespace wp;

CollisionObjectAABB::CollisionObjectAABB(string const& name, mpp::Colour const& colour)
	: CollisionObject(CollisionObjectType::AABB, name)
	, mColour(colour)
{
}

void CollisionObjectAABB::setPosition(Vector2 const& pos)
{
	CollisionObject::setPosition(pos);
	mBox.setPosition(getPosition());
}

void CollisionObjectAABB::move(Vector2 const& distance)
{
	CollisionObject::move(distance);
	mBox.setPosition(getPosition());
}

void CollisionObjectAABB::setSize(Vector2 const& size)
{
	mBox.setSize(size);
}

void CollisionObjectAABB::getExtents(Vector2& minExtent, Vector2& maxExtent) const
{
	mBox.getExtents(minExtent, maxExtent);
}

void CollisionObjectAABB::scale(float amt)
{
	Vector2 pos = mBox.getPosition();
	Vector2 size = mBox.getSize();
	Vector2 newSize = size * amt;

	Vector2 deltaPos = (newSize - size) / 2.0f;
	pos -= deltaPos;

	setPosition(pos);
	setSize(newSize);
}

bool CollisionObjectAABB::pointInObject(float x, float y) const
{
	return mBox.pointInside(x, y);
}

void CollisionObjectAABB::update(float frameTime)
{

}

void CollisionObjectAABB::renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	Vector2 pos = mBox.getPosition();
	Vector2 size = mBox.getSize();

	if (isColliding())
	{
//		renderSystem->renderQuad2d(pos.x, pos.y, size.x, size.y, mpp::Colour(1.0f, 0.5f, 0.0f));
	}
	else if (isHovered() || isSelected())
	{
//		renderSystem->renderQuad2d(pos.x, pos.y, size.x, size.y, mpp::Colour::White);
	}
}

void CollisionObjectAABB::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	Vector2 pos = mBox.getPosition();
	Vector2 size = mBox.getSize();

	if (isColliding() || isHovered() || isSelected())
	{
		pos.x += 5;
		pos.y += 5;
		size.x -= 10;
		size.y -= 10;
	}

//	renderSystem->renderQuad2d(pos.x, pos.y, size.x, size.y, mColour);
}
