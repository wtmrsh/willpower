#include "collision/CollisionObject.h"

using namespace std;
using namespace wp;

CollisionObject::CollisionObject(CollisionObjectType type, string const& name)
	: mType(type)
	, mName(name)
	, mPosition(Vector2::ZERO)
	, mHovered(false)
	, mSelected(false)
	, mColliding(false)
{
}

CollisionObjectType CollisionObject::getType() const
{
	return mType;
}

string const& CollisionObject::getName() const
{
	return mName;
}

void CollisionObject::setPosition(Vector2 const& pos)
{
	mPosition = pos;
}

Vector2 const& CollisionObject::getPosition() const
{
	return mPosition;
}

void CollisionObject::move(Vector2 const& distance)
{
	mPosition += distance;
}

void CollisionObject::setHover(bool hovered)
{
	mHovered = hovered;
}

bool CollisionObject::isHovered() const
{
	return mHovered;
}

void CollisionObject::setSelected(bool selected)
{
	mSelected = selected;
}

bool CollisionObject::isSelected() const
{
	return mSelected;
}	

void CollisionObject::setColliding(bool colliding)
{
	mColliding = colliding;
}

bool CollisionObject::isColliding() const
{
	return mColliding;
}