#include "collision/CollisionObjectPoint.h"

using namespace std;
using namespace wp;

CollisionObjectPoint::CollisionObjectPoint(string const& name, mpp::Colour const& colour)
	: CollisionObject(CollisionObjectType::Point, name)
	, mColour(colour)
{
	mBox.setWidth(9);
	mBox.setHeight(9);
}

void CollisionObjectPoint::setPosition(Vector2 const& pos)
{
	CollisionObject::setPosition(pos);
	mBox.setPosition(getPosition() - mBox.getSize() / 2);
}

void CollisionObjectPoint::move(Vector2 const& distance)
{
	CollisionObject::move(distance);
	mBox.setPosition(getPosition() - mBox.getSize() / 2);
}

bool CollisionObjectPoint::pointInObject(float x, float y) const
{
	return mBox.pointInside(x, y);
}

void CollisionObjectPoint::update(float frameTime)
{

}

void CollisionObjectPoint::renderHalo(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{

	Vector2 pos = mBox.getPosition();
	Vector2 size = mBox.getSize();

	if (isColliding())
	{
//		renderSystem->renderQuad2d(pos.x, pos.y, size.x, 2, mpp::Colour(1.0f, 0.5f, 0.0f));
//		renderSystem->renderQuad2d(pos.x, pos.y + size.y, size.x, -2, mpp::Colour(1.0f, 0.5f, 0.0f));
//		renderSystem->renderQuad2d(pos.x, pos.y, 2, size.y, mpp::Colour(1.0f, 0.5f, 0.0f));
//		renderSystem->renderQuad2d(pos.x + size.x, pos.y, -2, size.y, mpp::Colour(1.0f, 0.5f, 0.0f));
	}
	else if (isHovered() || isSelected())
	{
//		renderSystem->renderQuad2d(pos.x, pos.y, size.x, 2, mpp::Colour::White);
//		renderSystem->renderQuad2d(pos.x, pos.y + size.y, size.x, -2, mpp::Colour::White);
//		renderSystem->renderQuad2d(pos.x, pos.y, 2, size.y, mpp::Colour::White);
//		renderSystem->renderQuad2d(pos.x + size.x, pos.y, -2, size.y, mpp::Colour::White);
	}
}

void CollisionObjectPoint::render(mpp::RenderSystem* renderSystem, mpp::ResourceManager* resourceMgr)
{
	Vector2 pos = mBox.getPosition();
	Vector2 size = mBox.getSize();

//	renderSystem->renderQuadLined2d(pos.x, pos.y, size.x, size.y, mColour);

	auto centre = getPosition();
//	renderSystem->renderQuadLined2d(centre.x, centre.y, 1, 1, mColour);
}
