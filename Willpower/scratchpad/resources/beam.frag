@@Version

@@Texture(sampler2D TEX1)

void main()
{
	vec2 tc = @In(TEXCOORDS);
	vec4 colour = @Vec4(@In(COLOUR));
	@Out(vec4 COLOUR) = texture(@Texture(TEX1), tc) * colour;
}