#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/firepower/Platform.h"

namespace WP_NAMESPACE
{
	namespace firepower
	{
		struct DynamicCollisionObject
		{
			Vector2 origin, size;
		};

	} // firepower
} // WP_NAMESPACW#pragma once
