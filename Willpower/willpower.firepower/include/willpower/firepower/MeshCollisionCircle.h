#pragma once

#include "willpower/common/Vector2.h"

#include "willpower/firepower/Platform.h"

namespace WP_NAMESPACE
{
	namespace firepower
	{
		struct MeshCollisionCircle
		{
			Vector2 position;
			float radius;
		};

	} // firepower
} // WP_NAMESPACW