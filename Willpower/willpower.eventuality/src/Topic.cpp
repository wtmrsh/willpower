#include "willpower/eventuality/Topic.h"

using namespace std;

namespace WP_NAMESPACE
{
	namespace eventuality
	{

		Topic::Topic(std::string const& name)
			: mName(name)
		{
		}

		string const& Topic::getName() const
		{
			return mName;
		}

	} // eventuality
} // WP_NAMESPACE