// The MIT License(MIT)
// 
// Copyright(c) 2015 Stefan Reinalter
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#pragma once

#include "willpower/eventuality/Platform.h"

#include <cassert>
#include <functional>

namespace WP_NAMESPACE
{
	namespace eventuality
	{

		template <typename T>
		class Delegate {};

		template <typename T, typename... Args>
		class Delegate<T(Args...)>
		{
			typedef T(*ProxyFunction)(void*, Args...);

		private:

			template <T(*Function)(Args...)>
			static inline T functionProxy(void*, Args... args)
			{
				return Function(std::forward<Args>(args)...);
			}

			template <class C, T(C::*Function)(Args...)>
			static inline T methodProxy(void* instance, Args... args)
			{
				return (static_cast<C*>(instance)->*Function)(std::forward<Args>(args)...);
			}

			template <class C, T(C::*Function)(Args...) const>
			static inline T constMethodProxy(void* instance, Args... args)
			{
				return (static_cast<const C*>(instance)->*Function)(std::forward<Args>(args)...);
			}

		private:

			void* mInstance;

			ProxyFunction mProxy;

		public:

			Delegate()
				: mInstance(nullptr)
				, mProxy(nullptr)
			{
			}

			template <T(*Function)(Args...)>
			void bind()
			{
				mInstance = nullptr;
				mProxy = &functionProxy<Function>;
			}

			template <class C, T(C::*Function)(Args...)>
			void bind(C* instance)
			{
				mInstance = instance;
				mProxy = &methodProxy<C, Function>;
			}

			template <class C, T(C::*Function)(Args...) const>
			void bind(C const* instance)
			{
				mInstance = const_cast<C*>(instance);
				mProxy = &constMethodProxy<C, Function>;
			}

			T invoke(Args... args) const
			{
				assert((mProxy != nullptr) && "Cannot invoke unbound Delegate. Call bind() first.");
				return mProxy(mInstance, std::forward<Args>(args)...);
			}
		};

	} // eventuality
} // WP_NAMESPACE