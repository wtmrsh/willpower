#pragma once

#include <string>

#include "willpower/eventuality/Platform.h"

namespace WP_NAMESPACE
{
	namespace eventuality
	{

		class WP_EVENTUALITY_API Topic
		{
			std::string mName;

		public:

			explicit Topic(std::string const& name);

			std::string const& getName() const;
		};

	} // eventuality
} // WP_NAMESPACE
