#pragma once

#include <any>
#include <vector>

#include "willpower/eventuality/Platform.h"

namespace WP_NAMESPACE
{
	namespace eventuality
	{

		class GenericData
		{
			std::vector<std::any> mData;

		public:

			GenericData() = default;

			template<typename T>
			void addValue(T value)
			{
				mData.push_back(value);
			}

			template<typename T>
			void setValue(int index, T value)
			{
				mData[index] = value;
			}

			template<typename T>
			T getValue(int index)
			{
				return std::any_cast<T>(mData[i]);
			}
		};

	} // eventuality
} // WP_NAMESPACE
