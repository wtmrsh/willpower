# Resource specifications

Resources are defined as structured data.  Currently, only XML is supported.

## Resource file definition

### XML

The basic layout of the document, plus one resource, is as follows:

```
<?xml version="1.0"?>
<Resources>
  <Resource type="" name="" location="">
    <DependentResource id="" ref="" />
    <Option name=""></Option>
    <Definition>
    </Definition>
  </Resource>
</Resources>
```

This defines one resource within the default namespace.  To define a resource within another namespace, use:

```
<?xml version="1.0"?>
<Resources>
  <Namespace name="">
    <Resource type="" name="" location="" />
  </Namespace>
</Resources>
```

Resources are optional, that is to say it is not an error to not declare any resources within a namespace.  Extra, non-default namespaces are also optional.

#### Namespace element

This has the following attributes:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| name | Name of namespace | Yes | N/A |

#### Resource element

As already stated, this is optional.  

It has the following attributes:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| type | Registered resource type | Yes | N/A |
| name | (Unique) name of resource | Yes | N/A |
| location | Location of resource, interpreted depending on resource type | By some resources | N/A |

##### DependentResource element

Optional.

This is used to enforce dependencies between resources, and ensure that dependent resources are loaded before depending resources need to access them.

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| id | Alias for dependent resource, used by the depending resource | Yes | N/A |
| ref | Name of the resource to look for | Yes | N/A |

For example, if you had the following definition:

```
<Resource type="Image" name="CharactersImage" location="characters.png" />

<Resource type="ImageSet" name="Characters">
  <DependentResource id="Image" ref="CharactersImage" />
  <Definition />
</Resource>
```

Then in code, the `Characters` resource would retrieve the `CharactersImage` sub-resource with `auto resource = getDependentResource("Image");`

##### Option element

Optional.

This is used to specify key/value pairs which the resource-specific parser may expect.  The list of accepted keys for each resource is below.

```
<Resource type="Image" name="CharactersImage" location="characters.png">
  <Option name="filtering">none</Option>
</Resource>
```

##### Definition

Optional, though most resource types require a definition.

This is a resource-specific definition element, described per-resource below.

### Resource types

#### Image

##### location

The location attribute refers to the path of the image file, relative to the resource file location.

##### DependentResources

None required.

##### Options

- uv-style
	- either *atlas* or do not specify.  By default, the image will be loaded as a non-atlas.
- filtering
	- if set to *none*, the texture will use nearest-neighbour filtering.  If not specified, it will use bilinear.

##### Definition

 This resource does not require a definition.

##### Example

```
<Resource type="Image" name="CursorsImage" location="cursors.png">
  <Option name="filtering">none</Option>
</Resource>
```

#### ImageSet

##### location

Not required.

##### DependentResources

- Image
	- Should refer to the Image resource to be used for the sub-images in the set.

##### Options

 This resource does not support any options.

##### Definition

A required ```Images``` element, with optional nested ```Image``` or ```ImageSet``` sub-elements within that.
Each ```Image``` element has the following attributes:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| name | Alias for sub-image | Yes | N/A |
| x | x-offset within image in pixels | Yes | N/A |
| y | y-offset within image in pixels | Yes | N/A |
| width | width of sub-image in pixels| Yes | N/A |
| height | height of sub-image in pixels | Yes | N/A |

Each ```ImageSet``` has the same elements as ```Image```, plus the following:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| count | Number of sub-images | Yes | N/A |
| dx | difference in pixels across between sub-images | Yes | N/A |
| dy | differences in pixels down between sub-images | Yes | N/A |

##### Example

```
<Resource type="ImageSet" name="Cursors">
  <DependentResource id="Image" ref="CursorsImage" />
		
  <Definition>
    <Images>
      <Image name="HandCursor" x="0" y="0" width="32" height="32" />
      <Image name="MoveCursor" x="32" y="0" width="32" height="32" />
      <Image name="PointCursor" x="64" y="0" width="32" height="32" />
	  <ImageSet name="Set1" x="0" y="0" width="32" height="32" count="2" dx="32" dy="0" />
    </Images>
  </Definition>
</Resource>
```

#### AnimationSet

##### location

Not required.

##### DependentResources

- Image
	- Should refer to the Image resource to be used for the sub-images in the set.

##### Options

This resource does not support any options.

##### Definition

A required ```Animations``` element, with optional nested ```Animation``` sub-elements within that.

Each ```Animation``` element has the following attributes:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| name | animation name | Yes | N/A |
| loopStyle | one of *forwards*\|*once*\|*pingpong* | No | *forwards* |

... and can be defined in one of two ways.  There is a required ```Frames``` sub-element.  This has the following attributes:

| Attribute | Description | Required | Default |
| -- | -- | -- | -- |
| imageset | imageset specified within the ImageSet dependent resource | No | N/A |
| x | x-offset within image in pixels | No | 0 |
| y | y-offset within image in pixels | No | 0 |
| width | default frame width in pixels | No | If not specified, requires the individual frames to do so |
| height | default frame height in pixels | No | If not specified, requires the individual frames to do so |
| xoff | x render-offset in pixels | No | 0 |
| yoff | y render offset in pixels | No | 0 |
| time | time in seconds until next frame | No | If not specified, requires the individual frames to do so |
| count | number of frames in animation | No | If not specified, it is required to list out all the frames, otherwise they are calculated from the given information |

If ```imageset``` is specified, then frame data is taken from the ImageSet definition of that name.  The ```count``` can be a subsection (from zero) of the total frames
in that image set.

###### Tags

Each frame can have a tag, which is a key:value pair to be interpreted by the application - for instance for timing when to play a sound.  The structure of the ```Tags``` element is as follows:

```
<Tags>
  <Tag key="x" value="y" />
</Tags>
```

These are embedded in the ```Frame``` element.

##### Example

```
<Resource type="AnimationSet" name="AgentAnimations">
  <DependentResource id="Image" ref="AgentImageSet" />
		
  <Definition>
    <Animations>
      <Animation name="idle" loopStyle="pingpong">
        <Frames time="0.3" >
          <Frame image="idle0" xoff="0" yoff="0" time="0.4">
            <Tags>
              <Tag key="StartSound" value="woohah.mp3" />
            </Tags>
          </Frame>
          <Frame x="32" y="0" width="64">
            <Tags>
              <Tag key="StartSound" value="woohah.mp3" />
            </Tags>
          </Frame>
        <Frames>
      </Animation>
    </Animations>
  </Definition>
</Resource>
```

```
<Resource type="AnimationSet" name="AgentAnimations">
  <DependentResource id="Image" ref="AgentImageSet" />
		
  <Definition>
    <Animations>
      <Animation name="idle" loopStyle="pingpong">
        <Frames imageset="Idle" time="0.3" count="8">
          <Frame frame="0" xoff="0" yoff="1" time="0.5" />
          <Frame frame="1">
            <Tags>
              <Tag frame="0" key="A" value="b" />
            </Tags>
          </Frame>
        </Frames>
      </Animation>
    </Animations>
  </Definition>
</Resource>
```

#### Shader

##### location

Not required.

##### DependentResources

None required.

##### Options

This resource does not support any options.

##### Definition

This resource does not require a definition.

##### Example

```
	<Resource type="Shader" name="VertexShader1" location="v1.vert" />
```

#### Program

##### location

The location attribute refers to the path of the text file, relative to the resource file location.

##### DependentResources

- Vertex
	- Should refer to the Shader resource to be used for vertex shader
- Fragment
	- Should refer to the Shader resource to be used for fragment shader

##### Options

This resource does not support any options.

##### Definition

This resource does not require a definition.

##### Example

```
	<Resource type="Program" name="Program1">
      <DependentResource id="Vertex" ref="VertexShader1" />
      <DependentResource id="Fragment" ref="FragmentShader1" />      
	</Resource>
```