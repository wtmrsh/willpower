#include "willpower/collide/StaticLine.h"

using namespace std;

namespace WP_NAMESPACE
{
	namespace collide
	{

		using namespace WP_NAMESPACE;

		StaticLine::StaticLine()
			: mFriction(1.0f)
			, mEnabled(true)
		{
		}

		StaticLine::StaticLine(Vector2 const& v0, Vector2 const& v1, float friction)
			: mEnabled(true)
		{
			mVertices[0] = v0;
			mVertices[1] = v1;
			mFriction = friction;

			mNormal = (mVertices[1] - mVertices[0]).perpendicular().normalisedCopy();
		}

		Vector2 const& StaticLine::getNormal() const
		{
			return mNormal;
		}

		Vector2 const& StaticLine::getVertex(int index) const
		{
			return mVertices[index];
		}

		void StaticLine::getVertices(Vector2& v0, Vector2& v1) const
		{
			v0 = mVertices[0];
			v1 = mVertices[1];
		}

		float StaticLine::getFriction() const
		{
			return mFriction;
		}

		void StaticLine::enable(bool enable)
		{
			mEnabled = enable;
		}

		bool StaticLine::enabled() const
		{
			return mEnabled;
		}
	} // collide
} //WP_NAMESPACE
