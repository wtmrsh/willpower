# Willpower

2D game engine.

## Cloning

git clone --recurse-submodules https://wtmrsh@bitbucket.org/wtmrsh/willpower.git

## Building

- devenv ext\massivepolypusher\ext\utils\build\vs2017\Utils.sln /Build Release
- devenv ext\massivepolypusher\build\vs2017\MassivePolyPusher.sln /Build Release
- devenv ext\utils\build\vs2017\Utils.sln /Build Release
- devenv Willpower\build\vs2017\Willpower.sln /Build Release
- devenv Launcher\build\vs2017\Launcher.sln Build Release

Then build individual applications.
