#pragma once

#include <vector>
#include <tuple>

#include <willpower/application/StateFactory.h>

#include <willpower/collide/ColliderAABB.h>

#include <willpower/viz/DynamicTriangleRenderer.h>

#include <applib/StatePlay.h>
#include <applib/EntityManager.h>

#include "Platform.h"
#include "EntityHandlerTemplate.h"
#include "CollisionSimulationDataProvider.h"
#include "MapGridDataProvider.h"


class APPLICATION_API StatePlayTemplate : public applib::StatePlay
{
	typedef wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat> DoorRenderer;

private:

	wp::collide::Collider* mPlayerCollider;

	std::shared_ptr<CollisionSimulationDataProvider> mCollisionSimDataProvider;

	std::shared_ptr<MapGridDataProvider> mMapGridDataProvider;

private:

	std::map<std::string, std::tuple<wp::viz::Renderer*, int, bool>> createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr) override;

	void setupPlayerCollision();

	void registerInput() override;

	void createGameObjects(wp::application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args) override;

	void destroyGameObjects() override;

	void setupEntityFacades() override;

	void setupEntities() override;

	void updatePreInput(float frameTime) override;

	void updatePostEntities(float frameTime) override;

protected:

	void updateActions(std::vector<std::string> const& activeStates, float frameTime) override;

	void updatePreRenderers(float frameTime) override;

public:

	StatePlayTemplate();

	std::vector<std::string> getDebuggingText() const override;
};

class StatePlayTemplateFactory : public wp::application::StateFactory
{
	wp::Logger* mLogger;

public:

	explicit StatePlayTemplateFactory(wp::Logger* logger)
		: wp::application::StateFactory("Play")
		, mLogger(logger)
	{
	}

	wp::application::State* createState()
	{
		auto state = new StatePlayTemplate();
		state->setLogger(mLogger);
		return state;
	}
};
