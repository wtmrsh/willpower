#pragma once

#include "Platform.h"


enum class BulletType
{
	RedDot,
	Fireball,
	Comet,
};
