#pragma once

#include <applib/MapResourceDefinitionFactory.h>

#include "Platform.h"


class MapTemplateDefinitionFactory : public applib::MapResourceDefinitionFactory
{
public:

	MapTemplateDefinitionFactory();

	void create(wp::application::resourcesystem::Resource* resource, wp::application::resourcesystem::ResourceManager* resourceMgr, utils::XmlNode* node) override;
};

