#pragma once

#include <vector>
#include <set>

#include <willpower/viz/GeometryMeshRenderer.h>

#include <applib/GeometryMeshRendererFactory.h>

#include "Platform.h"


class TemplateGeometryMeshRenderer : public wp::viz::GeometryMeshRenderer
{
	std::vector<std::set<std::string>> mPolygonIndexLookup;

	std::vector<uint32_t> mViewerPolygonIndices;

private:

	void createMeshes(mpp::ProgrammaticModelStream* stream, mpp::ResourceManager* resourceMgr) override;

	std::string getPolygonMeshNamePrefix(uint32_t polygonIndex) const override;

public:

	TemplateGeometryMeshRenderer(std::string const& name, std::shared_ptr<wp::geometry::Mesh> mesh, wp::viz::StaticRenderer::GridOptions const& gridOptions, size_t indexWidth, mpp::ResourceManager* renderResourceMgr);

	void setViewerPolygonIndices(std::vector<uint32_t> const& polygonIndices);

	void update(wp::BoundingBox const& viewBounds, float frameTime) override;
};

class TemplateGeometryMeshRendererFactory : public applib::GeometryMeshRendererFactory
{
public:

	wp::viz::GeometryMeshRenderer* create(std::string const& name, std::shared_ptr<wp::geometry::Mesh> mesh, wp::viz::StaticRenderer::GridOptions const& gridOptions, size_t indexWidth, mpp::ResourceManager* renderResourceMgr) override
	{
		return new TemplateGeometryMeshRenderer(name, mesh, gridOptions, indexWidth, renderResourceMgr);
	}

};
