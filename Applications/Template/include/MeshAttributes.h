#pragma once

#include <willpower/geometry/UserAttributes.h>

#include "Platform.h"
#include "GameException.h"


struct TemplatePolygonAttributes
{
};

struct TemplatePolygonVertexAttributes
{
	float u0, v0;
	float w0, w1;
};

class TemplatePolygonAttributeHolder : public wp::geometry::UserAttributes<TemplatePolygonAttributes>
{
public:

	TemplatePolygonAttributeHolder()
	{
	}

	TemplatePolygonAttributeHolder(TemplatePolygonAttributeHolder const& other)
	{
		mAttributes = other.mAttributes;
	}

	uint32_t createAttribute(void const* data) override
	{
		auto const* pa = static_cast<TemplatePolygonAttributes const*>(data);
		return addAttribute(*pa);
	}

	void updateAttribute(uint32_t index, void const* data) override
	{
		auto const* pa = static_cast<TemplatePolygonAttributes const*>(data);
		setAttribute(index, *pa);
	}

	void const* readAttribute(uint32_t index) override
	{
		auto const& pa = getAttribute(index);
		return static_cast<void const*>(&pa);
	}

	void getMaterialAttribute(void const* data, std::string& material) const override
	{
		VAR_UNUSED(data);
		material = "";
	}
};

class TemplatePolygonAttributeFactory : public wp::geometry::UserAttributesFactory
{
public:

	wp::geometry::UserAttributesBase* create() override
	{
		return new TemplatePolygonAttributeHolder();
	}

	wp::geometry::UserAttributesBase* copy(wp::geometry::UserAttributesBase const* source) override
	{
		return new TemplatePolygonAttributeHolder(*dynamic_cast<TemplatePolygonAttributeHolder const*>(source));
	}
};

class TemplatePolygonVertexAttributeHolder : public wp::geometry::UserAttributes<TemplatePolygonVertexAttributes>
{
public:

	TemplatePolygonVertexAttributeHolder()
	{
	}

	TemplatePolygonVertexAttributeHolder(TemplatePolygonVertexAttributeHolder const& other)
	{
		mAttributes = other.mAttributes;
	}

	uint32_t createAttribute(void const* data) override
	{
		auto const* pva = static_cast<TemplatePolygonVertexAttributes const*>(data);
		return addAttribute(*pva);
	}

	void updateAttribute(uint32_t index, void const* data) override
	{
		auto const* pva = static_cast<TemplatePolygonVertexAttributes const*>(data);
		setAttribute(index, *pva);
	}

	void const* readAttribute(uint32_t index) override
	{
		auto const& pva = getAttribute(index);
		return static_cast<void const*>(&pva);
	}

	void getUvAttribute(void const* data, uint32_t textureIndex, float& u, float& v) const override
	{
		VAR_UNUSED(textureIndex);

		auto const* pva = static_cast<TemplatePolygonVertexAttributes const*>(data);

		u = pva->u0;
		v = pva->v0;
	}

	void getUvWeightAttribute(void const* data, uint32_t textureIndex, float& weight) const override
	{
		auto const* pva = static_cast<TemplatePolygonVertexAttributes const*>(data);
		switch (textureIndex)
		{
		case 0:
			weight = pva->w0;
			break;

		case 1:
			weight = pva->w1;
			break;

		default:
			throw GameException(STR_FORMAT("Texture index {} not supported", textureIndex));
		}
	}
};

class TemplatePolygonVertexAttributeFactory : public wp::geometry::UserAttributesFactory
{
public:

	wp::geometry::UserAttributesBase* create() override
	{
		return new TemplatePolygonVertexAttributeHolder();
	}

	wp::geometry::UserAttributesBase* copy(wp::geometry::UserAttributesBase const* source) override
	{
		return new TemplatePolygonVertexAttributeHolder(*dynamic_cast<TemplatePolygonVertexAttributeHolder const*>(source));
	}
};