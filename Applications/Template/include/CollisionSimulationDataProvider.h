#pragma once

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>

#include <mpp/helper/LineBatchDataProvider.h>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>
#include <willpower/common/MathsUtils.h>

#include <willpower/collide/Simulation.h>

#include "Platform.h"


class CollisionSimulationDataProvider : public mpp::helper::LineBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeUnsignedByte>
{
	struct Line
	{
		wp::Vector2 v[2];
		int type;
	};

private:

	wp::collide::Simulation* mwSimulation;

	glm::vec3 mBounds[2];

	std::vector<Line> mLines;

public:

	explicit CollisionSimulationDataProvider(wp::collide::Simulation* simulation)
		: mwSimulation(simulation)
	{
		update(wp::BoundingBox(), 0.0f);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin = mBounds[0];
		bMax = mBounds[1];
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1)
	{
		auto const& line = mLines[index];
		
		x0 = line.v[0].x;
		y0 = line.v[0].y;
		x1 = line.v[1].x;
		y1 = line.v[1].y;
	}

	void colour(uint32_t index, uint8_t& red, uint8_t& green, uint8_t& blue, uint8_t& alpha)
	{
		auto const& line = mLines[index];

		switch (line.type)
		{
		case 0:
			// Disabled
			red = 255;
			green = 0;
			blue = 64;
			break;
		case 1:
			// Enabled
			red = 0;
			green = 255;
			blue = 64;
			break;
		case 2:
			// Collider
			red = 64;
			green = 64;
			blue = 255;
			break;
		default:
			red = 255;
			green = 255;
			blue = 255;
			break;
		}

		alpha = 255;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::White;
	}

	bool update(wp::BoundingBox const& viewBounds, float frameTime)
	{
		VAR_UNUSED(frameTime);

		mBounds[0].x = 1e10;
		mBounds[0].y = 1e10;
		mBounds[1].x = -1e10;
		mBounds[1].y = -1e10;

		wp::Vector2 viewMin, viewMax;
		viewBounds.getExtents(viewMin, viewMax);

		mLines.clear();

		// Static lines
		for (uint32_t i = 0; i < mwSimulation->getNumStaticLines(); ++i)
		{
			auto const& line = mwSimulation->getStaticLine(i);

			wp::Vector2 v0, v1;
			line.getVertices(v0, v1);

			auto points = wp::MathsUtils::clipLineAgainstQuad(v0, v1, viewMin, viewMax);
			if (points.size() == 2)
			{
				mLines.push_back({ points[0], points[1], line.enabled() ? 1 : 0 });

				mBounds[0].x = std::min(mBounds[0].x, points[0].x);
				mBounds[0].y = std::min(mBounds[0].y, points[0].y);
				mBounds[1].x = std::max(mBounds[1].x, points[1].x);
				mBounds[1].y = std::max(mBounds[1].y, points[1].y);
			}
		}

		// Colliders
		auto colliders = mwSimulation->getColliders();
		for (auto collider : colliders)
		{
			auto bounds = collider->getBounds();

			wp::Vector2 bMin, bMax;
			bounds.getExtents(bMin, bMax);

			mLines.push_back({ wp::Vector2(bMin.x, bMin.y), wp::Vector2(bMax.x, bMin.y), 2 });
			mLines.push_back({ wp::Vector2(bMax.x, bMin.y), wp::Vector2(bMax.x, bMax.y), 2 });
			mLines.push_back({ wp::Vector2(bMax.x, bMax.y), wp::Vector2(bMin.x, bMax.y), 2 });
			mLines.push_back({ wp::Vector2(bMin.x, bMax.y), wp::Vector2(bMin.x, bMin.y), 2 });
		}

		setNumPrimitives(mLines.size());
		return true;
	}
};
