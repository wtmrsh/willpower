#pragma once

#include <applib/GameDefaultDefinitionFactory.h>

#include "Platform.h"


class GameDefinitionFactory : public applib::GameDefaultDefinitionFactory
{
protected:

	uint32_t getBulletIdFromName(std::string const& name) override;

public:

	GameDefinitionFactory();
};

