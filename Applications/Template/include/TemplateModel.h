#pragma once

#include <applib/Model.h>

#include "Platform.h"


struct TemplateModel : public applib::Model
{
	TemplateModel(applib::EntityHandlerFactoryFunction handlerFactory, wp::application::resourcesystem::ResourceManager* resourceMgr)
		: applib::Model(handlerFactory, resourceMgr)
	{
	}
};
