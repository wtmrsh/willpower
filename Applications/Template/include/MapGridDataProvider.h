#pragma once

#include <mpp/helper/LineBatchDataProvider.h>

#include <willpower/common/BoundingBox.h>

#include "Platform.h"


class MapGridDataProvider : public mpp::helper::LineBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeUnsignedByte>
{
	struct Line
	{
		wp::Vector2 v[2];
	};

private:

	glm::vec3 mBounds[2];

	std::vector<Line> mLines;

public:

	MapGridDataProvider()
	{
		update(0.0f, wp::BoundingBox(), 0.0f);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin = mBounds[0];
		bMax = mBounds[1];
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1)
	{
		auto const& line = mLines[index];

		x0 = line.v[0].x;
		y0 = line.v[0].y;
		x1 = line.v[1].x;
		y1 = line.v[1].y;
	}

	void colour(uint32_t index, uint8_t& red, uint8_t& green, uint8_t& blue, uint8_t& alpha)
	{
		VAR_UNUSED(index);

		red = 192;
		green = 160;
		blue = 255;
		alpha = 255;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::Red;
	}

	bool update(float gridSize, wp::BoundingBox const& viewBounds, float frameTime)
	{
		VAR_UNUSED(frameTime);

		wp::Vector2 viewMin, viewMax;
		viewBounds.getExtents(viewMin, viewMax);

		mBounds[0].x = viewMin.x;
		mBounds[0].y = viewMin.y;
		mBounds[1].x = viewMax.x;
		mBounds[1].y = viewMax.y;

		mLines.clear();

		if (gridSize == 0)
		{
			return false;
		}

		auto viewSize = viewMax - viewMin;

		// Assume map starts at 0, 0
		float x0 = std::max(viewMin.x - fmod(viewMin.x, gridSize), 0.0f);
		float y0 = std::max(viewMin.y - fmod(viewMin.y, gridSize), 0.0f);

		auto x = x0;
		while (x <= viewMax.x)
		{
			mLines.push_back({
				wp::Vector2(x, y0),
				wp::Vector2(x, y0 + viewSize.y + gridSize)
			});

			x += gridSize;
		}

		auto y = y0;
		while (y <= viewMax.y)
		{
			mLines.push_back({
				wp::Vector2(x0, y),
				wp::Vector2(x0 + viewSize.x + gridSize, y)
			});

			y += gridSize;
		}

		setNumPrimitives(mLines.size());
		return true;
	}
};
