#include <utils/XmlReader.h>

#include <willpower/application/resourcesystem/ResourceExceptions.h>
#include <willpower/common/Exceptions.h>

#include <applib/MapGeometryObjectAttributes.h>

#include "MapTemplateDefinitionFactory.h"
#include "MeshAttributes.h"


MapTemplateDefinitionFactory::MapTemplateDefinitionFactory()
	: applib::MapResourceDefinitionFactory("Template",
		new applib::VertexAttributeFactory,
		new applib::EdgeAttributeFactory,
		new TemplatePolygonAttributeFactory,
		new TemplatePolygonVertexAttributeFactory)
{
}

void MapTemplateDefinitionFactory::create(wp::application::resourcesystem::Resource* resource, wp::application::resourcesystem::ResourceManager* resourceMgr, utils::XmlNode* node)
{
	auto mapRes = static_cast<applib::Map*>(resource);

	// Create mesh
	createMesh(mapRes);

	// Load from code, or wherver
	auto funcNode = node->getChild("Function");
	auto funcName = funcNode->getValue();
	auto func = mapRes->getMeshCreationFunction(funcName);

	func(mapRes->getMesh().get(), resourceMgr);
}
