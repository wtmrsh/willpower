#include <utils/StringUtils.h>

#include "TemplateGeometryMeshRenderer.h"
#include "MeshAttributes.h"

using namespace std;


TemplateGeometryMeshRenderer::TemplateGeometryMeshRenderer(string const& name, shared_ptr<wp::geometry::Mesh> mesh, wp::viz::StaticRenderer::GridOptions const& gridOptions, size_t indexWidth, mpp::ResourceManager* renderResourceMgr)
	: wp::viz::GeometryMeshRenderer(name, mesh, gridOptions, indexWidth, renderResourceMgr)
{
	mPolygonIndexLookup.resize(mesh->getNumPolygons(), {});
}

string TemplateGeometryMeshRenderer::getPolygonMeshNamePrefix(uint32_t polygonIndex) const
{
	VAR_UNUSED(polygonIndex);
	return "";
}

void TemplateGeometryMeshRenderer::createMeshes(mpp::ProgrammaticModelStream* stream, mpp::ResourceManager* resourceMgr)
{
	int gridDimX, gridDimY;
	if (usingLookupGrid())
	{
		gridDimX = mLookupGrid->getCellDimensionX();
		gridDimY = mLookupGrid->getCellDimensionY();
	}
	else
	{
		gridDimX = 1;
		gridDimY = 1;
	}

	for (int y = 0; y < gridDimY; ++y)
	{
		for (int x = 0; x < gridDimX; ++x)
		{
			wp::BoundingBox cellBounds;
			if (usingLookupGrid())
			{
				// Get cell clipping rect
				wp::Vector2 minExtent, maxExtent;
				mLookupGrid->getCellExtents(x, y, minExtent, maxExtent);

				cellBounds.setPosition(minExtent);
				cellBounds.setSize(maxExtent - minExtent);
			}

			// Clip each polygon against it.
			uint32_t polygonIndex = mMesh->getFirstPolygonIndex();
			while (!mMesh->polygonIndexIterationFinished(polygonIndex))
			{
				auto const& polygon = mMesh->getPolygon(polygonIndex);
				if (!polygon.isHole())
				{
					// Bounds check
					auto polyBounds = polygon.getBoundingBox();
					if (!usingLookupGrid() || cellBounds.intersectsBoundingObject(&polyBounds))
					{
						//auto polygonUserData = static_cast<TemplatePolygonAttributes const*>(mMesh->getPolygonUserData(polygonIndex));

						for (size_t i = 0; i < polygon.getTriangulationTriangleCount(); ++i)
						{
							uint32_t i0, i1, i2;
							polygon.getTriangulationVertexIndices(i, i0, i1, i2);

							auto const& v0 = mMesh->getVertex(i0);
							auto const& v1 = mMesh->getVertex(i1);
							auto const& v2 = mMesh->getVertex(i2);

							// Position
							wp::Vector2 p[3];
							p[0] = v0.getPosition();
							p[1] = v1.getPosition();
							p[2] = v2.getPosition();

							// Test triangle against the grid
							switch (getGridCellStrategy())
							{
							case GridOptions::CellStrategy::Intersects:
								if (!cellBounds.intersectsTriangle(p[0], p[1], p[2]))
								{
									continue;
								}
								break;

							case GridOptions::CellStrategy::CentreInside:
								if (!cellBounds.pointInside((p[0] + p[1] + p[2]) / 3.0f))
								{
									continue;
								}
								break;
							}

							// Get material
							string matName;
							mMesh->getPolygonMaterialAttribute(polygonIndex, matName);

							if (matName == "")
							{
								matName = getPolygonMaterialName(polygonIndex);
							}

							// Create mesh
							auto meshNamePrefix = getPolygonMeshNamePrefix(polygonIndex);
							string meshName = STR_FORMAT("Background_{}_{}_{}_{}", meshNamePrefix, matName, x, y);

							// Add to lookup so we can get the mesh from the polygon index quickly
							mPolygonIndexLookup[polygonIndex].insert(meshName);

							auto res = mBackgroundMeshes.insert(pair(meshName, BackgroundMeshData()));
							auto& data = res.first->second;
							auto exists = !res.second;

							if (!exists)
							{
								data.material = resourceMgr->getResource(matName);
								data.numVertices = 0;
								data.meshId = stream->createMesh(meshName, mMeshSpecifications["Background"], matName, (int)mIndexWidth);

								if (usingLookupGrid())
								{
									mBackgroundMeshLookup[(uint32_t)data.meshId] = meshName;

									wp::BoundingBox bbox = cellBounds;
									bbox.expand(-0.9f);
									mLookupGrid->addItem((uint32_t)data.meshId, bbox);
								}
							}

							// Texcoord set 1
							float t00[2] = { 0.0f, 0.0f };
							float t01[2] = { 0.0f, 0.0f };
							float t02[2] = { 0.0f, 0.0f };

							mMesh->getPolygonVertexUvAttribute(polygonIndex, i0, 0, t00[0], t00[1]);
							mMesh->getPolygonVertexUvAttribute(polygonIndex, i1, 0, t01[0], t01[1]);
							mMesh->getPolygonVertexUvAttribute(polygonIndex, i2, 0, t02[0], t02[1]);

							// Texcoord set 2
							float t10[2] = { 0.0f, 0.0f };
							float t11[2] = { 0.0f, 0.0f };
							float t12[2] = { 0.0f, 0.0f };

							mMesh->getPolygonVertexUvAttribute(polygonIndex, i0, 1, t10[0], t10[1]);
							mMesh->getPolygonVertexUvAttribute(polygonIndex, i1, 1, t11[0], t11[1]);
							mMesh->getPolygonVertexUvAttribute(polygonIndex, i2, 1, t12[0], t12[1]);

							// Texcoord weights
							float w0[2] = { 1.0f, 0.0f };
							float w1[2] = { 1.0f, 0.0f };
							float w2[2] = { 1.0f, 0.0f };

							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i0, 0, w0[0]);
							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i1, 0, w1[0]);
							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i2, 0, w2[0]);
							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i0, 1, w0[1]);
							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i1, 1, w1[1]);
							mMesh->getPolygonVertexUvWeightAttribute(polygonIndex, i2, 1, w2[1]);

							// Colour
							float c0[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
							float c1[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
							float c2[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

							// Vertex 1
							data.vertices.push_back(p[0].x); data.vertices.push_back(p[0].y);
							data.vertices.push_back(w0[0]);  data.vertices.push_back(w0[1]);
							data.vertices.push_back(t00[0]); data.vertices.push_back(t00[1]);
							data.vertices.push_back(t10[0]); data.vertices.push_back(t10[1]);
							data.vertices.push_back(c0[0]);
							data.vertices.push_back(c0[1]);
							data.vertices.push_back(c0[2]);
							data.vertices.push_back(c0[3]);

							// Vertex 2
							data.vertices.push_back(p[1].x); data.vertices.push_back(p[1].y);
							data.vertices.push_back(w1[0]);  data.vertices.push_back(w1[1]);
							data.vertices.push_back(t01[0]); data.vertices.push_back(t01[1]);
							data.vertices.push_back(t11[0]); data.vertices.push_back(t11[1]);
							data.vertices.push_back(c1[0]);
							data.vertices.push_back(c1[1]);
							data.vertices.push_back(c1[2]);
							data.vertices.push_back(c1[3]);

							// Vertex 3
							data.vertices.push_back(p[2].x); data.vertices.push_back(p[2].y);
							data.vertices.push_back(w2[0]);  data.vertices.push_back(w2[1]);
							data.vertices.push_back(t02[0]); data.vertices.push_back(t02[1]);
							data.vertices.push_back(t12[0]); data.vertices.push_back(t12[1]);
							data.vertices.push_back(c2[0]);
							data.vertices.push_back(c2[1]);
							data.vertices.push_back(c2[2]);
							data.vertices.push_back(c2[3]);

							// Vertex count
							data.numVertices += 3;
						}
					}
				}

				polygonIndex = mMesh->getNextPolygonIndex(polygonIndex);
			}
		}
	}

	auto const& trianglesMeshSpec = mMeshSpecifications["Triangles"];
	auto const& linesMeshSpec = mMeshSpecifications["Lines"];
	auto const& verticesMeshSpec = mMeshSpecifications["Vertices"];

	// Create line meshes
	auto polygonEdgesMeshId = stream->createMesh("PolygonEdges", linesMeshSpec, mMaterialNames["Lines"], (int)mIndexWidth);
	auto borderMeshId = stream->createMesh("Border", linesMeshSpec, mMaterialNames["Lines"], (int)mIndexWidth);
	auto triangulationMeshId = stream->createMesh("Triangulation", linesMeshSpec, mMaterialNames["Lines"], (int)mIndexWidth);
	auto polygonEdgeNormalsMeshId = stream->createMesh("PolygonEdgeNormals", linesMeshSpec, mMaterialNames["Lines"], (int)mIndexWidth);
	auto polygonEdgeDirectionsMeshId = stream->createMesh("PolygonEdgeDirections", trianglesMeshSpec, mMaterialNames["Triangles"], (int)mIndexWidth);

	// Create point meshes
	auto vertexMeshId = stream->createMesh("Vertices", verticesMeshSpec, mMaterialNames["Vertices"], (int)mIndexWidth);

	// Add vertices.  Get the number of vertices first, by counting the number of triangles each polygon is
	// broken down into, then iterate over the polygons again to add the data.
	size_t numPolygonEdgesVertices{ 0 }, numBorderVertices{ 0 }, numTriangulationVertices{ 0 };
	size_t numVertices = mMesh->getNumVertices(), numPolygonDirectionVertices{ 0 };

	uint32_t polygonIndex = mMesh->getFirstPolygonIndex();
	while (!mMesh->polygonIndexIterationFinished(polygonIndex))
	{
		auto const& polygon = mMesh->getPolygon(polygonIndex);
		if (!polygon.isHole())
		{
			auto edgeSet = polygon.getEdgeIndexSet();

			numTriangulationVertices += polygon.getTriangulationTriangleCount() * 3 * 2;
			numPolygonEdgesVertices += edgeSet.size() * 2;

			for (auto edgeIndex : edgeSet)
			{
				auto const& edge = mMesh->getEdge(edgeIndex);
				if (edge.getConnectivity() == wp::geometry::Edge::Connectivity::External)
				{
					numBorderVertices += 2;
				}
			}
		}

		polygonIndex = mMesh->getNextPolygonIndex(polygonIndex);
	}

	numPolygonDirectionVertices = 3 * numPolygonEdgesVertices / 2;

	// Create vertex data for background meshes
	for (auto& kvp : mBackgroundMeshes)
	{
		auto& data = kvp.second;
		data.vertexData = new mpp::mesh::VertexData(mMeshSpecifications["Background"], kvp.second.numVertices);

		for (uint32_t i = 0; i < data.numVertices; ++i)
		{
			auto offset = i * 12;

			wp::Vector2 p(data.vertices[offset + 0], data.vertices[offset + 1]);
			wp::Vector2 w(data.vertices[offset + 2], data.vertices[offset + 3]);
			wp::Vector2 t0(data.vertices[offset + 4], data.vertices[offset + 5]);
			wp::Vector2 t1(data.vertices[offset + 6], data.vertices[offset + 7]);

			addVertexData(data.vertexData, p, w, t0, t1, &(data.vertices[offset + 8]));
		}
	}

	mpp::mesh::VertexData polygonEdgeVertexData(linesMeshSpec, numPolygonEdgesVertices);
	mpp::mesh::VertexData borderVertexData(linesMeshSpec, numBorderVertices);
	mpp::mesh::VertexData triangulationVertexData(linesMeshSpec, numTriangulationVertices);
	mpp::mesh::VertexData vertexVertexData(verticesMeshSpec, numVertices);
	mpp::mesh::VertexData polygonEdgeNormalsVertexData(linesMeshSpec, numPolygonEdgesVertices);
	mpp::mesh::VertexData polygonEdgeDirectionsVertexData(trianglesMeshSpec, numPolygonDirectionVertices);

	// Build vertex render data
	uint32_t vertexIndex = mMesh->getFirstVertexIndex();
	while (!mMesh->vertexIndexIterationFinished(vertexIndex))
	{
		auto const& vertex = mMesh->getVertex(vertexIndex);

		wp::Vector2 v = vertex.getPosition();

		float t[2] = { 0.0f, 0.0f };
		float c[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

		// Get user data and see if that has overrides
		mMesh->getVertexUvAttribute(vertexIndex, 0, t[0], t[1]);
		mMesh->getVertexRgbaAttribute(vertexIndex, c[0], c[1], c[2], c[3]);

		addVertexData(&vertexVertexData, v, wp::Vector2(t[0], t[1]), c);

		vertexIndex = mMesh->getNextVertexIndex(vertexIndex);
	}

	polygonIndex = mMesh->getFirstPolygonIndex();
	while (!mMesh->polygonIndexIterationFinished(polygonIndex))
	{
		auto const& polygon = mMesh->getPolygon(polygonIndex);
		if (!polygon.isHole())
		{
			// Build background triangle / triangulation line render data
			for (size_t i = 0; i < polygon.getTriangulationTriangleCount(); ++i)
			{
				uint32_t i0, i1, i2;
				polygon.getTriangulationVertexIndices(i, i0, i1, i2);

				auto const& v0 = mMesh->getVertex(i0);
				auto const& v1 = mMesh->getVertex(i1);
				auto const& v2 = mMesh->getVertex(i2);

				// Position
				wp::Vector2 p0, p1, p2;
				p0 = v0.getPosition();
				p1 = v1.getPosition();
				p2 = v2.getPosition();

				// Triangulation edges
				float dc[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

				addVertexData(&triangulationVertexData, p0, wp::Vector2::ZERO, dc);
				addVertexData(&triangulationVertexData, p1, wp::Vector2::ZERO, dc);
				addVertexData(&triangulationVertexData, p1, wp::Vector2::ZERO, dc);
				addVertexData(&triangulationVertexData, p2, wp::Vector2::ZERO, dc);
				addVertexData(&triangulationVertexData, p2, wp::Vector2::ZERO, dc);
				addVertexData(&triangulationVertexData, p0, wp::Vector2::ZERO, dc);
			}

			// Build polygon edge render data
			auto edgeSet = polygon.getEdgeIndexSet();
			for (uint32_t edgeIndex : edgeSet)
			{
				auto const& edge = mMesh->getEdge(edgeIndex);

				wp::Vector2 v0 = mMesh->getVertex(edge.getFirstVertex()).getPosition();
				wp::Vector2 v1 = mMesh->getVertex(edge.getSecondVertex()).getPosition();
				float colour[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

				// Polygon edges
				addVertexData(&polygonEdgeVertexData, v0, { 0.0f, 0.0f }, colour);
				addVertexData(&polygonEdgeVertexData, v1, { 0.0f, 0.0f }, colour);

				// Border edges
				if (edge.getConnectivity() == wp::geometry::Edge::Connectivity::External)
				{
					addVertexData(&borderVertexData, v0, { 0.0f, 0.0f }, colour);
					addVertexData(&borderVertexData, v1, { 0.0f, 0.0f }, colour);
				}
			}

			// Build edge normal & direction render data
			auto edgeIt = polygon.getFirstEdge();
			while (edgeIt != polygon.getEndEdge())
			{
				auto n = polygon.getEdgeNormal(edgeIt);
				auto d = polygon.getEdgeDirection(edgeIt);
				auto c = polygon.getEdgeCentre(edgeIt);

				auto v0 = c + n * 10.0f;
				float colour[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

				// Edge normals
				addVertexData(&polygonEdgeNormalsVertexData, c, { 0.0f, 0.0f }, colour);
				addVertexData(&polygonEdgeNormalsVertexData, v0, { 0.0f, 0.0f }, colour);

				// Edge directions
				auto v1 = c + n * 5.0f;
				auto v2 = c - n * 5.0f;
				auto v3 = c + d * 5.0f;
				addVertexData(&polygonEdgeDirectionsVertexData, v1, { 0.0f, 0.0f }, colour);
				addVertexData(&polygonEdgeDirectionsVertexData, v2, { 0.0f, 0.0f }, colour);
				addVertexData(&polygonEdgeDirectionsVertexData, v3, { 0.0f, 0.0f }, colour);

				edgeIt++;
			}
		}

		polygonIndex = mMesh->getNextPolygonIndex(polygonIndex);
	}

	for (auto const& kvp : mBackgroundMeshes)
	{
		stream->addVertexData(kvp.second.meshId, *kvp.second.vertexData);
	}

	stream->addVertexData(polygonEdgesMeshId, polygonEdgeVertexData);
	stream->addVertexData(borderMeshId, borderVertexData);
	stream->addVertexData(triangulationMeshId, triangulationVertexData);
	stream->addVertexData(vertexMeshId, vertexVertexData);
	stream->addVertexData(polygonEdgeNormalsMeshId, polygonEdgeNormalsVertexData);
	stream->addVertexData(polygonEdgeDirectionsMeshId, polygonEdgeDirectionsVertexData);

	// Tidy up
	for (auto& kvp : mBackgroundMeshes)
	{
		delete kvp.second.vertexData;
		kvp.second.vertexData = nullptr;
	}
}

void TemplateGeometryMeshRenderer::setViewerPolygonIndices(std::vector<uint32_t> const& polygonIndices)
{
	mViewerPolygonIndices = polygonIndices;
}

void TemplateGeometryMeshRenderer::update(wp::BoundingBox const& viewBounds, float frameTime)
{
	StaticRenderer::update(viewBounds, frameTime);

	if (usingLookupGrid())
	{
		auto modelRenderParams = getModelRenderParams();

		// Make all invisible
		for (auto& kvp : mBackgroundMeshes)
		{
			auto const& meshName = kvp.first;
			modelRenderParams->setMeshFlags(meshName, 0);
		}

		// Make sure the viewer is in this mesh
		set<string> viewerMeshNames;
		for (uint32_t polygonIndex: mViewerPolygonIndices)
		{
			auto const& polys = mPolygonIndexLookup[polygonIndex];
			viewerMeshNames.insert(polys.begin(), polys.end());
		}

		// Get visible ones and set
		auto visibleItems = mLookupGrid->getCandidateItemsInBoundingArea(viewBounds);
		for (uint32_t item : visibleItems)
		{
			bool include = !usingGridIncludeMasking() || gridIdInIncludeMask(item);
			bool exclude = usingGridExcludeMasking() && gridIdInExcludeMask(item);

			if (include && !exclude)
			{
				auto const& meshName = mBackgroundMeshLookup[item];

				if (viewerMeshNames.find(meshName) != viewerMeshNames.end())
				{
					modelRenderParams->setMeshFlags(meshName, mpp::ModelRenderParams::Flag_Visible);
				}
			}
		}
	}
}

