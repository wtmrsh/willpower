#include <willpower/application/resourcesystem/ResourceExceptions.h>

#include <willpower/geometry/MeshHelpers.h>
#include <willpower/geometry/MeshOperations.h>
#include <willpower/geometry/EdgeFilter.h>

#include "Map.h"
#include "MapException.h"

using namespace std;
using namespace wp;
using namespace wp::geometry;


Map::Map(string const& name,
	string const& namesp,
	string const& source,
	map<string, string> const& tags,
	application::resourcesystem::ResourceLocation* location)
	: applib::Map(name, namesp, source, tags, location, 512)
{
	registerMeshCreationFunction("testMap1", bind(&Map::testMap1, this, placeholders::_1, placeholders::_2));
}

Map::~Map()
{
}

void Map::testMap1(Mesh* mesh, application::resourcesystem::ResourceManager* resourceMgr)
{
	VAR_UNUSED(resourceMgr);

	auto v0 = mesh->addVertex(wp::geometry::Vertex(-256, -256));
	auto v1 = mesh->addVertex(wp::geometry::Vertex(-256, 256));
	auto v2 = mesh->addVertex(wp::geometry::Vertex(256, 256));
	auto v3 = mesh->addVertex(wp::geometry::Vertex(256, -256));

	auto e0 = mesh->addEdge(wp::geometry::Edge(v0, v1));
	auto e1 = mesh->addEdge(wp::geometry::Edge(v1, v2));
	auto e2 = mesh->addEdge(wp::geometry::Edge(v2, v3));
	auto e3 = mesh->addEdge(wp::geometry::Edge(v3, v0));

	mesh->addPolygon(wp::geometry::Polygon({
		v0, v1, e0,
		v1, v2, e1,
		v2, v3, e2,
		v3, v0, e3
	}));
}