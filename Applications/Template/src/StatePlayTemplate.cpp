#include <algorithm>

#include <willpower/application/StateExceptions.h>

#include <willpower/geometry/MeshQuery.h>

#include <willpower/viz/AccelerationGridRenderer.h>
#include <willpower/viz/DynamicLineRenderer.h>
#include <willpower/viz/CollisionSimulationRenderer.h>
#include <willpower/viz/FirepowerMeshRenderer.h>

#include <applib/ModelInstance.h>
#include <applib/EntityFacade.h>
#include <applib/EntityProperties.h>
#include <applib/VisualSpriteEntityFacade.h>

#include "StatePlayTemplate.h"
#include "TemplateModel.h"
#include "EntityType.h"
#include "BulletType.h"
#include "TriMeshDataProvider.h"
#include "TriMeshEntityFacadeFactory.h"
#include "TemplateGeometryMeshRenderer.h"
#include "Map.h"
#include "MeshAttributes.h"

using namespace std;
using namespace wp;

StatePlayTemplate::StatePlayTemplate()
	: StatePlay()
	, mPlayerCollider(nullptr)
{
}

map<string, tuple<wp::viz::Renderer*, int, bool>> StatePlayTemplate::createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr)
{
	// Map rendering acceleration grid
	auto gridRenderer = new viz::AccelerationGridRenderer("Geometry_Grid", mMapRenderer->getLookupGrid(), 32, renderResourceMgr);
	
	// Handler for player collisions with world
	mCollisionSimDataProvider = make_shared<CollisionSimulationDataProvider>(mMapCollisionSim.get());
	auto collideRenderer = new viz::DynamicLineRenderer("Collision_Sim", mCollisionSimDataProvider, renderResourceMgr);

	// Handler for bullet collisions with world
	auto firepowerRenderer = new viz::FirepowerMeshRenderer("Firepower_World", mMeshCollisionMgr.get(), 32, renderResourceMgr);

	// Debug grid for the map
	mMapGridDataProvider = make_shared<MapGridDataProvider>();
	auto mapGridRenderer = new viz::DynamicLineRenderer("Map_Grid", mMapGridDataProvider, renderResourceMgr);

	return {
		make_pair("Grid", make_tuple(gridRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("CollisionSim", make_tuple(collideRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("Firepower", make_tuple(firepowerRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("MapGrid", make_tuple(mapGridRenderer, (int)RenderOrder::MapDebug, false)),
	};
}

void StatePlayTemplate::setupPlayerCollision()
{
	auto const& playerEnt = getPlayerEntity();
	
	auto const& playerPhysical = applib::ModelInstance::entityHandler()->getEntityComponent<applib::PhysicalStats>(playerEnt);
	mPlayerCollider = new wp::collide::ColliderAABB(playerPhysical.bounds);

	auto cb = [](wp::collide::SweepResult* result, wp::collide::StaticLine const& line, void* user) -> bool
	{
		VAR_UNUSED(user);

		auto const& normal = line.getNormal();

		// Push the result position away from the line a small amount
		result->newPosition += normal * 0.01f; // * MathsUtils::Epsilon;
		result->movementDone = result->newPosition - result->oldPosition;
		result->distanceMoved = result->movementDone.length();

		// Calculate edge normal to slide along
		Winding angleDir;
		float angle = 180 - result->movementDesired.minimumAngleTo(normal, &angleDir);

		Vector2 newDirection = angleDir ==
			Winding::Clockwise ? normal.perpendicular() : -normal.perpendicular();

		// Get remaining movement
		result->movementLeft = newDirection * result->movementDesired.distanceTo(result->movementDone) * sin(WP_DEGTORAD(angle));
		return true;
	};

	mPlayerCollider->setHitLineCallback(cb);
	mMapCollisionSim->addCollider(mPlayerCollider);
	applib::ModelInstance::entityHandler()->setupCollisions(mMapCollisionSim.get(), mPlayerCollider);
}

void StatePlayTemplate::registerInput()
{
	//										Keys pressed/released/down						// Buttons		Wheel up/down
	registerInputState("Up",				{}, {}, { application::Key::UpArrow },			{},	{},	{},		false, false, 0);
	registerInputState("Down",				{}, {}, { application::Key::DownArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Left",				{}, {}, { application::Key::LeftArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Right",				{},	{},	{ application::Key::RightArrow },		{},	{},	{},		false, false, 0);
	registerInputState("FireBullet",		{},	{},	{},										{ application::MouseButton::Left },	{},	{},	false, false, 0);
	registerInputState("EngageBeam",		{ application::Key::Space }, {}, {},			{},	{}, {},		false, false, 0);
	registerInputState("DisengageBeam",		{},	{ application::Key::Space }, {},			{},	{},	{},		false, false, 0);

	// Visuals debugging
	registerInputState("ToggleGridDebug",	{ application::Key::Tab }, {}, {}, {},			{},	{},			false, false, 0);

	// Rendering
	registerInputState("ToggleRenderAccelerationGrid",	{ application::Key::F1 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderCollisionSim",		{ application::Key::F2 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderFirepower",			{ application::Key::F3 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderMapVertices",		{ application::Key::F4 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderMapLines",			{ application::Key::F5 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleMapGrid",					{ application::Key::F6 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleDebugCamera",				{ application::Key::Backspace }, {}, {}, {}, {}, {}, false, false, 0);
}

void StatePlayTemplate::createGameObjects(application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args)
{
	VAR_UNUSED(resourceMgr);
	VAR_UNUSED(renderSystem);
	VAR_UNUSED(renderResourceMgr);
	VAR_UNUSED(args);

	// Add player to collision sim
	setupPlayerCollision();
}

void StatePlayTemplate::destroyGameObjects()
{
}

void StatePlayTemplate::setupEntityFacades()
{
	applib::VisualSpriteEntityFacadeRenderOptions options;

	options.rotationType = wp::viz::RotationOptions::None;

	// Create Quad EntityFacade
	auto quadsResource = mwResourceMgr->getResource("EntityAnimations");
	auto animDatabase = applib::ModelInstance::animationDatabase();
	mEntityMgr->registerFacadeFactory("Sprites", new applib::VisualSpriteEntityFacadeFactory(quadsResource, animDatabase));

	createEntityFacade(
		"Sprites",
		{ (int)EntityType::Player },
		options,
		64);

	// Create triangles EntityFacade
	auto trisProviderFactory = [](auto facade) {
		return std::make_shared<TriMeshDataProvider>(facade);
	};

	mEntityMgr->registerFacadeFactory("TriMesh", new TriMeshEntityFacadeFactory(trisProviderFactory, nullptr));
}

vector<string> StatePlayTemplate::getDebuggingText() const
{
	auto mouseScreen = getMouseScreenPosition();
	auto mouseWorld = getMouseWorldPosition();

	// Get mouse polygon
	auto map = (applib::Map*)mMap.get();
	auto meshQuery = wp::geometry::MeshQuery(map->getMesh().get());

	auto mousePolyIndex = meshQuery.getPolygonContainingPoint(mouseWorld.x, mouseWorld.y);
	
	return {
		STR_FORMAT("Mouse screen: {},{}", mouseScreen.x, mouseScreen.y),
		STR_FORMAT("Mouse world: {},{}", mouseWorld.x, mouseWorld.y),
		STR_FORMAT("Mouse poly: {}", mousePolyIndex)
	};
}

void StatePlayTemplate::setupEntities()
{
	Vector2 playerPos(0, 0);

	createEntity((int)EntityType::Player, playerPos, 0, true);
}

void StatePlayTemplate::updatePreInput(float frameTime)
{
	VAR_UNUSED(frameTime);
}

void StatePlayTemplate::updatePostEntities(float frameTime)
{
	VAR_UNUSED(frameTime);
}

void StatePlayTemplate::updateActions(vector<string> const& activeStates, float frameTime)
{
	VAR_UNUSED(frameTime);

	for (auto const& state : activeStates)
	{
		// Visual debugging
		if (state == "ToggleGridDebug")
		{
			mMapRenderer->useGridIncludeMasking(!mMapRenderer->usingGridIncludeMasking());
		}
		else if (state == "ToggleRenderAccelerationGrid")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Grid"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderCollisionSim")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["CollisionSim"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderFirepower")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Firepower"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderMapVertices")
		{
			auto params = static_cast<viz::GeometryMeshRenderParams*>(mMapRenderer->getParams().get());
			params->setRenderVertices(!params->getRenderVertices());
			mMapRenderer->updateRenderParams();
		}
		else if (state == "ToggleRenderMapLines")
		{
			auto params = static_cast<viz::GeometryMeshRenderParams*>(mMapRenderer->getParams().get());
			params->setRenderPolygonEdges(!params->getRenderPolygonEdges());
			mMapRenderer->updateRenderParams();
		}
		else if (state == "ToggleMapGrid")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["MapGrid"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "Triggers")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Triggers"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleDebugCamera")
		{
			useDebugCamera(!usingDebugCamera());
		}
	}
}

void StatePlayTemplate::updatePreRenderers(float frameTime)
{
	auto viewBounds = getViewBounds();

	// Update map grid renderer
	auto mapGridRenderer = get<0>(mAdditionalRenderers["MapGrid"]);
	if (mapGridRenderer->isVisible())
	{
		mMapGridDataProvider->update(static_cast<Map*>(mMap.get())->getGridOptions().cellSize, viewBounds, frameTime);
	}

	// Update collision sim renderer
	auto collisionSimRenderer = get<0>(mAdditionalRenderers["CollisionSim"]);
	if (collisionSimRenderer->isVisible())
	{
		mCollisionSimDataProvider->update(viewBounds, frameTime);
	}

	// Update map grid renderer
	if (mMapRenderer->usingGridIncludeMasking())
	{
		mMapRenderer->clearGridIncludeMask();

		auto grid = mMapRenderer->getLookupGrid();
		auto mouseWorldPos = getMouseWorldPosition();

		int cellX, cellY;
		grid->getContainingCell(true, mouseWorldPos.x, mouseWorldPos.y, cellX, cellY);
		if (cellX >= 0 && cellY >= 0)
		{
			auto const& items = grid->_getCellItems(cellX, cellY);
			for (auto item : items)
			{
				mMapRenderer->addToGridIncludeMask(item);
			}
		}
	}
}
