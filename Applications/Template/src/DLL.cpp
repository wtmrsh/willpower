#include "willpower/common/Logger.h"

#include "willpower/application/StateFactory.h"

#include <applib/ModelInstance.h>
#include <applib/StateLoad.h>
#include <applib/StateUnload.h>
#include <applib/StateMapLoad.h>
#include <applib/StateMapUnload.h>
#include <applib/StateMapTransition.h>
#include <applib/Game.h>
#include <applib/MapDefaultDefinitionFactory.h>
#include <applib/MapTiledDefinitionFactory.h>
#include <applib/ProtoEntityDefaultDefinitionFactory.h>
#include <applib/ImageSetTiledDefinitionFactory.h>

#include "MapTemplateDefinitionFactory.h"
#include "GameDefinitionFactory.h"
#include "ProtoEntityDefinitionFactory.h"

// Model
#include "TemplateModel.h"
#include "EntityHandlerTemplate.h"

// States
#include "StateControllerTemplate.h"
#include "StatePlayTemplate.h"

// Resources
#include "Game.h"
#include "Map.h"
#include "ProtoEntity.h"

#include "Animation.h"

#include "TemplateGeometryMeshRenderer.h"

using namespace std;

// Model
static applib::Model* model = nullptr;

// State factories
static int nextStateFactory = 0;
static StateControllerTemplateFactory* stateControllerFactory = nullptr;
static applib::StateLoadFactory* stateLoadFactory = nullptr;
static applib::StateUnloadFactory* stateUnloadFactory = nullptr;
static applib::StateMapLoadFactory* stateMapLoadFactory = nullptr;
static applib::StateMapUnloadFactory* stateMapUnloadFactory = nullptr;
static applib::StateMapTransitionFactory* stateMapTransitionFactory = nullptr;
static StatePlayTemplateFactory* statePlayTemplateFactory = nullptr;

// Other
TemplateGeometryMeshRendererFactory* meshRendererFactory = nullptr;

extern "C"
{
	__declspec(dllexport) char const* dllGetName()
	{
		return "Template";
	}

	__declspec(dllexport) void dllSetArgument(char const* arg, char const* value)
	{
		VAR_UNUSED(arg);
		VAR_UNUSED(value);
	}

	__declspec(dllexport) wp::application::StateFactory* dllGetNextStateFactory()
	{
		wp::application::StateFactory* stateFactory;
		switch (nextStateFactory)
		{
		case 0:
			stateFactory = stateControllerFactory;
			break;
		case 1:
			stateFactory = stateLoadFactory;
			break;
		case 2:
			stateFactory = stateUnloadFactory;
			break;
		case 3:
			stateFactory = stateMapLoadFactory;
			break;
		case 4:
			stateFactory = stateMapUnloadFactory;
			break;
		case 5:
			stateFactory = stateMapTransitionFactory;
			break;
		case 6:
			stateFactory = statePlayTemplateFactory;
			break;
		default:
			stateFactory = nullptr;
			break;
		}

		nextStateFactory++;
		return stateFactory;
	}

	__declspec(dllexport) void dllOnEntry(wp::Logger* logger, wp::application::resourcesystem::ResourceManager* resourceMgr)
	{
		auto entityHandlerFactory = [](shared_ptr<applib::AnimationDatabase> animDatabase)
		{
			return new EntityHandlerTemplate(animDatabase);
		};

		model = new TemplateModel(entityHandlerFactory, resourceMgr);
		applib::ModelInstance::set(model);

		meshRendererFactory = new TemplateGeometryMeshRendererFactory();

		// Create state factories
		stateControllerFactory = new StateControllerTemplateFactory(logger);
		stateLoadFactory = new applib::StateLoadFactory(logger, resourceMgr, true);
		stateUnloadFactory = new applib::StateUnloadFactory(logger, resourceMgr, true);
		stateMapLoadFactory = new applib::StateMapLoadFactory(logger, resourceMgr, meshRendererFactory, true);
		stateMapUnloadFactory = new applib::StateMapUnloadFactory(logger, resourceMgr, true);
		stateMapTransitionFactory = new applib::StateMapTransitionFactory(logger, resourceMgr, meshRendererFactory, true);
		statePlayTemplateFactory = new StatePlayTemplateFactory(logger);

		// Add resource factories
		resourceMgr->addResourceFactory(new GameResourceFactory(model->animationDatabase));
		resourceMgr->addResourceFactory(new MapResourceFactory());

		resourceMgr->addResourceFactory(new ProtoEntityResourceFactory(model->entityHandler, model->animationDatabase));

		// Add resource definition factories
		resourceMgr->addResourceDefinitionFactory(new GameDefinitionFactory());
		resourceMgr->addResourceDefinitionFactory(new MapTemplateDefinitionFactory());
		resourceMgr->addResourceDefinitionFactory(new applib::MapTiledDefinitionFactory());
		resourceMgr->addResourceDefinitionFactory(new ProtoEntityDefinitionFactory());
		resourceMgr->addResourceDefinitionFactory(new applib::ImageSetTiledDefinitionFactory());
	}

	__declspec(dllexport) void dllOnExit()
	{
		// Destroy state factories
		delete stateControllerFactory;
		stateControllerFactory = nullptr;

		delete stateLoadFactory;
		stateLoadFactory = nullptr;

		delete stateUnloadFactory;
		stateUnloadFactory = nullptr;

		delete stateMapLoadFactory;
		stateMapLoadFactory = nullptr;

		delete stateMapUnloadFactory;
		stateMapUnloadFactory = nullptr;

		delete stateMapTransitionFactory;
		stateMapTransitionFactory = nullptr;

		delete statePlayTemplateFactory;
		statePlayTemplateFactory = nullptr;

		// Model
		delete model;
		model = nullptr;

		// Misc
		delete meshRendererFactory;
		meshRendererFactory = nullptr;
	}

}