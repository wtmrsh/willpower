#include <utils/XmlReader.h>

#include <willpower/application/resourcesystem/ResourceExceptions.h>
#include <willpower/common/Exceptions.h>

#include <applib/MapGeometryObjectAttributes.h>

#include "HiveWorldMapDefinitionFactory.h"
#include "MeshAttributes.h"


HiveWorldMapDefinitionFactory::HiveWorldMapDefinitionFactory()
	: applib::MapResourceDefinitionFactory("HiveWorld",
		new applib::VertexAttributeFactory,
		new applib::EdgeAttributeFactory,
		new HiveWorldPolygonAttributeFactory,
		new HiveWorldPolygonVertexAttributeFactory)
{
}

void HiveWorldMapDefinitionFactory::create(wp::application::resourcesystem::Resource* resource, wp::application::resourcesystem::ResourceManager* resourceMgr, utils::XmlNode* node)
{
	auto mapRes = static_cast<applib::Map*>(resource);

	// Create mesh
	createMesh(mapRes);

	// Load from code, or wherver
	auto funcNode = node->getChild("Function");
	auto funcName = funcNode->getValue();
	auto func = mapRes->getMeshCreationFunction(funcName);

	func(mapRes->getMesh().get(), resourceMgr);
}
