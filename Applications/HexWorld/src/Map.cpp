#include <willpower/application/resourcesystem/ResourceExceptions.h>

#include <willpower/geometry/MeshHelpers.h>
#include <willpower/geometry/MeshOperations.h>
#include <willpower/geometry/EdgeFilter.h>

#include <applib/ModelInstance.h>

#include "Hive/Build/HiveBuilder.h"

#include "Map.h"
#include "MapException.h"
#include "HiveWorldModel.h"

using namespace std;
using namespace wp;
using namespace wp::geometry;


Map::Map(string const& name,
	string const& namesp,
	string const& source,
	map<string, string> const& tags,
	application::resourcesystem::ResourceLocation* location)
	: applib::Map(name, namesp, source, tags, location, 512)
	, mGenerator(nullptr)
	, mMiniMap(nullptr)
	, mWorld(nullptr)
{
	registerMeshCreationFunction("testMap1", bind(&Map::testMap1, this, placeholders::_1, placeholders::_2));
}

Map::~Map()
{
	delete mGenerator;
	delete mMiniMap;
	delete mWorld;
}

MiniMap const* Map::getMiniMap() const
{
	return mMiniMap;
}

hive::live::HiveWorld* Map::getWorld()
{
	return mWorld;
}

wp::Vector2 const& Map::getPlayerStartPosition() const
{
	return mPlayerStartPosition;
}

hive::live::HiveWorld* Map::createHiveWorld(hive::build::HiveBuilder const& builder)
{
	return builder.createHiveWorld();
}

void Map::testMap1(Mesh* mesh, application::resourcesystem::ResourceManager* resourceMgr)
{
	// Create map generator instance for a hex world
	delete mGenerator;
		
	mGenerator = new hive::gen::Generator(8, 0.8f);
		
	mGenerator->initialise();

	if (!mGenerator->placePrefab(hive::gen::RoomPrefabType::Arena1, hex::Hex(0, 0), 0))
	{
		throw exception("Could not place prefab");
	}
	
	if (!mGenerator->placePrefab(hive::gen::RoomPrefabType::Arena1, hex::Hex(3, -3), 1))
	{
		throw exception("Could not place prefab");
	}

	if (!mGenerator->placePrefab(hive::gen::RoomPrefabType::Arena1, hex::Hex(-3, 0), 2))
	{
		throw exception("Could not place prefab");
	}
	
	mGenerator->generate();
	
	// Build the game world
	auto layout = static_cast<HiveWorldModel*>(applib::ModelInstance::get())->getLayout();

	auto builder = hive::build::HiveBuilder(
		layout,	{
		128,	// Connection length
		96,		// Connection width
		16		// Door depth
	});
	
	// Mesh must be built first as it has geometry that the HiveWold needs to know about
	builder.buildMapMesh(mGenerator, mesh, resourceMgr);

	mWorld = createHiveWorld(builder);

	// Create minimap
	delete mMiniMap;
	mMiniMap = new MiniMap();

	mMiniMap->create(mGenerator);

	// Set player start position
	mPlayerStartPosition = layout.toWorld(hex::Hex(0, -1));
}