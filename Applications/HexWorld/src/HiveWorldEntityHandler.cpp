#include <willpower/common/Vector2.h>

#include <applib/Bullet.h>
#include <applib/BeamData.h>

#include "HiveWorldEntityHandler.h"
#include "EntityType.h"
#include "Animation.h"
#include "BulletType.h"
#include "GameException.h"
#include "EntityProperties.h"

using namespace std;
using namespace wp;
using namespace applib;


map<EntityType, string> gEntityTypeNames = {
	{EntityType::Player, "Player"}
};

HiveWorldEntityHandler::HiveWorldEntityHandler(shared_ptr<AnimationDatabase> animationDatabase)
	: EntityHandler()
	, mAnimationDatabase(animationDatabase)
{
}

string HiveWorldEntityHandler::getPrototypeName(int type)
{
	auto it = gEntityTypeNames.find((EntityType)type);

	if (it != gEntityTypeNames.end())
	{
		return it->second;
	}
	else
	{
		throw GameException(STR_FORMAT("Unknown entity type: {}", type));
	}
}

void HiveWorldEntityHandler::setupImpl(Entity* entity)
{
	VAR_UNUSED(entity);
}

void HiveWorldEntityHandler::destroyImpl(Entity *entity)
{
	VAR_UNUSED(entity);
}

bool HiveWorldEntityHandler::updateImpl(Entity *entity, bool inputControlled, float frameTime)
{
	if (inputControlled)
	{
		auto velocity = Vector2::ZERO;
		float playerSpeed{ 60 };

		for (auto const& state : mActiveInputStates)
		{
			if (state == "Up")
			{
				velocity.y += playerSpeed * frameTime;
			}
			else if (state == "Down")
			{
				velocity.y -= playerSpeed * frameTime;
			}
			else if (state == "Left")
			{
				velocity.x -= playerSpeed * frameTime;
			}
			else if (state == "Right")
			{
				velocity.x += playerSpeed * frameTime;
			}
			else if (state == "FireBullet")
			{
				int bulletType = rand() % 3;

				applib::BulletParams params{
					128.0f,
					-1.0f,
					BULLET_HIT_EXPLODE
				};

				applib::BulletFireParams fireParams{
					0.0f,
				};

				fireBullet(entity, (int)BulletType::RedDot + bulletType, params, fireParams);
			}
			else if (state == "EngageBeam")
			{
				// TODO: instead of this action, we want a "fire selected weapon" action
				// ...

				auto& beamEmitter = getEntityComponent<applib::BeamEmitter>(*entity);
				beamEmitter.id = mwBeamMgr->addBeam(0, { 32, 128 }, entity);
			}
			else if (state == "DisengageBeam")
			{
				auto& beamEmitter = getEntityComponent<applib::BeamEmitter>(*entity);
				mwBeamMgr->removeBeam(beamEmitter.id);
				beamEmitter.id = -1;
			}
		}

		// Set desired movement
		velocity.normalise();
		mwPlayerCollider->setMovement(velocity * 200);

		// Set angle (view/facing angle, not movement direction).
		auto& physicalStats = getEntityComponent<PhysicalStats>(*entity);
		Vector2 dMouse = mMouseWorld - physicalStats.position;

		dMouse.normalise();
		physicalStats.angle = Vector2::UNIT_Y.clockwiseAngleTo(dMouse);
	}
	else
	{
		auto entityType = entity->getType();

		switch (entityType)
		{
		case (int)EntityType::Player:
			break;

		default:
			throw GameException(STR_FORMAT("Unhandled entity type: {}", (int)entityType));
		}
	}

	return true;
}

void HiveWorldEntityHandler::updateVisual(Entity* entity, float frameTime)
{
	auto visual = mComponentRegistry.try_get<VisualSprite>(entity->mCompSysId);
	if (!visual)
	{
		return;
	}

	auto const& anim = mAnimationDatabase->getAnimation((uint32_t)visual->animation);
	auto frame = mAnimationDatabase->getAnimationFrame((uint32_t)visual->animation, visual->frame);

	visual->timer += frameTime;
	while (visual->timer >= frame.time)
	{
		visual->timer -= frame.time;
		visual->frame += visual->direction;

		// Check if we've hit the end of the animation
		if (visual->frame == 0 || visual->frame == anim.count)
		{
			switch (anim.style)
			{
			case AnimationDatabase::LoopStyle::Forwards:
				visual->frame = 0;
				break;

			case AnimationDatabase::LoopStyle::Once:
				visual->frame = anim.count - 1;
				break;

			case AnimationDatabase::LoopStyle::PingPong:
				visual->direction *= -1;
				visual->frame += visual->direction;
				break;

			default:
				throw Exception("Unknown loop style.  Must be one of Forwards|Once|PingPoing.");
			}
		}

		// Get next frame details, in case we have skipped past the new frame entirely
		frame = mAnimationDatabase->getAnimationFrame((uint32_t)visual->animation, visual->frame);
	}
}

bool HiveWorldEntityHandler::update(Entity* entity, bool controlActive, float frameTime)
{
	// Update logic
	bool inputControlled = controlActive && (entity->getId() == 0);
	bool alive = updateImpl(entity, inputControlled, frameTime);

	if (inputControlled)
	{
		// Update simulation and player
		if (mwSimulation)
		{
			mwSimulation->update(frameTime);

			// Update position
			auto physicalStats = mComponentRegistry.try_get<PhysicalStats>(entity->mCompSysId);
			if (physicalStats)
			{
				physicalStats->position = mwPlayerCollider->getCentre();
			}
		}
	}

	if (alive)
	{
		// Update visual
		updateVisual(entity, frameTime);

		// Update beam emitter
		auto physicalStats = mComponentRegistry.try_get<PhysicalStats>(entity->mCompSysId);
		auto beamEmitter = mComponentRegistry.try_get<BeamEmitter>(entity->mCompSysId);
		if (physicalStats && beamEmitter)
		{
			beamEmitter->anchorPos = physicalStats->position;
			beamEmitter->anchorAngle = physicalStats->angle;
		}
	}

	return alive;
}