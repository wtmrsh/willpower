#include "MiniMap.h"

using namespace std;


MiniMap::MiniMap()
{
}

hive::gen::CellDataMap const& MiniMap::getCellGenData() const
{
	return mCellGenData;
}

vector<hive::gen::Room> const& MiniMap::getRooms() const
{
	return mRooms;
}

void MiniMap::create(hive::gen::Generator* generator)
{
	mCellGenData = generator->getCellGenData();
	mRooms = generator->getRooms();
}