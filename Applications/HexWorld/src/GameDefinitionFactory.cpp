#include <map>

#include "GameDefinitionFactory.h"
#include "BulletType.h"
#include "GameException.h"

using namespace std;


static map<string, BulletType> gBulletNameIdMapping{
	{ "RedDot", BulletType::RedDot },
	{ "Fireball", BulletType::Fireball },
	{ "Comet", BulletType::Comet }
};

GameDefinitionFactory::GameDefinitionFactory()
	: applib::GameDefaultDefinitionFactory()
{
}

uint32_t GameDefinitionFactory::getBulletIdFromName(string const& name)
{
	try
	{
		return (uint32_t)gBulletNameIdMapping[name];
	}
	catch (std::out_of_range&)
	{
		throw GameException(STR_FORMAT("Unknown bullet type: {}", name));
	}
}