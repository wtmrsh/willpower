#include <algorithm>

#include <willpower/application/StateExceptions.h>

#include <willpower/geometry/MeshQuery.h>

#include <willpower/viz/AccelerationGridRenderer.h>
#include <willpower/viz/DynamicLineRenderer.h>
#include <willpower/viz/DynamicTriangleRenderer.h>
#include <willpower/viz/CollisionSimulationRenderer.h>
#include <willpower/viz/FirepowerMeshRenderer.h>

#include <applib/ModelInstance.h>
#include <applib/EntityFacade.h>
#include <applib/EntityProperties.h>
#include <applib/VisualSpriteEntityFacade.h>

#include "HiveWorldStatePlay.h"
#include "HiveWorldModel.h"
#include "EntityType.h"
#include "BulletType.h"
#include "TriMeshDataProvider.h"
#include "TriMeshEntityFacadeFactory.h"
#include "HiveWorldGeometryMeshRenderer.h"
#include "Map.h"
#include "MeshAttributes.h"

using namespace std;
using namespace wp;


HiveWorldStatePlay::HiveWorldStatePlay()
	: StatePlay()
	, mPlayerCollider(nullptr)
{
}

map<string, tuple<wp::viz::Renderer*, int, bool>> HiveWorldStatePlay::createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr)
{
	// Door renderer
	viz::TriangleRendererOptions doorRenderOptions
	{
		false,
		false,
		false,
		false,
		renderResourceMgr->getResource("__mpp_tex_none__")
	};

	mDoorRendererDataProvider = make_shared<DoorRendererDataProvider>();
	auto doorRenderer = new DoorRenderer("Doors", doorRenderOptions, mDoorRendererDataProvider, renderResourceMgr);

	// Map rendering acceleration grid
	auto gridRenderer = new viz::AccelerationGridRenderer("Geometry_Grid", mMapRenderer->getLookupGrid(), 32, renderResourceMgr);
	
	// Handler for player collisions with world
	mCollisionSimDataProvider = make_shared<CollisionSimulationDataProvider>(mMapCollisionSim.get());
	auto collideRenderer = new viz::DynamicLineRenderer("Collision_Sim", mCollisionSimDataProvider, renderResourceMgr);

	// Handler for bullet collisions with world
	auto firepowerRenderer = new viz::FirepowerMeshRenderer("Firepower_World", mMeshCollisionMgr.get(), 32, renderResourceMgr);

	// Debug grid for the map
	mMapGridDataProvider = make_shared<MapGridDataProvider>();
	auto mapGridRenderer = new viz::DynamicLineRenderer("Map_Grid", mMapGridDataProvider, renderResourceMgr);

	// Triggers
	mTriggerDataProvider = make_shared<TriggerDataProvider>();
	auto triggerRenderer = new viz::DynamicLineRenderer("Triggers", mTriggerDataProvider, renderResourceMgr);

	// Mini map
	auto mapResource = mMap.get();
	auto miniMap = static_cast<::Map*>(mapResource)->getMiniMap();

	auto builder = hive::build::HiveBuilder(static_cast<HiveWorldModel*>(applib::ModelInstance::get())->getLayout(), {});
	mMiniMapDataProvider = make_shared<MiniMapDataProvider>(miniMap, builder);

	wp::viz::TriangleRendererOptions miniMapRenderOptions{
		false,
		false,
		false,
		false,
		renderResourceMgr->getResource("__mpp_tex_none__")
	};

	auto miniMapRenderer = 
		new viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>(
			"MiniMap", miniMapRenderOptions, mMiniMapDataProvider, renderResourceMgr);

	// Set map render params
	auto params = static_cast<viz::GeometryMeshRenderParams*>(mMapRenderer->getParams().get());
	params->setRenderVertices(false);
	params->setRenderPolygonEdges(false);
	mMapRenderer->updateRenderParams();

	return {
		make_pair("Doors", make_tuple(doorRenderer, (int)RenderOrder::Entities, true)),
		make_pair("Grid", make_tuple(gridRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("CollisionSim", make_tuple(collideRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("Firepower", make_tuple(firepowerRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("MapGrid", make_tuple(mapGridRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("Triggers", make_tuple(triggerRenderer, (int)RenderOrder::MapDebug, false)),
		make_pair("MiniMap", make_tuple(miniMapRenderer, (int)RenderOrder::MapDebug, false))
	};
}

void HiveWorldStatePlay::setupPlayerCollision()
{
	auto const& playerEnt = getPlayerEntity();
	
	auto const& playerPhysical = applib::ModelInstance::entityHandler()->getEntityComponent<applib::PhysicalStats>(playerEnt);
	mPlayerCollider = new wp::collide::ColliderAABB(playerPhysical.bounds);

	auto cb = [](wp::collide::SweepResult* result, wp::collide::StaticLine const& line, void* user) -> bool
	{
		VAR_UNUSED(user);

		auto const& normal = line.getNormal();

		// Push the result position away from the line a small amount
		result->newPosition += normal * 0.01f; // * MathsUtils::Epsilon;
		result->movementDone = result->newPosition - result->oldPosition;
		result->distanceMoved = result->movementDone.length();

		// Calculate edge normal to slide along
		Winding angleDir;
		float angle = 180 - result->movementDesired.minimumAngleTo(normal, &angleDir);

		Vector2 newDirection = angleDir ==
			Winding::Clockwise ? normal.perpendicular() : -normal.perpendicular();

		// Get remaining movement
		result->movementLeft = newDirection * result->movementDesired.distanceTo(result->movementDone) * sin(WP_DEGTORAD(angle));
		return true;
	};

	mPlayerCollider->setHitLineCallback(cb);
	mMapCollisionSim->addCollider(mPlayerCollider);
	applib::ModelInstance::entityHandler()->setupCollisions(mMapCollisionSim.get(), mPlayerCollider);
}

void HiveWorldStatePlay::registerInput()
{
	//										Keys pressed/released/down						// Buttons		Wheel up/down
	registerInputState("Up",				{}, {}, { application::Key::UpArrow },			{},	{},	{},		false, false, 0);
	registerInputState("Down",				{}, {}, { application::Key::DownArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Left",				{}, {}, { application::Key::LeftArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Right",				{},	{},	{ application::Key::RightArrow },		{},	{},	{},		false, false, 0);
	registerInputState("FireBullet",		{},	{},	{},										{ application::MouseButton::Left },	{},	{},	false, false, 0);
	registerInputState("EngageBeam",		{ application::Key::Space }, {}, {},			{},	{}, {},		false, false, 0);
	registerInputState("DisengageBeam",		{},	{ application::Key::Space }, {},			{},	{},	{},		false, false, 0);
	registerInputState("OpenDoor",			{}, {}, {},										{ application::MouseButton::Right }, {}, {}, false, false, 0);

	// Gameplay debugging
	registerInputState("CycleConnectors",	{ application::Key::C }, {}, {}, {},			{}, {},			false, false, 0);

	// Visuals debugging
	registerInputState("ToggleGridDebug",	{ application::Key::Tab }, {}, {}, {},			{},	{},			false, false, 0);

	// Rendering
	registerInputState("ToggleRenderAccelerationGrid",	{ application::Key::F1 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderCollisionSim",		{ application::Key::F2 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderFirepower",			{ application::Key::F3 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderMapVertices",		{ application::Key::F4 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleRenderMapLines",			{ application::Key::F5 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("ToggleMapGrid",					{ application::Key::F6 }, {}, {},	{}, {}, {},		false, false, 0);
	registerInputState("Triggers",						{ application::Key::F7 }, {}, {}, {}, {}, {},		false, false, 0);
	registerInputState("MiniMap",						{ application::Key::F8 }, {}, {}, {}, {}, {},		false, false, 0);
	registerInputState("ToggleDebugCamera",				{ application::Key::Backspace }, {}, {}, {}, {}, {}, false, false, 0);
}

void HiveWorldStatePlay::setupWorldDynamicCollisions()
{
	auto world = static_cast<HiveWorldModel*>(applib::ModelInstance::get())->world;
	uint32_t doorIndices[2];

	for (uint32_t i = 0; i < world->getNumConnectors(); ++i)
	{
		auto const* connector = world->getConnector(i);
		doorIndices[0] = connector->getDoorIndex0();
		doorIndices[1] = connector->getDoorIndex1();

		for (int j = 0; j < 2; ++j)
		{
			auto* door = world->getDoor(doorIndices[j]);

			wp::Vector2 v0, v1, v2, v3;
			door->getVertices(1.0f, v0, v1, v2, v3);

			door->setCollisionLineRanges(
				mMapCollisionSim->addStaticLine(v0, v3),
				mMapCollisionSim->addStaticLine(v1, v2)
			);
		}
	}
}

void HiveWorldStatePlay::createGameObjects(application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args)
{
	VAR_UNUSED(resourceMgr);
	VAR_UNUSED(renderSystem);
	VAR_UNUSED(renderResourceMgr);
	VAR_UNUSED(args);

	// Create HiveWorld
	auto world = static_cast<::Map*>(mMap.get())->getWorld();
	static_cast<HiveWorldModel*>(applib::ModelInstance::get())->setWorld(world);

	// Add dynamic collision primitives from the HiveWorld to the collision simulation
	setupWorldDynamicCollisions();

	// Add player to collision sim
	setupPlayerCollision();
}

void HiveWorldStatePlay::destroyGameObjects()
{
	static_cast<HiveWorldModel*>(applib::ModelInstance::get())->deleteWorld();
}

void HiveWorldStatePlay::setupEntityFacades()
{
	applib::VisualSpriteEntityFacadeRenderOptions options;

	options.rotationType = wp::viz::RotationOptions::None;

	// Create Quad EntityFacade
	auto quadsResource = mwResourceMgr->getResource("EntityAnimations");
	auto animDatabase = applib::ModelInstance::animationDatabase();
	mEntityMgr->registerFacadeFactory("Sprites", new applib::VisualSpriteEntityFacadeFactory(quadsResource, animDatabase));

	createEntityFacade(
		"Sprites",
		{ (int)EntityType::Player },
		options,
		64);

	// Create triangles EntityFacade
	auto trisProviderFactory = [](auto facade) {
		return std::make_shared<TriMeshDataProvider>(facade);
	};

	mEntityMgr->registerFacadeFactory("TriMesh", new TriMeshEntityFacadeFactory(trisProviderFactory, nullptr));
}

vector<string> HiveWorldStatePlay::getDebuggingText() const
{
	auto mouseScreen = getMouseScreenPosition();
	auto mouseWorld = getMouseWorldPosition();

	// Get mouse polygon
	auto map = (applib::Map*)mMap.get();
	auto meshQuery = wp::geometry::MeshQuery(map->getMesh().get());

	auto mousePolyIndex = meshQuery.getPolygonContainingPoint(mouseWorld.x, mouseWorld.y);
	
	auto playerPos = getPlayerPosition();
	auto playerPolyIndex = meshQuery.getPolygonContainingPoint(playerPos.x, playerPos.y);

	return {
		STR_FORMAT("Mouse screen: {},{}", mouseScreen.x, mouseScreen.y),
		STR_FORMAT("Mouse world: {},{}", mouseWorld.x, mouseWorld.y),
		STR_FORMAT("Mouse poly: {}", mousePolyIndex),
		STR_FORMAT("Player poly: {}", playerPolyIndex)
	};
}

void HiveWorldStatePlay::setupEntities()
{
	Vector2 playerPos = getMap()->getPlayerStartPosition();

	createEntity((int)EntityType::Player, playerPos, 0, true);
}

void HiveWorldStatePlay::updatePreInput(float frameTime)
{
	// Store player position from previous frame for trigger checks.
	mPrevPlayerPosition = getPlayerPosition();
	mPrevPlayerLocator = getPlayerLocator();

	getWorld()->update(mMapCollisionSim.get(), frameTime);
}

void HiveWorldStatePlay::checkLocationChange()
{
	auto curPlayerLocator = getPlayerLocator();

	if (curPlayerLocator != mPrevPlayerLocator)
	{
		auto world = getWorld();

		world->onExitArea(mPrevPlayerLocator, mMapCollisionSim.get());
		world->onEnterArea(curPlayerLocator, mMapCollisionSim.get());
	}
}

void HiveWorldStatePlay::checkTriggers()
{
	getWorld()->checkTriggers(mPrevPlayerPosition, getPlayerPosition(), getPlayerLocator());
}

void HiveWorldStatePlay::updatePostEntities(float frameTime)
{
	VAR_UNUSED(frameTime);

	checkLocationChange();
	checkTriggers();
}

void HiveWorldStatePlay::updateActions(vector<string> const& activeStates, float frameTime)
{
	VAR_UNUSED(frameTime);

	auto world = getWorld();
	for (auto const& state : activeStates)
	{
		if (state == "OpenDoor")
		{
			auto worldPos = getMouseWorldPosition();

			auto locator = getPlayerLocator();

			if (locator.type == HexLocator::Type::Room)
			{
				// Check mouse against all doors
				auto const* room = world->getRoom(locator.typeIndex);
				for (auto doorIndex : room->getDoorIndices())
				{
					auto const* door = world->getDoor(doorIndex);

					wp::Vector2 dv0, dv1, dv2, dv3;
					door->getVertices(1.0f, dv0, dv1, dv2, dv3);

					if (wp::MathsUtils::pointInConvexPolygon(worldPos, { dv0, dv1, dv2, dv3 }))
					{
						auto connector = world->getConnector(door->getConnectorIndex());
						connector->tryCycleOpen(doorIndex, world);
					}
				}
			}
		}

		// Gameplay debugging
		if (state == "CycleConnectors")
		{
			auto room = world->getRoom(getPlayerLocator());
			room->tryOpenAllConnectors(world);
		}

		// Visual debugging
		if (state == "ToggleGridDebug")
		{
			mMapRenderer->useGridIncludeMasking(!mMapRenderer->usingGridIncludeMasking());
		}
		else if (state == "ToggleRenderAccelerationGrid")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Grid"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderCollisionSim")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["CollisionSim"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderFirepower")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Firepower"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleRenderMapVertices")
		{
			auto params = static_cast<viz::GeometryMeshRenderParams*>(mMapRenderer->getParams().get());
			params->setRenderVertices(!params->getRenderVertices());
			mMapRenderer->updateRenderParams();
		}
		else if (state == "ToggleRenderMapLines")
		{
			auto params = static_cast<viz::GeometryMeshRenderParams*>(mMapRenderer->getParams().get());
			params->setRenderPolygonEdges(!params->getRenderPolygonEdges());
			mMapRenderer->updateRenderParams();
		}
		else if (state == "ToggleMapGrid")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["MapGrid"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "Triggers")
		{
			auto const&[renderer, order, show] = mAdditionalRenderers["Triggers"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "MiniMap")
		{
			auto const& [renderer, order, show] = mAdditionalRenderers["MiniMap"];
			renderer->setVisible(!renderer->isVisible());
		}
		else if (state == "ToggleDebugCamera")
		{
			useDebugCamera(!usingDebugCamera());
		}
	}
}

hive::live::HiveWorld* HiveWorldStatePlay::getWorld() const
{
	return static_cast<HiveWorldModel*>(applib::ModelInstance::get())->world;
}

::Map* HiveWorldStatePlay::getMap()
{
	return static_cast<Map*>(mMap.get());
}

HexLocator HiveWorldStatePlay::getPlayerLocator()
{
	auto mapMesh = getMap()->getMesh().get();
	auto meshQuery = wp::geometry::MeshQuery(mapMesh);
	auto playerPos = getPlayerPosition();
	auto playerPolyIndex = meshQuery.getPolygonContainingPoint(playerPos.x, playerPos.y);

	if (playerPolyIndex >= 0)
	{
		auto const* polyAttrs = static_cast<HiveWorldPolygonAttributes const*>(mapMesh->getPolygonUserData(playerPolyIndex));
		return polyAttrs->locator;
	}
	else
	{
		return {};
	}
}

wp::Vector2 const& HiveWorldStatePlay::getPlayerPosition() const
{
	return mPlayerCollider->getCentre();
}

void HiveWorldStatePlay::updatePreRenderers(float frameTime)
{
	auto viewBounds = getViewBounds();

	auto world = getWorld();

	// Calculate visibility for map/door renderers
	vector<uint32_t> visiblePolygonIndices, visibleDoorIndices;
	vector<HexLocator> visibleAreas;

	auto playerLocator = getPlayerLocator();

	if (playerLocator.type != HexLocator::Type::None &&
		playerLocator.type != HexLocator::Type::Unknown)
	{
		visibleAreas = getWorld()->getVisibleAreas(playerLocator);

		for (auto const& area : visibleAreas)
		{
			for (uint32_t polygonIndex : area.polygonIndices)
			{
				visiblePolygonIndices.push_back(polygonIndex);
			}

			if (area.type == HexLocator::Type::Door)
			{
				visibleDoorIndices.push_back(area.typeIndex);
			}
		}
	}

	static_cast<HiveWorldGeometryMeshRenderer*>(mMapRenderer.get())->setViewerPolygonIndices(visiblePolygonIndices);

	// Update door renderer
	auto doorRenderer = get<0>(mAdditionalRenderers["Doors"]);
	if (doorRenderer->isVisible())
	{
		mDoorRendererDataProvider->update(world, visibleDoorIndices, frameTime);
	}

	// Update trigger renderer
	auto triggerRenderer = get<0>(mAdditionalRenderers["Triggers"]);
	if (triggerRenderer->isVisible())
	{
		mTriggerDataProvider->update(world, visibleAreas, viewBounds, frameTime);
	}

	// Update map grid renderer
	auto mapGridRenderer = get<0>(mAdditionalRenderers["MapGrid"]);
	if (mapGridRenderer->isVisible())
	{
		mMapGridDataProvider->update(static_cast<Map*>(mMap.get())->getGridOptions().cellSize, viewBounds, frameTime);
	}

	// Update collision sim renderer
	auto collisionSimRenderer = get<0>(mAdditionalRenderers["CollisionSim"]);
	if (collisionSimRenderer->isVisible())
	{
		mCollisionSimDataProvider->update(viewBounds, frameTime);
	}

	// Update map grid renderer
	if (mMapRenderer->usingGridIncludeMasking())
	{
		mMapRenderer->clearGridIncludeMask();

		auto grid = mMapRenderer->getLookupGrid();
		auto mouseWorldPos = getMouseWorldPosition();

		int cellX, cellY;
		grid->getContainingCell(true, mouseWorldPos.x, mouseWorldPos.y, cellX, cellY);
		if (cellX >= 0 && cellY >= 0)
		{
			auto const& items = grid->_getCellItems(cellX, cellY);
			for (auto item : items)
			{
				mMapRenderer->addToGridIncludeMask(item);
			}
		}
	}

	// Update minimap renderer
	auto miniMapRenderer = get<0>(mAdditionalRenderers["MiniMap"]);
	if (miniMapRenderer->isVisible())
	{
		auto const& playerPos = getPlayerPosition();

		// Centre minimap on player
		auto const& playerOffset = mMiniMapDataProvider->getTransformedPosition(playerPos);
		mMiniMapDataProvider->update(playerPos);

		auto screenCentre = viewBounds.getMinExtent() + getWindowSize() / 2.0f;
		miniMapRenderer->setOrigin(screenCentre - playerOffset);
	}
}
