#include "willpower/application/resourcesystem/ResourceExceptions.h"

#include "Game.h"
#include "BulletType.h"
#include "Animation.h"

using namespace std;
using namespace wp;


map<string, BulletType> gBulletReferenceMapping = {
	{"RedDot", BulletType::RedDot},
	{"Fireball", BulletType::Fireball},
	{"Comet", BulletType::Comet}
};

map<string, Animation> gBulletAnimationReferenceMapping = {
	{"fire", Animation::A_Fire},
	{"explode", Animation::A_Explode},
};

Game::Game(string const& name,
	string const& namesp,
	string const& source,
	map<string, string> const& tags,
	application::resourcesystem::ResourceLocation* location,
	shared_ptr<applib::AnimationDatabase> animDatabase)
	: applib::Game(name, namesp, source, tags, location, animDatabase)
{
}

uint32_t Game::getBulletReferenceId(string const& bulletName)
{
	try
	{
		return (uint32_t)gBulletReferenceMapping[bulletName];
	}
	catch (std::out_of_range&)
	{
		throw Exception("No reference id was specified for bullet type '" + bulletName + "'.");
	}
}

uint32_t Game::getBulletAnimationReferenceId(string const& animationType)
{
	try
	{
		return (uint32_t)gBulletAnimationReferenceMapping[animationType];
	}
	catch (std::out_of_range&)
	{
		throw Exception("No animation reference id was specified for bullet animation type '" + animationType + "'.");
	}
}

