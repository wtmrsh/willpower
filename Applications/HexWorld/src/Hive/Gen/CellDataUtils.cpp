#include "Hive/Gen/CellDataUtils.h"


namespace hive
{
	namespace gen
	{
		namespace utils
		{

			using namespace std;

			uint32_t getNumEmptyCells(CellDataMap const& cellMap)
			{
				uint32_t numEmptyCells{ 0 };

				for (auto const& cell : cellMap)
				{
					auto const& [hex, data] = cell;
					if (data.roomIndex < 0)
					{
						numEmptyCells++;
					}
				}

				return numEmptyCells;
			}

			hex::Hex chooseRandomEmptyCell(CellDataMap const& cellMap)
			{
				auto numEmptyCells = getNumEmptyCells(cellMap);

				int i = 0, c = rand() % numEmptyCells;

				for (auto const& cell : cellMap)
				{
					auto const& [hex, data] = cell;
					if (data.roomIndex < 0)
					{
						if (i == c)
						{
							return hex;
						}

						i++;
					}
				}

				return hex::Hex(-1, -1, -1);
			}

			int32_t chooseRandomFreeNeighbourDir(CellDataMap const& cellMap, hex::Hex const& hex)
			{
				vector<int> dirs;

				for (int i = 0; i < 6; ++i)
				{
					auto it = cellMap.find(hex.neighbour(i));

					// Check the neighbour is in the map
					if (it != cellMap.end())
					{
						if (it->second.roomIndex < 0)
						{
							dirs.push_back(i);
						}
					}
				}

				return dirs.empty() ? -1 : dirs[rand() % dirs.size()];
			}

			map<int32_t, vector<hex::Hex>> groupHexesByRoom(CellDataMap const& cellMap)
			{
				// Rooms are assumed to be made of connecting hexes
				map<int32_t, vector<hex::Hex>> roomHexMap;

				// Iterate over and create a list for each room
				for (auto const& item : cellMap)
				{
					auto const& [hex, roomData] = item;

					if (roomData.roomIndex >= 0)
					{
						roomHexMap[roomData.roomIndex] = {};
					}
				}

				// Iterate over again and add each hex to its corresponding room
				for (auto const& item : cellMap)
				{
					auto const& [hex, roomData] = item;

					if (roomData.roomIndex >= 0)
					{
						roomHexMap[roomData.roomIndex].push_back(hex);
					}
				}

				return roomHexMap;
			}

		} // utils
	} // gen
} // hive