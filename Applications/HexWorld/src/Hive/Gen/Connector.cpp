#include "Hive/Gen/Connector.h"

#include "MapException.h"


namespace hive
{
	namespace gen
	{

		using namespace std;

		Connector::Connector(uint32_t roomIndex0, hex::HexDirected cell0, uint32_t roomIndex1, hex::HexDirected cell1)
		{
			mRoomIndices[0] = roomIndex0;
			mRoomIndices[1] = roomIndex1;

			mDoorIndices[0] = ~0u;
			mDoorIndices[1] = ~0u;

			mRoomCells[0] = cell0;
			mRoomCells[1] = cell1;
		}

		void Connector::getRoomIndices(uint32_t* roomIndex0, uint32_t* roomIndex1) const
		{
			*roomIndex0 = mRoomIndices[0];
			*roomIndex1 = mRoomIndices[1];
		}

		void Connector::getDoorIndices(uint32_t* doorIndex0, uint32_t* doorIndex1) const
		{
			*doorIndex0 = mDoorIndices[0];
			*doorIndex1 = mDoorIndices[1];
		}

		void Connector::getRoomCells(hex::HexDirected* cell0, hex::HexDirected* cell1) const
		{
			*cell0 = mRoomCells[0];
			*cell1 = mRoomCells[1];
		}

		void Connector::addDoorIndex(uint32_t doorIndex)
		{
			if (mDoorIndices[0] == ~0u)
			{
				mDoorIndices[0] = doorIndex;
			}
			else if (mDoorIndices[1] == ~0u)
			{
				mDoorIndices[1] = doorIndex;
			}
			else
			{
				throw MapException("Tried to add more than two doors to a connector.");
			}
		}

	} // gen
} // hive