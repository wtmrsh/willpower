#include "Hive/Gen/RoomPrefab.h"
#include "Hive/Gen/Room.h"


namespace hive
{
	namespace gen
	{

		using namespace std;

		RoomPrefab::RoomPrefab(RoomPrefabType type, vector<RoomPrefabCell> const& cells)
			: mType(type)
			, mCells(cells)
		{
		}

		RoomPrefabType RoomPrefab::getType() const
		{
			return mType;

		}
		vector<RoomPrefabCell> RoomPrefab::getCells(hex::Hex const& origin, int rotation) const
		{
			return transformCells(origin, rotation);
		}

		bool RoomPrefab::canPlace(CellDataMap const& hiveMap, vector<Room> const& rooms, hex::Hex const& origin, int rotation) const
		{
			auto transformedCells = transformCells(origin, rotation);

			for (auto const& transformedCell : transformedCells)
			{
				// Check for placing on cells which already have something on them
				auto it = hiveMap.find(transformedCell.hex);

				if (it != hiveMap.end())
				{
					auto const& mapCell = it->second;
					if (mapCell.roomIndex >= 0)
					{
						return false;
					}
				}

				// Check this cell's mandatory connectors
				for (int i = 0; i < 6; ++i)
				{
					if (transformedCell.connectors[i] == RoomConnectorType::Mandatory)
					{
						auto n = transformedCell.hex.neighbour(i);

						// Neighbour must not be in transformedCells as this would mean the door
						// is internal to the room
						for (auto const& tc : transformedCells)
						{
							if (n == tc.hex)
							{
								return false;
							}
						}

						// Neighbour must be within map extents, otherwise the door would lead to the void
						if (hiveMap.find(n) == hiveMap.end())
						{
							return false;
						}

						// Hive neighbour cell connector must not be None
						// Need to get the room, and then query the room to see if there is a
						// connector at that location/inverse direction: there needs to be one
						auto hiveNeighbour = hiveMap.at(n);

						if (hiveNeighbour.roomIndex >= 0)
						{
							auto const& room = rooms[hiveNeighbour.roomIndex];

							if (!room.hasConnector({ n, 5 - i }))
							{
								return false;
							}
						}
					}
				}
			}

			return true;
		}

		vector<RoomPrefabCell> RoomPrefab::transformCells(hex::Hex const& origin, int rotation) const
		{
			vector<RoomPrefabCell> transformedCells;
			transformedCells.reserve(mCells.size());

			auto roomRotation = 6 - rotation;
			for (auto const& cell : mCells)
			{
				RoomPrefabCell tCell;

				// Rotate hex
				auto tHex = cell.hex;
				for (int i = 0; i < rotation; ++i)
				{
					tHex = tHex.rotateClockwise();
				}

				tCell.hex = origin + tHex;

				// Rotate connectors
				for (int i = 0; i < 6; ++i)
				{
					tCell.connectors[i] = cell.connectors[(i + roomRotation) % 6];
				}

				transformedCells.push_back(tCell);
			}

			return transformedCells;
		}

	} // gen
} // hive