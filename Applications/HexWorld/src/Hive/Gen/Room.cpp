#include "Hive/Gen/Room.h"


namespace hive
{ 
	namespace gen
	{

		using namespace std;

		Room::Room(RoomPrefab const& roomPrefab, hex::Hex const& origin, int rotation)
			: mPrefab(roomPrefab)
			, mOrigin(origin)
			, mRotation(rotation)
		{
			auto const& cells = roomPrefab.getCells(origin, rotation);

			for (auto const& cell : cells)
			{
				for (int i = 0; i < 6; ++i)
				{
					if (cell.connectors[i] != RoomConnectorType::None)
					{
						addConnector({ cell.hex, i }, cell.connectors[i]);
					}
				}
			}
		}

		RoomPrefab const& Room::getPrefab() const
		{
			return mPrefab;
		}

		hex::Hex Room::getOrigin() const
		{
			return mOrigin;
		}

		int Room::getRotation() const
		{
			return mRotation;
		}

		vector<RoomConnector> Room::getConnectors() const
		{
			auto connectors = mMandatoryConnectors;
			connectors.insert(connectors.end(), mOptionalConnectors.begin(), mOptionalConnectors.end());

			return connectors;
		}

		vector<RoomConnector> const& Room::getOptionalConnectors() const
		{
			return mOptionalConnectors;
		}

		vector<RoomConnector> const& Room::getMandatoryConnectors() const
		{
			return mMandatoryConnectors;
		}

		void Room::addConnector(hex::HexDirected const& hexd, RoomConnectorType type)
		{
			if (type == RoomConnectorType::Optional)
			{
				mOptionalConnectors.push_back({ hexd, type });
			}
			else if (type == RoomConnectorType::Mandatory)
			{
				mMandatoryConnectors.push_back({ hexd, type });
			}
		}

		void Room::removeOptionalConnectors()
		{
			mOptionalConnectors.clear();
		}

		void Room::setOptionalConnector(uint32_t index, RoomConnectorType type)
		{
			switch (type)
			{ 
			case RoomConnectorType::Mandatory:
				mMandatoryConnectors.push_back({ mOptionalConnectors[index].hexd, type });
				[[fallthrough]];
			case RoomConnectorType::None:
				for (uint32_t i = index; i < mOptionalConnectors.size() - 1; ++i)
				{
					mOptionalConnectors[i] = mOptionalConnectors[i + 1];
				}
				mOptionalConnectors.pop_back();
				[[fallthrough]];
			case RoomConnectorType::Optional:
				break;
			}
		}

		void Room::setOptionalConnector(hex::HexDirected const& hexd, RoomConnectorType type)
		{
			for (uint32_t i = 0; i < mOptionalConnectors.size(); ++i)
			{
				if (mOptionalConnectors[i].hexd == hexd)
				{
					setOptionalConnector(i, type);
					return;
				}
			}
		}

		bool Room::hasConnector(hex::HexDirected const& hexd) const
		{
			auto conns = getConnectors();

			for (auto const& conn : conns)
			{
				if (hexd == conn.hexd)
				{
					return true;
				}
			}

			return false;
		}

	} // gen
} // hive