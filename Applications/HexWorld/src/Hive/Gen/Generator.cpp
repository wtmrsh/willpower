#include <algorithm>
#include <random>
#include <set>

#include "Hive/Utils.h"
#include "Hive/Gen/Generator.h"
#include "Hive/Gen/CellDataUtils.h"

#include "MapException.h"

/*
Cell configurations for textures, where neighbours are at 12, 2, 4, 6, 8, 10PM (clockwise):
- 1x all 6 neighbours (6N)
- 1x 5N with 6 rotations 
- 3x 4N: if we have one empty neighbour at 12, then the other is either at 2, 4 or 6.  Or a mirror.
-    3N: taking 4N as a starting point, if we remove one neighbour from each, 




*/


namespace hive
{
	namespace gen
	{
		using namespace std;

		vector<RoomPrefab> gRoomPrefabs = {
			// None
			RoomPrefab(RoomPrefabType::None, {}),
			// Arena1
			RoomPrefab(RoomPrefabType::Arena1, {
				{ hex::Hex(0, 0), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::Mandatory,
					RoomConnectorType::None
				}},
				{ hex::Hex(1, -1), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}},
				{ hex::Hex(1, 0), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}},
				{ hex::Hex(0, 1), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}},
				{ hex::Hex(-1, 1), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}},
				{ hex::Hex(-1, 0), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}},/*
				{ hex::Hex(0, -1), {
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None,
					RoomConnectorType::None
				}}*/
			})
		};


		Generator::Generator(int radius, float cellCoveragePercent)
			: mWorldRadius(radius)
			, mStyliseRooms(false)
			, mState(State::Uninitialised)
			, mCellCoveragePercent(cellCoveragePercent)
		{
			if (mWorldRadius < 0 || mWorldRadius > 10)
			{
				throw MapException("World radius out of bounds.");
			}
		}

		void Generator::initialise()
		{
			int N = getWorldRadius();

			// Create and initialise all hexes.
			for (int q = -N; q <= N; q++)
			{
				int r1 = std::max(-N, -q - N);
				int r2 = std::min(N, -q + N);

				for (int r = r1; r <= r2; r++)
				{
					auto hex = hex::Hex(q, r, -q - r);
					mCellGenData[hex] = {};
				}
			}

			// Clear rooms
			mRooms.clear();

			mState = State::Initialised;
		}

		uint32_t Generator::getWorldRadius() const
		{
			return mWorldRadius;
		}

		vector<Generator::RoomConnectionDetail> Generator::getRoomConnectorDetails() const
		{
			unordered_map<hex::HexDirected, pair<int, RoomConnector>> sources;

			for (uint32_t i = 0; i < mRooms.size(); ++i)
			{
				auto const& room = mRooms[i];
				auto conns = room.getConnectors();

				for (auto const& conn : conns)
				{
					if (conn.type == RoomConnectorType::Optional ||
						conn.type == RoomConnectorType::Mandatory)
					{
						sources[conn.hexd] = make_pair((int)i, conn);
					}
				}
			}

			vector<RoomConnectionDetail> roomConns;
			for (auto const& source : sources)
			{
				auto const& [hex, data] = source;

				auto n = hex.mirror();
				auto it = sources.find(n);

				if (it != sources.end())
				{
					roomConns.push_back({
						data.first,
						data.second,
						it->second.first,
						it->second.second
					});
				}
			}

			return roomConns;
		}

		bool Generator::generate()
		{
			if (mState != State::Initialised)
			{
				return false;
			}

			// Create rooms
			if (!createRooms())
			{
				return false;
			}

			// Finally, convert optional connectors to mandatory where required to connect rooms
			if (!connectRooms())
			{
				return false;
			}
			
			mState = State::Complete;
			return true;
		}

		bool Generator::createRooms()
		{
			mState = State::GeneratingRooms;

			// Check all empty cells are reachable from each other.  This ensures we haven't placed
			// prefabs in such a way that part of the map is unreachable from another.
			if (!emptyCellsConnectable())
			{
				return false;
			}

			// Generate rooms to connect to already-placed rooms with mandatory connectors
			// One iteration of placing rooms, so as to cover all mandatory connectors
			adjoinMandatoryConnectors();
			
			//return true;

			// Then, iterate, placing a room until all rooms are connected and we've placed enough rooms
			auto maxEmptyCells = (uint32_t)(mCellGenData.size() * (1.0f - mCellCoveragePercent));

			while (!roomsConnectable() || utils::getNumEmptyCells(mCellGenData) > maxEmptyCells)
			{
				// Place a room at a random starting point
				auto startCell = utils::chooseRandomEmptyCell(mCellGenData);

				if (!startCell.valid())
				{
					return false;
				}

				placeRoom(startCell, false, 4 + (rand() % 4));
			}

			return true;
		}

		bool Generator::connectRooms()
		{
			mState = State::GeneratingConnectors;
	
			// Go through all connectors and if there is a mirrored
			// one, specify the connection between rooms.
			auto roomConns = getRoomConnectorDetails();

			// Change any optional-mandatory or mandatory-optional connections to 
			// mandatory-mandatory, and then remove all entries, for which their
			// room indices (unordered) have a mandatory-mandatory connection somewhere.  
			// This leaves us with just a list of ordered connections which are only
			// connected by optional connectors.
			typedef pair<int, int> RoomIndexPair;
			typedef pair<RoomConnector, RoomConnector> RoomConnectorPair;

			map<RoomIndexPair, vector<RoomConnectorPair>> optionalConns;
			set<RoomIndexPair> connsToRemove;

			for (auto const& roomConn : roomConns)
			{
				auto const& [roomA, connA, roomB, connB] = roomConn;

				if (connA.type == RoomConnectorType::Optional && connB.type == RoomConnectorType::Mandatory)
				{
					// Upgrade to mandatory
					mRooms[roomA].setOptionalConnector(connA.hexd, RoomConnectorType::Mandatory);
				}

				// If both are optional, then add
				auto p = make_pair(roomA, roomB);

				if (connA.type == RoomConnectorType::Optional && connB.type == RoomConnectorType::Optional)
				{
					auto q = make_pair(connA, connB);

					optionalConns.insert(make_pair(p, vector<RoomConnectorPair>()));
					optionalConns[p].push_back(q);
				}
				else
				{
					// Else, mark to be removed
					connsToRemove.insert(p);
				}
			}

			// Remove the connections for room pairs which have a mandatory connection already
			for (auto const& c : connsToRemove)
			{
				optionalConns.erase(c);
			}

			set<pair<int, int>> connsDone;
			for (auto const& item : optionalConns)
			{
				auto const& [roomIds, connList] = item;

				// If we've already created a connector for this pair, ignore
				if (connsDone.find(roomIds) != connsDone.end())
				{
					continue;
				}

				// Choose a random connection
				int index = rand() % connList.size();
				auto const& c = connList[index];

				mRooms[roomIds.first].setOptionalConnector(c.first.hexd, RoomConnectorType::Mandatory);
				mRooms[roomIds.second].setOptionalConnector(c.second.hexd, RoomConnectorType::Mandatory);

				connsDone.insert(make_pair(roomIds.first, roomIds.second));
				connsDone.insert(make_pair(roomIds.second, roomIds.first));
			}


			// At this point, we might want to add some more random connectors in
			// ...

			// Finally remove all optional connectors
			for (auto& room : mRooms)
			{
				room.removeOptionalConnectors();
			}

			// Now create connector objects, getting the undirected connections and filtering out
			// the duplicates to get the undirected ones
			std::vector<RoomConnectionDetail> undirectedRoomConns,
				directedRoomConns = getRoomConnectorDetails();

			vector<pair<hex::HexDirected, hex::HexDirected>> foundDrcs;

			for (auto const& drc : directedRoomConns)
			{
				auto p = make_pair(drc.connector0.hexd, drc.connector1.hexd);

				if (find(foundDrcs.begin(), foundDrcs.end(), p) == foundDrcs.end())
				{
					undirectedRoomConns.push_back(drc);

					foundDrcs.push_back(p);
					foundDrcs.push_back(make_pair(drc.connector1.hexd, drc.connector0.hexd));
				}
			}

			// Now go over undirected and create connectors and doors from them
			for (auto const& urc : undirectedRoomConns)
			{
				uint32_t connIndex = (uint32_t)createConnector(urc);

				// Create doors
				createDoor(urc.connector0.hexd, urc.roomIndex0, connIndex, false, 0);
				createDoor(urc.connector1.hexd, urc.roomIndex1, connIndex, false, 0);
			}

			return true;
		}

		void Generator::makeRandomOptionalConnectorMandatory(int roomIndexA, int roomIndexB)
		{
			auto& roomA = mRooms[roomIndexA];

			auto const& connectorsA = roomA.getOptionalConnectors();
			uint32_t connIndex = rand() % connectorsA.size();

			roomA.setOptionalConnector(connIndex, RoomConnectorType::Mandatory);

			auto& roomB = mRooms[roomIndexB];
			auto cellB = connectorsA[connIndex].hexd.mirror();

			roomB.setOptionalConnector(cellB, RoomConnectorType::Mandatory);
		}

		uint32_t Generator::placeRoom(hex::Hex const& startCell, bool addMandatoryConnectors, uint32_t numCells)
		{
			return drunkenWalkRoom(startCell, addMandatoryConnectors, numCells);
		}

		vector<RoomConnector> Generator::getRoomConnectors() const
		{
			vector<RoomConnector> mapConns;

			for (auto const& room : mRooms)
			{
				auto roomConns = room.getConnectors();
				mapConns.insert(mapConns.end(), roomConns.begin(), roomConns.end());
			}

			return mapConns;
		}

		vector<RoomConnector> Generator::getOptionalConnectors() const
		{
			vector<RoomConnector> mapConns;

			for (auto const& room : mRooms)
			{
				auto const& roomConns = room.getOptionalConnectors();
				mapConns.insert(mapConns.end(), roomConns.begin(), roomConns.end());
			}

			return mapConns;
		}

		vector<RoomConnector> Generator::getMandatoryConnectors() const
		{
			vector<RoomConnector> mapConns;

			for (auto const& room : mRooms)
			{
				auto const& roomConns = room.getMandatoryConnectors();
				mapConns.insert(mapConns.end(), roomConns.begin(), roomConns.end());
			}

			return mapConns;
		}

		uint32_t Generator::createRoom(RoomPrefab const& roomPrefab, hex::Hex const& origin, int rotation)
		{
			auto roomIndex = (uint32_t)mRooms.size();

			mRooms.push_back({ 
				roomPrefab, 
				origin, 
				rotation 
			});

			return roomIndex;
		}

		uint32_t Generator::createDoor(hex::HexDirected const& hexd, uint32_t roomIndex, uint32_t connectorIndex, bool locked, uint32_t key)
		{
			auto doorIndex = (uint32_t)mDoors.size();

			mDoors.push_back({
				hexd,
				roomIndex,
				connectorIndex,
				locked,
				key
			});

			// Add door index to connector
			mConnectors[connectorIndex].addDoorIndex(doorIndex);

			return doorIndex;
		}

		uint32_t Generator::createConnector(RoomConnectionDetail const& rcd)
		{
			auto connIndex = (uint32_t)mConnectors.size();
			auto const& [roomIndex0, hex0, roomIndex1, hex1] = rcd;
			
			mConnectors.push_back({ (uint32_t)roomIndex0, hex0.hexd, (uint32_t)roomIndex1, hex1.hexd });
			return connIndex;
		}

		uint32_t Generator::drunkenWalkRoom(hex::Hex const& startCell, bool addMandatoryConnectors, uint32_t numCells)
		{
			// Get all mandatory connectors for quick lookup
			vector<RoomConnector> mandatoryConnectors;

			if (addMandatoryConnectors)
			{
				for (auto const& room : mRooms)
				{
					auto mcs = room.getMandatoryConnectors();
					mandatoryConnectors.insert(mandatoryConnectors.end(), mcs.begin(), mcs.end());
				}
			}

			// Create cells for room
			vector<RoomPrefabCell> chosenCells;

			chosenCells.push_back({ startCell, {
				RoomConnectorType::None,
				RoomConnectorType::None,
				RoomConnectorType::None,
				RoomConnectorType::None,
				RoomConnectorType::None,
				RoomConnectorType::None
			} });

			for (auto const& manConn : mandatoryConnectors)
			{
				if (manConn.hexd.target() == startCell)
				{
					auto c = manConn.hexd.mirror();
					chosenCells.back().connectors[c.dir] = RoomConnectorType::Mandatory;
				}
			}

			// Peek room index
			uint32_t roomIndex = (uint32_t)mRooms.size();

			mCellGenData[startCell].roomIndex = roomIndex;

			// Iterate over cells, choosing one at a time
			auto rng = default_random_engine{};
			for (uint32_t i = 0; i < numCells - 1; ++i)
			{
				// The way we randomly choose is to shuffle the list each time.
				// This way we can then iterate over the list in a random way until
				// we find a cell which is suitable
				std::ranges::shuffle(chosenCells, rng);

				bool foundNewCell{ false };
				hex::Hex newCell;
				for (auto const& cell : chosenCells)
				{
					auto walkDir = utils::chooseRandomFreeNeighbourDir(mCellGenData, cell.hex);
					if (walkDir < 0)
					{
						continue;
					}

					newCell = cell.hex.neighbour(walkDir);
					foundNewCell = true;
					break;
				}

				if (foundNewCell)
				{
					mCellGenData[newCell].roomIndex = roomIndex;

					chosenCells.push_back({ newCell, {
						RoomConnectorType::None,
						RoomConnectorType::None,
						RoomConnectorType::None,
						RoomConnectorType::None,
						RoomConnectorType::None,
						RoomConnectorType::None
					} });

					// Add a mandatory connector if required
					for (auto const& manConn : mandatoryConnectors)
					{
						if (manConn.hexd.target() == newCell)
						{
							auto c = manConn.hexd.mirror();
							chosenCells.back().connectors[c.dir] = RoomConnectorType::Mandatory;
						}
					}
				}
				else
				{
					// Can't expand this room any further
					goto finished;
				}
			}

		finished: ;

			// Place optional connectors on all external edges which are marked as 'none' for connections
			for (auto& cell : chosenCells)
			{
				for (int i = 0; i < 6; ++i)
				{
					auto c = mCellGenData[cell.hex];
					auto n = cell.hex.neighbour(i);

					auto it = mCellGenData.find(n);
					if (it != mCellGenData.end())
					{
						if (it->second.roomIndex != (int32_t)roomIndex)
						{
							if (cell.connectors[i] != RoomConnectorType::Mandatory)
							{
								cell.connectors[i] = RoomConnectorType::Optional;
							}
						}
					}
				}
			}

			// Fill in map cells
			return createRoom(RoomPrefab(RoomPrefabType::None, chosenCells), hex::Hex(0, 0), 0);
		}

		bool Generator::placePrefab(RoomPrefabType prefabType, hex::Hex const& origin, int rotation)
		{
			if (mState != State::Initialised)
			{
				return false;
			}

			auto const& prefab = gRoomPrefabs[prefabType];

			if (!prefab.canPlace(mCellGenData, mRooms, origin, rotation))
			{
				return false;
			}

			// Get prefab index and add room
			auto roomIndex = createRoom(prefab, origin, rotation);
	
			// Set cells
			auto prefabCells = prefab.getCells(origin, rotation);

			for (auto const& cell : prefabCells)
			{
				mCellGenData[cell.hex].roomIndex = roomIndex;
			}

			return true;
		}

		bool Generator::adjoinMandatoryConnectors()
		{
			// Place rooms down so that all mandatory connectors have something they can connect to.
			// For each mandatory connector, find the neighbour, and create a room from there, for a
			// given number of hexes.
			uint32_t numInitialRooms = (uint32_t)mRooms.size();
			for (uint32_t i = 0; i < numInitialRooms; ++i)
			{
				auto const& room = mRooms[i];
				auto const& manConns = room.getMandatoryConnectors();

				for (auto const& manConn : manConns)
				{
					auto startHexDirected = manConn.hexd.mirror();

					// It may be that a previous iteration has created a connecting room for us
					if (mCellGenData[startHexDirected.hex].roomIndex >= 0)
					{
						continue;
					}

					auto roomIndex = placeRoom(startHexDirected.hex, true, 4 + (rand() % 3));
				}
			}

			return true;
		}

		CellDataMap const& Generator::getCellGenData() const
		{
			return mCellGenData;
		}

		vector<Room> const& Generator::getRooms() const
		{
			return mRooms;
		}

		vector<Door> const& Generator::getDoors() const
		{
			return mDoors;
		}

		vector<Connector> const& Generator::getConnectors() const
		{
			return mConnectors;
		}

		vector<set<int>> Generator::getDisconnectedRoomIndices(vector<RoomConnector> const& conns) const
		{
			vector<pair<int, int>> edges;
			
			hex::HexDirected c;
			hex::Hex n;

			try
			{
				for (auto const& conn : conns)
				{
					c = conn.hexd;
					n = c.target();
					edges.push_back(make_pair(mCellGenData.at(c.hex).roomIndex, mCellGenData.at(n).roomIndex));
				}
			}
			catch (exception& e)
			{
				throw MapException(e.what());
			}

			return hive::utils::getDisconnectedSubGraphs(edges);
		}

		bool Generator::roomsConnectable() const
		{
			return getDisconnectedRoomIndices(getRoomConnectors()).size() < 2;
		}

		bool Generator::emptyCellsConnectable() const
		{
			auto conns = getRoomConnectors();

			vector<pair<bool, bool>> edges;
			for (auto const& item : mCellGenData)
			{
				auto const& [cell, data] = item;

				for (int i = 0; i < 6; ++i)
				{
					auto n = cell.neighbour(i);

					auto it = mCellGenData.find(n);
					if (it != mCellGenData.end())
					{
						edges.push_back(make_pair(data.roomIndex >= 0, it->second.roomIndex >= 0));
					}
				}
			}

			auto subgraphs = hive::utils::getDisconnectedSubGraphs(edges);

			// We only want one subgraph with values of 'false' (ie cell not filled in)
			bool foundFirst{ false };

			for (auto const& subgraph : subgraphs)
			{
				if (!*subgraph.begin())
				{
					if (foundFirst)
					{
						return false;
					}
					else
					{
						foundFirst = true;
					}
				}
			}

			return true;
		}

	} // gen
} // hive