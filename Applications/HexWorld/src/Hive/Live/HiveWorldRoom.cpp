#include "Hive/Live/HiveWorldRoom.h"
#include "Hive/Live/HiveWorld.h"


namespace hive
{
	namespace live
	{

		using namespace std;

		HiveWorldRoom::HiveWorldRoom(HiveWorldRoomType type)
			: HiveWorldArea({})
			, mType(type)
			, mFlags(0)
			, mDoorIndices({})
		{
			if (mType == HiveWorldRoomType::Arena)
			{
				mFlags |= HIVE_ROOM_ARENA;
				mFlags |= HIVE_ROOM_ARENA_STATE_DORMANT;
			}
		}

		HiveWorldRoom::HiveWorldRoom(HiveWorldRoomType type, set<uint32_t> const& meshPolygonIndices, vector<uint32_t> const& doorIndices)
			: HiveWorldArea(meshPolygonIndices)
			, mType(type)
			, mFlags(0)
			, mDoorIndices(doorIndices)
		{
			if (mType == HiveWorldRoomType::Arena)
			{
				mFlags |= HIVE_ROOM_ARENA;
				mFlags |= HIVE_ROOM_ARENA_STATE_DORMANT;
			}
		}

		HiveWorldRoomType HiveWorldRoom::getType() const
		{
			return mType;
		}

		uint64_t HiveWorldRoom::getFlags() const
		{
			return mFlags;
		}

		void HiveWorldRoom::_setFlag(uint32_t flag)
		{
			mFlags |= flag;
		}

		void HiveWorldRoom::_unsetFlag(uint32_t flag)
		{
			mFlags &= ~flag;
		}

		vector<uint32_t> const& HiveWorldRoom::getDoorIndices() const
		{
			return mDoorIndices;
		}

		bool HiveWorldRoom::tryOpenAllConnectors(HiveWorld* world)
		{
			bool success{ true };

			for (auto doorIndex : mDoorIndices)
			{
				auto door = world->getDoor(doorIndex);
				auto connector = world->getConnector(door->getConnectorIndex());
				success = success && connector->tryCycleOpen(doorIndex, world);
			}

			return success;
		}

		void HiveWorldRoom::update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime)
		{
			VAR_UNUSED(world);
			VAR_UNUSED(collisionSim);
			VAR_UNUSED(frameTime);
		}

	} // live
} // hive