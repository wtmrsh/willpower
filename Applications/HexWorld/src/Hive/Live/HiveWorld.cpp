#include <utils/StringUtils.h>

#include "Hive/Live/HiveWorld.h"

#include "GameException.h"

using namespace std;


namespace hive
{
	namespace live
	{

		HiveWorld::HiveWorld()
		{
		}

		uint32_t HiveWorld::addRoom(HiveWorldRoom const& room)
		{
			auto index = (uint32_t)mRooms.size();

			mRooms.push_back(room);
			return index;
		}

		uint32_t HiveWorld::addConnector(HiveWorldConnector const& conn)
		{
			auto index = (uint32_t)mConnectors.size();

			mConnectors.push_back(conn);
			return index;
		}

		uint32_t HiveWorld::addDoor(HiveWorldDoor const& door)
		{
			auto index = (uint32_t)mDoors.size();

			mDoors.push_back(door);
			return index;
		}

		uint32_t HiveWorld::getNumRooms() const
		{
			return (uint32_t)mRooms.size();
		}

		uint32_t HiveWorld::getNumConnectors() const
		{
			return (uint32_t)mConnectors.size();
		}

		uint32_t HiveWorld::getNumDoors() const
		{
			return (uint32_t)mDoors.size();
		}

		void HiveWorld::traverseDoorVisibility(uint32_t index, vector<HexLocator>& locators, VisibilityCalculationCache& cache) const
		{
			if (cache.doors.find(index) != cache.doors.end())
			{
				return;
			}

			auto const& door = mDoors[index];

			cache.doors.insert(index);
			locators.push_back(HexLocator::fromDoor(index, door.getMeshPolygonIndices()));

			if (!door.blocksVision())
			{
				traverseConnectorVisibility(door.getConnectorIndex(), locators, cache);
				traverseRoomVisibility(door.getRoomIndex(), locators, cache);
			}
		}

		void HiveWorld::traverseRoomVisibility(uint32_t index, vector<HexLocator>& locators, VisibilityCalculationCache& cache) const
		{
			if (cache.rooms.find(index) != cache.rooms.end())
			{
				return;
			}

			cache.rooms.insert(index);

			auto const& room = mRooms[index];
			locators.push_back(HexLocator::fromRoom(index, room.getMeshPolygonIndices()));

			for (auto doorIndex : room.getDoorIndices())
			{
				traverseDoorVisibility(doorIndex, locators, cache);
			}
		}

		void HiveWorld::traverseConnectorVisibility(uint32_t index, vector<HexLocator>& locators, VisibilityCalculationCache& cache) const
		{
			if (cache.connectors.find(index) != cache.connectors.end())
			{
				return;
			}

			cache.connectors.insert(index);

			auto const& connector = mConnectors[index];
			locators.push_back(HexLocator::fromConnector(index, connector.getMeshPolygonIndices()));

			traverseDoorVisibility(connector.getDoorIndex0(), locators, cache);
			traverseDoorVisibility(connector.getDoorIndex1(), locators, cache);
		}

		vector<HexLocator> HiveWorld::getVisibleAreas(HexLocator const& locator) const
		{
			vector<HexLocator> locators;
			VisibilityCalculationCache cache;

			switch (locator.type)
			{
			case HexLocator::Type::Room:
				traverseRoomVisibility(locator.typeIndex, locators, cache);
				break;
			case HexLocator::Type::Connector:
				traverseConnectorVisibility(locator.typeIndex, locators, cache);
				break;
			case HexLocator::Type::Door:
				traverseDoorVisibility(locator.typeIndex, locators, cache);
				break;
			default:
				break;
			}

			return locators;
		}

		HiveWorldArea* HiveWorld::getArea(HexLocator const& locator)
		{
			switch (locator.type)
			{
			case HexLocator::Type::Connector:
				return getConnector(locator.typeIndex);
			case HexLocator::Type::Door:
				return getDoor(locator.typeIndex);
			case HexLocator::Type::Room:
				return getRoom(locator.typeIndex);
			default:
				throw GameException(STR_FORMAT("Unknown Locator type: {}", (int)locator.type));
			}
		}

		HiveWorldRoom const* HiveWorld::getRoom(HexLocator const& locator) const
		{
			if (locator.type != HexLocator::Type::Room)
			{
				throw GameException("Locator is not a Room.");
			}

			return &mRooms[locator.typeIndex];
		}

		HiveWorldRoom* HiveWorld::getRoom(HexLocator const& locator)
		{
			if (locator.type != HexLocator::Type::Room)
			{
				throw GameException("Locator is not a Room.");
			}

			return &mRooms[locator.typeIndex];
		}

		HiveWorldRoom const* HiveWorld::getRoom(uint32_t index) const
		{
			return &mRooms[index];
		}

		HiveWorldRoom* HiveWorld::getRoom(uint32_t index)
		{
			return &mRooms[index];
		}

		HiveWorldConnector const* HiveWorld::getConnector(uint32_t index) const
		{
			return &mConnectors[index];
		}

		HiveWorldConnector* HiveWorld::getConnector(uint32_t index)
		{
			return &mConnectors[index];
		}

		HiveWorldDoor const* HiveWorld::getDoor(uint32_t index) const
		{
			return &mDoors[index];
		}

		HiveWorldDoor* HiveWorld::getDoor(uint32_t index)
		{
			return &mDoors[index];
		}

		void HiveWorld::onEnterArea(HexLocator const& locator, wp::collide::Simulation* collisionSim)
		{
			getArea(locator)->onEnter(this, collisionSim);
		}

		void HiveWorld::onExitArea(HexLocator const& locator, wp::collide::Simulation* collisionSim)
		{
			getArea(locator)->onExit(this, collisionSim);
		}

		void HiveWorld::checkTriggers(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator)
		{
			getArea(locator)->checkTriggers(oldPos, newPos, locator);
		}

		void HiveWorld::update(wp::collide::Simulation* collisionSim, float frameTime)
		{
			for (auto& door : mDoors)
			{
				door.update(this, collisionSim, frameTime);
			}

			for (auto& connector : mConnectors)
			{
				connector.update(this, collisionSim, frameTime);
			}

			for (auto& room : mRooms)
			{
				room.update(this, collisionSim, frameTime);
			}
		}

	} // live
} // hive