#include "Hive/Live/HiveWorldArea.h"
#include "Hive/Live/HiveWorld.h"

#include "GameException.h"


namespace hive
{
	namespace live
	{

		using namespace std;

		HiveWorldArea::HiveWorldArea(set<uint32_t> meshPolygonIndices)
			: mMeshPolygonIndices(meshPolygonIndices)
		{
		}

		set<uint32_t> const& HiveWorldArea::getMeshPolygonIndices() const
		{
			return mMeshPolygonIndices;
		}

		void HiveWorldArea::onEnter(HiveWorld* world, wp::collide::Simulation* collisionSim)
		{
			VAR_UNUSED(world);
			VAR_UNUSED(collisionSim);
		}

		void HiveWorldArea::onExit(HiveWorld* world, wp::collide::Simulation* collisionSim)
		{
			VAR_UNUSED(world);
			VAR_UNUSED(collisionSim);
		}

	} // live
} // hive