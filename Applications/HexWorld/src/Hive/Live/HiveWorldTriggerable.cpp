#include <exception>

#include "Hive/Live/HiveWorldTriggerable.h"


namespace hive
{
	namespace live
	{

		using namespace std;

		HiveWorldTriggerable::HiveWorldTriggerable()
		{
		}

		uint32_t HiveWorldTriggerable::getNumTriggers() const
		{
			return (uint32_t)mTriggers.size();
		}

		Trigger const& HiveWorldTriggerable::getTrigger(uint32_t index) const
		{
			return mTriggers[index];
		}

		void HiveWorldTriggerable::addTrigger(Trigger const& trigger)
		{
			mTriggers.push_back(trigger);
		}

		void HiveWorldTriggerable::resetTriggers()
		{
			for (auto& trigger : mTriggers)
			{
				trigger.reset();
			}
		}

		void HiveWorldTriggerable::checkTriggers(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator)
		{
			for (auto& trigger : mTriggers)
			{
				trigger.check(oldPos, newPos, locator);
			}
		}

	} // live
} // hive