#include "Hive/Live/HiveWorldConnector.h"
#include "Hive/Live/HiveWorldRoom.h"
#include "Hive/Live/HiveWorld.h"

#include "GameException.h"


namespace hive
{
	namespace live
	{

		using namespace std;

		HiveWorldConnector::HiveWorldConnector(uint32_t meshPolygonIndex, uint32_t doorIndex0, uint32_t doorIndex1)
			: HiveWorldArea({ meshPolygonIndex })
			, mState(State::LockEngaged)
			, mCommand(CycleCommand::None)
			, mCycleState(CycleState::Equilibrium)
			, mIntermissionTimer(0.0f)
		{
			mDoorIndices[0] = doorIndex0;
			mDoorIndices[1] = doorIndex1;
		}

		uint32_t HiveWorldConnector::getDoorIndex0() const
		{
			return mDoorIndices[0];
		}

		uint32_t HiveWorldConnector::getDoorIndex1() const
		{
			return mDoorIndices[1];
		}

		bool HiveWorldConnector::tryCycleOpen(uint32_t doorIndex, HiveWorld* world)
		{
			if (mDoorIndices[0] == doorIndex)
			{
				switch (mCommand)
				{
				case CycleCommand::OpenDoor0:
					return true;

				case CycleCommand::None:
					if (world->getDoor(mDoorIndices[0])->getState() == HiveWorldDoor::State::Locked)
					{
						return false;
					}
					[[fallthrough]];
				case CycleCommand::OpenDoor1:
					mCommand = CycleCommand::OpenDoor0;
					mCycleState = CycleState::ClosingDoor1;
					return true;

				default:
					return false;
				}
			}
			else if (mDoorIndices[1] == doorIndex)
			{
				switch (mCommand)
				{
				case CycleCommand::OpenDoor1:
					return true;

				case CycleCommand::None:
					if (world->getDoor(mDoorIndices[1])->getState() == HiveWorldDoor::State::Locked)
					{
						return false;
					}
					[[fallthrough]];
				case CycleCommand::OpenDoor0:
					mCommand = CycleCommand::OpenDoor1;
					mCycleState = CycleState::ClosingDoor0;
					return true;

				default:
					return false;
				}
			}
			else
			{
				throw GameException("Could not match Door to a Room.");
			}
		}

		bool HiveWorldConnector::tryCycle(HiveWorld* world)
		{
			// If the lock is disengaged this Connector is essentially non-functional.
			if (mState == State::LockDisengaged)
			{
				return false;
			}

			// If we're already in a cycle, don't interrupt it.
			if (mCycleState != CycleState::Equilibrium)
			{
				return true;
			}

			// Check door states.  One should be open(ing) and one should closed/closing.
			auto door0 = world->getDoor(mDoorIndices[0]);
			auto doorState0 = door0->getState();

			auto door1 = world->getDoor(mDoorIndices[1]);
			auto doorState1 = door1->getState();

			if (doorState0 == HiveWorldDoor::State::Locked || doorState1 == HiveWorldDoor::State::Locked)
			{
				return false;
			}

			bool door0Open = doorState0 == HiveWorldDoor::State::Open || doorState0 == HiveWorldDoor::State::Opening;
			bool door1Open = doorState1 == HiveWorldDoor::State::Open || doorState1 == HiveWorldDoor::State::Opening;

			// If both are open(ing), we have an error.	
			if (door0Open && door1Open)
			{
				throw GameException("Engaged Connector has two open/opening Doors.");
			}

			// If both are closed/closing/locked, we have an error.
			if (!door0Open && !door1Open)
			{
				throw GameException("Engaged Connector has two closed/closing/locked Doors.");
			}

			if (door0Open)
			{
				// Close door0, and once that is done, open door1
				mCommand = CycleCommand::OpenDoor1;
				mCycleState = CycleState::ClosingDoor0;
			}
			else
			{
				// Close door1, and once that is done, open door0
				mCommand = CycleCommand::OpenDoor0;
				mCycleState = CycleState::ClosingDoor1;
			}

			return true;
		}

		void HiveWorldConnector::processOpenDoor0(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime)
		{
			auto door0 = world->getDoor(mDoorIndices[0]);
			auto door1 = world->getDoor(mDoorIndices[1]);

			switch (mCycleState)
			{
			case CycleState::ClosingDoor1:
				door1->close(collisionSim);
				if (door1->getState() == HiveWorldDoor::State::Closed || door1->getState() == HiveWorldDoor::State::Locked)
				{
					mCycleState = CycleState::Intermission;
					mIntermissionTimer = 0.0f;
				}
				break;

			case CycleState::Intermission:
				mIntermissionTimer += frameTime;
				if (mIntermissionTimer >= IntermissionTime)
				{
					mIntermissionTimer = 0.0f;
					mCycleState = CycleState::OpeningDoor0;
				}
				break;

			case CycleState::OpeningDoor0:
				door0->tryOpen();
				if (door0->getState() == HiveWorldDoor::State::Open)
				{
					mCycleState = CycleState::Equilibrium;
					mCommand = CycleCommand::None;
				}
				break;

			default:
				throw GameException("Invalid CycleState for Connector while opening Door 0.");
			}
		}

		void HiveWorldConnector::processOpenDoor1(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime)
		{
			auto door0 = world->getDoor(mDoorIndices[0]);
			auto door1 = world->getDoor(mDoorIndices[1]);

			switch (mCycleState)
			{
			case CycleState::ClosingDoor0:
				door0->close(collisionSim);
				if (door0->getState() == HiveWorldDoor::State::Closed || door0->getState() == HiveWorldDoor::State::Locked)
				{
					mCycleState = CycleState::Intermission;
					mIntermissionTimer = 0.0f;
				}
				break;

			case CycleState::Intermission:
				mIntermissionTimer += frameTime;
				if (mIntermissionTimer >= IntermissionTime)
				{
					mIntermissionTimer = 0.0f;
					mCycleState = CycleState::OpeningDoor1;
				}
				break;

			case CycleState::OpeningDoor1:
				door1->tryOpen();
				if (door1->getState() == HiveWorldDoor::State::Open)
				{
					mCycleState = CycleState::Equilibrium;
					mCommand = CycleCommand::None;
				}
				break;

			default:
				throw GameException("Invalid CycleState for Connector while opening Door 1.");
			}
		}

		void HiveWorldConnector::update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime)
		{
			if (mState == State::LockDisengaged)
			{
				return;
			}

			switch (mCommand)
			{
			case CycleCommand::OpenDoor0:
				processOpenDoor0(world, collisionSim, frameTime);
				break;

			case CycleCommand::OpenDoor1:
				processOpenDoor1(world, collisionSim, frameTime);
				break;

			default:
				break;
			}
		}

	} // live
} // hive