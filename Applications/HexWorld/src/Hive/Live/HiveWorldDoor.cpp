#include <exception>

#include <utils/StringUtils.h>

#include "Hive/Live/HiveWorldDoor.h"
#include "Hive/Live/HiveWorld.h"

#include "GameException.h"
#include "HiveWorldModel.h"


namespace hive
{
	namespace live
	{

		using namespace std;

		HiveWorldDoor::HiveWorldDoor(uint32_t meshPolygonIndex, uint32_t connectorIndex, uint32_t roomIndex, wp::Vector2 const& centre, wp::Vector2 const& extents, hex::direction_type direction, uint32_t flags, uint32_t key)
			: HiveWorldArea({ meshPolygonIndex })
			, mConnectorIndex(connectorIndex)
			, mRoomIndex(roomIndex)
			, mCentre(centre)
			, mExtents(extents)
			, mDirection(direction)
			, mState(State::Closed)
			, mFlags(flags)
			, mForceLockAfterCloseCount(0)
			, mBaseKey(key)
			, mCurKey(key)
			, mTimer(0.0f)
		{
			if (flags & HIVE_DOOR_LOCKED)
			{
				if (!(flags & HIVE_DOOR_LOCKABLE))
				{
					throw GameException("Door was created as locked, but not lockable.");
				}

				if (key == 0)
				{
					throw GameException("Door was created as locked, but key was set to zero.");
				}
			}
			else
			{
				if (flags & HIVE_DOOR_LOCKABLE)
				{
					throw GameException("Door was created as lockable, but not locked.");
				}
			}

			mCollisionLineRanges[0] = make_pair<uint32_t>(0, 0);
			mCollisionLineRanges[1] = make_pair<uint32_t>(0, 0);
		}

		uint32_t HiveWorldDoor::getConnectorIndex() const
		{
			return mConnectorIndex;
		}

		uint32_t HiveWorldDoor::getRoomIndex() const
		{
			return mRoomIndex;
		}

		wp::Vector2 const& HiveWorldDoor::getCentre() const
		{
			return mCentre;
		}

		wp::Vector2 const& HiveWorldDoor::getExtents() const
		{
			return mExtents;
		}

		void HiveWorldDoor::getVertices(float closedAmount, wp::Vector2& v0, wp::Vector2& v1, wp::Vector2& v2, wp::Vector2& v3) const
		{
			v0 = mCentre - mExtents * 0.5f;
			v1 = v0; v1.y += mExtents.y * closedAmount;
			v3 = v0; v3.x += mExtents.x;
			v2 = v3; v2.y += mExtents.y * closedAmount;

			auto angle = getAngle();

			v0.rotateAnticlockwiseAround(mCentre, angle);
			v1.rotateAnticlockwiseAround(mCentre, angle);
			v2.rotateAnticlockwiseAround(mCentre, angle);
			v3.rotateAnticlockwiseAround(mCentre, angle);
		}

		hex::direction_type HiveWorldDoor::getDirection() const
		{
			return mDirection;
		}

		float HiveWorldDoor::getAngle() const
		{
			return ((int)mDirection - 1) * 60.0f;
		}

		HiveWorldDoor::State HiveWorldDoor::getState() const
		{
			return mState;
		}

		bool HiveWorldDoor::blocksVision() const
		{
			switch (mState)
			{
			case HiveWorldDoor::State::Open:
			case HiveWorldDoor::State::Opening:
			case HiveWorldDoor::State::Closing:
				return false;
			default:
				return true;
			}
		}

		float HiveWorldDoor::getOpenPercentage() const
		{
			switch (mState)
			{
			case State::Open:
				return 1.0f;

			case State::Opening:
				return mTimer / OpenCloseTime;

			case State::Closed:
				return 0.0f;

			case State::Closing:
				return 1.0f - mTimer / OpenCloseTime;

			case State::Locked:
				return 0.0f;

			default:
				throw GameException(STR_FORMAT("Unknown Door state: {}", (int)mState));
			}
		}

		bool HiveWorldDoor::isLockable() const
		{
			return ((mFlags & HIVE_DOOR_LOCKABLE) != 0);
		}

		bool HiveWorldDoor::isLocked() const
		{
			return ((mFlags & HIVE_DOOR_LOCKED) != 0);
		}

		void HiveWorldDoor::setCollisionLineRanges(pair<uint32_t, uint32_t> const& range1, pair<uint32_t, uint32_t> const& range2)
		{
			mCollisionLineRanges[0] = range1;
			mCollisionLineRanges[1] = range2;
		}

		void HiveWorldDoor::useKey(uint32_t key)
		{
			if (key > mCurKey)
			{
				throw GameException("Key used on door is greater than active door key.");
			}

			mCurKey -= key;
			
			if (mCurKey == 0)
			{
				unlock();
			}
		}

		void HiveWorldDoor::_lock(uint32_t key)
		{
			mCurKey |= key;
			mFlags |= HIVE_DOOR_LOCKED;
		}

		void HiveWorldDoor::lock()
		{
			if (!isLockable())
			{
				throw GameException("Tried to lock non-lockable door.");
			}

			if (mState != State::Closed)
			{
				throw GameException("Tried to lock non-closed door.");
			}

			_lock(mBaseKey);
		}

		void HiveWorldDoor::_unlock()
		{
			mCurKey = 0;
			mFlags &= ~HIVE_DOOR_LOCKED;
		}

		void HiveWorldDoor::unlock()
		{
			if (!isLockable())
			{
				throw GameException("Tried to unlock non-lockable door.");
			}

			if (mState != State::Closed)
			{
				throw GameException("Tried to unlock non-closed door.");
			}

			_unlock();
		}

		void HiveWorldDoor::checkForceLock()
		{
			if (mForceLockAfterCloseCount > 0)
			{
				_lock(MagicKey); // Magic key, to stop player from unlocking
				mForceLockAfterCloseCount--;
			}
		}

		bool HiveWorldDoor::tryOpen()
		{
			if (isLocked())
			{
				return false;
			}

			switch (mState)
			{
			case State::Open:
			case State::Opening:
				// Nothing to do
				return true;

			case State::Closed:
				mTimer = 0.0f;
				mState = State::Opening;
				return true;

			case State::Closing:
				mTimer = OpenCloseTime - mTimer;
				mState = State::Opening;
				return true;

			case State::Locked:
				return false;

			default:
				throw GameException(STR_FORMAT("Unknown Door state: {}", (int)mState));
			}
		}

		void HiveWorldDoor::close(wp::collide::Simulation* collisionSim)
		{
			switch (mState)
			{
			case State::Open:
				mTimer = 0.0f;
				mState = State::Closing;
				collisionSim->enableStaticLines(mCollisionLineRanges[0].first, mCollisionLineRanges[0].second, true);
				collisionSim->enableStaticLines(mCollisionLineRanges[1].first, mCollisionLineRanges[1].second, true);
				break;

			case State::Opening:
				mTimer = OpenCloseTime - mTimer;
				mState = State::Closing;
				collisionSim->enableStaticLines(mCollisionLineRanges[0].first, mCollisionLineRanges[0].second, true);
				collisionSim->enableStaticLines(mCollisionLineRanges[1].first, mCollisionLineRanges[1].second, true);
				break;

			case State::Closed:
			case State::Locked:
				checkForceLock();
				break;

			case State::Closing:
				// Nothing to do
				break;

			default:
				throw GameException(STR_FORMAT("Unknown Door state: {}", (int)mState));
			}
		}

		void HiveWorldDoor::closeAndForceLock(wp::collide::Simulation* collisionSim)
		{
			mForceLockAfterCloseCount = 1;
			close(collisionSim);
		}

		void HiveWorldDoor::forceUnlock()
		{
			_unlock();
		}

		void HiveWorldDoor::update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime)
		{
			VAR_UNUSED(world);

			switch (mState)
			{
			case State::Open:
				break;

			case State::Opening:
				mTimer += frameTime;
				if (mTimer >= OpenCloseTime)
				{
					mState = State::Open;
					mTimer = 0.0f;

					collisionSim->enableStaticLines(mCollisionLineRanges[0].first, mCollisionLineRanges[0].second, false);
					collisionSim->enableStaticLines(mCollisionLineRanges[1].first, mCollisionLineRanges[1].second, false);
				}
				break;

			case State::Closed:
				break;

			case State::Closing:
				mTimer += frameTime;
				if (mTimer >= OpenCloseTime)
				{
					mState = State::Closed;
					mTimer = 0.0f;
					checkForceLock();
				}
				break;

			case State::Locked:
				break;

			default:
				throw GameException(STR_FORMAT("Unknown Door state: {}", (int)mState));
			}
		}

	} // live
} // hive