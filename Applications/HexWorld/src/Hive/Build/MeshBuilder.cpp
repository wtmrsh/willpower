#pragma once

#include "Hive/Build/MeshBuilder.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		MeshBuilder::MeshBuilder(uint32_t objectIndex, wp::geometry::Mesh* mesh, BuildOptions const& options)
			: mObjectIndex(objectIndex)
			, mMesh(mesh)
			, mOptions(options)
		{
		}

		void MeshBuilder::style(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);
		}

		void MeshBuilder::setMeshMedata(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);
		}

		void MeshBuilder::generate(hex::Layout const& layout)
		{
			generateGeometry(layout);
			addGeometryToMesh(layout);
			style(layout);
			setMeshMedata(layout);
		}

	} // build
} // hive
