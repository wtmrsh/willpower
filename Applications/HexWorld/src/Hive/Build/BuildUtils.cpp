#include <map>
#include <unordered_map>

#include "Hive/Build/BuildUtils.h"


namespace hive
{
	namespace build
	{
		namespace utils
		{
			using namespace std;

			vector<HexVertexData> generateHexVertices(vector<hex::Hex> const& hexes, hex::Layout const& layout, float insetDistance, vector<wp::Vector2>& vertices)
			{
				vector<HexVertexData> vertexData;

				unordered_map<hex::Corner, vector<hex::Corner>> cornerLookup;

				// Get and classify the corners based on the number of hexes touching them
				for (auto const& hex : hexes)
				{
					for (int i = 0; i < 6; ++i)
					{
						hex::Corner corner(hex, i);

						auto it = cornerLookup.find(corner);

						if (it != cornerLookup.end())
						{
							it->second.push_back(corner);
						}
						else
						{
							cornerLookup[corner] = { corner };
						}
					}
				}

				// Generate vertex positions based on the number of hexes touching each corner
				unordered_map<hex::Corner, uint32_t> vertexIndexLookup;
				for (auto const& item : cornerLookup)
				{
					auto const& [corner, corners] = item;

					wp::Vector2 centre, vertexPos;
					switch (corners.size())
					{
						
					case 1:
						// Inset towards hex centre
						centre = layout.toWorld(corner.hex);
						vertexPos = layout.hexCornerOffset(corner.corner);
						vertexPos *= ((layout.radius - insetDistance) / layout.radius);
						break;
						
					case 2:
						// Inset along edge between the two hexes
						centre = layout.toWorld(corners[0].hex);
					
						if (corners[0].rotated(-1) == corners[1].rotated(1))
						{
							auto dir = layout.hexCornerOffset(corners[0].rotated(-1).corner)
								.directionTo(layout.hexCornerOffset(corner.corner));

							centre += layout.hexCornerOffset(corners[0].rotated(-1).corner);
							vertexPos = dir * (layout.radius - insetDistance);
						}
						else
						{
							auto dir = layout.hexCornerOffset(corners[0].rotated(1).corner)
								.directionTo(layout.hexCornerOffset(corner.corner));

							centre += layout.hexCornerOffset(corners[0].rotated(1).corner);
							vertexPos = dir * (layout.radius - insetDistance);
						}
						break;
						
					case 3:
						// Don't inset
						centre = layout.toWorld(corner.hex);
						vertexPos = layout.hexCornerOffset(corner.corner);
						break;

					default:
						throw exception("A corner adjoined by an invalid number of hexes: something is NOT right here!");
					}

					vertexPos += centre;

					auto vIndex = (uint32_t)vertices.size();
					
					vertices.push_back(vertexPos);
					vertexIndexLookup[corner] = vIndex;
				}

				// Build data
				for (auto const& hex : hexes)
				{
					HexVertexData hvd;

					hvd.hex = hex;

					for (int i = 0; i < 6; ++i)
					{
						hex::Corner c{ hex, i };

						hvd.vertexIndices[i] = vertexIndexLookup[c];
					}

					vertexData.push_back(hvd);
				}

				return vertexData;
			}

		}
	}
}