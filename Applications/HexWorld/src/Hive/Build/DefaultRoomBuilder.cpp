#pragma once

#include "Hive/Build/DefaultRoomBuilder.h"
#include "Hive/Build/BuildUtils.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		DefaultRoomBuilder::DefaultRoomBuilder(hive::live::HiveWorldRoomType type, uint32_t index, hive::gen::Room const& room, vector<uint32_t> const& doorIndices, wp::geometry::Mesh* mesh, BuildOptions const& options)
			: RoomBuilder(index, room, doorIndices, mesh, options)
			, mType(type)
		{
		}

		void DefaultRoomBuilder::generateGeometry(hex::Layout const& layout)
		{
			vector<wp::Vector2> vertices;

			auto cells = mRoom.getPrefab().getCells(mRoom.getOrigin(), mRoom.getRotation());

			vector<hex::Hex> hexes;
			for (auto const& cell : cells)
			{
				hexes.push_back(cell.hex);
			}

			auto hexData = hive::build::utils::generateHexVertices(hexes, layout, mOptions.insetDistance(), vertices);

			// Set generated mesh
			mGeneratedRoomMesh.type = mType;
			mGeneratedRoomMesh.cells.clear();
			mGeneratedRoomMesh.vertices.clear();
			mGeneratedRoomMesh.meshPolygonIndices.clear();

			for (auto const& vertex : vertices)
			{
				mGeneratedRoomMesh.vertices.push_back({ vertex });
			}

			auto const& conns = mRoom.getConnectors();

			for (auto const& hd : hexData)
			{
				RoomMeshCell rmc;

				rmc.hex = hd.hex;

				for (int i = 0; i < 6; ++i)
				{
					rmc.connections[i] = false;
					rmc.doorEdgeIndices[i] = -1;
					rmc.vertexIndices.push_back(hd.vertexIndices[i]);
				}

				// Set connectors
				for (auto const& conn : conns)
				{
					if (conn.hexd.hex == rmc.hex)
					{
						rmc.connections[conn.hexd.dir] = true;
					}
				}

				mGeneratedRoomMesh.cells.push_back(rmc);
			}
		}

		RoomMesh DefaultRoomBuilder::generateVertices(hex::Layout const& layout)
		{
			RoomMesh roomMesh;

			vector<wp::Vector2> vertices;

			auto cells = mRoom.getPrefab().getCells(mRoom.getOrigin(), mRoom.getRotation());
			
			vector<hex::Hex> hexes;
			for (auto const& cell : cells)
			{
				hexes.push_back(cell.hex);
			}

			auto hexData = hive::build::utils::generateHexVertices(hexes, layout, mOptions.insetDistance(), vertices);

			for (auto const& vertex : vertices)
			{
				roomMesh.vertices.push_back({ vertex, ~0u });
			}

			auto const& conns = mRoom.getConnectors();

			for (auto const& hd : hexData)
			{
				RoomMeshCell rmc;

				rmc.hex = hd.hex;

				for (int i = 0; i < 6; ++i)
				{
					rmc.connections[i] = false;
					rmc.doorEdgeIndices[i] = -1;
					rmc.vertexIndices.push_back(hd.vertexIndices[i]);
				}

				// Set connectors
				for (auto const& conn : conns)
				{
					if (conn.hexd.hex == rmc.hex)
					{
						rmc.connections[conn.hexd.dir] = true;
					}
				}

				roomMesh.cells.push_back(rmc);
			}

			return roomMesh;
		}

	} // build
} // hive
