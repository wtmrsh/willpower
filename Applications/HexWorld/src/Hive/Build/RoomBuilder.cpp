#pragma once

#include <willpower/geometry/MeshOperations.h>

#include "Hive/Build/RoomBuilder.h"

#include "MeshAttributes.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		RoomBuilder::RoomBuilder(uint32_t index, hive::gen::Room const& room, vector<uint32_t> const& doorIndices, wp::geometry::Mesh* mesh, BuildOptions const& options, uint32_t styleFlags)
			: MeshBuilder(index, mesh, options)
			, mDoorIndices(doorIndices)
			, mStyleFlags(styleFlags)
			, mRoom(room)
		{
		}

		RoomMesh const& RoomBuilder::getRoomMesh() const
		{
			return mGeneratedRoomMesh;
		}

		void RoomBuilder::addGeometryToMesh(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			for (auto& vertex : mGeneratedRoomMesh.vertices)
			{
				vertex.meshVertexIndex = mMesh->addVertex(wp::geometry::Vertex(vertex.pos));
			}

			map<pair<uint32_t, uint32_t>, uint32_t> edgeMap;

			for (uint32_t c = 0; c < (uint32_t)mGeneratedRoomMesh.cells.size(); ++c)
			{
				auto& cell = mGeneratedRoomMesh.cells[c];

				// Need to create edges, but only once
				auto numCellVertices = (uint32_t)cell.vertexIndices.size();

				vector<uint32_t> meshEdgeData;
				for (uint32_t i = 0; i < numCellVertices; ++i)
				{
					auto v0 = cell.vertexIndices[i];
					auto v1 = cell.vertexIndices[(i + 1) % numCellVertices];

					auto mvi0 = (int)mGeneratedRoomMesh.vertices[v0].meshVertexIndex;
					auto mvi1 = (int)mGeneratedRoomMesh.vertices[v1].meshVertexIndex;

					auto ep0 = make_pair(mvi0, mvi1);
					auto ep1 = make_pair(mvi1, mvi0);

					if (edgeMap.find(ep0) == edgeMap.end() && edgeMap.find(ep1) == edgeMap.end())
					{
						auto edgeIndex = mMesh->addEdge(wp::geometry::Edge(mvi0, mvi1));

						edgeMap[ep0] = edgeIndex;
						edgeMap[ep1] = edgeIndex;
					}

					// Create polygon data: get correct direction
					auto eIndex = edgeMap[ep0];

					meshEdgeData.push_back(mvi0);
					meshEdgeData.push_back(mvi1);
					meshEdgeData.push_back(eIndex);
				}

				auto polygonIndex = mMesh->addPolygon(wp::geometry::Polygon(meshEdgeData));
				mPolygonCellMap[polygonIndex] = c;

				// Create door edges
				for (int i = 0; i < 6; ++i)
				{
					if (cell.connections[i])
					{
						wp::geometry::SplitEdgeResult splitResult;

						auto eIndex = meshEdgeData[i * 3 + 2];
						wp::geometry::MeshOperations::splitEdge(mMesh, eIndex, 3, &splitResult);

						// Make sure the door is the right width
						auto doorEdgeIndex = splitResult.newEdgeIndices[1];

						mMesh->setEdgeLength(doorEdgeIndex, mOptions.connectionWidth);

						cell.doorEdgeIndices[i] = doorEdgeIndex;
					}
				}
			}

			// Update room mesh
			set<uint32_t> polygonIndices;

			transform(
				mPolygonCellMap.begin(), 
				mPolygonCellMap.end(),
				inserter(polygonIndices, polygonIndices.end()),
				[] (auto const& item) { return item.first; }
			);

			mGeneratedRoomMesh.meshPolygonIndices = polygonIndices;
			mGeneratedRoomMesh.doorIndices = mDoorIndices;
		}

		void RoomBuilder::style(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			// Style all external vertices
			vector<uint32_t> externalVertexIndices;

			for (auto const& vertex : mGeneratedRoomMesh.vertices)
			{
				auto const& meshVertex = mMesh->getVertex(vertex.meshVertexIndex);

				uint32_t numExternalEdges{ 0 };
				for (auto edgeIndex : meshVertex.getEdgeReferences())
				{
					auto const& edge = mMesh->getEdge(edgeIndex);

					if (edge.getConnectivity() == wp::geometry::Edge::Connectivity::External)
					{
						numExternalEdges++;
					}
				}

				if (numExternalEdges == 2)
				{
					externalVertexIndices.push_back(vertex.meshVertexIndex);
				}
			}

			// Chamfer
			for (uint32_t vertexIndex : externalVertexIndices)
			{
				wp::geometry::MeshOperations::chamferVertex(mMesh, vertexIndex, 16);
			}
		}

		void RoomBuilder::setMeshMedata(hex::Layout const& layout)
		{
			auto const& polygonIndices = mGeneratedRoomMesh.meshPolygonIndices;

			for (auto polygonIndex : polygonIndices)
			{
				// Set polygon vertex render data
				auto& polygon = mMesh->getPolygon(polygonIndex);
				auto const& polygonVertexIndices = polygon.getVertexIndexList();

				auto const& cell = mGeneratedRoomMesh.cells[mPolygonCellMap[polygonIndex]];
				auto cellCentre = layout.toWorld(cell.hex);

				for (auto vertexIndex : polygonVertexIndices)
				{
					// Set UV data for polygon vertices.  The hex is fitted to a unit square in [0, 1].
					auto vertexPos = (mMesh->getVertex(vertexIndex).getPosition() - cellCentre) / layout.radius;
					auto uvCoords = vertexPos * 0.5f + 0.5f;

					HiveWorldPolygonVertexAttributes hwpva{
						uvCoords.x,	uvCoords.y,
						1.0f, 0.0f
					};

					mMesh->setPolygonVertexUserData(polygonIndex, vertexIndex, &hwpva);
				}

				// Set polygon render data
				HiveWorldPolygonAttributes hwpa{
					HexLocator::fromRoom(mObjectIndex, polygonIndices),
					"Room1Material"
				};

				mMesh->setPolygonUserData(polygonIndex, &hwpa);
			}

		}

	} // build
} // hive
