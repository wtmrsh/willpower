#pragma once

#include "HexLocator.h"

#include "Hive/Build/ConnectorBuilder.h"

#include "MeshAttributes.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		ConnectorBuilder::ConnectorBuilder(uint32_t index, hive::gen::Connector const& conn, uint32_t edgeIndex0, uint32_t edgeIndex1, wp::geometry::Mesh* mesh, BuildOptions const& options)
			: MeshBuilder(index, mesh, options)
			, mConnector(conn)
			, mEdgeIndex0(edgeIndex0)
			, mEdgeIndex1(edgeIndex1)
		{
		}

		ConnectorMesh const& ConnectorBuilder::getConnectorMesh() const
		{
			return mGeneratedConnectorMesh;
		}

		void ConnectorBuilder::generateGeometry(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			hex::HexDirected cell0, cell1;

			mConnector.getRoomCells(&cell0, &cell1);

			// For each cell, get centre of the edge, then extrude along the edge get the
			// connection starting points.  Then project into space based on the connector length.
			auto const& edge0 = mMesh->getEdge(mEdgeIndex0);
			auto const& edgeNormal0 = edge0.getNormal();

			auto const& v00 = mMesh->getVertex(edge0.getFirstVertex());
			auto const& v01 = mMesh->getVertex(edge0.getSecondVertex());

			auto c00 = v00.getPosition() - edgeNormal0 * mOptions.doorDepth;
			auto c01 = v01.getPosition() - edgeNormal0 * mOptions.doorDepth;

			auto const& edge1 = mMesh->getEdge(mEdgeIndex1);
			auto const& edgeNormal1 = edge1.getNormal();

			auto const& v10 = mMesh->getVertex(edge1.getFirstVertex());
			auto const& v11 = mMesh->getVertex(edge1.getSecondVertex());

			auto c10 = v10.getPosition() - edgeNormal1 * mOptions.doorDepth;
			auto c11 = v11.getPosition() - edgeNormal1 * mOptions.doorDepth;

			// Set generated mesh
			mGeneratedConnectorMesh.doorEdgeIndices.clear();
			mGeneratedConnectorMesh.meshPolygonIndex = ~0u;
			mGeneratedConnectorMesh.vertices.clear();

			mGeneratedConnectorMesh.vertices.push_back({ c01 });
			mGeneratedConnectorMesh.vertices.push_back({ c00 });
			mGeneratedConnectorMesh.vertices.push_back({ c11 });
			mGeneratedConnectorMesh.vertices.push_back({ c10 });

			mGeneratedConnectorMesh.doorEdgeLookup = make_pair(cell0.hex, cell1.hex);
			mConnector.getDoorIndices(&mGeneratedConnectorMesh.doorIndices[0], &mGeneratedConnectorMesh.doorIndices[1]);

			mGeneratedConnectorMesh.triggerLine[0] = c00.lerp(c11, 0.5f);
			mGeneratedConnectorMesh.triggerLine[1] = c01.lerp(c10, 0.5f);
		}

		void ConnectorBuilder::addGeometryToMesh(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			vector<uint32_t> meshVertexIndices, meshEdgeData;
			meshVertexIndices.reserve(mGeneratedConnectorMesh.vertices.size());

			for (auto const& vertex : mGeneratedConnectorMesh.vertices)
			{
				auto vertexIndex = mMesh->addVertex(wp::geometry::Vertex(vertex.pos));
				meshVertexIndices.push_back(vertexIndex);
			}

			for (int i = 0; i < 4; ++i)
			{
				auto ev0 = meshVertexIndices[i];
				auto ev1 = meshVertexIndices[(i + 1) % 4];

				auto edgeIndex = mMesh->addEdge(wp::geometry::Edge(ev0, ev1));

				meshEdgeData.push_back(ev0);
				meshEdgeData.push_back(ev1);
				meshEdgeData.push_back(edgeIndex);
			}

			// Update connector mesh
			mGeneratedConnectorMesh.meshPolygonIndex = mMesh->addPolygon(wp::geometry::Polygon(meshEdgeData));
			mGeneratedConnectorMesh.doorEdgeIndices[mGeneratedConnectorMesh.doorEdgeLookup.first] = meshEdgeData[2];
			mGeneratedConnectorMesh.doorEdgeIndices[mGeneratedConnectorMesh.doorEdgeLookup.second] = meshEdgeData[8];
		}

		void ConnectorBuilder::setMeshMedata(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			auto polygonIndex = mGeneratedConnectorMesh.meshPolygonIndex;

			auto& polygon = mMesh->getPolygon(polygonIndex);
			auto const& polygonVertexIndices = polygon.getVertexIndexList();

			// Set polygon vertex render data
			float vertexUvCoords[8] = {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
			};

			for (int i = 0; i < 4; ++i)
			{
				auto vertexIndex = polygonVertexIndices[i];

				HiveWorldPolygonVertexAttributes hwpva{
					vertexUvCoords[i * 2 + 0], vertexUvCoords[i * 2 + 1],
					1.0f, 0.0f
				};

				mMesh->setPolygonVertexUserData(polygonIndex, vertexIndex, &hwpva);
			}

			// Set polygon render data
			HiveWorldPolygonAttributes hwpa{
				HexLocator::fromConnector(mObjectIndex, { polygonIndex }),
				"Room1Material"
			};

			mMesh->setPolygonUserData(polygonIndex, &hwpa);
		}

	} // build
} // hive
