#include <willpower/common/Vector2.h>

#include <willpower/geometry/MeshOperations.h>

#include <applib/ModelInstance.h>

#include "Hive/Build/HiveBuilder.h"
#include "Hive/Build/RoomBuilder.h"
#include "Hive/Build/DefaultRoomBuilder.h"
#include "Hive/Build/DoorBuilder.h"
#include "Hive/Build/ConnectorBuilder.h"
#include "Hive/Live/HiveWorldRoomType.h"

#include "HiveWorldModel.h"
#include "MeshAttributes.h"
#include "MapException.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		HiveBuilder::HiveBuilder(hex::Layout const& layout, BuildOptions const& options)
			: mLayout(layout)
			, mOptions(options)
		{
			// Check options
			if (options.connectionWidth >= layout.radius)
			{
				throw MapException("Connection width must be less than cell radius");
			}
			if (options.doorDepth >= (options.connectionLength / 2))
			{
				throw MapException("Door depth must be less than half the connection length");
			}
		}

		hex::Layout const& HiveBuilder::getHexLayout() const
		{
			return mLayout;
		}

		map<int32_t, vector<hive::build::utils::HexVertexData>> HiveBuilder::generateRoomVertices(hive::gen::CellDataMap const& cellMap, hex::Layout const& layout, float insetDistance, vector<wp::Vector2>& vertices)
		{
			map<int32_t, vector<hive::build::utils::HexVertexData>> roomVertexMap;

			auto roomHexMap = hive::gen::utils::groupHexesByRoom(cellMap);

			for (auto const& item : roomHexMap)
			{
				auto const& [roomIndex, hexes] = item;

				roomVertexMap[roomIndex] = hive::build::utils::generateHexVertices(hexes, layout, insetDistance, vertices);
			}

			return roomVertexMap;
		}

		void HiveBuilder::generateRoom(uint32_t roomIndex, hive::gen::Room const& room, vector<uint32_t> const& doorIndices, hex::Layout const& layout, wp::geometry::Mesh* mesh)
		{
			unique_ptr<RoomBuilder> roomBuilder;

			switch (room.getPrefab().getType())
			{
			case hive::gen::RoomPrefabType::None:
				roomBuilder = make_unique<DefaultRoomBuilder>(hive::live::HiveWorldRoomType::Open, roomIndex, room, doorIndices, mesh, mOptions);
				break;

			case hive::gen::RoomPrefabType::Arena1:
				roomBuilder = make_unique<DefaultRoomBuilder>(hive::live::HiveWorldRoomType::Arena, roomIndex, room, doorIndices, mesh, mOptions);
				break;

			default:
				throw MapException("Unknown room prefab type.");
			}

			if (roomBuilder)
			{
				roomBuilder->generate(layout);
				mRooms.push_back(roomBuilder->getRoomMesh());
			}
		}

		void HiveBuilder::generateDoor(uint32_t doorIndex, hive::gen::Door const& door, uint32_t roomIndex, uint32_t connIndex, uint32_t roomEdgeIndex, uint32_t connEdgeIndex, hex::Layout const& layout, wp::geometry::Mesh* mesh)
		{
			DoorBuilder doorBuilder(doorIndex, door, roomIndex, connIndex, roomEdgeIndex, connEdgeIndex, mesh, mOptions);

			doorBuilder.generate(layout);
			mDoors.push_back(doorBuilder.getDoorMesh());
		}

		void HiveBuilder::generateConnector(uint32_t connIndex, hive::gen::Connector const& conn, uint32_t edgeIndex0, uint32_t edgeIndex1, hex::Layout const& layout, wp::geometry::Mesh* mesh)
		{
			ConnectorBuilder connectorBuilder(connIndex, conn, edgeIndex0, edgeIndex1, mesh, mOptions);

			connectorBuilder.generate(layout);
			mConnectors.push_back(connectorBuilder.getConnectorMesh());
		}

		map<int32_t, vector<hive::build::utils::HexVertexData>> HiveBuilder::buildMiniMap(hive::gen::CellDataMap const& cellMap, hex::Layout const& layout, float insetDistance, vector<wp::Vector2>& vertices) const
		{
			return generateRoomVertices(cellMap, layout, insetDistance, vertices);
		}

		map<uint32_t, vector<uint32_t>> HiveBuilder::getRoomDoorIndices(hive::gen::Generator const* generator) const
		{
			map<uint32_t, vector<uint32_t>> roomDoorMap;

			auto const& doors = generator->getDoors();
			for (uint32_t i = 0 ; i < (uint32_t)doors.size(); ++i)
			{
				auto const& door = doors[i];

				auto res = roomDoorMap.insert(make_pair(door.roomIndex, vector<uint32_t>()));
				
				res.first->second.push_back(i);
			}

			return roomDoorMap;
		}

		void HiveBuilder::buildMapMesh(hive::gen::Generator const* generator, wp::geometry::Mesh* mesh, wp::application::resourcesystem::ResourceManager* resourceMgr)
		{
			VAR_UNUSED(resourceMgr);

			// Get all door indices for each room
			auto roomDoorMap = getRoomDoorIndices(generator);

			// Generate rooms
			auto const& rooms = generator->getRooms();
			for (uint32_t i = 0; i < (uint32_t)rooms.size(); ++i)
			{
				auto const& room = rooms[i];

				auto doorIndicesIt = roomDoorMap.find(i);
				vector<uint32_t> doorIndices = doorIndicesIt != roomDoorMap.end() ? doorIndicesIt->second : vector<uint32_t>();
				
				generateRoom(i, room, doorIndices, mLayout, mesh);
			}

			// Generate connectors after rooms, as we need the edge indices
			// of where the doors will go
			auto const& connectors = generator->getConnectors();
			for (uint32_t i = 0; i < (uint32_t)connectors.size(); ++i)
			{
				auto const& connector = connectors[i];
				
				uint32_t roomIndex0, roomIndex1, edgeIndex0{ ~0u }, edgeIndex1{ ~0u };
				hex::HexDirected roomCell0, roomCell1;

				connector.getRoomIndices(&roomIndex0, &roomIndex1);
				connector.getRoomCells(&roomCell0, &roomCell1);

				for (auto const& cell : mRooms[roomIndex0].cells)
				{
					if (cell.hex == roomCell0.hex)
					{
						edgeIndex0 = cell.doorEdgeIndices[roomCell0.dir];
					}
				}

				for (auto const& cell : mRooms[roomIndex1].cells)
				{
					if (cell.hex == roomCell1.hex)
					{
						edgeIndex1 = cell.doorEdgeIndices[roomCell1.dir];
					}
				}

				if (edgeIndex0 == ~0u || edgeIndex1 == ~0u)
				{
					throw MapException("Could not find door edge indices for connector.");
				}

				generateConnector(
					i, 
					connector, 
					edgeIndex0, 
					edgeIndex1, 
					mLayout, 
					mesh
				);
			}

			// Generate doors last as they need both room and connector mesh information
			auto const& doors = generator->getDoors();
			for (uint32_t i = 0; i < (uint32_t)doors.size(); ++i)
			{
				auto const& door = doors[i];

				auto const& doorRoom = mRooms[door.roomIndex];
				auto const& doorConn = mConnectors[door.connectorIndex];

				for (auto const& cell : doorRoom.cells)
				{
					if (cell.hex == door.hexd.hex)
					{
						auto roomEdgeIndex = cell.doorEdgeIndices[door.hexd.dir];
						
						generateDoor(
							i, 
							door, 
							door.roomIndex, 
							door.connectorIndex, 
							roomEdgeIndex, 
							doorConn.doorEdgeIndices.at(cell.hex), 
							mLayout, 
							mesh
						);
					}
				}
			}
		}

		hive::live::HiveWorld* HiveBuilder::createHiveWorld() const
		{
			auto world = new hive::live::HiveWorld();

			// Add rooms
			for (auto const& roomMesh : mRooms)
			{
				auto roomIndex = world->addRoom({
					roomMesh.type,
					roomMesh.meshPolygonIndices,
					roomMesh.doorIndices
					});

				auto room = world->getRoom(roomIndex);

				// Add debug trigger to mark arena as complete
				if (room->getType() == hive::live::HiveWorldRoomType::Arena)
				{
					auto completeArenaCallback = [](HexLocator const& locator, Trigger const* trigger, TriggerSide side, TriggerData const& data) {
						VAR_UNUSED(data);

						auto model = static_cast<HiveWorldModel*>(applib::ModelInstance::get());

						auto room = model->world->getRoom(locator.typeIndex);
						auto const& doorIndices = room->getDoorIndices();

						for (auto doorIndex : doorIndices)
						{
							auto door = model->world->getDoor(doorIndex);
							door->forceUnlock();
						}

						room->_unsetFlag(HIVE_ROOM_ARENA_STATE_ACTIVE);
						room->_setFlag(HIVE_ROOM_ARENA_STATE_EXTINCT);
					};

					room->addTrigger(
						Trigger::fromCircle(
							1,
							{},
							{ completeArenaCallback },
							wp::Vector2(0, 0),
							64
						)
					);
				}
			}

			// Add doors
			for (auto const& doorMesh : mDoors)
			{
				uint32_t doorFlags{ 0 };
				uint32_t doorKey{ 0 };

				if (doorMesh.locked)
				{
					doorFlags |= HIVE_DOOR_LOCKABLE;
					doorFlags |= HIVE_DOOR_LOCKED;
					doorKey = doorMesh.key;
				}

				auto doorIndex = world->addDoor({
					doorMesh.meshPolygonIndex,
					doorMesh.connectorIndex,
					doorMesh.roomIndex,
					doorMesh.centre,
					doorMesh.extents,
					doorMesh.direction,
					doorFlags,
					doorKey
				});

				auto room = world->getRoom(doorMesh.roomIndex);

				int doorTriggerCount;
				TriggerSide doorTriggerSide;
				TriggerCallback doorTriggerCallback;

				switch (room->getType())
				{
				case hive::live::HiveWorldRoomType::Open:
					doorTriggerCount = -1;
					doorTriggerSide = TriggerSide::Into;
					doorTriggerCallback = [](HexLocator const& locator, Trigger const* trigger, TriggerSide side, TriggerData const& data) {
						VAR_UNUSED(locator);

						auto model = static_cast<HiveWorldModel*>(applib::ModelInstance::get());
						auto door = model->world->getDoor(data.targetIndex);

						door->tryOpen();
					};
					break;

				case hive::live::HiveWorldRoomType::Arena:
					doorTriggerCount = -1;
					doorTriggerSide = TriggerSide::Any;
					doorTriggerCallback = [](HexLocator const& locator, Trigger const* trigger, TriggerSide side, TriggerData const& data) {
						VAR_UNUSED(data);

						auto model = static_cast<HiveWorldModel*>(applib::ModelInstance::get());

						auto room = model->world->getRoom(locator.typeIndex);
						auto const& doorIndices = room->getDoorIndices();

						// Get room flags: 
						auto roomFlags = room->getFlags();

						// - If the arena is dormant and side is out-of, then activate
						// - If the arena is active, then ignore
						// - If the arena is extinct and side is into, then cycle door
						if (roomFlags & HIVE_ROOM_ARENA_STATE_DORMANT && side == TriggerSide::OutOf)
						{
							for (auto doorIndex : doorIndices)
							{
								auto door = model->world->getDoor(doorIndex);
								door->closeAndForceLock(model->collisionSim);
							}

							room->_unsetFlag(HIVE_ROOM_ARENA_STATE_DORMANT);
							room->_setFlag(HIVE_ROOM_ARENA_STATE_ACTIVE);
						}
						else if (roomFlags & HIVE_ROOM_ARENA_STATE_EXTINCT && side == TriggerSide::Into)
						{
							auto door = model->world->getDoor(data.targetIndex);
							door->tryOpen();
						}
					};
					break;

				default:
					throw MapException("Unknown HiveWorldRoomType.");
				}
				
				// Create trigger to open the door on proximity
				world->getRoom(doorMesh.roomIndex)->addTrigger(
					Trigger::fromArc(
						doorTriggerCount,
						{ (int32_t)doorIndex },
						{ doorTriggerCallback },
						doorMesh.centre,
						doorMesh.extents.x / 2.0f,
						270 - (float)mLayout.hexEdgeAngle(doorMesh.direction),
						180.0f,
						doorTriggerSide
					)
				);
			}
			
			// Add connectors
			for (auto const& connMesh : mConnectors)
			{
				auto connIndex = world->addConnector({
					connMesh.meshPolygonIndex,
					connMesh.doorIndices[0],
					connMesh.doorIndices[1]
					});

				auto connector = world->getConnector(connIndex);
				
				// Create trigger to cycle the connector
				auto cycleConnectorCallback = [](HexLocator const& locator, Trigger const* trigger, TriggerSide side, TriggerData const& data) {
					VAR_UNUSED(data);

					auto model = static_cast<HiveWorldModel*>(applib::ModelInstance::get());
					auto conn = model->world->getConnector(locator.typeIndex);

					conn->tryCycle(model->world);
				};

				connector->addTrigger(
					Trigger::fromLine(
						-1, 
						{},
						{ cycleConnectorCallback }, 
						connMesh.triggerLine[0], 
						connMesh.triggerLine[1]
					)
				);
			}

			return world;
		}

	} // build
} // hive