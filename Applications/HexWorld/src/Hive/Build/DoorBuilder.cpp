#pragma once

#include <willpower/geometry/MeshOperations.h>

#include "Hex.h"

#include "Hive/Build/DoorBuilder.h"

#include "MeshAttributes.h"


namespace hive
{
	namespace build
	{
		using namespace std;

		DoorBuilder::DoorBuilder(uint32_t index, hive::gen::Door const& door, uint32_t roomIndex, uint32_t connIndex, uint32_t roomEdgeIndex, uint32_t connEdgeIndex, wp::geometry::Mesh* mesh, BuildOptions const& options)
			: MeshBuilder(index, mesh, options)
			, mRoomIndex(roomIndex)
			, mConnectorIndex(connIndex)
			, mDoor(door)
			, mRoomEdgeIndex(roomEdgeIndex)
			, mConnectorEdgeIndex(connEdgeIndex)
		{
		}

		DoorMesh const& DoorBuilder::getDoorMesh() const
		{
			return mGeneratedDoorMesh;
		}

		void DoorBuilder::calculateDoorExtents(wp::Vector2 const& v0, wp::Vector2 const& v1, wp::Vector2 const& v2, wp::Vector2 const& v3, wp::Vector2* centre, wp::Vector2* extents) const
		{
			// Input vertices are clockwise from lower-left
			auto u0 = v0.lerp(v1, 0.5f);
			auto u1 = v3.lerp(v2, 0.5f);

			*centre = u0.lerp(u1, 0.5f);
			extents->set(u0.distanceTo(u1), v0.distanceTo(v1));
		}

		void DoorBuilder::generateGeometry(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			// Set generated mesh
			mGeneratedDoorMesh = {
				mRoomIndex,
				mConnectorIndex,
				mRoomEdgeIndex,
				mConnectorEdgeIndex,
				wp::Vector2::ZERO,
				wp::Vector2::ZERO,
				mDoor.hexd.dir,
				mDoor.locked,
				mDoor.key,
				~0u
			};
		}

		void DoorBuilder::addGeometryToMesh(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			// Bridge edges from room to connector
			wp::geometry::BridgeEdgesOptions options;
			wp::geometry::BridgeEdgesResult result;

			wp::geometry::MeshOperations::bridgeEdges(mMesh, mRoomEdgeIndex, mConnectorEdgeIndex, options, &result);

			// Get extents
			auto const& edge0 = mMesh->getEdge(mConnectorEdgeIndex);
			auto const& edge1 = mMesh->getEdge(mRoomEdgeIndex);

			auto const& v0 = mMesh->getVertex(edge0.getFirstVertex()).getPosition();
			auto const& v1 = mMesh->getVertex(edge1.getSecondVertex()).getPosition();
			auto const& v2 = mMesh->getVertex(edge1.getFirstVertex()).getPosition();
			auto const& v3 = mMesh->getVertex(edge0.getSecondVertex()).getPosition();

			calculateDoorExtents(v0, v1, v2, v3, &mGeneratedDoorMesh.centre, &mGeneratedDoorMesh.extents);
			mGeneratedDoorMesh.meshPolygonIndex = result.polygons[0].index;
		}

		void DoorBuilder::setMeshMedata(hex::Layout const& layout)
		{
			VAR_UNUSED(layout);

			auto polygonIndex = mGeneratedDoorMesh.meshPolygonIndex;
			auto& polygon = mMesh->getPolygon(polygonIndex);
			auto const& polygonVertexIndices = polygon.getVertexIndexList();

			// Set polygon vertex render data
			float vertexUvCoords[8] = {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f
			};

			for (int i = 0; i < 4; ++i)
			{
				auto vertexIndex = polygonVertexIndices[i];

				HiveWorldPolygonVertexAttributes hwpva{
					vertexUvCoords[i * 2 + 0], vertexUvCoords[i * 2 + 1],
					1.0f, 0.0f
				};

				mMesh->setPolygonVertexUserData(polygonIndex, vertexIndex, &hwpva);
			}

			// Set polygon render data
			HiveWorldPolygonAttributes hwpa{
				HexLocator::fromDoor(mObjectIndex, { polygonIndex }),
				"Room1Material"
			};

			mMesh->setPolygonUserData(polygonIndex, &hwpa);

		}

	} // build
} // hive
