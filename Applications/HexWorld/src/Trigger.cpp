#include <exception>

#include <utils/StringUtils.h>

#include <willpower/common/MathsUtils.h>

#include "Trigger.h"
#include "GameException.h"
#include "NotImplementedException.h"

using namespace std;


Trigger::Trigger()
	: mShape(Shape::None)
	, mSide(TriggerSide::Any)
	, mInitialCount(0)
	, mCount(0)
	, mData({})
{
}

void Trigger::initTrigger(Trigger* trigger, Shape shape, TriggerSide side, int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks)
{
	trigger->mShape = shape;
	trigger->mSide = side;
	trigger->mInitialCount = count;
	trigger->mCount = count;
	trigger->mData = data;
	trigger->mCallbacks = callbacks;
}

Trigger Trigger::fromLine(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Left && side != TriggerSide::Right)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Line, side, count, data, callbacks);

	t.mVertices = { v0, v1 };

	return t;
}

Trigger Trigger::fromArc(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, float angle, float width, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Arc, side, count, data, callbacks);

	t.mVertices = { centre, {radius, angle}, { angle - width / 2.0f, angle + width / 2.0f } };

	return t;
}

Trigger Trigger::fromSemicircle(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, float angle, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Semicircle, side, count, data, callbacks);
	
	t.mVertices = { centre, {radius, angle} };

	return t;
}

Trigger Trigger::fromCircle(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Circle, side, count, data, callbacks);

	t.mVertices = { centre, {radius, radius} };

	return t;
}

Trigger Trigger::fromHexagon(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Hexagon, side, count, data, callbacks);
	
	t.mVertices = { centre, {radius, radius} };

	return t;
}

Trigger Trigger::fromAABB(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, wp::Vector2 const& minExtent, wp::Vector2 const& maxExtent, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::AABB, side, count, data, callbacks);

	t.mVertices = { minExtent, maxExtent };

	return t;
}

Trigger Trigger::fromPolygon(int count, TriggerData const& data, vector<TriggerCallback> const& callbacks, vector<wp::Vector2> const& vertices, TriggerSide side)
{
	Trigger t;

	if (side != TriggerSide::Any && side != TriggerSide::Into && side != TriggerSide::OutOf)
	{
		throw GameException("Cannot create trigger with the given side value.");
	}

	initTrigger(&t, Shape::Polygon, side, count, data, callbacks);

	t.mVertices = vertices;

	return t;
}

vector<wp::Vector2> Trigger::generateCircleVertices(uint32_t count, float startAngle) const
{
	vector<wp::Vector2> vertices(count);

	auto const& centre = mVertices[0];
	float radius = mVertices[1].x;
	
	float angle = startAngle;
	float dAngle = 360.0f / (count - 1);

	for (uint32_t i = 0; i < count; ++i)
	{
		auto p = centre + wp::Vector2::UNIT_Y.rotatedCopy(angle) * radius;
		vertices[i] = p;

		angle += dAngle;
	}

	return vertices;
}

vector<wp::Vector2> Trigger::generateSegmentVertices(float density, float spanAngle, bool close) const
{
	auto numVertices = (uint32_t)(spanAngle * density);

	if (numVertices < 3)
	{
		numVertices = 3;
	}

	vector<wp::Vector2> vertices(numVertices);

	auto const& centre = mVertices[0];
	float radius = mVertices[1].x;
	float startAngle = mVertices[1].y - spanAngle * 0.5f;
	float endAngle = startAngle + spanAngle;
	float dAngle = (endAngle - startAngle) / (numVertices - 1);

	float angle = startAngle;
	for (uint32_t i = 0; i < numVertices; ++i)
	{
		auto p = centre + wp::Vector2::UNIT_Y.rotatedCopy(angle) * radius;
		vertices[i] = p;

		angle += dAngle;
	}

	// Add final vertex to close the loop
	if (close)
	{
		vertices.push_back(centre + wp::Vector2::UNIT_Y.rotatedCopy(startAngle) * radius);
	}

	return vertices;
}

vector<wp::Vector2> Trigger::getLineVertices() const
{
	vector<wp::Vector2> vertices;

	switch (mShape)
	{
	case Shape::None:
		break;

	case Shape::Line:
		vertices = mVertices;
		break;

	case Shape::Arc:
		vertices = generateSegmentVertices(0.15f, mVertices[2].y - mVertices[2].x, false);
		break;

	case Shape::Semicircle:
		vertices = generateSegmentVertices(0.15f, 180.0f, true);
		break;

	case Shape::Circle:
		vertices = generateCircleVertices(32, 0.0f);
		break;

	case Shape::Hexagon:
		vertices = generateCircleVertices(6, 30.0f);
		break;

	case Shape::AABB:
		vertices.push_back(mVertices[0]);
		vertices.push_back(wp::Vector2(mVertices[0].x, mVertices[1].y));
		vertices.push_back(mVertices[1]);
		vertices.push_back(wp::Vector2(mVertices[1].x, mVertices[0].y));
		vertices.push_back(mVertices[0]);
		break;

	case Shape::Polygon:
		vertices = mVertices;
		vertices.push_back(mVertices[0]);
		break;

	default:
		throw GameException(STR_FORMAT("Unknown Trigger shape: {}", (int)mShape));
	}

	return vertices;
}

void Trigger::getHitSide(wp::LineHit const& hit, TriggerSide* side)
{
	auto flags = hit.getFlags();
	if (flags & wp::LineHit::Flags::HitEnters)
	{
		*side = TriggerSide::Into;
	}
	else if (flags & wp::LineHit::Flags::HitExits)
	{
		*side = TriggerSide::OutOf;
	}
}

bool Trigger::testLine(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	wp::LineHit hit;

	auto result = wp::MathsUtils::lineLineIntersection(v0, v1, mVertices[0], mVertices[1], &hit);

	if (side)
	{
		getHitSide(hit, side);
	}

	return result == wp::MathsUtils::Intersecting;
}

bool Trigger::testArc(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	wp::LineHit hit;
	auto result = wp::MathsUtils::lineArcIntersection(v0, v1, mVertices[0], mVertices[1].x, mVertices[2].x, mVertices[2].y, &hit);
	
	bool intersects = result == wp::MathsUtils::Intersecting || result == wp::MathsUtils::DoublyIntersecting;
	auto flags = hit.getFlags();

	if (side)
	{
		getHitSide(hit, side);
	}

	switch (mSide)
	{
	case TriggerSide::Any:
		return intersects;
	case TriggerSide::Into:
		return intersects && (flags & wp::LineHit::Flags::HitEnters);
	case TriggerSide::OutOf:
		return intersects && (flags & wp::LineHit::Flags::HitExits);
	default:
		throw GameException("Cannot test trigger with the given side value.");
	}
}

bool Trigger::testSemicircle(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	VAR_UNUSED(v0);
	VAR_UNUSED(v1);
	VAR_UNUSED(side);

	THROW_NOT_IMPLEMENTED
}

bool Trigger::testCircle(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	wp::LineHit hit;
	auto result = wp::MathsUtils::lineCircleIntersection(v0, v1, mVertices[0], mVertices[1].x, &hit);

	bool intersects = result == wp::MathsUtils::Intersecting || result == wp::MathsUtils::DoublyIntersecting;
	auto flags = hit.getFlags();

	if (side)
	{
		getHitSide(hit, side);
	}

	switch (mSide)
	{
	case TriggerSide::Any:
		return intersects;
	case TriggerSide::Into:
		return intersects && (flags & wp::LineHit::Flags::HitEnters);
	case TriggerSide::OutOf:
		return intersects && (flags & wp::LineHit::Flags::HitExits);
	default:
		throw GameException("Cannot test trigger with the given side value.");
	}
}

bool Trigger::testHexagon(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	VAR_UNUSED(v0);
	VAR_UNUSED(v1);
	VAR_UNUSED(side);

	THROW_NOT_IMPLEMENTED
}

bool Trigger::testAABB(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	wp::LineHit hit;
	auto result = wp::MathsUtils::lineBoxIntersection(v0, v1, mVertices[0], mVertices[1], &hit);
	
	if (side)
	{
		getHitSide(hit, side);
	}

	switch (result)
	{
	case wp::MathsUtils::LineIntersectionType::Intersecting:
		return (hit.getFlags() & wp::LineHit::Flags::HitEnters) != 0;

	case wp::MathsUtils::LineIntersectionType::DoublyIntersecting:
		return true;

	default:
		return false;
	}
}

bool Trigger::testPolygon(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side) const
{
	VAR_UNUSED(v0);
	VAR_UNUSED(v1);
	VAR_UNUSED(side);

	THROW_NOT_IMPLEMENTED
}

int Trigger::getCount() const
{
	return mCount;
}

void Trigger::setCount(int count)
{
	mInitialCount = mCount = count;
}

void Trigger::reset()
{
	mCount = mInitialCount;
}

void Trigger::check(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator)
{
	if (mCount == 0)
	{
		return;
	}

	bool activated;
	TriggerSide side;

	switch (mShape)
	{
	case Shape::None:
		activated = false;
		break;

	case Shape::Line:
		activated = testLine(oldPos, newPos, &side);
		break;

	case Shape::Arc:
		activated = testArc(oldPos, newPos, &side);
		break;

	case Shape::Semicircle:
		activated = testSemicircle(oldPos, newPos, &side);
		break;

	case Shape::Circle:
		activated = testCircle(oldPos, newPos, &side);
		break;

	case Shape::Hexagon:
		activated = testHexagon(oldPos, newPos, &side);
		break;

	case Shape::AABB:
		activated = testAABB(oldPos, newPos, &side);
		break;

	case Shape::Polygon:
		activated = testPolygon(oldPos, newPos, &side);
		break;

	default:
		throw GameException(STR_FORMAT("Unknown Trigger shape: {}", (int)mShape));
	}

	if (activated)
	{
		mCount--;

		for (auto callback : mCallbacks)
		{
			callback(locator, this, side, mData);
		}
	}
}