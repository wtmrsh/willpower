#include "ProtoEntityDefinitionFactory.h"
#include "Animation.h"
#include "GameException.h"

using namespace std;


ProtoEntityDefinitionFactory::ProtoEntityDefinitionFactory()
	: applib::ProtoEntityDefaultDefinitionFactory()
{
}

uint32_t ProtoEntityDefinitionFactory::getAnimationIdFromName(string const& actor, string const& anim)
{
	VAR_UNUSED(actor);

	if (anim == "idle")
	{
		return (uint32_t)Animation::A_Idle;
	}
	else
	{
		throw GameException(STR_FORMAT("Unknown animation: {}", anim));
	}
}