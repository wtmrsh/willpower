@@Version

@@Uniform(vec4 DIFFUSE);
@@Texture(sampler2D TEX1);
@@Texture(sampler2D TEX2);

void main()
{
	vec2 weights = @Vec2(@In(WEIGHTS));
	vec4 colour = @Vec4(@In(COLOUR));
    colour *= @Uniform(DIFFUSE);

	vec2 tc0 = @In(TEXCOORDS).st;
	vec2 tc1 = @In(TEXCOORDS).pq;

	@Out(vec4 COLOUR) = (texture(@Texture(TEX1), tc0) * weights.x + texture(@Texture(TEX2), tc1) * weights.y) * colour;
}