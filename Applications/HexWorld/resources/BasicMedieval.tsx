<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="BasicMedieval" tilewidth="48" tileheight="48" tilecount="100" columns="10">
 <image source="ground.png" width="512" height="512"/>
 <tile id="0">
  <properties>
   <property name="Name" value="Grass1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="Name" value="Grass1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="Name" value="Grass1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="Name" value="Grass1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="Name" value="Cobbles1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="Name" value="Cobbles1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="Name" value="Cobbles1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="Name" value="Dirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="Name" value="Dirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="Name" value="Dirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="Name" value="Dirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="Name" value="Tiles1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="Name" value="Tiles2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="Name" value="Tiles3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="Name" value="Tiles4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="Name" value="Stones1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="Name" value="Stones2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="Name" value="Stones3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="Name" value="Stones4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="Name" value="Dirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="Name" value="Dirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="Name" value="Dirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="Name" value="Dirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="Frame" type="int" value="0"/>
   <property name="Name" value="Water"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="Frame" type="int" value="1"/>
   <property name="Name" value="Water"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="Frame" type="int" value="2"/>
   <property name="Name" value="Water"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="Frame" type="int" value="3"/>
   <property name="Name" value="Water"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="Name" value="Dirt4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="Name" value="Dirt4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="Name" value="Dirt4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="Name" value="Dirt4"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="Frame" type="int" value="0"/>
   <property name="Name" value="Slime"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="Frame" type="int" value="1"/>
   <property name="Name" value="Slime"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="Frame" type="int" value="2"/>
   <property name="Name" value="Slime"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="Frame" type="int" value="3"/>
   <property name="Name" value="Slime"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="Name" value="BakedDirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="Name" value="BakedDirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="Name" value="BakedDirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="Name" value="BakedDirt1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="Frame" type="int" value="1"/>
   <property name="Name" value="Lava"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="Frame" type="int" value="2"/>
   <property name="Name" value="Lava"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="Frame" type="int" value="3"/>
   <property name="Name" value="Lava"/>
   <property name="Type" value="Animated"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="Name" value="BakedDirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="Name" value="BakedDirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="Name" value="BakedDirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="Name" value="BakedDirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="Name" value="BakedDirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="71">
  <properties>
   <property name="Name" value="BakedDirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="72">
  <properties>
   <property name="Name" value="BakedDirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="Name" value="BakedDirt3"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="76">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="78">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="4"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="Name" value="Regolith"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="5"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="Name" value="Dirt2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="Name" value="Grass2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="Name" value="Grass2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="Name" value="Grass2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="Name" value="Grass2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="Name" value="Grass2"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="4"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="Name" value="CrackedStone1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="91">
  <properties>
   <property name="Name" value="CrackedStone1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="92">
  <properties>
   <property name="Name" value="CrackedStone1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="Name" value="CrackedStone1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="Name" value="Mess1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="Name" value="Mess1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="96">
  <properties>
   <property name="Name" value="Mess1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="97">
  <properties>
   <property name="Name" value="Mess1"/>
   <property name="Type" value="Static"/>
   <property name="Variant" type="int" value="3"/>
  </properties>
 </tile>
</tileset>
