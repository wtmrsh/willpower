#pragma once

#include <vector>
#include <unordered_map>

#include <willpower/common/Vector2.h>

#include "Platform.h"


namespace hive
{
	namespace build
	{

		struct ConnectorVertex
		{
			wp::Vector2 pos;
		};

		struct ConnectorMesh
		{
			std::vector<ConnectorVertex> vertices;
			std::pair<hex::Hex, hex::Hex> doorEdgeLookup;
			uint32_t doorIndices[2];

			// wp::geometry::Mesh data
			uint32_t meshPolygonIndex;
			std::unordered_map<hex::Hex, uint32_t> doorEdgeIndices;

			// Central line for cycling trigger
			wp::Vector2 triggerLine[2];
		};

	} // build
} // hive
