#pragma once

#include "Platform.h"


namespace hive
{
	namespace build
	{

		struct BuildOptions
		{
			float connectionLength{ 48.0f };
			float connectionWidth{ 16.0f };
			float doorDepth{ 16.0f };

			float insetDistance() const
			{
				return connectionLength / 2.0f;
			}
		};

	} // build
} // hive
