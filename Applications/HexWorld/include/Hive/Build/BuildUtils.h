#pragma once

#include <vector>

#include "Platform.h"
#include "Hex.h"


namespace hive
{
	namespace build
	{
		namespace utils
		{

			struct HexVertexData
			{
				hex::Hex hex;
				uint32_t vertexIndices[6];
			};

			std::vector<HexVertexData> generateHexVertices(std::vector<hex::Hex> const& hexes, hex::Layout const& layout, float insetDistance, std::vector<wp::Vector2>& vertices);

		} // utils
	} // build
} // hive
