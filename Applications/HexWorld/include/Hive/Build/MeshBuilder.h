#pragma once

#include <willpower/geometry/Mesh.h>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Build/BuildOptions.h"


namespace hive
{
	namespace build
	{

		class MeshBuilder
		{
		protected:

			uint32_t mObjectIndex;
			
			wp::geometry::Mesh* mMesh;

			BuildOptions mOptions;

		private:

			virtual void addGeometryToMesh(hex::Layout const& layout) = 0;

			virtual void generateGeometry(hex::Layout const& layout) = 0;

			virtual void style(hex::Layout const& layout);

			virtual void setMeshMedata(hex::Layout const& layout);

		public:

			MeshBuilder(uint32_t objectIndex, wp::geometry::Mesh* mesh, BuildOptions const& options);

			virtual ~MeshBuilder() = default;

			virtual void generate(hex::Layout const& layout);
		};

	} // build
} // hive
