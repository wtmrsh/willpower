#pragma once

#include <willpower/application/resourcesystem/ResourceManager.h>

#include <willpower/geometry/Mesh.h>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/Generator.h"
#include "Hive/Gen/CellDataMap.h"
#include "Hive/Gen/CellDataUtils.h"
#include "Hive/Build/BuildUtils.h"
#include "Hive/Build/RoomMesh.h"
#include "Hive/Build/DoorMesh.h"
#include "Hive/Build/ConnectorMesh.h"
#include "Hive/Build/BuildOptions.h"
#include "Hive/Live/HiveWorld.h"


namespace hive
{
	namespace build
	{

		class HiveBuilder
		{
			BuildOptions mOptions;

			hex::Layout mLayout;

			std::vector<RoomMesh> mRooms;

			std::vector<DoorMesh> mDoors;

			std::vector<ConnectorMesh> mConnectors;

		private:

			static std::vector<hive::build::utils::HexVertexData> generateHexVertices(std::vector<hex::Hex> const& hexes, hex::Layout const& layout, float insetDistance, std::vector<wp::Vector2>& vertices);

			static std::map<int32_t, std::vector<hive::build::utils::HexVertexData>> generateRoomVertices(hive::gen::CellDataMap const& cellMap, hex::Layout const& layout, float insetDistance, std::vector<wp::Vector2>& vertices);

			std::map<uint32_t, std::vector<uint32_t>> getRoomDoorIndices(hive::gen::Generator const* generator) const;

			void generateRoom(uint32_t roomIndex, hive::gen::Room const& room, std::vector<uint32_t> const& doorIndices, hex::Layout const& layout, wp::geometry::Mesh* mesh);

			void generateDoor(uint32_t doorIndex, hive::gen::Door const& door, uint32_t roomIndex, uint32_t connIndex, uint32_t roomEdgeIndex, uint32_t connEdgeIndex, hex::Layout const& layout, wp::geometry::Mesh* mesh);

			void generateConnector(uint32_t connIndex, hive::gen::Connector const& conn, uint32_t edgeIndex0, uint32_t edgeIndex1, hex::Layout const& layout, wp::geometry::Mesh* mesh);

		public:

			HiveBuilder(hex::Layout const& layout, BuildOptions const& options);

			hex::Layout const& getHexLayout() const;

			std::map<int32_t, std::vector<hive::build::utils::HexVertexData>> buildMiniMap(hive::gen::CellDataMap const& cellMap, hex::Layout const& layout, float insetDistance, std::vector<wp::Vector2>& vertices) const;

			void buildMapMesh(hive::gen::Generator const* generator, wp::geometry::Mesh* mesh, wp::application::resourcesystem::ResourceManager* resourceMgr);
		
			hive::live::HiveWorld* createHiveWorld() const;
		};

	} // build
} // hive
