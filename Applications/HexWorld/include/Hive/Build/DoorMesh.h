#pragma once

#include <vector>

#include <willpower/common/Vector2.h>

#include "Platform.h"
#include "Hex.h"

namespace hive
{
	namespace build
	{

		struct DoorVertex
		{
			wp::Vector2 pos;
		};

		struct DoorMesh
		{
			uint32_t roomIndex, connectorIndex, roomEdgeIndex, connectorEdgeIndex;

			wp::Vector2 centre, extents;
			hex::direction_type direction;

			bool locked;
			uint32_t key;

			// wp::geometry::Mesh data
			uint32_t meshPolygonIndex;
		};

	} // build
} // hive
