#pragma once

#include "Platform.h"

#include "Hive/Gen/Door.h"
#include "Hive/Build/MeshBuilder.h"
#include "Hive/Build/DoorMesh.h"


namespace hive
{
	namespace build
	{

		class DoorBuilder : public MeshBuilder
		{
			uint32_t mRoomIndex;

			uint32_t mConnectorIndex;

		protected:

			hive::gen::Door mDoor;

			uint32_t mRoomEdgeIndex;

			uint32_t mConnectorEdgeIndex;

			DoorMesh mGeneratedDoorMesh;

		private:

			void calculateDoorExtents(wp::Vector2 const& v0, wp::Vector2 const& v1, wp::Vector2 const& v2, wp::Vector2 const& v3, wp::Vector2* centre, wp::Vector2* extents) const;

			void generateGeometry(hex::Layout const& layout) override;
			
			void addGeometryToMesh(hex::Layout const& layout) override;

			void setMeshMedata(hex::Layout const& layout) override;

		public:

			DoorBuilder(uint32_t index, hive::gen::Door const& door, uint32_t roomIndex, uint32_t connIndex, uint32_t roomEdgeIndex, uint32_t connEdgeIndex, wp::geometry::Mesh* mesh, BuildOptions const& options);

			DoorMesh const& getDoorMesh() const;
		};

	} // build
} // hive
