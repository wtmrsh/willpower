#pragma once

#include "Platform.h"

#include "Hive/Build/RoomBuilder.h"
#include "Hive/Live/HiveWorldRoomType.h"

namespace hive
{
	namespace build
	{

		class DefaultRoomBuilder : public RoomBuilder
		{
			hive::live::HiveWorldRoomType mType;

		private:

			void generateGeometry(hex::Layout const& layout) override;

		public:

			DefaultRoomBuilder(hive::live::HiveWorldRoomType type, uint32_t index, hive::gen::Room const& room, std::vector<uint32_t> const& doorIndices, wp::geometry::Mesh* mesh, BuildOptions const& options);

			RoomMesh generateVertices(hex::Layout const& layout) override;
		};

	} // build
} // hive
