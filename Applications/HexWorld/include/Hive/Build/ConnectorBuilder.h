#pragma once

#include "Platform.h"

#include "Hive/Gen/Connector.h"
#include "Hive/Build/MeshBuilder.h"
#include "Hive/Build/ConnectorMesh.h"


namespace hive
{
	namespace build
	{

		class ConnectorBuilder : public MeshBuilder
		{
		protected:

			hive::gen::Connector mConnector;
			
			uint32_t mEdgeIndex0;
			
			uint32_t mEdgeIndex1;

			ConnectorMesh mGeneratedConnectorMesh;

		private:

			void generateGeometry(hex::Layout const& layout) override;
			
			void addGeometryToMesh(hex::Layout const& layout) override;

			void setMeshMedata(hex::Layout const& layout) override;

		public:

			ConnectorBuilder(uint32_t index, hive::gen::Connector const& conn, uint32_t edgeIndex0, uint32_t edgeIndex1, wp::geometry::Mesh* mesh, BuildOptions const& options);

			ConnectorMesh const& getConnectorMesh() const;
		};

	} // build
} // hive
