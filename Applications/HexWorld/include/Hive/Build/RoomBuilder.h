#pragma once

#include "Platform.h"

#include "Hive/Gen/Room.h"
#include "Hive/Build/MeshBuilder.h"
#include "Hive/Build/RoomMesh.h"

#define HIVE_BUILD_STYLE_ROOM_CHAMFER_VERTICES		0x01


namespace hive
{
	namespace build
	{

		class RoomBuilder : public MeshBuilder
		{
			std::vector<uint32_t> const& mDoorIndices;

			uint32_t mStyleFlags;

			std::map<uint32_t, uint32_t> mPolygonCellMap;

		protected:

			hive::gen::Room mRoom;

			RoomMesh mGeneratedRoomMesh;

		private:

			void addGeometryToMesh(hex::Layout const& layout) override;

			void style(hex::Layout const& layout) override;

			void setMeshMedata(hex::Layout const& layout) override;

		public:

			RoomBuilder(uint32_t index, hive::gen::Room const& room, std::vector<uint32_t> const& doorIndices, wp::geometry::Mesh* mesh, BuildOptions const& options, uint32_t styleFlags = 0);

			RoomMesh const& getRoomMesh() const;

			virtual RoomMesh generateVertices(hex::Layout const& layout) = 0;
		};

	} // build
} // hive
