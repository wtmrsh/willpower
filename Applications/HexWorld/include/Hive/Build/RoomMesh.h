#pragma once

#include <vector>
#include <set>
#include <array>

#include <willpower/common/Vector2.h>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Live/HiveWorldRoomType.h"


namespace hive
{
	namespace build
	{

		struct RoomMeshVertex
		{
			wp::Vector2 pos;
			uint32_t meshVertexIndex;
		};

		struct RoomMeshCell
		{
			hex::Hex hex;
			std::array<bool, 6> connections;

			std::array<int32_t, 6> doorEdgeIndices;
			std::vector<uint32_t> vertexIndices;
		};

		struct RoomMesh
		{
			hive::live::HiveWorldRoomType type;
			std::vector<RoomMeshVertex> vertices;
			std::vector<RoomMeshCell> cells;
			std::vector<uint32_t> doorIndices;

			// wp::geometry::Mesh data
			std::set<uint32_t> meshPolygonIndices;
		};

	} // build
} // hive
