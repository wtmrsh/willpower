#pragma once

#include <cstdint>

#include "Platform.h"


namespace hive
{
	namespace gen
	{

		struct CellData
		{
			int32_t roomIndex{ -1 };
		};

	} // gen
} // hive

