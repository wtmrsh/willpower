#pragma once

#include <functional>


typedef std::function<bool(void)> RoomMeshCreationFunction;
