#pragma once

#include <array>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/RoomConnectorType.h"


namespace hive
{
	namespace gen
	{

		struct RoomPrefabCell
		{
			hex::Hex hex;
			std::array<RoomConnectorType, 6> connectors;
		};

	} // gen
} // hive