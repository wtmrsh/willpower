#pragma once

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/CellDataMap.h"
#include "Hive/Gen/RoomPrefabCell.h"
#include "Hive/Gen/RoomPrefabType.h"
#include "Hive/Gen/RoomConnectorType.h"


namespace hive
{
	namespace gen
	{
		class Room;

		class RoomPrefab
		{
			RoomPrefabType mType;

			std::vector<RoomPrefabCell> mCells;

		private:

			std::vector<RoomPrefabCell> transformCells(hex::Hex const& origin, int rotation) const;

		public:
	
			RoomPrefab(RoomPrefabType type, std::vector<RoomPrefabCell> const& cells);

			RoomPrefabType getType() const;

			std::vector<RoomPrefabCell> getCells(hex::Hex const& origin, int rotation) const;

			bool canPlace(CellDataMap const& hiveMap, std::vector<Room> const& rooms, hex::Hex const& origin, int rotation) const;
		};

	} // gen
} // hive