#pragma once

#include <set>

#include "Platform.h"

#include "Hive/Gen/RoomPrefab.h"
#include "Hive/Gen/RoomPrefabType.h"
#include "Hive/Gen/CellData.h"
#include "Hive/Gen/Room.h"
#include "Hive/Gen/Door.h"
#include "Hive/Gen/Connector.h"
#include "Hive/Gen/CellDataMap.h"


namespace hive
{
	namespace gen {

		class Generator
		{
			enum class State
			{
				Uninitialised,
				Initialised,
				GeneratingRooms,
				GeneratingConnectors,
				Complete
			};

		private:

			struct RoomConnectionDetail
			{
				int roomIndex0;
				RoomConnector connector0;
				int roomIndex1;
				RoomConnector connector1;

			};

		private:

			uint32_t mWorldRadius;

			bool mStyliseRooms;

			State mState;

			CellDataMap mCellGenData;

			std::vector<Room> mRooms;

			std::vector<Door> mDoors;

			std::vector<Connector> mConnectors;

			float mCellCoveragePercent;

		private:

			uint32_t createRoom(RoomPrefab const& roomPrefab, hex::Hex const& origin, int rotation);

			uint32_t createDoor(hex::HexDirected const& hexd, uint32_t roomIndex, uint32_t connectorIndex, bool locked, uint32_t key);

			uint32_t createConnector(RoomConnectionDetail const& rcd);

			std::vector<RoomConnectionDetail> getRoomConnectorDetails() const;

			std::vector<RoomConnector> getRoomConnectors() const;

			std::vector<RoomConnector> getOptionalConnectors() const;

			std::vector<RoomConnector> getMandatoryConnectors() const;

			bool adjoinMandatoryConnectors();

			bool roomsConnectable() const;

			bool emptyCellsConnectable() const;

			std::vector<std::set<int>> getDisconnectedRoomIndices(std::vector<RoomConnector> const& conns) const;

			uint32_t drunkenWalkRoom(hex::Hex const& startCell, bool addMandatoryConnectors, uint32_t numCells);

			uint32_t placeRoom(hex::Hex const& startCell, bool addMandatoryConnectors, uint32_t numCells);

			bool createRooms();

			bool connectRooms();

			void makeRandomOptionalConnectorMandatory(int roomIndexA, int roomIndexB);

		public:

			Generator(int radius, float cellCoveragePercent);

			void initialise();

			bool generate();

			bool placePrefab(RoomPrefabType prefabType, hex::Hex const& origin, int rotation);

			uint32_t getWorldRadius() const;

			CellDataMap const& getCellGenData() const;

			std::vector<Room> const& getRooms() const;

			std::vector<Door> const& getDoors() const;

			std::vector<Connector> const& getConnectors() const;
		};

	} // gen
} // hive