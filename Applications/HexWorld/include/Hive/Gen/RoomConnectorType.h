#pragma once

#include "Platform.h"


namespace hive
{
	namespace gen
	{

		enum class RoomConnectorType
		{
			None,
			Optional,
			Mandatory
		};

	} // gen
} // hive