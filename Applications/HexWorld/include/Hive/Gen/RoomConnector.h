#pragma once

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/RoomConnectorType.h"


namespace hive
{
	namespace gen
	{

		struct RoomConnector
		{
			hex::HexDirected hexd;
			RoomConnectorType type;
		};

	} // gen
} // hive