#pragma once

#include <map>
#include <vector>
#include <set>

#include <willpower/common/Vector2.h>

#include "Platform.h"

#include "Hive/Gen/CellData.h"
#include "Hive/Gen/CellDataMap.h"
#include "Hive/Gen/Room.h"


namespace hive
{
	namespace gen
	{

		namespace utils
		{

			uint32_t getNumEmptyCells(CellDataMap const& cellMap);

			hex::Hex chooseRandomEmptyCell(CellDataMap const& cellMap);

			int32_t chooseRandomFreeNeighbourDir(CellDataMap const& cellMap, hex::Hex const& hex);

			std::map<int32_t, std::vector<hex::Hex>> groupHexesByRoom(CellDataMap const& cellMap);

		} // utils
	} // gen
} // hive