#pragma once

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/RoomConnectorType.h"


namespace hive
{
	namespace gen
	{

		struct Door
		{
			hex::HexDirected hexd;
			uint32_t roomIndex, connectorIndex;
			bool locked;
			uint32_t key;
		};

	} // gen
} // hive