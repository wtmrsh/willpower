#pragma once

#include <unordered_map>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/CellData.h"


namespace hive
{
	namespace gen
	{

		typedef std::unordered_map<hex::Hex, CellData> CellDataMap;

	} // gen
} // hive
