#pragma once

#include <vector>

#include "Platform.h"
#include "Hex.h"


namespace hive
{
	namespace gen
	{

		class Connector
		{
			uint32_t mRoomIndices[2];

			uint32_t mDoorIndices[2];

			hex::HexDirected mRoomCells[2];

		public:

			Connector(uint32_t roomIndex0, hex::HexDirected cell0, uint32_t roomIndex1, hex::HexDirected cell1);

			void getRoomIndices(uint32_t* roomIndex0, uint32_t* roomIndex1) const;

			void getDoorIndices(uint32_t* doorIndex0, uint32_t* doorIndex1) const;

			void getRoomCells(hex::HexDirected* cell0, hex::HexDirected* cell1) const;

			void addDoorIndex(uint32_t doorIndex);
		};

	} // gen
} // hive