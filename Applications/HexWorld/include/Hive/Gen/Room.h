#pragma once

#include <vector>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Gen/RoomConnector.h"
#include "Hive/Gen/RoomPrefab.h"


namespace hive
{

	namespace gen
	{

		class Room
		{
			RoomPrefab mPrefab;

			hex::Hex mOrigin;

			int mRotation;

			std::vector<RoomConnector> mOptionalConnectors;

			std::vector<RoomConnector> mMandatoryConnectors;

		public:

			Room(RoomPrefab const& roomPrefab, hex::Hex const& origin, int rotation);

			RoomPrefab const& getPrefab() const;

			hex::Hex getOrigin() const;

			int getRotation() const;

			std::vector<RoomConnector> getConnectors() const;

			std::vector<RoomConnector> const& getOptionalConnectors() const;

			std::vector<RoomConnector> const& getMandatoryConnectors() const;

			void addConnector(hex::HexDirected const& hexd, RoomConnectorType type);

			void removeOptionalConnectors();

			void setOptionalConnector(uint32_t index, RoomConnectorType type);

			void setOptionalConnector(hex::HexDirected const& hexd, RoomConnectorType type);

			bool hasConnector(hex::HexDirected const& hexd) const;
		};

	} // gen
} // hive