#pragma once

#include <cstdint>

#include <willpower/collide/Simulation.h>

#include "Platform.h"

#include "Hive/Live/HiveWorldTriggerable.h"


namespace hive
{
	namespace live
	{

		class HiveWorld;

		class HiveWorldArea : public HiveWorldTriggerable
		{
			std::set<uint32_t> mMeshPolygonIndices;

		public:

			explicit HiveWorldArea(std::set<uint32_t> meshPolygonIndices);

			std::set<uint32_t> const& getMeshPolygonIndices() const;

			virtual void onEnter(HiveWorld* world, wp::collide::Simulation* collisionSim);

			virtual void onExit(HiveWorld* world, wp::collide::Simulation* collisionSim);

			virtual void update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime) = 0;
		};

	} // live
} // hive