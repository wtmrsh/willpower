#pragma once

#include "Platform.h"


namespace hive
{
	namespace live
	{

		enum HiveWorldRoomType
		{
			Open,
			Arena
		};

	} // gen
} // hive