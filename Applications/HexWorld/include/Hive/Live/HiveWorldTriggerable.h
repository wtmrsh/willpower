#pragma once

#include "Platform.h"
#include "Trigger.h"


namespace hive
{
	namespace live
	{

		class HiveWorldTriggerable
		{
			std::vector<Trigger> mTriggers;

		public:

			HiveWorldTriggerable();

			virtual ~HiveWorldTriggerable() = default;

			void addTrigger(Trigger const& trigger);

			uint32_t getNumTriggers() const;

			Trigger const& getTrigger(uint32_t index) const;

			void resetTriggers();

			void checkTriggers(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator);

		};

	} // live
} // hive
