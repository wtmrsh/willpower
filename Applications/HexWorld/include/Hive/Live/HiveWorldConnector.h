#pragma once

#include <cstdint>

#include <willpower/collide/Simulation.h>

#include "Platform.h"

#include "Hive/Live/HiveWorldArea.h"


namespace hive
{
	namespace live
	{

		class HiveWorld;

		class HiveWorldConnector : public HiveWorldArea
		{
			friend class HiveWorldRoom;

		public:

			enum class State
			{
				LockEngaged,
				LockDisengaged
			};

			enum class CycleCommand
			{
				None,
				OpenDoor0,
				OpenDoor1
			};

			enum class CycleState
			{
				Equilibrium,
				OpeningDoor0,
				ClosingDoor0,
				Intermission,
				OpeningDoor1,
				ClosingDoor1,
			};

		private:

			const float IntermissionTime = 1.0f;

			uint32_t mDoorIndices[2];

			State mState;

			CycleCommand mCommand;

			CycleState mCycleState;

			float mIntermissionTimer;

		private:

			void processOpenDoor0(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime);

			void processOpenDoor1(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime);

		public:

			HiveWorldConnector(uint32_t meshPolygonIndex, uint32_t doorIndex0, uint32_t doorIndex1);

			uint32_t getDoorIndex0() const;

			uint32_t getDoorIndex1() const;

			bool tryCycleOpen(uint32_t doorIndex, HiveWorld* world);

			bool tryCycle(HiveWorld* world);

			void update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime) override;
		};

	} // live
} // hive