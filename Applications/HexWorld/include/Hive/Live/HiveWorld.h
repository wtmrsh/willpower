#pragma once

#include <cstdint>
#include <set>
#include <vector>

#include <willpower/collide/Simulation.h>

#include "Platform.h"
#include "Hex.h"
#include "HexLocator.h"
#include "Hive/Live/HiveWorldRoom.h"
#include "Hive/Live/HiveWorldConnector.h"
#include "Hive/Live/HiveWorldDoor.h"


namespace hive
{
	namespace live
	{

		class HiveWorld
		{
			struct VisibilityCalculationCache
			{
				std::set<uint32_t> rooms;
				std::set<uint32_t> connectors;
				std::set<uint32_t> doors;
			};

		private:

			std::vector<HiveWorldRoom> mRooms;

			std::vector<HiveWorldConnector> mConnectors;

			std::vector<HiveWorldDoor> mDoors;

		private:

			void traverseRoomVisibility(uint32_t index, std::vector<HexLocator>& locators, VisibilityCalculationCache& cache) const;

			void traverseConnectorVisibility(uint32_t index, std::vector<HexLocator>& locators, VisibilityCalculationCache& cache) const;

			void traverseDoorVisibility(uint32_t index, std::vector<HexLocator>& locators, VisibilityCalculationCache& cache) const;

			HiveWorldArea* getArea(HexLocator const& locator);

		public:

			HiveWorld();

			uint32_t addRoom(HiveWorldRoom const& room);

			uint32_t addConnector(HiveWorldConnector const& conn);

			uint32_t addDoor(HiveWorldDoor const& door);

			uint32_t getNumRooms() const;

			uint32_t getNumConnectors() const;

			uint32_t getNumDoors() const;

			std::vector<HexLocator> getVisibleAreas(HexLocator const& locator) const;

			HiveWorldRoom const* getRoom(HexLocator const& locator) const;

			HiveWorldRoom* getRoom(HexLocator const& locator);

			HiveWorldRoom const* getRoom(uint32_t index) const;

			HiveWorldRoom* getRoom(uint32_t index);

			HiveWorldConnector const* getConnector(uint32_t index) const;

			HiveWorldConnector* getConnector(uint32_t index);

			HiveWorldDoor const* getDoor(uint32_t index) const;

			HiveWorldDoor* getDoor(uint32_t index);

			void onEnterArea(HexLocator const& locator, wp::collide::Simulation* collisionSim);

			void onExitArea(HexLocator const& locator, wp::collide::Simulation* collisionSim);

			void checkTriggers(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator);

			void update(wp::collide::Simulation* collisionSim, float frameTime);
		};

	} // live
} // hive