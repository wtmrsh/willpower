#pragma once

#include <cstdint>

#include <willpower/common/Vector2.h>

#include <willpower/collide/Simulation.h>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Live/HiveWorldArea.h"


#define HIVE_DOOR_LOCKABLE		0x01
#define HIVE_DOOR_LOCKED		0x02


namespace hive
{
	namespace live
	{

		class HiveWorld;

		class HiveWorldDoor : public HiveWorldArea
		{
			const float OpenCloseTime = 1.0f;

			const uint32_t MagicKey = 987654321;

		public:

			enum class State
			{
				Closed,
				Open,
				Opening,
				Closing,
				Locked
			};

		private:

			uint32_t mConnectorIndex;

			uint32_t mRoomIndex;

			wp::Vector2 mCentre;

			wp::Vector2 mExtents;

			hex::direction_type mDirection;

			State mState;

			uint32_t mFlags;

			uint32_t mForceLockAfterCloseCount;

			const uint32_t mBaseKey;
				
			uint32_t mCurKey;

			float mTimer;

			std::pair<uint32_t, uint32_t> mCollisionLineRanges[2];

		private:

			void checkForceLock();

			void _lock(uint32_t key);

			void _unlock();

		public:

			HiveWorldDoor(uint32_t meshPolygonIndex, uint32_t connectorIndex, uint32_t roomIndex, wp::Vector2 const& centre, wp::Vector2 const& extents, hex::direction_type direction, uint32_t flags, uint32_t key = 0);

			uint32_t getConnectorIndex() const;

			uint32_t getRoomIndex() const;

			wp::Vector2 const& getCentre() const;

			wp::Vector2 const& getExtents() const;

			void getVertices(float closedAmount, wp::Vector2& v0, wp::Vector2& v1, wp::Vector2& v2, wp::Vector2& v3) const;

			hex::direction_type getDirection() const;

			float getAngle() const;

			State getState() const;

			bool blocksVision() const;

			float getOpenPercentage() const;

			bool isLockable() const;

			bool isLocked() const;

			void setCollisionLineRanges(std::pair<uint32_t, uint32_t> const& range1, std::pair<uint32_t, uint32_t> const& range2);

			bool tryOpen();

			void useKey(uint32_t key);

			void lock();

			void unlock();

			void close(wp::collide::Simulation* collisionSim);

			void closeAndForceLock(wp::collide::Simulation* collisionSim);

			void forceUnlock();

			void update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime) override;
		};

	} // live
} // hive