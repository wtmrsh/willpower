#pragma once

#include <cstdint>
#include <set>
#include <vector>

#include <willpower/collide/Simulation.h>

#include "Platform.h"

#include "Hive/Live/HiveWorldArea.h"
#include "Hive/Live/HiveWorldRoomType.h"

#define HIVE_ROOM_ARENA						0x01
#define HIVE_ROOM_ARENA_STATE_DORMANT		0x02
#define HIVE_ROOM_ARENA_STATE_ACTIVE		0x04
#define HIVE_ROOM_ARENA_STATE_EXTINCT		0x08		


namespace hive
{
	namespace live
	{

		class HiveWorld;

		class HiveWorldRoom : public HiveWorldArea
		{
			HiveWorldRoomType mType;

			uint64_t mFlags;

			std::vector<uint32_t> mDoorIndices;

		public:

			explicit HiveWorldRoom(HiveWorldRoomType type);

			HiveWorldRoom(HiveWorldRoomType type, std::set<uint32_t> const& meshPolygonIndices, std::vector<uint32_t> const& doorIndices);

			HiveWorldRoomType getType() const;

			uint64_t getFlags() const;

			void _setFlag(uint32_t flag);

			void _unsetFlag(uint32_t flag);

			std::vector<uint32_t> const& getDoorIndices() const;

			bool tryOpenAllConnectors(HiveWorld* world);

			void update(HiveWorld* world, wp::collide::Simulation* collisionSim, float frameTime) override;
		};

	} // live
} // hive