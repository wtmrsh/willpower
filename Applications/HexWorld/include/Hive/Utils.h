#pragma once

#include <vector>
#include <set>

#include "Platform.h"


namespace hive
{
	namespace utils
	{

		template<typename T>
		std::vector<std::set<T>>  getDisconnectedSubGraphs(std::vector<std::pair<T, T>> const& edges)
		{
			std::vector<std::set<T>> groups;

			for (auto const& edge : edges)
			{
				auto const& [a, b] = edge;

				int set_a{ -1 }, set_b{ -1 };
				for (uint32_t i = 0; i < (uint32_t)groups.size(); ++i)
				{
					auto const& island = groups[i];
					if (island.find(a) != island.end())
					{
						set_a = (int)i;
					}
					if (island.find(b) != island.end())
					{
						set_b = (int)i;
					}
				}

				// If A is in a set but not B, add B to A's set
				if (set_a >= 0 && set_b < 0)
				{
					groups[set_a].insert(b);
				}

				// If B is in a set but not A, add A to B's set
				if (set_b >= 0 && set_a < 0)
				{
					groups[set_b].insert(a);
				}

				// If A and B are both in different sets, merge the sets
				if (set_a >= 0 && set_b >= 0 && set_a != set_b)
				{
					groups[set_a].merge(groups[set_b]);
					for (uint32_t i = (uint32_t)set_b; i < (groups.size() - 1); ++i)
					{
						groups[i] = groups[i + 1];
					}

					groups.pop_back();
				}

				// If neither are in a set, create a new set with both
				if (set_a < 0 && set_b < 0)
				{
					groups.push_back({ a, b });
				}
			}

			return groups;
		}

	} // utils
} // hive
