#pragma once

#include <vector>
#include <functional>

#include <willpower/common/Vector2.h>
#include <willpower/common/LineHit.h>

#include "Platform.h"
#include "HexLocator.h"

struct TriggerData
{
	int32_t targetIndex;
};

enum class TriggerSide
{
	Left,
	Right,
	Into,
	OutOf,
	Any
};

class Trigger;


typedef std::function<void(HexLocator const&, Trigger const*, TriggerSide, TriggerData const&)> TriggerCallback;

class Trigger
{
	enum class Shape
	{
		None,
		Line,
		Arc,
		Semicircle,
		Circle,
		Hexagon,
		AABB,
		Polygon
	};

private:

	Shape mShape;

	TriggerSide mSide;

	int mInitialCount, mCount;

	TriggerData mData;

	std::vector<TriggerCallback> mCallbacks;

	// For circle and hexagon, first vertex is centre and second is radius
	// For semicircle, first vertex is centre, and second is radius (x) and angle (y)
	std::vector<wp::Vector2> mVertices;

private:

	bool testLine(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testArc(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testSemicircle(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testCircle(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testHexagon(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testAABB(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	bool testPolygon(wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide* side = nullptr) const;

	static void getHitSide(wp::LineHit const& hit, TriggerSide* side);

	std::vector<wp::Vector2> generateCircleVertices(uint32_t count, float startAngle) const;

	std::vector<wp::Vector2> generateSegmentVertices(float density, float spanAngle, bool close) const;

	static void initTrigger(Trigger* trigger, Shape shape, TriggerSide side, int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks);
	
public:

	Trigger();

	static Trigger fromLine(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& v0, wp::Vector2 const& v1, TriggerSide side = TriggerSide::Any);

	static Trigger fromArc(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, float angle, float width, TriggerSide side = TriggerSide::Any);

	static Trigger fromSemicircle(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, float angle, TriggerSide side = TriggerSide::Any);

	static Trigger fromCircle(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, TriggerSide side = TriggerSide::Any);

	static Trigger fromHexagon(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& centre, float radius, TriggerSide side = TriggerSide::Any);

	static Trigger fromAABB(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, wp::Vector2 const& minExtent, wp::Vector2 const& maxExtent, TriggerSide side = TriggerSide::Any);

	static Trigger fromPolygon(int count, TriggerData const& data, std::vector<TriggerCallback> const& callbacks, std::vector<wp::Vector2> const& vertices, TriggerSide side = TriggerSide::Any);

	std::vector<wp::Vector2> getLineVertices() const;

	int getCount() const;

	void setCount(int count);

	void reset();

	void check(wp::Vector2 const& oldPos, wp::Vector2 const& newPos, HexLocator const& locator);
};
