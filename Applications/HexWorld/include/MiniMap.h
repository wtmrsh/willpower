#pragma once

#include <vector>

#include <willpower/common/Vector2.h>

#include "Platform.h"

#include "Hive/Gen/Generator.h"
#include "Hive/Gen/CellDataMap.h"
#include "Hive/Gen/Room.h"


class MiniMap
{
	hive::gen::CellDataMap mCellGenData;

	std::vector<hive::gen::Room> mRooms;

public:

	MiniMap();

	hive::gen::CellDataMap const& getCellGenData() const;

	std::vector<hive::gen::Room> const& getRooms() const;

	void create(hive::gen::Generator* generator);
};
