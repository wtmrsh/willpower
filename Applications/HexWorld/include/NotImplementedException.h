#pragma once

#include <string>
#include <exception>

#include <utils/StringUtils.h>

#include "Platform.h"


class NotImplementedException : public std::exception
{
public:

	explicit NotImplementedException(std::string const& msg)
		: exception(msg.c_str())
	{
	}
};

#define THROW_NOT_IMPLEMENTED \
	throw NotImplementedException(STR_FORMAT("{} is not implemented.", __FUNCTION__));
