#pragma once

#include <applib/MapResourceDefinitionFactory.h>

#include "Platform.h"


class HiveWorldMapDefinitionFactory : public applib::MapResourceDefinitionFactory
{
public:

	HiveWorldMapDefinitionFactory();

	void create(wp::application::resourcesystem::Resource* resource, wp::application::resourcesystem::ResourceManager* resourceMgr, utils::XmlNode* node) override;
};

