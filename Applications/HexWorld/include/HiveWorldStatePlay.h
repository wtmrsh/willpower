#pragma once

#include <vector>
#include <tuple>

#include <willpower/application/StateFactory.h>

#include <willpower/collide/ColliderAABB.h>

#include <willpower/viz/DynamicTriangleRenderer.h>

#include <applib/StatePlay.h>
#include <applib/EntityManager.h>

#include "Platform.h"
#include "Hex.h"
#include "Map.h"
#include "HiveWorldEntityHandler.h"
#include "CollisionSimulationDataProvider.h"
#include "MapGridDataProvider.h"
#include "DoorRendererDataProvider.h"
#include "TriggerDataProvider.h"
#include "MiniMapDataProvider.h"

#include "Hive/Live/HiveWorld.h"


class APPLICATION_API HiveWorldStatePlay : public applib::StatePlay
{
	typedef wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat> DoorRenderer;

private:

	wp::collide::Collider* mPlayerCollider;

	std::shared_ptr<CollisionSimulationDataProvider> mCollisionSimDataProvider;

	std::shared_ptr<MapGridDataProvider> mMapGridDataProvider;

	std::shared_ptr<DoorRendererDataProvider> mDoorRendererDataProvider;

	std::shared_ptr<TriggerDataProvider> mTriggerDataProvider;

	std::shared_ptr<MiniMapDataProvider> mMiniMapDataProvider;

	wp::Vector2 mPrevPlayerPosition;

	HexLocator mPrevPlayerLocator;

private:

	std::map<std::string, std::tuple<wp::viz::Renderer*, int, bool>> createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr) override;

	void setupWorldDynamicCollisions();

	void setupPlayerCollision();

	void registerInput() override;

	void createGameObjects(wp::application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args) override;

	void destroyGameObjects() override;

	void setupEntityFacades() override;

	void setupEntities() override;

	void updatePreInput(float frameTime) override;

	void updatePostEntities(float frameTime) override;

	void checkLocationChange();

	void checkTriggers();

	hive::live::HiveWorld* getWorld() const;

	::Map* getMap();

	HexLocator getPlayerLocator();

	wp::Vector2 const& getPlayerPosition() const;

protected:

	void updateActions(std::vector<std::string> const& activeStates, float frameTime) override;

	void updatePreRenderers(float frameTime) override;

public:

	HiveWorldStatePlay();

	std::vector<std::string> getDebuggingText() const override;
};

class HiveWorldStatePlayFactory : public wp::application::StateFactory
{
	wp::Logger* mLogger;

public:

	explicit HiveWorldStatePlayFactory(wp::Logger* logger)
		: wp::application::StateFactory("Play")
		, mLogger(logger)
	{
	}

	wp::application::State* createState()
	{
		auto state = new HiveWorldStatePlay();
		state->setLogger(mLogger);
		return state;
	}
};
