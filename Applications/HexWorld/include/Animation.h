#pragma once

#include "Platform.h"


enum class Animation
{
	// Entities
	A_Idle,
	
	// Bullets
	A_Fire,
	A_Explode
};
