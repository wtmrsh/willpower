#pragma once

#include <string>

#include <willpower/application/resourcesystem/Resource.h>
#include <willpower/application/resourcesystem/ResourceFactory.h>

#include <willpower/geometry/Mesh.h>

#include <applib/Map.h>

#include "Platform.h"

#include "Hive/Gen/Generator.h"
#include "Hive/Build/HiveBuilder.h"
#include "Hive/Live/HiveWorld.h"

#include "MiniMap.h"


class Map : public applib::Map
{
	hive::gen::Generator* mGenerator;

	MiniMap* mMiniMap;

	hive::live::HiveWorld* mWorld;

	wp::Vector2 mPlayerStartPosition;

private:

	hive::live::HiveWorld* createHiveWorld(hive::build::HiveBuilder const& builder);

public:

	Map(std::string const& name,
		std::string const& namesp,
		std::string const& source,
		std::map<std::string, std::string> const& tags,
		wp::application::resourcesystem::ResourceLocation* location);

	~Map();

	MiniMap const* getMiniMap() const;
	
	hive::live::HiveWorld* getWorld();

	wp::Vector2 const& getPlayerStartPosition() const;

	void testMap1(wp::geometry::Mesh* mesh, wp::application::resourcesystem::ResourceManager* resourceMgr);
};

class MapResourceFactory : public wp::application::resourcesystem::ResourceFactory
{
public:

	MapResourceFactory()
		: wp::application::resourcesystem::ResourceFactory("Map")
	{
	}

	wp::application::resourcesystem::Resource* createResource(std::string const& name, std::string const& namesp, std::string const& source, std::map<std::string, std::string> const& tags, wp::application::resourcesystem::ResourceLocation* location) override
	{
		return new Map(name, namesp, source, tags, location);
	}
};