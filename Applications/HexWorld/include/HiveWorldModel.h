#pragma once

#include <applib/Model.h>

#include "Platform.h"
#include "Hex.h"

#include "Hive/Live/HiveWorld.h"

#include "GameException.h"


struct HiveWorldModel : public applib::Model
{
	hive::live::HiveWorld* world;

	HiveWorldModel(applib::EntityHandlerFactoryFunction handlerFactory, wp::application::resourcesystem::ResourceManager* resourceMgr)
		: applib::Model(handlerFactory, resourceMgr)
		, world(nullptr)
	{
	}

	void setWorld(hive::live::HiveWorld* _world)
	{
		if (world)
		{
			throw GameException("Cannot set the Model's world when an instance already exists");
		}

		world = _world;
	}

	void deleteWorld()
	{
		world = nullptr;
	}

	hex::Layout getLayout() const
	{
		return hex::Layout(hex::g_layoutFlat, 256, wp::Vector2::ZERO);
	}
};
