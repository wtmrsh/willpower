#pragma once

#include <cmath>
#include <cstdint>
#include <vector>
#include <cassert>

#include <willpower/common/Vector2.h>

#include "Platform.h"


// Based on https://www.redblobgames.com/grids/hexagons/implementation.html
// See that page for algorithms on how to generate a hex-shaped world grid

namespace hex
{

	typedef int32_t direction_type;

	struct Hex
	{
		int q, r, s;

		Hex()
			: Hex(0, 0, 0)
		{
		}

		Hex(int q_, int r_, int s_)
			: q(q_), r(r_), s(s_)
		{
			assert(q + r + s == 0);
		}

		Hex(int q_, int r_)
			: Hex(q_, r_, 0 - q_ - r_)
		{
		}

		Hex(Hex const& other)
		{
			q = other.q;
			r = other.r;
			s = other.s;
		}

		Hex& operator=(Hex const& other)
		{
			q = other.q;
			r = other.r;
			s = other.s;
			return *this;
		}

		bool operator ==(Hex const& other) const
		{
			return this->q == other.q && this->r == other.r && this->s == other.s;
		}

		bool operator !=(Hex other) const
		{
			return !(*this == other);
		}

		bool operator <(Hex const& other) const
		{
			return q < other.q ? true : r < other.r;
		}

		Hex operator +(Hex const& other) const
		{
			return Hex(q + other.q, r + other.r, s + other.s);
		}

		Hex operator -(Hex other) const
		{
			return Hex(q - other.q, r - other.r, s - other.s);
		}

		Hex operator *(int k)  const
		{
			return Hex(q * k, r * k, s * k);
		}

		bool valid() const
		{
			return q + r + s == 0;
		}

		uint32_t length() const
		{
			return int((abs(q) + abs(r) + abs(s)) / 2);
		}

		uint32_t distance(Hex const& other) const
		{
			auto t = *this - other;
			return t.length();
		}

		//       e1
		//   v2 ____ v1
		//  e2 /    \ e0
		// v3 /      \ v0
		//    \      / 
		//  e3 \____/ e5
		//   v4  e4 v5  
		//
		Hex offset(int dir /* 0 to 5 */)  const
		{
			const std::vector<Hex> offsets =
			{
				Hex(1, 0, -1), Hex(0, 1, -1), Hex(-1, 1, 0), 
				Hex(-1, 0, 1), Hex(0, -1, 1), Hex(1, -1, 0)
			};

			assert(0 <= dir && dir < 6);
			return offsets[dir];
		}

		Hex neighbour(int dir) const
		{
			return *this + offset(dir);
		}

		Hex rotateClockwise() const
		{
			return Hex(-r, -s, -q);
		}

		Hex rotateAntilockwise() const
		{
			return Hex(-s, -q, -r);
		}
	};

	struct Edge
	{
		Hex hex;
		int edge;

		bool operator==(Edge const& other) const
		{
			if (hex == other.hex && edge == other.edge)
			{
				return true;
			}

			if (hex.neighbour(edge) == other.hex)
			{
				return true;
			}

			return false;
		}

		bool operator!=(Edge const& other) const
		{
			return !(*this == other);
		}
	};

	struct Corner
	{
		Hex hex;
		int corner;

		bool operator==(Corner const& other) const
		{
			if (hex == other.hex && corner == other.corner)
			{
				return true;
			}

			auto n0 = hex.neighbour((corner + 5) % 6);
			if (n0 == other.hex && other.corner == ((corner + 2) % 6))
			{
				return true;
			}

			auto n1 = hex.neighbour(corner);
			if (n1 == other.hex && other.corner == ((corner + 4) % 6))
			{
				return true;
			}

			return false;
		}

		bool operator!=(Corner const& other) const
		{
			return !(*this == other);
		}

		Corner rotated(int r) const
		{
			while (r < 0)
			{
				r += 6;
			}

			return { hex, (corner + r) % 6 };
		}

		void getNeighbourHexes(Hex* n0, Hex* n1) const
		{
			*n0 = hex.neighbour((corner + 5) % 6);
			*n1 = hex.neighbour(corner);
		}
	};

	struct HexDirected
	{
		Hex hex;
		direction_type dir;

		bool operator==(HexDirected const& other) const
		{
			return hex == other.hex && dir == other.dir;
		}

		bool operator!=(HexDirected const& other) const
		{
			return !(*this == other);
		}

		Hex target() const
		{
			return hex.neighbour(dir);
		}

		HexDirected mirror() const
		{
			return {
				target(),
				(dir + 3) % 6
			};
		}

		direction_type mirrorDir() const
		{
			return (dir + 3) % 6;
		}
	};

	struct FractionalHex 
	{
		const double q, r, s;

		FractionalHex(double q_, double r_, double s_)
			: q(q_)
			, r(r_)
			, s(s_) 
		{
		}

		Hex roundToHex() 
		{
			int qq = int(round(q));
			int rr = int(round(r));
			int ss = int(round(s));

			double q_diff = abs(qq - q);
			double r_diff = abs(rr - r);
			double s_diff = abs(ss - s);

			if (q_diff > r_diff && q_diff > s_diff) 
			{
				qq = -rr - ss;
			}
			else if (r_diff > s_diff) 
			{
				rr = -qq - ss;
			}
			else 
			{
				ss = -qq - rr;
			}

			return Hex(qq, rr, ss);
		}
	};

	struct Orientation
	{
		double f0, f1, f2, f3;
		double b0, b1, b2, b3;
		double startAngle; // in multiples of 60�

		Orientation()
			: f0(0), f1(0), f2(0), f3(0)
			, b0(0), b1(0), b2(0), b3(0)
			, startAngle(0)
		{
		}

		Orientation(
			double f0_, double f1_, double f2_, double f3_,
			double b0_, double b1_, double b2_, double b3_,
			double startAngle)
			: f0(f0_), f1(f1_), f2(f2_), f3(f3_)
			, b0(b0_), b1(b1_), b2(b2_), b3(b3_)
			, startAngle(startAngle)
		{
		}

		void copyFrom(Orientation const& other)
		{
			f0 = other.f0;
			f1 = other.f1;
			f2 = other.f2;
			f3 = other.f3;
			b0 = other.b0;
			b1 = other.b1;
			b2 = other.b2;
			b3 = other.b3;
			startAngle = other.startAngle;
		}

		Orientation(Orientation const& other)
		{
			copyFrom(other);
		}

		Orientation& operator=(Orientation const& other)
		{
			copyFrom(other);
			return *this;
		}
	};

	const Orientation g_layoutPointy = Orientation(
		sqrt(3.0), sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0,
		sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0,
		0.5
	);

	const Orientation g_layoutFlat = Orientation(
		3.0 / 2.0, 0.0, sqrt(3.0) / 2.0, sqrt(3.0),
		2.0 / 3.0, 0.0, -1.0 / 3.0, sqrt(3.0) / 3.0,
		0.0
	);

	struct Layout 
	{
		Orientation orientation;
		float radius;
		wp::Vector2 origin;

		Layout()
			: Layout(g_layoutFlat, 0.0f, wp::Vector2::ZERO)
		{
		}

		Layout(Orientation orientation_, float radius_, wp::Vector2 const& origin_)
			: orientation(orientation_)
			, radius(radius_)
			, origin(origin_)
		{
		}

		void copyFrom(Layout const& other)
		{
			orientation = other.orientation;
			radius = other.radius;
			origin = other.origin;
		}

		Layout(Layout const& other)
		{
			copyFrom(other);
		}

		Layout& operator=(Layout const& other)
		{
			copyFrom(other);
			return *this;
		}

		Layout copy() const
		{
			return Layout(orientation, radius, origin);
		}

		wp::Vector2 toWorld(Hex const& hex) const
		{
			Orientation const& M = orientation;

			double x = (M.f0 * hex.q + M.f1 * hex.r) * radius;
			double y = (M.f2 * hex.q + M.f3 * hex.r) * radius;

			return wp::Vector2((float)(x + origin.x), (float)(y + origin.y));
		}

		wp::Vector2 toWorld(HexDirected const& hd) const
		{
			Orientation const& M = orientation;

			double x = (M.f0 * hd.hex.q + M.f1 * hd.hex.r) * radius;
			double y = (M.f2 * hd.hex.q + M.f3 * hd.hex.r) * radius;

			return wp::Vector2((float)(x + origin.x), (float)(y + origin.y));
		}

		FractionalHex toHex(wp::Vector2 const& p) const
		{
			const Orientation& M = orientation;
			wp::Vector2 pt = wp::Vector2((float)((p.x - origin.x) / radius), (float)((p.y - origin.y) / radius));

			double q = M.b0 * pt.x + M.b1 * pt.y;
			double r = M.b2 * pt.x + M.b3 * pt.y;

			return FractionalHex(q, r, -q - r);
		}

		double hexCornerAngle(int corner) const
		{
			return 360.0 * (orientation.startAngle + corner) / 6;
		}

		wp::Vector2 hexCornerDirection(int corner) const
		{
			auto angle = hexCornerAngle(corner) * 3.14159 / 180.0;
			return wp::Vector2((float)cos(angle), (float)sin(angle));
		}

		wp::Vector2 hexCornerOffset(int corner) const
		{
			return hexCornerDirection(corner) * radius;
		}

		double hexEdgeAngle(int edge) const
		{
			return 360.0 * (orientation.startAngle + (edge + 0.5)) / 6;
		}

		wp::Vector2 hexEdgeDirection(int edge) const
		{
			auto angle = hexEdgeAngle(edge) * 3.14159 / 180.0;
			return wp::Vector2((float)cos(angle), (float)sin(angle));
		}

		wp::Vector2 hexEdgeOffset(int edge) const
		{
			return hexEdgeDirection(edge) * radius * sqrtf(3.0f) / 2.0f;
		}

		std::vector<wp::Vector2> polygonCorners(Hex hex) const
		{
			std::vector<wp::Vector2> corners{};

			wp::Vector2 centre = toWorld(hex);
			for (int i = 0; i < 6; i++) 
			{
				corners.push_back(centre + hexCornerOffset(i));
			}

			return corners;
		}
	};

}

namespace std
{
	template<>
	struct hash<hex::Hex>
	{
		size_t operator()(const hex::Hex& h) const
		{
			hash<int> int_hash;
			size_t hq = int_hash(h.q);
			size_t hr = int_hash(h.r);
			return hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2));
		}
	};

	template<>
	struct hash<hex::Corner>
	{
		size_t operator()(const hex::Corner& c) const
		{
			hex::Hex n1, n2;
			c.getNeighbourHexes(&n1, &n2);

			hash<hex::Hex> h;

			auto h0 = h(c.hex);
			auto h1 = h(n1);
			auto h2 = h(n2);

			return h0 ^ h1 ^ h2;
		}
	};

	template<>
	struct hash<hex::HexDirected>
	{
		size_t operator()(const hex::HexDirected& h) const
		{
			hash<int> int_hash;
			size_t hq = int_hash(h.hex.q);
			size_t hr = int_hash(h.hex.r);

			hash<hex::direction_type> dt_hash;
			size_t hd = dt_hash(h.dir);

			auto value = hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2));
			return value ^ (hd + 0x9e3779b9 + (value << 6) + (value >> 2));
		}
	};

} // hex