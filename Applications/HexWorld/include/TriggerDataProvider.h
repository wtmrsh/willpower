#pragma once

#include <mpp/helper/LineBatchDataProvider.h>

#include <willpower/common/BoundingBox.h>

#include "Platform.h"
#include "HexLocator.h"

#include "Hive/Live/HiveWorld.h"


class TriggerDataProvider : public mpp::helper::LineBatchDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeUnsignedByte>
{
	struct Line
	{
		wp::Vector2 v[2];
		int tCount;
	};

private:

	glm::vec3 mBounds[2];

	std::vector<Line> mLines;

private:

	void addTrigger(Trigger const& trigger)
	{
		auto triggerVerts = trigger.getLineVertices();
		auto triggerCount = trigger.getCount();

		for (uint32_t j = 0; j < triggerVerts.size() - 1; ++j)
		{
			mLines.push_back({ triggerVerts[j + 0], triggerVerts[j + 1], triggerCount });
		}
	}

public:

	TriggerDataProvider()
	{
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin = mBounds[0];
		bMax = mBounds[1];
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1)
	{
		auto const& line = mLines[index];

		x0 = line.v[0].x;
		y0 = line.v[0].y;
		x1 = line.v[1].x;
		y1 = line.v[1].y;
	}

	void colour(uint32_t index, uint8_t& red, uint8_t& green, uint8_t& blue, uint8_t& alpha)
	{
		auto const& line = mLines[index];

		if (line.tCount != 0)
		{
			red = 255;
			green = 255;
			blue = 255;
		}
		else
		{
			red = 0;
			green = 0;
			blue = 0;
		}

		alpha = 255;
	}

	mpp::Colour diffuse()
	{
		return mpp::Colour::Red;
	}

	bool update(hive::live::HiveWorld const* world, std::vector<HexLocator> const& locators, wp::BoundingBox const& viewBounds, float frameTime)
	{
		VAR_UNUSED(frameTime);

		wp::Vector2 viewMin, viewMax;
		viewBounds.getExtents(viewMin, viewMax);

		mBounds[0].x = viewMin.x;
		mBounds[0].y = viewMin.y;
		mBounds[1].x = viewMax.x;
		mBounds[1].y = viewMax.y;

		mLines.clear();

		for (auto const& locator : locators)
		{
			switch (locator.type)
			{
			case HexLocator::Type::Door:
				for (uint32_t i = 0; i < world->getDoor(locator.typeIndex)->getNumTriggers(); ++i)
				{
					auto const& trigger = world->getDoor(locator.typeIndex)->getTrigger(i);
					addTrigger(trigger);
				}
				break;

			case HexLocator::Type::Connector:
				for (uint32_t i = 0; i < world->getConnector(locator.typeIndex)->getNumTriggers(); ++i)
				{
					auto const& trigger = world->getConnector(locator.typeIndex)->getTrigger(i);
					addTrigger(trigger);
				}
				break;

			case HexLocator::Type::Room:
				for (uint32_t i = 0; i < world->getRoom(locator.typeIndex)->getNumTriggers(); ++i)
				{
					auto const& trigger = world->getRoom(locator.typeIndex)->getTrigger(i);
					addTrigger(trigger);
				}
				break;

			default:
				break;
			}
		}

		setNumPrimitives(mLines.size());
		return true;
	}
};
