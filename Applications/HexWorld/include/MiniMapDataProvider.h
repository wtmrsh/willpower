#pragma once

#include <vector>
#include <map>

#include <mpp/helper/TriangleBatchDataProvider.h>

#include <willpower/common/Vector2.h>

#include "Platform.h"

#include "Hive/Build/HiveBuilder.h"

#include "MiniMap.h"


class MiniMapDataProvider : public mpp::helper::TriangleBatch2DDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>
{
	std::vector<float> mVertexData;
	std::map<uint32_t, int> mRoomIds;
	hex::Layout mLayout;
	float mScale;

private:

	const float RenderSize = 32.0f;

public:

	MiniMapDataProvider(MiniMap const* miniMap, hive::build::HiveBuilder const& builder)
		: mLayout(builder.getHexLayout())
		, mScale(1.0f)
	{
		auto worldRadius = mLayout.radius;

		// Recalculate cell radius
		mLayout.radius = RenderSize;
		mScale = RenderSize / worldRadius;
		
		float insetDist = 4;
		float insetRadius = mLayout.radius - insetDist;
		float connWidth = 8;

		// Rooms
		auto const& cellGenData = miniMap->getCellGenData();
		
		std::vector<wp::Vector2> vertexPositions;
		auto hexVertexDataMap = builder.buildMiniMap(cellGenData, mLayout, insetDist, vertexPositions);

		uint32_t numPrimitives{ 0 };
		for (auto const& item : hexVertexDataMap)
		{
			auto const& [roomIndex, hexVertexData] = item;

			for (auto const& hexVertices : hexVertexData)
			{
				auto vc = mLayout.toWorld(hexVertices.hex);

				for (int i = 0; i < 6; ++i)
				{
					auto v0 = vertexPositions[hexVertices.vertexIndices[i]];
					auto v1 = vertexPositions[hexVertices.vertexIndices[(i + 1) % 6]];

					mVertexData.push_back(vc.x);
					mVertexData.push_back(vc.y);
					mVertexData.push_back(v0.x);
					mVertexData.push_back(v0.y);
					mVertexData.push_back(v1.x);
					mVertexData.push_back(v1.y);

					mRoomIds[numPrimitives] = roomIndex;
					numPrimitives++;
				}
			}
		}

		// Connectors
		auto const& rooms = miniMap->getRooms();
		for (auto const& room : rooms)
		{
			auto connectors = room.getConnectors();
			for (auto const& connector : connectors)
			{
				int roomIdMapping = connector.type == hive::gen::RoomConnectorType::Mandatory ? 99998 : 99999;

				auto hexCentre = mLayout.toWorld(connector.hexd.hex);
				auto connDir = mLayout.hexEdgeDirection(connector.hexd.dir);
				
				auto conn0 = hexCentre + connDir * insetRadius * sqrtf(3.0f) / 2.0f;
				
				auto connSide = connDir.perpendicular();
				auto conn00 = conn0 - connSide * connWidth * 0.5f;
				auto conn10 = conn00 + connDir * insetDist;

				auto conn01 = conn0 + connSide * connWidth * 0.5f;
				auto conn11 = conn01 + connDir * insetDist;

				mVertexData.push_back(conn00.x);
				mVertexData.push_back(conn00.y);
				mVertexData.push_back(conn10.x);
				mVertexData.push_back(conn10.y);
				mVertexData.push_back(conn01.x);
				mVertexData.push_back(conn01.y);

				mRoomIds[numPrimitives] = roomIdMapping;
				numPrimitives++;

				mVertexData.push_back(conn01.x);
				mVertexData.push_back(conn01.y);
				mVertexData.push_back(conn11.x);
				mVertexData.push_back(conn11.y);
				mVertexData.push_back(conn10.x);
				mVertexData.push_back(conn10.y);

				mRoomIds[numPrimitives] = roomIdMapping;
				numPrimitives++;
			}
		}

		// Player rendering
		mRoomIds[numPrimitives++] = 99997;
		mRoomIds[numPrimitives++] = 99997;

		for (int i = 0; i < 12; ++i)
		{
			mVertexData.push_back(0);
		}

		setNumPrimitives(numPrimitives);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin.x = bMin.y = bMin.z = -1e10f;
		bMax.x = bMax.y = bMax.z = 1e10f;
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1, float& x2, float& y2) override
	{
		auto i = index * 6;
		x0 = mVertexData[i + 0];
		y0 = mVertexData[i + 1];
		x1 = mVertexData[i + 2];
		y1 = mVertexData[i + 3];
		x2 = mVertexData[i + 4];
		y2 = mVertexData[i + 5];
	}

	void texcoords(uint32_t index, float& u0, float& v0, float& u1, float& v1, float& u2, float& v2) override
	{
		VAR_UNUSED(index);

		u0 = 0;
		v0 = 0;
		u1 = 1;
		v1 = 1;
		u2 = 1;
		v2 = 0;
	}

	void colour(uint32_t index, float& red, typename float& green, typename float& blue, typename float& alpha) override
	{
		auto roomId = mRoomIds[index];

		if (roomId == 99997)
		{
			red = 0.0f;
			green = 0.0f;
			blue = 0.0f;
		}
		else if (roomId == 99998)
		{
			red = 1.0f;
			green = 1.0f;
			blue = 1.0f;
		}
		else if (roomId == 99999)
		{
			red = 0.0f;
			green = 0.0f;
			blue = 1.0f;
		}
		else
		{
			red = ((roomId + 1) % 4) / 3.0f;
			green = (roomId % 3) / 2.0f;
			blue = (roomId % 2) / 1.0f;
		}

		alpha = 1;
	}

	mpp::Colour diffuse() override
	{
		return mpp::Colour::White;
	}

	wp::Vector2 getTransformedPosition(wp::Vector2 const& pos) const
	{
		return pos * mScale;
	}

	void update(wp::Vector2 const& playerPos)
	{
		auto mapPos = getTransformedPosition(playerPos);

		// Set player vertex data
		auto vi = (uint32_t)mVertexData.size() - 12;

		mVertexData[vi + 0] = mapPos.x - 3;
		mVertexData[vi + 1] = mapPos.y - 3;
		mVertexData[vi + 2] = mapPos.x - 3;
		mVertexData[vi + 3] = mapPos.y + 3;
		mVertexData[vi + 4] = mapPos.x + 3;
		mVertexData[vi + 5] = mapPos.y + 3;

		mVertexData[vi + 6] = mapPos.x + 3;
		mVertexData[vi + 7] = mapPos.y + 3;
		mVertexData[vi + 8] = mapPos.x + 3;
		mVertexData[vi + 9] = mapPos.y - 3;
		mVertexData[vi + 10] = mapPos.x - 3;
		mVertexData[vi + 11] = mapPos.y - 3;
	}

};
