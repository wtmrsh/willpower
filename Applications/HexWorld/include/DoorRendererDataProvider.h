#pragma once

#include <mpp/helper/TriangleBatchDataProvider.h>

#include <willpower/common/Vector2.h>

#include "Platform.h"

#include "Hive/Live/HiveWorld.h"


class DoorRendererDataProvider : public mpp::helper::TriangleBatch2DDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>
{
	struct Data
	{
		bool locked;
	};

private:

	std::vector<float> mVertexData;

	std::vector<Data> mData;

public:

	DoorRendererDataProvider()
	{
		setNumPrimitives(0);
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin.x = bMin.y = bMin.z = -1e10f;
		bMax.x = bMax.y = bMax.z = 1e10f;
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1, float& x2, float& y2) override
	{
		auto i = index * 6;
		x0 = mVertexData[i + 0];
		y0 = mVertexData[i + 1];
		x1 = mVertexData[i + 2];
		y1 = mVertexData[i + 3];
		x2 = mVertexData[i + 4];
		y2 = mVertexData[i + 5];
	}

	void texcoords(uint32_t index, float& u0, float& v0, float& u1, float& v1, float& u2, float& v2) override
	{
		VAR_UNUSED(index);

		u0 = 0;
		v0 = 0;
		u1 = 1;
		v1 = 1;
		u2 = 1;
		v2 = 0;
	}

	void colour(uint32_t index, float& red, typename float& green, typename float& blue, typename float& alpha) override
	{
		auto const& data = mData[index / 2];

		red = 1;
		alpha = 1;

		if (data.locked)
		{
			green = 0;
			blue = 0;
		}
		else
		{
			green = 1;
			blue = 1;
		}
	}

	mpp::Colour diffuse() override
	{
		return mpp::Colour::White;
	}

	bool update(hive::live::HiveWorld const* world, std::vector<uint32_t> const& doorIndices, float frameTime)
	{
		VAR_UNUSED(frameTime);

		const uint32_t trisPerDoor{ 2 };
		const uint32_t numDoors = (uint32_t)doorIndices.size();

		auto numPrimitives = numDoors * trisPerDoor;
		
		mVertexData.resize(numPrimitives * trisPerDoor * 6);
		mData.resize(numDoors);

		int i = 0;
		for (uint32_t doorIndex : doorIndices)
		{
			uint32_t di = i / 12;

			auto const* door = world->getDoor(doorIndex);

			wp::Vector2 v0, v1, v2, v3;
			door->getVertices(1.0f - door->getOpenPercentage(), v0, v1, v2, v3);

			// Triangle 1
			mVertexData[i++] = v0.x;
			mVertexData[i++] = v0.y;
			mVertexData[i++] = v1.x;
			mVertexData[i++] = v1.y;
			mVertexData[i++] = v2.x;
			mVertexData[i++] = v2.y;

			// Triangle 2
			mVertexData[i++] = v2.x;
			mVertexData[i++] = v2.y;
			mVertexData[i++] = v3.x;
			mVertexData[i++] = v3.y;
			mVertexData[i++] = v0.x;
			mVertexData[i++] = v0.y;

			// Data
			mData[di] = {
				door->isLocked()
			};
		}

		setNumPrimitives(numPrimitives);
		return true;
	}
};
