#pragma once

#include <cstdint>
#include <set>

#include "Platform.h"
#include "Hex.h"


struct HexLocator
{
	enum class Type
	{
		None,
		Unknown,
		Room,
		Connector,
		Door
	};

	Type type{ Type::None };
	uint32_t typeIndex{ ~0u };
	std::set<uint32_t> polygonIndices;

public:

	static HexLocator fromRoom(uint32_t index, std::set<uint32_t> const& polygonIndices)
	{
		return HexLocator{
			Type::Room,
			index,
			polygonIndices
		};
	}

	static HexLocator fromConnector(uint32_t index, std::set<uint32_t> const& polygonIndices)
	{
		return HexLocator{
			Type::Connector,
			index,
			polygonIndices
		};
	}

	static HexLocator fromDoor(uint32_t index, std::set<uint32_t> const& polygonIndices)
	{
		return HexLocator{
			Type::Door,
			index,
			polygonIndices
		};
	}

	bool operator==(HexLocator const& other) const
	{
		return type == other.type && typeIndex == other.typeIndex;
	}

	bool operator!=(HexLocator const& other) const
	{
		return !(*this == other);
	}
};
