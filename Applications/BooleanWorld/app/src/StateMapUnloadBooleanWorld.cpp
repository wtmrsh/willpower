#include <willpower/application/StateExceptions.h>
#include <willpower/application/ServiceLocator.h>
#include <willpower/application/ApplicationSettings.h>

#include "StateMapUnloadBooleanWorld.h"

using namespace std;
using namespace wp;


StateMapUnloadBooleanWorld::StateMapUnloadBooleanWorld(bool useThreading)
	: applib::StateMapUnload(useThreading)
{
}

vector<applib::ThreadableLoadState::ThreadableWorkFunction> StateMapUnloadBooleanWorld::getPreWork(applib::StateTransitionData* transitionData)
{
	return {};
}