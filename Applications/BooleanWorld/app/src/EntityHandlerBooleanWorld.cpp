#include <willpower/common/Vector2.h>

#include <applib/Bullet.h>
#include <applib/BeamData.h>

#include "EntityHandlerBooleanWorld.h"
#include "EntityType.h"

#include "GameException.h"
#include "EntityProperties.h"

using namespace std;
using namespace wp;
using namespace applib;


map<EntityType, string> gEntityTypeNames = {
	{EntityType::Player, "Player"}
};

EntityHandlerBooleanWorld::EntityHandlerBooleanWorld(shared_ptr<AnimationDatabase> animationDatabase)
	: EntityHandler()
	, mAnimationDatabase(animationDatabase)
{
}

string EntityHandlerBooleanWorld::getPrototypeName(int type)
{
	auto it = gEntityTypeNames.find((EntityType)type);

	if (it != gEntityTypeNames.end())
	{
		return it->second;
	}
	else
	{
		throw GameException(STR_FORMAT("Unknown entity type: {}", type));
	}
}

void EntityHandlerBooleanWorld::setupImpl(Entity* entity)
{
	VAR_UNUSED(entity);
}

void EntityHandlerBooleanWorld::destroyImpl(Entity *entity)
{
	VAR_UNUSED(entity);
}

bool EntityHandlerBooleanWorld::updateImpl(Entity *entity, bool inputControlled, float frameTime)
{
	if (inputControlled)
	{
		auto velocity = Vector2::ZERO;
		float playerSpeed{ 60 };

		for (auto const& state : mActiveInputStates)
		{
			if (state == "Up")
			{
				velocity.y += playerSpeed * frameTime;
			}
			else if (state == "Down")
			{
				velocity.y -= playerSpeed * frameTime;
			}
			else if (state == "Left")
			{
				velocity.x -= playerSpeed * frameTime;
			}
			else if (state == "Right")
			{
				velocity.x += playerSpeed * frameTime;
			}
		}

		// Set desired movement
		velocity.normalise();

		auto& physicalStats = getEntityComponent<PhysicalStats>(*entity);

		physicalStats.position += velocity;

		// Set angle (view/facing angle, not movement direction).
		Vector2 dMouse = mMouseWorld - physicalStats.position;

		dMouse.normalise();
		physicalStats.angle = Vector2::UNIT_Y.clockwiseAngleTo(dMouse);
	}
	else
	{
		auto entityType = entity->getType();

		switch (entityType)
		{
		case (int)EntityType::Player:
			break;

		default:
			throw GameException(STR_FORMAT("Unhandled entity type: {}", (int)entityType));
		}
	}

	return true;
}

void EntityHandlerBooleanWorld::updateVisual(Entity* entity, float frameTime)
{
	auto visual = mComponentRegistry.try_get<VisualSprite>(entity->mCompSysId);
	if (!visual)
	{
		return;
	}

	auto const& anim = mAnimationDatabase->getAnimation((uint32_t)visual->animation);
	auto frame = mAnimationDatabase->getAnimationFrame((uint32_t)visual->animation, visual->frame);

	visual->timer += frameTime;
	while (visual->timer >= frame.time)
	{
		visual->timer -= frame.time;
		visual->frame += visual->direction;

		// Check if we've hit the end of the animation
		if (visual->frame == 0 || visual->frame == anim.count)
		{
			switch (anim.style)
			{
			case AnimationDatabase::LoopStyle::Forwards:
				visual->frame = 0;
				break;

			case AnimationDatabase::LoopStyle::Once:
				visual->frame = anim.count - 1;
				break;

			case AnimationDatabase::LoopStyle::PingPong:
				visual->direction *= -1;
				visual->frame += visual->direction;
				break;

			default:
				throw Exception("Unknown loop style.  Must be one of Forwards|Once|PingPoing.");
			}
		}

		// Get next frame details, in case we have skipped past the new frame entirely
		frame = mAnimationDatabase->getAnimationFrame((uint32_t)visual->animation, visual->frame);
	}
}

bool EntityHandlerBooleanWorld::update(Entity* entity, bool controlActive, float frameTime)
{
	// Update logic
	bool inputControlled = controlActive && (entity->getId() == 0);
	bool alive = updateImpl(entity, inputControlled, frameTime);

	if (inputControlled)
	{
		auto physicalStats = mComponentRegistry.try_get<PhysicalStats>(entity->mCompSysId);
	}

	if (alive)
	{
		// Update visual
		updateVisual(entity, frameTime);
	}

	return alive;
}