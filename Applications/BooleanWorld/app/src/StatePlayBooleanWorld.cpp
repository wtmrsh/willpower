#include <algorithm>

#include <willpower/application/StateExceptions.h>

#include <willpower/geometry/MeshQuery.h>

#include <willpower/viz/AccelerationGridRenderer.h>
#include <willpower/viz/DynamicLineRenderer.h>
#include <willpower/viz/CollisionSimulationRenderer.h>
#include <willpower/viz/FirepowerMeshRenderer.h>

#include <applib/ModelInstance.h>
#include <applib/EntityFacade.h>
#include <applib/EntityProperties.h>
#include <applib/VisualSpriteEntityFacade.h>

#include <core/Timer.h>

#include "StatePlayBooleanWorld.h"
#include "BooleanWorldModel.h"
#include "EntityType.h"
#include "TriMeshDataProvider.h"
#include "TriMeshEntityFacadeFactory.h"
#include "Map.h"

using namespace std;
using namespace wp;


StatePlayBooleanWorld::StatePlayBooleanWorld()
	: StatePlay()
{
}

Map* StatePlayBooleanWorld::getMap()
{
	return static_cast<Map*>(mMap.get());
}

Map const* StatePlayBooleanWorld::getMap() const
{
	return static_cast<Map const*>(mMap.get());
}

void StatePlayBooleanWorld::setupMapRenderer(applib::StateTransitionData* transitionData)
{
	VAR_UNUSED(transitionData);

	auto world = getMap()->getWorld();

	auto worldExtents = world->getExtents();

	wp::Vector2 worldMinExtent, worldSize;
	worldMinExtent = worldExtents.getMinExtent();
	worldSize = worldExtents.getSize();

	mAccelGrid = make_shared<wp::AccelerationGrid>(
		worldMinExtent.x, 
		worldMinExtent.y, 
		worldSize.x,
		worldSize.y,
		16, 
		16);

	mWorldTriangleDataProvider = make_shared<WorldTriangleDataProvider>(world, mAccelGrid);

	mAccelGrid->clear();
	
	for (uint32_t i = 0; i < world->getNumPrimitives(); ++i)
	{
		auto primitive = world->getPrimitive(i);
		mAccelGrid->addItem(i, primitive->getBounds());
	}

	viz::TriangleRendererOptions triangleOptions
	{
		false,
		false,
		false,
		false,
		mwRenderResourceMgr->getResource("__mpp_tex_none__")
	};

	mWorldTriangleRenderer = make_shared<WorldTriangleRenderer>("WorldTriangles", triangleOptions, mWorldTriangleDataProvider, mwRenderResourceMgr);
	mWorldTriangleRenderer->build(mwRenderSystem, mwRenderResourceMgr);
	mWorldTriangleRenderer->addToScene(mScene, (int)RenderOrder::Map);
	mWorldTriangleRenderer->setVisible(true);
}

map<string, tuple<wp::viz::Renderer*, int, bool>> StatePlayBooleanWorld::createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr)
{
	mDebugLineDataProvider = make_shared<DebugLineDataProvider>();
	auto debugLineRenderer = new viz::DynamicLineRenderer("DebugLines", mDebugLineDataProvider, renderResourceMgr);

//	mGridRenderer = make_shared<viz::AccelerationGridRenderer>("Accel_Grid", mAccelGrid, 32, resourceMgr);
//	mGridRenderer->build(renderSystem, resourceMgr);
//	mGridRenderer->addToScene(getScene(), 1);

	return {
		make_pair("DebugLines", make_tuple(debugLineRenderer, (int)RenderOrder::MapDebug, true))
	};
}

void StatePlayBooleanWorld::registerInput()
{
	//										Keys pressed/released/down						// Buttons		Wheel up/down
	registerInputState("Up",				{}, {}, { application::Key::UpArrow },			{},	{},	{},		false, false, 0);
	registerInputState("Down",				{}, {}, { application::Key::DownArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Left",				{}, {}, { application::Key::LeftArrow },		{},	{},	{},		false, false, 0);
	registerInputState("Right",				{},	{},	{ application::Key::RightArrow },		{},	{},	{},		false, false, 0);
}

void StatePlayBooleanWorld::createGameObjects(application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args)
{
	VAR_UNUSED(resourceMgr);
	VAR_UNUSED(renderSystem);
	VAR_UNUSED(renderResourceMgr);
	VAR_UNUSED(args);
}

void StatePlayBooleanWorld::destroyGameObjects()
{
}

void StatePlayBooleanWorld::setupEntityFacades()
{
	applib::VisualSpriteEntityFacadeRenderOptions options;

	options.rotationType = wp::viz::RotationOptions::None;

	// Create Quad EntityFacade
	auto quadsResource = mwResourceMgr->getResource("EntityAnimations");
	auto animDatabase = applib::ModelInstance::animationDatabase();
	mEntityMgr->registerFacadeFactory("Sprites", new applib::VisualSpriteEntityFacadeFactory(quadsResource, animDatabase));

	createEntityFacade(
		"Sprites",
		{ (int)EntityType::Player },
		options,
		64);

	// Create triangles EntityFacade
	auto trisProviderFactory = [](auto facade) {
		return make_shared<TriMeshDataProvider>(facade);
	};

	mEntityMgr->registerFacadeFactory("TriMesh", new TriMeshEntityFacadeFactory(trisProviderFactory, nullptr));
}

vector<string> StatePlayBooleanWorld::getDebuggingText() const
{
	auto mouseScreen = getMouseScreenPosition();
	auto mouseWorld = getMouseWorldPosition();
	
	auto const& worldStats = getMap()->getWorld()->getStats();

	return {
		STR_FORMAT("Mouse screen: {},{}", mouseScreen.x, mouseScreen.y),
		STR_FORMAT("Mouse world: {},{}", mouseWorld.x, mouseWorld.y),
		STR_FORMAT("Primitives processed: {}", worldStats.primitivesProcessed),
		STR_FORMAT("Polygons generated: {}", worldStats.polygonsGenerated),
		STR_FORMAT("Vertices generated: {}", worldStats.verticesGenerated),
		STR_FORMAT("Clip time: {}", bw::core::Timer::nsToString(worldStats.clipNs)),
		STR_FORMAT("Triangulate time: {}", bw::core::Timer::nsToString(worldStats.triangulateNs))
	};
}

void StatePlayBooleanWorld::setupEntities()
{
	Vector2 playerPos(0, 0);

	createEntity((int)EntityType::Player, playerPos, 0, true);
}

void StatePlayBooleanWorld::setup(application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args)
{
	WP_UNUSED(resourceMgr);

	auto transitionData = static_cast<applib::StateTransitionData*>(args);

	// Set up objects to pass to next state
	mTransitionData.mapData.prevMap.map = transitionData->mapData.nextMap.map;
	mMap = transitionData->mapData.nextMap.map;

	createInput();
	createScreenFxManagement();
	createEntityManagement();

	createRenderers(renderResourceMgr, transitionData);

	setupScene();
	loadAllReferencedResources();

	// Set up input
	registerInput();

	// For subclasses
	createGameObjects(resourceMgr, renderSystem, renderResourceMgr, args);
}

void StatePlayBooleanWorld::updatePreInput(float frameTime)
{

	VAR_UNUSED(frameTime);

	getMap()->getWorld()->newFrame();
}

void StatePlayBooleanWorld::updatePreEntities(float frameTime)
{
	int mouseX, mouseY;
	auto model = static_cast<BooleanWorldModel*>(applib::ModelInstance::get());
	model->entityHandler->getMouseScreenPosition(&mouseX, &mouseY);

	auto const& windowSize = getWindowSize();

	float dx = mouseX / windowSize.x;

	getMap()->getWorld()->getPrimitive(1)->setT(bw::core::VertexTransformer::Key::Angle, dx);
}

void StatePlayBooleanWorld::updatePostEntities(float frameTime)
{
	auto model = static_cast<BooleanWorldModel*>(applib::ModelInstance::get());
	auto const& physicalStats = model->entityHandler->getEntityComponent<applib::PhysicalStats>(mEntityMgr->getPlayerEntity());

	getMap()->getWorld()->update(frameTime, physicalStats.position, physicalStats.angle);
}

void StatePlayBooleanWorld::updateActions(vector<string> const& activeStates, float frameTime)
{
	VAR_UNUSED(activeStates);
	VAR_UNUSED(frameTime);
}

void StatePlayBooleanWorld::updatePreRenderers(float frameTime)
{
	auto viewBounds = getViewBounds();

	// World triangles
	mWorldTriangleDataProvider->update(viewBounds);
	mWorldTriangleRenderer->update(viewBounds, frameTime);

	// Debug lines
	auto debugLineRenderer = get<0>(mAdditionalRenderers["DebugLines"]);
	if (debugLineRenderer->isVisible())
	{
		mDebugLineDataProvider->update(viewBounds);

		// Add player direction
		auto model = static_cast<BooleanWorldModel*>(applib::ModelInstance::get());
		auto const& physicalStats = model->entityHandler->getEntityComponent<applib::PhysicalStats>(mEntityMgr->getPlayerEntity());

		auto playerLookAt = physicalStats.position + wp::Vector2::UNIT_Y.rotatedClockwiseCopy(physicalStats.angle) * 40.0f;
		mDebugLineDataProvider->addLine(physicalStats.position, playerLookAt, 255, 255, 255, 255);
	}
}

void StatePlayBooleanWorld::updateImpl(float frameTime)
{
	updatePreInput(frameTime);
	updateInput(frameTime);
	updatePreEntities(frameTime);
	updateEntityManagement(frameTime);
	updatePostEntities(frameTime);
	updateCamera(frameTime);
	updatePreRenderers(frameTime);
	updateScreenFxManagement(frameTime);
	updateRenderers(frameTime);
}
