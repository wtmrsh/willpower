#pragma once

#include <vector>

#include <applib/EntityHandler.h>
#include <applib/ObjectArray.h>
#include <applib/AnimationDatabase.h>

#include "Platform.h"


class EntityHandlerBooleanWorld : public applib::EntityHandler
{
	std::shared_ptr<applib::AnimationDatabase> mAnimationDatabase;

private:

	void updateVisual(applib::Entity* entity, float frameTime);

	std::string getPrototypeName(int type) override;

	void setupImpl(applib::Entity* entity) override;

	void destroyImpl(applib::Entity* entity) override;

	bool updateImpl(applib::Entity *entity, bool inputControlled, float frameTime) override;

public:

	explicit EntityHandlerBooleanWorld(std::shared_ptr<applib::AnimationDatabase> animationDatabase);

	bool update(applib::Entity* entity, bool controlActive, float frameTime) override;
};