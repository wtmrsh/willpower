#pragma once

#include <mpp/helper/TriangleBatchDataProvider.h>

#include <willpower/common/AccelerationGrid.h>
#include <willpower/common/BoundingBox.h>

#include <core/World.h>


class WorldTriangleDataProvider : public mpp::helper::TriangleBatch2DDataProvider<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat>
{
	std::vector<float> mVertexData;

	bw::core::World* mWorld;

	std::shared_ptr<wp::AccelerationGrid> mGrid;

public:

	WorldTriangleDataProvider(bw::core::World* world, std::shared_ptr<wp::AccelerationGrid> grid)
		: mWorld(world)
		, mGrid(grid)
	{
		setNumPrimitives(0);
	}

	void setWorld(bw::core::World* world)
	{
		mWorld = world;
	}

	void getBounds(glm::vec3& bMin, glm::vec3& bMax) override
	{
		bMin.x = bMin.y = bMin.z = -1e10f;
		bMax.x = bMax.y = bMax.z = 1e10f;
	}

	void position(uint32_t index, float& x0, float& y0, float& x1, float& y1, float& x2, float& y2) override
	{
		auto const& vd = &mVertexData[index * 6];

		x0 = vd[0];
		y0 = vd[1];
		x1 = vd[2];
		y1 = vd[3];
		x2 = vd[4];
		y2 = vd[5];
	}

	void texcoords(uint32_t index, float& u0, float& v0, float& u1, float& v1, float& u2, float& v2) override
	{
		WP_UNUSED(index);

		u0 = 0;
		v0 = 0;
		u1 = 1;
		v1 = 1;
		u2 = 1;
		v2 = 0;
	}

	void colour(uint32_t index, float& red, typename float& green, typename float& blue, typename float& alpha) override
	{
		WP_UNUSED(index);

		red = 0;
		green = 0.3f;
		blue = 0.8f;
		alpha = 1;
	}

	mpp::Colour diffuse() override
	{
		return mpp::Colour::White;
	}

	bool update(wp::BoundingBox const& viewBounds)
	{
		mVertexData.clear();

		auto primitiveIndices = mGrid->getCandidateItemsInBoundingArea(viewBounds);

		auto vertices = mWorld->triangulate(viewBounds, primitiveIndices);

		uint32_t numVertices = (uint32_t)vertices.size();
		for (uint32_t i = 0; i < numVertices; i += 3)
		{
			mVertexData.push_back(vertices[i + 0].x);
			mVertexData.push_back(vertices[i + 0].y);
			mVertexData.push_back(vertices[i + 1].x);
			mVertexData.push_back(vertices[i + 1].y);
			mVertexData.push_back(vertices[i + 2].x);
			mVertexData.push_back(vertices[i + 2].y);
		}

		setNumPrimitives(numVertices / 3);

		return true;
	}
};