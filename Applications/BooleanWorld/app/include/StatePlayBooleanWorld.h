#pragma once

#include <vector>
#include <tuple>

#include <willpower/application/StateFactory.h>

#include <willpower/common/AccelerationGrid.h>

#include <willpower/viz/DynamicTriangleRenderer.h>

#include <applib/StatePlay.h>
#include <applib/EntityManager.h>

#include "Platform.h"
#include "EntityHandlerBooleanWorld.h"
#include "WorldTriangleDataProvider.h"
#include "DebugLineDataProvider.h"
#include "Map.h"


class APPLICATION_API StatePlayBooleanWorld : public applib::StatePlay
{
	typedef wp::viz::DynamicTriangleRenderer<mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat, mpp::mesh::DataTypeFloat> WorldTriangleRenderer;

private:

	std::shared_ptr<wp::AccelerationGrid> mAccelGrid;

	std::shared_ptr<WorldTriangleDataProvider> mWorldTriangleDataProvider;

	std::shared_ptr<WorldTriangleRenderer> mWorldTriangleRenderer;

	std::shared_ptr<DebugLineDataProvider> mDebugLineDataProvider;

private:

	Map* getMap();

	Map const* getMap() const;

	void setupMapRenderer(applib::StateTransitionData* transitionData) override;

	std::map<std::string, std::tuple<wp::viz::Renderer*, int, bool>> createAdditionalRenderers(mpp::ResourceManager* renderResourceMgr) override;
	
	void registerInput() override;

	void createGameObjects(wp::application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args) override;

	void destroyGameObjects() override;

	void setupEntityFacades() override;

	void setupEntities() override;

	void updatePreInput(float frameTime) override;

	void updatePreEntities(float frameTime) override;

	void updatePostEntities(float frameTime) override;

protected:

	void updateActions(std::vector<std::string> const& activeStates, float frameTime) override;

	void updatePreRenderers(float frameTime) override;

	void setup(wp::application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args) override;

	void updateImpl(float frameTime) override;

public:

	StatePlayBooleanWorld();

	std::vector<std::string> getDebuggingText() const override;
};

class StatePlayBooleanWorldFactory : public wp::application::StateFactory
{
	wp::Logger* mLogger;

public:

	explicit StatePlayBooleanWorldFactory(wp::Logger* logger)
		: wp::application::StateFactory("Play")
		, mLogger(logger)
	{
	}

	wp::application::State* createState()
	{
		auto state = new StatePlayBooleanWorld();
		state->setLogger(mLogger);
		return state;
	}
};
