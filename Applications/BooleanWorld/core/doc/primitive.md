# Primitives

Primitives are the building blocks of the world.  They are programmatic objects, and as such can have the parameters that
define them dynamically altered.

Primitives have the usual scale/rotate/translate transform applied to them on top of their basic vertices.  Further, they
can be rotated around and additional point ("orbit").

## Operation

### Union

The output is the combined outline of input polygons, regardless of how they intersect.

### Difference

This is the only non-commutative operation, and for operation A diff B, B is "subtracted" or cut from A, if they intersect.

### Intersection

The output is the areas where the input polygons overlap.

### XOR

The output is the areas where exactly one polygon exists.  In other words this is the same as doing both a union and an intersection,
and doing a difference of those two results.

## Fill rule

Complex polygons are defined by one or more closed paths that set both outer and inner polygon boundaries. But only portions of these
paths (or 'contours') may be setting polygon boundaries, so crossing a path may or may not mean entering or exiting a 'filled' polygon
region. For this reason complex polygons require filling rules that define which polygon sub-regions will be considered inside a given
polygon, and which sub-regions will not.

### NonZero

Only non-zero sub-regions are filled.

### Even-Odd

Only odd numbered sub-regions are filled.

## Priority

The order of operations on primitivese makes a difference to the final result.  The priority is a value in [0, 256] where lower values
get their operations done first.  The very first polygon's operation is ignored, and the first operation is performing the second
polygon's operation on the first, then the third polygon's operation on the result of that, and so on.

## Basic types

All basic types have the following parameters:

##### Number of sides

The number of sides (of equal length) that the circle has.  This explicitly sets the number of sides, ignoring the resolution
parameter.

### RegularPolygon

This primitive has at least 3 sides (ie a triangle), with the first vertex pointing up, so a 4-sided primitive is diamond-
shaped rather than square shaped.

### Circle

This is a standard circle, with variable resolution.

#### Parameters

##### Resolution

A value in [0, 1] which acts as a multiplier for the number of vertices making up the circle geometry.

### RectanglePolygon

#### Parameters

##### X/Y ratio

The ratio of the height to the width.  Ie height/width.

### PathPolygon

An inflated 2d path.

#### Parameters

##### Points

A list of 2d points making up the path.

##### Width

The width of the generated path.

##### JoinType

How to smooth the corners where two edges join at a vertex.  Options: bevel, mitre, round, square.

##### EndType

How to close the ends of the line.  Options: butt, joined, polygon, round, square.

### SuperformulaPolygon

An implementation of the Superformula.  This takes 6 parameters, and produces different shapes.

#### Parameters

##### Values

A list of 6  float parameters to define the Superformula.