#include "core/Serializable.h"

namespace bw
{
	namespace core
	{
		using namespace std;

		Serializable::Serializable()
			: mModified(true)
		{
		}

		Serializable::Serializable(Serializable const& other)
		{
			copyFrom(other);
		}

		Serializable& Serializable::operator=(Serializable const& other)
		{
			copyFrom(other);
			return *this;
		}

		void Serializable::copyFrom(Serializable const& other)
		{
			mDeserializationWarnings = other.mDeserializationWarnings;
			mDeserializationErrors = other.mDeserializationErrors;
			mModified = other.mModified;
		}

		void Serializable::addDeserializationWarning(string const& msg)
		{
			mDeserializationWarnings.push_back(msg);
		}

		void Serializable::addDeserializationError(string const& msg)
		{
			mDeserializationErrors.push_back(msg);
		}

		vector<string> const& Serializable::getDeserializationWarnings() const
		{
			return mDeserializationWarnings;
		}

		vector<string> const& Serializable::getDeserializationErrors() const
		{
			return mDeserializationErrors;
		}

		void Serializable::modify()
		{
			mModified = true;
		}

		bool Serializable::isModified() const
		{
			return childrenModified() || mModified;
		}

		void Serializable::copyErrorsAndWarnings(Serializable const* ser, bool errors, bool warnings)
		{
			if (warnings)
			{
				auto const& warnings = ser->getDeserializationWarnings();
				for (auto const& warning : warnings)
				{
					addDeserializationWarning(warning);
				}
			}

			if (errors)
			{
				auto const& errors = ser->getDeserializationErrors();
				for (auto const& error : errors)
				{
					addDeserializationError(error);
				}
			}
		}

		void Serializable::serialize(shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			serializeImpl(serializer, workData);
			mModified = false;
		}

		bool Serializable::deserialize(shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			mDeserializationWarnings.clear();
			mDeserializationErrors.clear();

			mModified = deserializeImpl(serializer, workData);
			return mModified;
		}

	} // core
} // bw