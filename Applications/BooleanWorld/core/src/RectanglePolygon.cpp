#include "core/RectanglePolygon.h"


namespace bw
{
	namespace core
	{

		using namespace std;

		RectanglePolygon::RectanglePolygon()
			: Primitive()
			, mXyRatio(1.0f)
		{
		}

		RectanglePolygon::RectanglePolygon(Operation operation, FillRule fillType, float xyRatio)
			: Primitive(operation, fillType)
			, mXyRatio(xyRatio)
		{
			generateVertices();
		}

		RectanglePolygon::RectanglePolygon(RectanglePolygon const& other)
		{
			copyFrom(other);
			this->mXyRatio = other.mXyRatio;
		}

		RectanglePolygon& RectanglePolygon::operator=(RectanglePolygon const& other)
		{
			copyFrom(other);
			return *this;
		}

		void RectanglePolygon::copyFrom(RectanglePolygon const& other)
		{
			Primitive::copyFrom(other);
		}

		Primitive* RectanglePolygon::copy() const
		{
			return new RectanglePolygon(*this);
		}

		string RectanglePolygon::getType() const
		{
			return "Rectangle";
		}

		void RectanglePolygon::serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			Primitive::serializeImpl(serializer, workData);

			serializer->writeFloat("xyRatio", mXyRatio);
		}

		bool RectanglePolygon::deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			if (!Primitive::deserializeImpl(serializer, workData))
			{
				return false;
			}

			float xyRatio;

			try
			{
				xyRatio = serializer->readFloat("xyRatio");
			}
			catch (exception& e)
			{
				addDeserializationError(e.what());
				return false;
			}

			// Commit
			mXyRatio = xyRatio;

			return true;

		}

		vector<PrimitiveVertex> RectanglePolygon::generateVerticesImpl()
		{
			float i1 = 1.0f / mXyRatio;

			return {
				{ wp::Vector2(1.0f, i1), 0 },
				{ wp::Vector2(-1.0f, i1), 0 },
				{ wp::Vector2(-1.0f, -i1), 0 },
				{ wp::Vector2(1.0f, -i1), 0 }
			};
		}

		void RectanglePolygon::setXyRatio(float xyRatio)
		{
			mXyRatio = xyRatio;
			generateVertices();
		}

		float RectanglePolygon::getXyRatio() const
		{
			return mXyRatio;
		}

	} // core
} // bw