#include <iterator>
#include <map>
#include <functional>

#include <mapbox/earcut.hpp>

#include <willpower/common/BezierSpline.h>
#include <willpower/common/MathsUtils.h>

#include "core/World.h"
#include "core/Clipper2Clipper.h"
#include "core/Timer.h"
#include "core/CoreException.h"

#include "core/PathPolygon.h"
#include "core/RectanglePolygon.h"
#include "core/RegularPolygon.h"
#include "core/CirclePolygon.h"
#include "core/SuperformulaPolygon.h"


namespace bw
{
	namespace core
	{

		using namespace std;

		World::World()
			: World(8192, 8192)
		{
		}

		World::World(float sizeX, float sizeY)
			: mExtents(-sizeX / 2, -sizeY / 2, sizeX, sizeY)
			, mPrimitiveLookupGrid(nullptr)
			, mClipNs(-1)
			, mTriangulateNs(-1)
			, mNumPrimitivesClipped(0)
			, mNumPolygonsGenerated(0)
			, mNumVerticesGenerated(0)
		{
		}

		World::World(World const& other)
		{
			copyFrom(other);
		}

		World& World::operator=(World const& other)
		{
			copyFrom(other);
			return *this;
		}

		World::~World()
		{
			clear();
		}

		void World::copyFrom(World const& other)
		{
			Serializable::copyFrom(other);

			mName = other.mName;

			// Create copies of primitives, making sure to instantiate correct subclass
			map<Primitive*, Primitive*> primitiveMap;
			for (auto* primitive : other.mPrimitives)
			{
				auto p = primitive->copy();

				primitiveMap[primitive] = p;
				mPrimitives.push_back(p);
			}

			// Add to Acceleration grid
			//delete mPrimitiveLookupGrid;
			if (other.mPrimitiveLookupGrid)
			{
				mPrimitiveLookupGrid = new wp::AccelerationGrid(
					other.mPrimitiveLookupGrid->getOffset(),
					other.mPrimitiveLookupGrid->getSize(),
					other.mPrimitiveLookupGrid->getCellDimensionX(),
					other.mPrimitiveLookupGrid->getCellDimensionY());

				for (uint32_t i = 0; i < mPrimitives.size(); ++i)
				{
					mPrimitiveLookupGrid->addItem(i, mPrimitives[i]->getBounds());
				}
			}
			else
			{
				mPrimitiveLookupGrid = nullptr;
			}

			mClipNs = other.mClipNs;
			mTriangulateNs = other.mTriangulateNs;
			mNumPrimitivesClipped = other.mNumPrimitivesClipped;
			mNumPolygonsGenerated = other.mNumPolygonsGenerated;
			mNumVerticesGenerated = other.mNumVerticesGenerated;
		}

		Primitive* World::instantiatePrimitive(string const& type) const
		{
			typedef function<Primitive*()> PrimitiveCreator;

			map<string, PrimitiveCreator> primCreators = {
				{ "Path", []() { return new PathPolygon; } },
				{ "Rectangle", []() { return new RectanglePolygon; } },
				{ "Regular", []() { return new RegularPolygon; } },
				{ "Circle", []() { return new CirclePolygon; } },
				{ "Superformula", []() { return new SuperformulaPolygon; } }
			};

			auto it = primCreators.find(type);
			if (it == primCreators.end())
			{
				throw CoreException(format("No primitive of type '{}' registered", type));
			}

			return it->second();
		}

		bool World::childrenModified() const
		{
			for (auto const* primitive : mPrimitives)
			{
				if (primitive->isModified())
				{
					return true;
				}
			}

			return false;
		}

		void World::serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			serializer->beginMap("root");
			{
				serializer->beginMap("world");
				{
					serializer->writeString("name", getName());

					serializer->writeVector2("minExtent", mExtents.getMinExtent());
					serializer->writeVector2("maxExtent", mExtents.getMaxExtent());

					serializer->beginArray("primitives");
					{
						for (auto const* primitive : mPrimitives)
						{
							// Serialize the type, so we can instantiate it ahead of time during deserialization.
							serializer->beginMap("primitive");
							{
								serializer->writeString("type", primitive->getType());
								
								primitive->serialize(serializer, workData);

								serializer->endMap(); // primitive
							}
						}
					
						serializer->endArray(); // primitives
					}

					serializer->endMap(); // world
				}
			}

			serializer->endMap(); // root
		}

		bool World::deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			// Read in to temporary objects
			string worldName;
			wp::Vector2 minExtent, maxExtent;
			vector<Primitive*> primitives;

			try
			{
				serializer->beginMap("root");
				{
					serializer->beginMap("world");
					{
						worldName = serializer->readString("name");
						minExtent = serializer->readVector2("minExtent");
						maxExtent = serializer->readVector2("maxExtent");

						serializer->beginArray("primitives");
						{
							while (serializer->nextArrayItem())
							{
								serializer->beginMap("primitive");
								{
									auto primitiveType = serializer->readString("type");

									// Instantiate primitive and deserialize
									auto primitive = instantiatePrimitive(primitiveType);
									
									if (!primitive->deserialize(serializer, workData))
									{
										copyErrorsAndWarnings(primitive, true, true);
										return false;
									}

									primitives.push_back(primitive);

									serializer->endMap(); // primitive
								}
							}
						}

						serializer->endArray(); // primitives
					}

					// Fix up VertexTransformer parents
					for (auto& item : workData.vtIdToVtMap)
					{
						auto& [id, vt] = item;

						auto it = workData.vtIdToParentMap.find(id);
						if (it != workData.vtIdToParentMap.end())
						{
							auto parentId = it->second;
							vt->setParent(workData.vtIdToVtMap[parentId]);
						}
					}

					serializer->endMap(); // world
				}

				serializer->endMap(); // root
			}
			catch (exception& e)
			{
				addDeserializationError(e.what());

				// No memory leaks on failed open
				for (auto primitive : primitives)
				{
					delete primitive;
				}

				return false;
			}

			// Commit
			clear();

			mName = worldName;
			mExtents.setPosition(minExtent);
			mExtents.setSize(maxExtent);
			mPrimitives = primitives;

			return true;
		}

		Stats World::getStats() const
		{
			return {
				mNumPrimitivesClipped,
				mNumPolygonsGenerated,
				mNumVerticesGenerated,
				mClipNs,
				mTriangulateNs
			};
		}

		void World::createPrimitiveAccelerationGrid()
		{
			if (!mPrimitiveLookupGrid)
			{
				wp::Vector2 minExtent, maxExtent;

				mExtents.getExtents(minExtent, maxExtent);

				auto worldOffset = minExtent;
				auto worldSize = maxExtent - minExtent;

				int dimsX = std::max(1, (int)(worldSize.x / 512));
				int dimsY = std::max(1,(int)(worldSize.y / 512));

				mPrimitiveLookupGrid = new wp::AccelerationGrid(worldOffset, worldSize, dimsX, dimsY);
			}
		}

		void World::clear()
		{
			mName = "";

			delete mPrimitiveLookupGrid;
			mPrimitiveLookupGrid = nullptr;

			for (auto primitive : mPrimitives)
			{
				delete primitive;
			}

			mPrimitives.clear();
		}

		void World::setName(string const& name)
		{
			mName = name;
		}

		string const& World::getName() const
		{
			return mName;
		}

		wp::BoundingBox const& World::getExtents() const
		{
			return mExtents;
		}

		uint32_t World::addPrimitive(Primitive* primitive)
		{
			auto index = (uint32_t)mPrimitives.size();

			mPrimitives.push_back(primitive);

			if (mPrimitiveLookupGrid)
			{
				mPrimitiveLookupGrid->addItem(index, primitive->getBounds());
			}

			primitive->mId = index;

			return index;
		}

		void World::removePrimitive(Primitive* primitive, bool failIfNotFound)
		{
			if (mPrimitiveLookupGrid)
			{
				throw exception("Cannot remove primitive from world when the acceleration grid is active");
			}

			auto numPrimitives = (uint32_t)mPrimitives.size();
			for (uint32_t i = 0; i < numPrimitives; ++i)
			{
				if (mPrimitives[i] == primitive)
				{
					delete primitive;

					for (uint32_t j = i; j < numPrimitives - 1; ++j)
					{
						mPrimitives[j] = mPrimitives[j + 1];
						mPrimitives[j]->mId = j;
					}

					mPrimitives.pop_back();
					return;
				}
			}

			if (failIfNotFound)
			{
				throw exception("Primitive not found in world");
			}
		}

		void World::removePrimitive(uint32_t index)
		{
			removePrimitive(mPrimitives[index]);
		}

		Primitive* World::getPrimitive(uint32_t index)
		{
			return mPrimitives[index];
		}

		Primitive const* World::getPrimitive(uint32_t index) const
		{
			return mPrimitives[index];
		}

		uint32_t World::getNumPrimitives() const
		{
			return (uint32_t)mPrimitives.size();
		}

		vector<Primitive*> const& World::getPrimitives() const
		{
			return mPrimitives;
		}

		vector<Primitive*> World::getPrimitivesByPriority() const
		{
			vector<Primitive*> sorted(mPrimitives.size());

			partial_sort_copy(mPrimitives.begin(), mPrimitives.end(), sorted.begin(), sorted.end(), SortPrimitivesByPriority());

			return sorted;
		}

		Primitive* World::findPrimitive(wp::Vector2 const& worldPos, bool exact) const
		{
			auto index = findPrimitiveIndex(worldPos, exact);

			return index != ~0u ? mPrimitives[index] : nullptr;
		}

		uint32_t World::findPrimitiveIndex(wp::Vector2 const& worldPos, bool exact) const
		{
			auto numPrimitives = getNumPrimitives();

			for (uint32_t i = 0; i < numPrimitives; ++i)
			{
				auto primitive = mPrimitives[i];

				auto const& bounds = primitive->getBounds();
				if (bounds.pointInside(worldPos))
				{
					if (exact)
					{
						// Create triangulation and check that
						auto triangulation = primitive->triangulate();
						if (triangulation.pointInside(worldPos))
						{
							return i;
						}
					}
					else
					{
						return i;
					}
				}
			}

			return ~0u;
		}

		void World::newFrame()
		{
			mClipNs = 0;
			mTriangulateNs = 0;
			mNumPrimitivesClipped = 0;
			mNumPolygonsGenerated = 0;
			mNumVerticesGenerated = 0;
		}

		void World::update(float frameTime, wp::Vector2 const& entityPos, float entityAngle)
		{
			for (auto primitive : mPrimitives)
			{
				primitive->setInputs(entityPos, entityAngle);
			}
		}

		vector<Primitive*> World::sortPrimitiveIndicesByPriority(set<uint32_t> const& indices) const
		{
			vector<Primitive*> primitives;

			for (auto index : indices)
			{
				primitives.push_back(mPrimitives[index]);
			}

			sort(primitives.begin(), primitives.end(), SortPrimitivesByPriority());

			return primitives;
		}

		vector<ClippedPolygon> World::calculatePolygons(wp::BoundingBox const& extents, vector<Primitive*> const& primitives) const
		{
			if (primitives.empty())
			{
				return {};
			}

			return Clipper2Clipper().clip(primitives, extents);
		}

		vector<wp::Vector2> World::triangulate(wp::BoundingBox const& extents, set<uint32_t> const& primitiveIndices, vector<ClippedPolygon>* outClippedPolygons) const
		{
			using Point = array<float, 2>;
	
			mNumPrimitivesClipped = (uint32_t)primitiveIndices.size();

			Timer timer;

			auto clippedPolygons = calculatePolygons(extents, sortPrimitiveIndicesByPriority(primitiveIndices));

			mClipNs = timer.elapsedNanoseconds();
			mNumPolygonsGenerated = (uint32_t)clippedPolygons.size();
			mNumVerticesGenerated = 0;
			for (auto const& poly : clippedPolygons)
			{
				mNumVerticesGenerated += (uint32_t)poly.vertices.size();
			}

			if (outClippedPolygons)
			{
				*outClippedPolygons = clippedPolygons;
			}
	
			vector<vector<Point>> triangulations;
			vector<wp::Vector2> triVertList, finalTriangleData;

			for (auto const& clippedPolygon : clippedPolygons)
			{
				auto const& polyVerts = clippedPolygon.vertices;

				// Read in points to required format for triangulation
				vector<Point> points(polyVerts.size());

				for (uint32_t i = 0; i < polyVerts.size(); ++i)
				{
					points[i] = { polyVerts[i].x, polyVerts[i].y };
				}
		
				if (!clippedPolygon.isHole)
				{
					// New polygon: time to calculate the current one
					if (!triangulations.empty())
					{
						auto triangleIndices = mapbox::earcut<uint32_t>(triangulations);

						for (auto triIndex : triangleIndices)
						{
							finalTriangleData.push_back(triVertList[triIndex]);
						}

						triVertList.clear();
						triangulations.clear();
					}
				}

				triangulations.push_back(points);

				// Store vertices in contiguous list for later indexing
				copy(polyVerts.begin(), polyVerts.end(), back_inserter(triVertList));
			}

			// Final triangulation
			auto triangleIndices = mapbox::earcut<uint32_t>(triangulations);

			mTriangulateNs = timer.elapsedNanoseconds() - mClipNs;

			for (auto triIndex : triangleIndices)
			{
				finalTriangleData.push_back(triVertList[triIndex]);
			}

			auto totalTime = timer.elapsedNanoseconds();

			return finalTriangleData;
		}

	} // core
} // bw