#include "core/tTransform.h"


namespace bw
{
	namespace core
	{
		using namespace std;

		tTransform::tTransform()
			: operation(Operation::Mul)
		{
			for (int i = 0; i < 2; ++i)
			{
				operands[i] = OperandType::Constant;
				constants[i] = 0.0f;
				inputs[i] = InputType::InfluenceEyeDistance;
			}
		}

		tTransform::tTransform(
			OperandType operand0,
			OperandType operand1,
			float constant0,
			float constant1,
			InputType input0,
			InputType input1,
			Operation _operation)
		{
			operands[0] = operand0;
			operands[1] = operand1;
			constants[0] = constant0;
			constants[1] = constant1;
			inputs[0] = input0;
			inputs[1] = input1;
			operation = _operation;
		}

		tTransform::tTransform(tTransform const& other)
		{
			copyFrom(other);
		}

		tTransform& tTransform::operator=(tTransform const& other)
		{
			copyFrom(other);
			return *this;
		}

		void tTransform::copyFrom(tTransform const& other)
		{
			for (int i = 0; i < 2; ++i)
			{
				operands[i] = other.operands[i];
				constants[i] = other.constants[i];
				inputs[i] = other.inputs[i];
			}

			operation = other.operation;
		}

		tTransform tTransform::makeConstant(float value)
		{
			return tTransform(
				OperandType::Constant,
				OperandType::Constant,
				value,
				value,
				InputType::InfluenceEyeDistance,
				InputType::InfluenceEyeDistance,
				Operation::Mul
			);
		}

		tTransform tTransform::makeZero()
		{
			return makeConstant(0.0f);
		}

		tTransform tTransform::makeOne()
		{
			return makeConstant(1.0f);
		}

		tTransform tTransform::makePassthroughPrevious()
		{
			return tTransform(
				OperandType::TransformOutput,
				OperandType::Constant,
				0.0f,
				1.0f,
				InputType::InfluenceEyeDistance,
				InputType::InfluenceEyeDistance,
				Operation::Mul
			);
		}

		tTransform tTransform::makePassthroughInput(InputType inputType)
		{
			return tTransform(
				OperandType::Input,
				OperandType::Constant,
				0.0f,
				1.0f,
				inputType,
				InputType::InfluenceEyeDistance,
				Operation::Mul
			);
		}

	} // core
} // bw