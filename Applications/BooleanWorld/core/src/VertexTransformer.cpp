#include "core/VertexTransformer.h"
#include "core/CoreException.h"


namespace bw
{
	namespace core
	{
		using namespace std;

		uint32_t VertexTransformer::msIdGenerator = 0;

		VertexTransformer::VertexTransformer(VertexTransformer* parent)
			: mId(msIdGenerator++)
			, mParent(parent)
			, mOrigin(wp::Vector2::ZERO)
			, mFollowOrbitAngle(false)
		{
			for (int i = 0; i < (int)Key::COUNT; ++i)
			{
				mT[i].value = 0.0f;
				mT[i].loop = LoopBehaviour::Stop;
				mT[i].dir = 1;
			}

			// Set default flows
			mTransformFlows[(int)InputType::InfluenceEyeDistance] = {tTransform::makePassthroughInput(InputType::InfluenceEyeDistance)};
			mTransformFlows[(int)InputType::InfluenceEyeAngle] = { tTransform::makePassthroughInput(InputType::InfluenceEyeAngle) };
			mTransformFlows[(int)InputType::PlayerAngle] = { tTransform::makePassthroughInput(InputType::PlayerAngle) };

			mAnimationInterpolators[(int)Key::Scale].setPoints({ { 0.0f, 1.0f}, {1.0f, 1.0f} });
			mAnimationInterpolators[(int)Key::Angle].setPoints({ { 0.0f, 1.0f}, {0.0f, 1.0f} });
			mAnimationInterpolators[(int)Key::OrbitAngle].setPoints({ { 0.0f, 1.0f}, {0.0f, 1.0f} });
			mAnimationInterpolators[(int)Key::OrbitDistance].setPoints({ { 0.0f, 1.0f}, {0.0f, 1.0f} });
			
			mAnimationInterpolators[(int)Key::Scale].setScale({ 0.0f, 10.0f }, { 1.0f, 1000.0f });
			mAnimationInterpolators[(int)Key::Angle].setScale({ 0.0f, 10.0f }, { 1.0f, 360.0f });
			mAnimationInterpolators[(int)Key::OrbitAngle].setScale({ 0.0f, 10.0f }, { 1.0f, 360.0f });
			mAnimationInterpolators[(int)Key::OrbitDistance].setScale({ 0.0f, 10.0f }, { 1.0f, 1000.0f });
		}

		VertexTransformer::VertexTransformer(VertexTransformer const& other)
		{
			copyFrom(other);
		}

		VertexTransformer& VertexTransformer::operator=(VertexTransformer const& other)
		{
			copyFrom(other);
			return *this;
		}

		void VertexTransformer::setParent(VertexTransformer* parent)
		{
			mParent = parent;
		}

		bool VertexTransformer::childrenModified() const
		{
			for (int i = 0; i < (int)Key::COUNT; ++i)
			{
				if (mAnimationInterpolators[i].isModified())
				{
					return true;
				}
			}

			return false;
		}

		wp::Vector2 VertexTransformer::calculateOffset() const
		{
			auto offset = mOrigin;

			if (mParent)
			{
				offset += mParent->calculateOffset();
			}

			return offset;
		}

		void VertexTransformer::copyFrom(VertexTransformer const& other)
		{
			mId = other.mId;
			mParent = other.mParent;
			mOrigin = other.mOrigin;

			mFollowOrbitAngle = other.mFollowOrbitAngle;

			for (int i = 0; i < (int)Key::COUNT; ++i)
			{
				mT[i] = other.mT[i];
				mAnimationInterpolators[i] = other.mAnimationInterpolators[i];
				mTransformFlows[i] = other.mTransformFlows[i];
			}

			mEntityInfluenceDistance = other.mEntityInfluenceDistance;
			mEntityInfluenceAngle = other.mEntityInfluenceAngle;
			mEntityAngle = other.mEntityAngle;
		}

		void VertexTransformer::serializeImpl(shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			serializer->writeInt32("id", (int32_t)mId);
			serializer->writeInt32("parentId", mParent ? (int32_t)mParent->mId : -1);

			serializer->writeVector2("origin", mOrigin);

			serializer->beginMap("scale");
			{
				mAnimationInterpolators[(int)Key::Scale].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("angle");
			{
				mAnimationInterpolators[(int)Key::Angle].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("orbitAngle");
			{
				mAnimationInterpolators[(int)Key::OrbitAngle].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("orbitDistance");
			{
				mAnimationInterpolators[(int)Key::OrbitDistance].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->writeBool("followOrbitAngle", mFollowOrbitAngle);

			serializer->beginArray("t");
			{
				for (int i = 0; i < (int)Key::COUNT; ++i)
				{
					serializer->beginMap("tValue");
					{
						serializer->writeFloat("value", mT[i].value);
						serializer->writeUint32("loop", (uint32_t)mT[i].loop);
						serializer->writeInt32("dir", mT[i].dir);

						serializer->endMap();
					}
				}

				serializer->endArray();
			}

			serializer->beginArray("tTransformFlows");
			{
				for (int i = 0; i < (int)Key::COUNT; ++i)
				{
					auto const& ttf = mTransformFlows[i];
					serializer->beginMap("tTransformFlow");
					serializer->beginArray("tTransformFlow");
					{
						for (auto const& tt : ttf)
						{
							serializer->beginMap("tTransform");
							{
								serializer->writeUint32("operand0", (uint32_t)tt.operands[0]);
								serializer->writeUint32("operand1", (uint32_t)tt.operands[1]);
								serializer->writeFloat("constant0", tt.constants[0]);
								serializer->writeFloat("constant1", tt.constants[1]);
								serializer->writeUint32("input0", (uint32_t)tt.inputs[0]);
								serializer->writeUint32("input1", (uint32_t)tt.inputs[1]);
								serializer->writeUint32("operation", (uint32_t)tt.operation);

								serializer->endMap();
							}
						}

						serializer->endArray();
						serializer->endMap();
					}
				}

				serializer->endArray();
			}
		}

		bool VertexTransformer::deserializeImpl(shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			uint32_t id = serializer->readUint32("id");
			uint32_t parentId = (uint32_t)serializer->readInt32("parentId");
			wp::Vector2 origin = serializer->readVector2("origin");

			// Update parent map to glue together later
			workData.vtIdToVtMap[id] = this;
			workData.vtIdToParentMap[id] = parentId;

			array<Interpolator<float>, (int)Key::COUNT> animationInterpolators;
			float followOrbitAngle;

			vector<TValue> tValues;
			vector<tTransformFlow> tTransformFlows;

			try
			{
				serializer->beginMap("scale");
				{
					if (!animationInterpolators[(int)Key::Scale].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&animationInterpolators[(int)Key::Scale], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("angle");
				{
					if (!animationInterpolators[(int)Key::Angle].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&animationInterpolators[(int)Key::Angle], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("orbitAngle");
				{
					if (!animationInterpolators[(int)Key::OrbitAngle].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&animationInterpolators[(int)Key::OrbitAngle], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("orbitDistance");
				{
					if (!animationInterpolators[(int)Key::OrbitDistance].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&animationInterpolators[(int)Key::OrbitDistance], true, true);
						return false;
					}

					serializer->endMap();
				}

				followOrbitAngle = serializer->readBool("followOrbitAngle");

				serializer->beginArray("t");
				{
					while (serializer->nextArrayItem())
					{
						serializer->beginMap("tValue");
						{
							float value = serializer->readFloat("value");
							LoopBehaviour loop = (LoopBehaviour)serializer->readUint32("loop");
							int32_t dir = serializer->readInt32("dir");

							tValues.push_back({ value, loop, dir });

							serializer->endMap();
						}
					}

					serializer->endArray();
				}

				if (tValues.size() > (int)Key::COUNT)
				{
					throw SerializationException(format("More than {} t-points found in VertexTransformer", (int)Key::COUNT));
				}

				serializer->beginArray("tTransformFlows");
				{
					while (serializer->nextArrayItem())
					{
						serializer->beginMap("tTransformFlow");
						{
							serializer->beginArray("tTransformFlow");
							{
								tTransformFlow tTransformFlow;

								while (serializer->nextArrayItem())
								{
									serializer->beginMap("tTransform");
									{
										tTransform tt;

										tt.operands[0] = (tTransform::OperandType)serializer->readUint32("operand0");
										tt.operands[1] = (tTransform::OperandType)serializer->readUint32("operand1");
										tt.constants[0] = serializer->readFloat("constant0");
										tt.constants[1] = serializer->readFloat("constant1");
										tt.inputs[0] = (InputType)serializer->readUint32("input0");
										tt.inputs[1] = (InputType)serializer->readUint32("input1");
										tt.operation = (tTransform::Operation)serializer->readUint32("operation");

										tTransformFlow.push_back(tt);
										serializer->endMap();
									}
								}

								tTransformFlows.push_back(tTransformFlow);
							}

							serializer->endArray();
							serializer->endMap();
						}
					}

					serializer->endArray();
				}

				if (tTransformFlows.size() > (int)Key::COUNT)
				{
					throw SerializationException(format("More than {} tTransformFlows found in VertexTransformer", (int)Key::COUNT));
				}
			}
			catch (exception& e)
			{
				addDeserializationError(e.what());
				return false;
			}

			// Commit
			mId = id;
			mParent = nullptr;  // to be fixed up later in the deserialization process 
			// once all VertexTransformers are created
			mOrigin = origin;
			mFollowOrbitAngle = followOrbitAngle;

			for (int i = 0; i < (int)Key::COUNT; ++i)
			{
				mT[i] = tValues[i];
				mAnimationInterpolators[i] = animationInterpolators[i];
				mTransformFlows[i] = tTransformFlows[i];
			}

			mEntityInfluenceDistance = 1.0f;
			mEntityInfluenceAngle = 0.0f;
			mEntityAngle = 0.0f;

			return true;
		}

		void VertexTransformer::deltaT(Key key, float delta)
		{
			auto& t = mT[(int)key];

			auto newValue = t.value + t.dir * delta;

			switch (t.loop)
			{
			case LoopBehaviour::Stop:
				newValue = min(max(0.0f, newValue), 1.0f);
				break;

			case LoopBehaviour::PingPong:
				if (newValue > 1.0f)
				{
					newValue = 2.0f - newValue;
					t.dir = -t.dir;
				}
				else if (newValue < 0.0f)
				{
					newValue = -newValue;
					t.dir = -t.dir;
				}
				break;

			case LoopBehaviour::Loop:
				while (newValue > 1.0f)
				{
					newValue -= 1.0f;
				}
				while (newValue < 0.0f)
				{
					newValue += 1.0f;
				}
				break;
			}

			t.value = newValue;
		}

		void VertexTransformer::setT(Key key, float value)
		{
			mT[(int)key].value = min(max(0.0f, value), 1.0f);
		}

		float VertexTransformer::getT(Key key) const
		{
			return mT[(int)key].value;
		}

		void VertexTransformer::setLoopBehaviour(Key key, LoopBehaviour behaviour)
		{
			mT[(int)key].loop = behaviour;
		}

		LoopBehaviour VertexTransformer::getLoopBehaviour(Key key) const
		{
			return mT[(int)key].loop;
		}

		void VertexTransformer::setOrigin(wp::Vector2 const& origin)
		{
			mOrigin = origin;
		}

		wp::Vector2 const& VertexTransformer::getOrigin() const
		{
			return mOrigin;
		}

		Interpolator<float> const& VertexTransformer::getInterpolator(Key key) const
		{
			return mAnimationInterpolators[(int)key];
		}

		Interpolator<float>& VertexTransformer::getInterpolator(Key key)
		{
			return mAnimationInterpolators[(int)key];
		}

		void VertexTransformer::setInterpolatorValues(Key key, vector<pair<float, float>> const& values)
		{
			mAnimationInterpolators[(int)key].setPoints(values);
		}

		vector<Interpolator<float>::Point> const& VertexTransformer::getInterpolatorValues(Key key) const
		{
			return mAnimationInterpolators[(int)key].getPoints();
		}

		void VertexTransformer::updateInterpolatorValue(Key key, uint32_t index, float time, float const& value)
		{
			mAnimationInterpolators[(int)key].updatePoint(index, time, value);
		}

		void VertexTransformer::addInterpolatorValue(Key key, float time, float value)
		{
			mAnimationInterpolators[(int)key].addPoint(time, value);
		}

		void VertexTransformer::removeInterpolatorValue(Key key, uint32_t index)
		{
			mAnimationInterpolators[(int)key].removePoint(index);
		}

		float VertexTransformer::getInterpolatorValue(Key key) const
		{
			return mAnimationInterpolators[(int)key].getValue(mT[(int)key].value);
		}

		float VertexTransformer::getInterpolatorValue(Key key, float t) const
		{
			return mAnimationInterpolators[(int)key].getValue(t);
		}

		void VertexTransformer::getInterpolatorScale(Key key, wp::Vector2* scaleMin, wp::Vector2* scaleMax)
		{
			mAnimationInterpolators[(int)key].getScale(scaleMin, scaleMax);
		}

		void VertexTransformer::setInterpolatorEasing(Key key, uint32_t segment, Easing easing)
		{
			mAnimationInterpolators[(int)key].setEasing(segment, easing);
		}

		vector<Interpolator<float>::Segment> const& VertexTransformer::getInterpolatorSegments(Key key) const
		{
			return mAnimationInterpolators[(int)key].getSegments();
		}

		vector<vector<Interpolator<float>::Point>> VertexTransformer::renderInterpolator(Key key, float resolution) const
		{
			return mAnimationInterpolators[(int)key].render(resolution);
		}

		void VertexTransformer::setScaleTransformFlow(tTransformFlow const& flow)
		{
			mTransformFlows[(int)Key::Scale] = flow;
		}

		tTransformFlow& VertexTransformer::getScaleTransformFlow()
		{
			return mTransformFlows[(int)Key::Scale];
		}

		void VertexTransformer::setAngleTransformFlow(tTransformFlow const& flow)
		{
			mTransformFlows[(int)Key::Angle] = flow;
		}

		tTransformFlow& VertexTransformer::getAngleTransformFlow()
		{
			return mTransformFlows[(int)Key::Angle];
		}

		void VertexTransformer::setOrbitAngleTransformFlow(tTransformFlow const& flow)
		{
			mTransformFlows[(int)Key::OrbitAngle] = flow;
		}

		tTransformFlow& VertexTransformer::getOrbitAngleTransformFlow()
		{
			return mTransformFlows[(int)Key::OrbitAngle];
		}

		void VertexTransformer::setOrbitDistanceTransformFlow(tTransformFlow const& flow)
		{
			mTransformFlows[(int)Key::OrbitDistance] = flow;
		}

		tTransformFlow& VertexTransformer::getOrbitDistanceTransformFlow()
		{
			return mTransformFlows[(int)Key::OrbitDistance];
		}

		void VertexTransformer::setFollowOrbitAngle(bool follow)
		{
			mFollowOrbitAngle = follow;
		}

		bool VertexTransformer::getFollowOrbitAngle() const
		{
			return mFollowOrbitAngle;
		}

		void VertexTransformer::setInputs(float entityInfluenceDistance, float entityInfluenceAngle, float entityAngle)
		{
			mEntityInfluenceDistance = entityInfluenceDistance;
			mEntityInfluenceAngle = entityInfluenceAngle;
			mEntityAngle = entityAngle;
		}

		float VertexTransformer::transformT(Key key) const
		{
			float value = mT[(int)key].value;
			auto const& ttf = mTransformFlows[(int)key];

			for (auto const& tt : ttf)
			{
				// Transform value
				float operands[2];

				for (int i = 0; i < 2; ++i)
				{
					switch (tt.operands[i])
					{
					case tTransform::OperandType::Input:
						switch (tt.inputs[i])
						{
						case InputType::InfluenceEyeDistance:
							operands[i] = mEntityInfluenceDistance;
							break;

						case InputType::InfluenceEyeAngle:
							operands[i] = mEntityInfluenceAngle;
							break;

						case InputType::PlayerAngle:
							operands[i] = mEntityAngle;
							break;
						}
						break;

					case tTransform::OperandType::Constant:
						operands[i] = tt.constants[i];
						break;

					case tTransform::OperandType::TransformOutput:
						operands[i] = value;
						break;
					}
				}

				switch (tt.operation)
				{
				case tTransform::Operation::Add:
					value = clamp(operands[0] + operands[1], 0.0f, 1.0f);
					break;
				case tTransform::Operation::Mul:
					value = clamp(operands[0] * operands[1], 0.0f, 1.0f);
					break;
				case tTransform::Operation::AbsDiff:
					value = clamp(fabs(operands[0] - operands[1]), 0.0f, 1.0f);
					break;
				case tTransform::Operation::Min:
					value = clamp(min(operands[0], operands[1]), 0.0f, 1.0f);
					break;
				case tTransform::Operation::Max:
					value = clamp(max(operands[0], operands[1]), 0.0f, 1.0f);
					break;
				case tTransform::Operation::Avg:
					value = clamp((operands[0] + operands[1]) * 0.5f, 0.0f, 1.0f);
					break;
				case tTransform::Operation::Less:
					value = operands[0] < operands[1] ? 1.0f : 0.0f;
					break;
				case tTransform::Operation::Greater:
					value = operands[0] > operands[1] ? 1.0f : 0.0f;
					break;
				case tTransform::Operation::LessEq:
					value = operands[0] <= operands[1] ? 1.0f : 0.0f;
					break;
				case tTransform::Operation::GreaterEq:
					value = operands[0] >= operands[1] ? 1.0f : 0.0f;
					break;
				}
			}

			return value;
		}

		wp::Vector2 VertexTransformer::transform(wp::Vector2 const& vertex) const
		{
			auto vt = vertex;

			// Get orbit angle first as we're using it twice
			auto orbitAngle = mAnimationInterpolators[(int)Key::OrbitAngle].getValue(transformT(Key::OrbitAngle));

			// Scale
			vt *= mAnimationInterpolators[(int)Key::Scale].getValue(transformT(Key::Scale)) * 0.5f;

			// Local rotation
			auto localAngle = mAnimationInterpolators[(int)Key::Angle].getValue(transformT(Key::Angle));

			if (mFollowOrbitAngle)
			{
				localAngle += orbitAngle;
			}

			vt.rotateAnticlockwise(localAngle);

			// Translate
			vt += calculateOffset();

			// Orbit
			auto orbitDir = wp::Vector2::UNIT_Y.rotatedAnticlockwiseCopy(orbitAngle);

			auto orbitDist = mAnimationInterpolators[(int)Key::OrbitDistance].getValue(transformT(Key::OrbitDistance));
			vt += orbitDir * orbitDist;

			return vt;
		}

	} // core
} // bw