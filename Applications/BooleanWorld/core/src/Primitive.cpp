#include <array>

#include <mapbox/earcut.hpp>

#include "core/Primitive.h"

namespace bw
{
	namespace core
	{

		using namespace std;

		Primitive::Primitive()
			: mId(~0u)
			, mOperation(Operation::Union)
			, mFillRule(FillRule::NonZero)
			, mPriority(0)
			, mCacheInvalid(true)
		{
		}

		Primitive::Primitive(Operation operation, FillRule fillType)
			: mId(~0u) 
			, mOperation(operation)
			, mFillRule(fillType)
			, mPriority(0)
			, mCacheInvalid(true)
		{
		}

		Primitive::Primitive(Primitive const& other)
		{
			copyFrom(other);
		}

		Primitive& Primitive::operator=(Primitive const& other)
		{
			copyFrom(other);
			return*this;
		}

		void Primitive::copyFrom(Primitive const& other)
		{
			VertexTransformerObject::copyFrom(other);

			mId = other.mId;
			mOperation = other.mOperation;
			mFillRule = other.mFillRule;
			mPriority = other.mPriority;
			mBounds = other.mBounds;
			mCacheInvalid = other.mCacheInvalid;
			mCachedVertices = other.mCachedVertices;
			mVertices = other.mVertices;
		}

		void Primitive::invalidatePostTransform()
		{
			invalidate();
		}

		void Primitive::invalidate()
		{
			mCacheInvalid = true;
			calculateBounds();
		}

		void Primitive::calculateBounds()
		{
			wp::Vector2 minExtent, maxExtent;
			auto vertices = generateTransformedVertices(&minExtent, &maxExtent);

			// Inflate very slightly
			minExtent -= 1.0f;
			maxExtent += 1.0f;

			mBounds.setPosition(minExtent);
			mBounds.setSize(maxExtent - minExtent);
		}

		string Primitive::getName() const
		{
			return getType();
		}

		uint32_t Primitive::getId() const
		{
			return mId;
		}

		void Primitive::setOperation(Operation operation)
		{
			mOperation = operation;
		}

		Primitive::Operation Primitive::getOperation() const
		{
			return mOperation;
		}

		void Primitive::setFillRule(FillRule fillRule)
		{
			mFillRule = fillRule;
		}

		Primitive::FillRule Primitive::getFillRule() const
		{
			return mFillRule;
		}

		void Primitive::setPriority(uint8_t priority)
		{
			mPriority = priority;
		}

		uint8_t Primitive::getPriority() const
		{
			return mPriority;
		}

		wp::BoundingBox const& Primitive::getBounds() const
		{
			return mBounds;
		}

		void Primitive::setVertices(vector<PrimitiveVertex> vertices)
		{
			mVertices = vertices;
			invalidate();
		}

		bool Primitive::childrenModified() const
		{
			return false;
		}

		void Primitive::serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			VertexTransformerObject::serializeImpl(serializer, workData);

			serializer->writeUint32("id", mId);
			serializer->writeUint32("operation", (uint32_t)mOperation);
			serializer->writeUint32("fillRule", (uint32_t)mFillRule);
			serializer->writeInt32("priority", (int32_t)mPriority);

			serializer->beginArray("vertices");
			{
				for (auto const& vertex : mVertices)
				{
					serializer->beginMap("vertex");
					{
						serializer->writeVector2("p", vertex.p);
						serializer->writeInt64("z", vertex.z);

						serializer->endMap();
					}
				}

				serializer->endArray();
			}
		}

		bool Primitive::deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			if (!VertexTransformerObject::deserializeImpl(serializer, workData))
			{
				return false;
			}

			uint32_t id;
			Operation operation;
			FillRule fillRule;
			uint8_t priority;
			std::vector<PrimitiveVertex> vertices;

			try
			{
				id = serializer->readUint32("id");
				operation = (Operation)serializer->readUint32("operation");
				fillRule = (FillRule)serializer->readUint32("fillRule");
				priority = (uint8_t)serializer->readInt32("priority");

				serializer->beginArray("vertices");
				{
					while (serializer->nextArrayItem())
					{
						serializer->beginMap("vertex");
						{
							wp::Vector2 p = serializer->readVector2("p");
							int64_t z = serializer->readInt64("z");

							vertices.push_back({ p, z });

							serializer->endMap();
						}
					}

					serializer->endArray();
				}
			}
			catch (exception& e)
			{
				addDeserializationError(e.what());
				return false;
			}

			// Commit
			mId = id;
			mOperation = operation;
			mFillRule = fillRule;
			mPriority = priority;
			mVertices = vertices;
			mCacheInvalid = true;
			calculateBounds();

			return true;
		}

		void Primitive::generateVertices()
		{
			setVertices(generateVerticesImpl());
		}

		vector<PrimitiveVertex> Primitive::generateTransformedVertices(wp::Vector2* minExtent, wp::Vector2* maxExtent) const
		{
			if (minExtent)
			{
				minExtent->set(1e10f, 1e10f);
			}
			if (maxExtent)
			{
				maxExtent->set(-1e10f, -1e10f);
			}

			auto numVertices = getNumVertices();
			vector<PrimitiveVertex> vertices(numVertices);

			for (uint32_t i = 0; i < numVertices; ++i)
			{
				vertices[i].p = transformVertex(mVertices[i].p);
				vertices[i].z = mVertices[i].z;

				if (minExtent)
				{
					if (vertices[i].p.x < minExtent->x)
					{
						minExtent->x = vertices[i].p.x;
					}
					if (vertices[i].p.y < minExtent->y)
					{
						minExtent->y = vertices[i].p.y;
					}
				}
				if (maxExtent)
				{
					if (vertices[i].p.x > maxExtent->x)
					{
						maxExtent->x = vertices[i].p.x;
					}
					if (vertices[i].p.y > maxExtent->y)
					{
						maxExtent->y = vertices[i].p.y;
					}
				}
			}

			return vertices;
		}

		vector<PrimitiveVertex> Primitive::getVertices() const
		{
			if (mCacheInvalid)
			{
				mCachedVertices = generateTransformedVertices();
				mCacheInvalid = false;
			}

			return mCachedVertices;
		}

		uint32_t Primitive::getNumVertices() const
		{
			return (uint32_t)mVertices.size();
		}

		Triangulation Primitive::triangulate() const
		{
			using Point = array<float, 2>;

			auto vertices = getVertices();

			// Read in points to required format for triangulation
			vector<Point> points(vertices.size());

			for (uint32_t i = 0; i < vertices.size(); ++i)
			{
				points[i] = { vertices[i].p.x, vertices[i].p.y };
			}

			vector<vector<Point>> polygon = { points };
			auto triangleIndices = mapbox::earcut<uint32_t>(polygon);

			Triangulation t;

			t.bounds = getBounds();

			for (uint32_t i = 0; i < (uint32_t)triangleIndices.size(); i += 3)
			{
				t.tris.push_back({
					vertices[triangleIndices[i + 0]].p,
					vertices[triangleIndices[i + 1]].p,
					vertices[triangleIndices[i + 2]].p
				});
			}

			return t;
		}

	} // core
} // bw