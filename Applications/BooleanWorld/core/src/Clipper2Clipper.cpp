#include <queue>

#define USINGZ

#include <clipper2/clipper.h>

#include "core/Clipper2Clipper.h"

#ifdef USINGZ
#define MAKE_POINT(x, y, z) Clipper2Lib::PointD(x, y, z)
#else
#define MAKE_POINT(x, y) Clipper2Lib::PointD(x, y)
#endif


namespace bw
{
	namespace core
	{

		using namespace std;

		struct Clipper2Polygon
		{
			bool isHole;
			Clipper2Lib::PathD path;
		};

		void traverseHole(Clipper2Lib::PolyPathD const* polyPath, vector<Clipper2Polygon>& outPaths);

		void traverseNonHole(Clipper2Lib::PolyPathD const* polyPath, vector<Clipper2Polygon>& outPaths)
		{
			// Traverse child holes breadth-first
			for (auto it = polyPath->begin(); it != polyPath->end(); ++it)
			{
				outPaths.push_back({ true, it->get()->Polygon() });
			}

			for (auto it = polyPath->begin(); it != polyPath->end(); ++it)
			{
				traverseHole(it->get(), outPaths);
			}
		}

		void traverseHole(Clipper2Lib::PolyPathD const* polyPath, vector<Clipper2Polygon>& outPaths)
		{
			// Traverse child non-holes depth-first
			for (auto it = polyPath->begin(); it != polyPath->end(); ++it)
			{
				auto const* c = it->get();
				outPaths.push_back({ false, c->Polygon() });

				traverseNonHole(c, outPaths);
			}
		}

		void traverseTree(Clipper2Lib::PolyTreeD const* polyTree, vector<Clipper2Polygon>& outPaths)
		{
			// Traverse child non-holes depth-first
			for (auto it = polyTree->begin(); it != polyTree->end(); ++it)
			{
				auto const* c = it->get();
				outPaths.push_back({ false, c->Polygon() });

				traverseNonHole(c, outPaths);
			}
		}

		void vertexCallbackZ(Clipper2Lib::PointD const& v00, Clipper2Lib::PointD const& v01,
			Clipper2Lib::PointD const& v10, Clipper2Lib::PointD const& v11, Clipper2Lib::PointD& p)
		{
			p.z = 0;
		}

		vector<Clipper2Polygon> executeClip(Clipper2Lib::ClipperD& clipper, Clipper2Lib::ClipType op, Clipper2Lib::FillRule fillRule)
		{
			Clipper2Lib::PolyTreeD polytree;

			clipper.SetZCallback(vertexCallbackZ);

			clipper.Execute(op, fillRule, polytree);

			// Go over polytree.  Root will be a collection of filled (non-hole) polygons.  These
			// need to be iterated over depth-first, adding all their holes.  Each added hole
			// should be added to a list, and afterwards, iterate over this list, treating the
			// hole as a root.
			vector<Clipper2Polygon> outPaths;
			traverseTree(&polytree, outPaths);

			return outPaths;
		}

		/*
		vector<Clipper2Polygon> clipAgainstView(Clipper2Lib::PathsD const& paths, wp::BoundingBox const& extents)
		{
			wp::Vector2 minExtent, maxExtent;

			extents.getExtents(minExtent, maxExtent);

			// Pad so we definitely capture everything on screen
			minExtent.x -= 1;
			minExtent.y -= 1;
			maxExtent.x += 1;
			maxExtent.y += 1;

			Clipper2Lib::PathD view = {
				{ (double)minExtent.x, (double)minExtent.y },
				{ (double)maxExtent.x, (double)minExtent.y },
				{ (double)maxExtent.x, (double)maxExtent.y },
				{ (double)minExtent.x, (double)maxExtent.y }
			};

			// TODO: use Clipper2's RectClip - quicker.
			//Clipper2Lib::PolyTreeD polytree;
			//Clipper2Lib::ClipperD clipper;

			//clipper.AddSubject(paths);
			//clipper.AddSubject({ view });

			//return executeClip(clipper, Clipper2Lib::ClipType::Intersection, Clipper2Lib::FillRule::EvenOdd);
		}
		*/

		Clipper2Lib::PathD getPath(Primitive const* primitive)
		{
			auto vertices = primitive->getVertices();

			auto path = vector<Clipper2Lib::PointD>();

			for (auto const& vertex : vertices)
			{
				path.push_back(MAKE_POINT((double)vertex.p.x, (double)vertex.p.y, vertex.z));
			}

			return path;
		}

		vector<ClippedPolygon> Clipper2Clipper::clip(vector<Primitive*> const& primitives, wp::BoundingBox const& extents)
		{
			vector<ClippedPolygon> result;

			Clipper2Lib::PathsD paths = { getPath(primitives[0]) };
	
			vector<Clipper2Polygon> clippedPolygons = {
				{ false, paths[0] }
			};

			// Apply each primitive to the current result in turn
			for (uint32_t i = 1; i < (uint32_t)primitives.size(); ++i)
			{
				auto const* primitive = primitives[i];

				auto path2 = getPath(primitive);

				Clipper2Lib::PolyTreeD polytree;
				Clipper2Lib::ClipperD clipper;

				clipper.AddSubject(paths);
				clipper.AddClip({ path2 });

				Clipper2Lib::ClipType clipType;
				switch (primitive->getOperation())
				{
				case Primitive::Operation::Union:
					clipType = Clipper2Lib::ClipType::Union;
					break;

				case Primitive::Operation::Intersection:
					clipType = Clipper2Lib::ClipType::Intersection;
					break;

				case Primitive::Operation::Difference:
					clipType = Clipper2Lib::ClipType::Difference;
					break;

				case Primitive::Operation::XOR:
					clipType = Clipper2Lib::ClipType::Xor;
					break;

				default:
					throw exception("Unknown boolean operation");
				}

				Clipper2Lib::FillRule fillRule;
				switch (primitive->getFillRule())
				{
				case Primitive::FillRule::EvenOdd:
					fillRule = Clipper2Lib::FillRule::EvenOdd;
					break;

				case Primitive::FillRule::NonZero:
					fillRule = Clipper2Lib::FillRule::NonZero;
					break;

				default:
					throw exception("Unknown fill rule");
				}

				clippedPolygons = executeClip(clipper, clipType, fillRule);
		
				paths.clear();
				for (auto const& clippedPoly : clippedPolygons)
				{
					paths.push_back(clippedPoly.path);
				}
			}

			// Clip against world-quad
			//paths = clipAgainstView(paths, extents);

			for (auto const& clippedPolygon : clippedPolygons)
			{
				vector<wp::Vector2> list;

				wp::Vector2 boundsMin{ 1e10f, 1e10f }, boundsMax{ -1e10f, -1e10f };
				for (auto const& point : clippedPolygon.path)
				{
					auto x = (float)point.x;
					auto y = (float)point.y;

					if (x > boundsMax.x)
					{
						boundsMax.x = x;
					}
					if (y > boundsMax.y)
					{
						boundsMax.y = y;
					}
					if (x < boundsMin.x)
					{
						boundsMin.x = x;
					}
					if (y < boundsMin.y)
					{
						boundsMin.y = y;
					}

					list.push_back(wp::Vector2(x, y));
				}

				result.push_back({
					clippedPolygon.isHole,
					list,
					wp::BoundingBox(boundsMin, boundsMax - boundsMin)
				});
			}

			return result;
		}

	} // core
} // bw