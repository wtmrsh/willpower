#include "core/VertexTransformerObject.h"


namespace bw
{
	namespace core
	{
		using namespace std;

		VertexTransformerObject::VertexTransformerObject()
			: mInfluenceOriginOffset(wp::Vector2::ZERO)
		{
			// Create default input interpolator frames
			for (int i = 0; i < (int)InputType::COUNT; ++i)
			{
				mT[i].value = 0.0f;
				mT[i].loop = LoopBehaviour::Stop;
				mT[i].dir = 1;
			}

			mInputInterpolators[(int)InputType::InfluenceEyeDistance].setScale({ 0.0f, 0.0f }, { 500.0f, 1.0f });
			mInputInterpolators[(int)InputType::InfluenceEyeDistance].setPoints({ { 0.0f, 1.0f }, { 500.0f, 0.0f } });

			mInputInterpolators[(int)InputType::InfluenceEyeAngle].setScale({ 0.0f, 0.0f }, { 360.0f, 1.0f });
			mInputInterpolators[(int)InputType::InfluenceEyeAngle].setPoints({ { 0.0f, 1.0f }, { 360.0f, 0.0f } });

			mInputInterpolators[(int)InputType::PlayerAngle].setScale({ 0.0f, 0.0f }, { 360.0f, 1.0f });
			mInputInterpolators[(int)InputType::PlayerAngle].setPoints({ { 0.0f, 1.0f }, { 360.0f, 0.0f } });
		}

		VertexTransformerObject::VertexTransformerObject(VertexTransformerObject const& other)
		{
			copyFrom(other);
		}

		VertexTransformerObject& VertexTransformerObject::operator=(VertexTransformerObject const& other)
		{
			copyFrom(other);
			return *this;
		}

		bool VertexTransformerObject::childrenModified() const
		{
			return mVertexTransformer.isModified();
		}

		void VertexTransformerObject::copyFrom(VertexTransformerObject const& other)
		{
			mVertexTransformer = other.mVertexTransformer;

			for (int i = 0; i < (int)InputType::COUNT; ++i)
			{
				mInputInterpolators[i] = other.mInputInterpolators[i];
				mT[i] = other.mT[i];
			}

			mInfluenceOriginOffset = other.mInfluenceOriginOffset;
		}

		void VertexTransformerObject::serializeImpl(shared_ptr<Serializer> serializer, SerializationWorkData& workData) const
		{
			serializer->beginMap("vertexTransformer");
			{
				mVertexTransformer.serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("influenceEyeDistance");
			{
				mInputInterpolators[0].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("influenceEyeAngle");
			{
				mInputInterpolators[1].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginMap("globalAngle");
			{
				mInputInterpolators[2].serialize(serializer, workData);
				serializer->endMap();
			}

			serializer->beginArray("t");
			{
				for (int i = 0; i < (int)InputType::COUNT; ++i)
				{
					serializer->beginMap("tValue");
					{
						serializer->writeFloat("value", mT[i].value);
						serializer->writeUint32("loop", (uint32_t)mT[i].loop);
						serializer->writeInt32("dir", mT[i].dir);

						serializer->endMap();
					}
				}

				serializer->endArray();
			}

			serializer->writeVector2("influenceOriginOffset", mInfluenceOriginOffset);
		}

		bool VertexTransformerObject::deserializeImpl(shared_ptr<Serializer> serializer, SerializationWorkData& workData)
		{
			VertexTransformer vertexTransformer;
			array<Interpolator<float>, 3> inputInterpolators;
			array<TValue, 3> tValues;
			wp::Vector2 influenceOriginOffset;

			try
			{
				serializer->beginMap("vertexTransformer");
				{
					if (!vertexTransformer.deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&vertexTransformer, true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("influenceEyeDistance");
				{
					if (!inputInterpolators[0].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&inputInterpolators[0], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("influenceEyeAngle");
				{
					if (!inputInterpolators[1].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&inputInterpolators[1], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginMap("globalAngle");
				{
					if (!inputInterpolators[2].deserialize(serializer, workData))
					{
						copyErrorsAndWarnings(&inputInterpolators[2], true, true);
						return false;
					}

					serializer->endMap();
				}

				serializer->beginArray("t");
				{
					int i = 0;
					while (serializer->nextArrayItem())
					{
						serializer->beginMap("tValue");
						{
							float value = serializer->readFloat("value");
							LoopBehaviour loop = (LoopBehaviour)serializer->readUint32("loop");
							int32_t dir = serializer->readInt32("dir");

							tValues[i] = {value, loop, dir};

							serializer->endMap();
							i++;
						}
					}

					serializer->endArray();
				}

				influenceOriginOffset = serializer->readVector2("influenceOriginOffset");

			}
			catch (exception& e)
			{
				addDeserializationError(e.what());
				return false;
			}

			// Commit
			mVertexTransformer = vertexTransformer;
			mInputInterpolators = inputInterpolators;

			for (int i = 0; i < (int)InputType::COUNT; ++i)
			{
				mT[i] = tValues[i];
			}

			mInfluenceOriginOffset = influenceOriginOffset;

			return true;
		}

		VertexTransformer* VertexTransformerObject::getVertexTransformer()
		{
			return &mVertexTransformer;
		}

		wp::Vector2 VertexTransformerObject::transformVertex(wp::Vector2 const& v) const
		{
			return mVertexTransformer.transform(v);
		}

		void VertexTransformerObject::setTransformerParent(VertexTransformer* transformer)
		{
			mVertexTransformer.setParent(transformer);
			invalidatePostTransform();
		}

		void VertexTransformerObject::setT(InputType type, float value)
		{
			mT[(int)type].value = min(max(0.0f, value), 1.0f);
		}

		float VertexTransformerObject::getT(InputType type) const
		{
			return mT[(int)type].value;
		}

		void VertexTransformerObject::setOrigin(wp::Vector2 const& origin)
		{
			mVertexTransformer.setOrigin(origin);
			invalidatePostTransform();
		}

		wp::Vector2 const& VertexTransformerObject::getOrigin() const
		{
			return mVertexTransformer.getOrigin();
		}

		void VertexTransformerObject::setInfluenceOriginOffset(wp::Vector2 const& originOffset)
		{
			mInfluenceOriginOffset = originOffset;
		}

		wp::Vector2 const& VertexTransformerObject::getInfluenceOriginOffset() const
		{
			return mInfluenceOriginOffset;
		}

		Interpolator<float> const& VertexTransformerObject::getAnimationInterpolator(VertexTransformer::Key key) const
		{
			return mVertexTransformer.getInterpolator(key);
		}

		Interpolator<float>& VertexTransformerObject::getAnimationInterpolator(VertexTransformer::Key key)
		{
			return mVertexTransformer.getInterpolator(key);
		}

		Interpolator<float> const& VertexTransformerObject::getInputInterpolator(InputType type) const
		{
			return mInputInterpolators[(int)type];
		}

		Interpolator<float>& VertexTransformerObject::getInputInterpolator(InputType type)
		{
			return mInputInterpolators[(int)type];
		}

		void VertexTransformerObject::setAnimationValues(VertexTransformer::Key key, vector<pair<float, float>> const& values)
		{
			mVertexTransformer.setInterpolatorValues(key, values);
			invalidatePostTransform();
		}

		vector<Interpolator<float>::Point> const& VertexTransformerObject::getAnimationValues(VertexTransformer::Key key) const
		{
			return mVertexTransformer.getInterpolatorValues(key);
		}

		void VertexTransformerObject::updateAnimationValue(VertexTransformer::Key key, uint32_t index, float time, float const& value)
		{
			mVertexTransformer.updateInterpolatorValue(key, index, time, value);
			invalidatePostTransform();
		}

		void VertexTransformerObject::addAnimationValue(VertexTransformer::Key key, float time, float value)
		{
			mVertexTransformer.addInterpolatorValue(key, time, value);
			invalidatePostTransform();
		}

		void VertexTransformerObject::removeAnimationValue(VertexTransformer::Key key, uint32_t index)
		{
			mVertexTransformer.removeInterpolatorValue(key, index);
			invalidatePostTransform();
		}

		float VertexTransformerObject::getAnimationValue(VertexTransformer::Key key) const
		{
			return mVertexTransformer.getInterpolatorValue(key);
		}

		float VertexTransformerObject::getAnimationValue(VertexTransformer::Key key, float t) const
		{
			return mVertexTransformer.getInterpolatorValue(key, t);
		}

		void VertexTransformerObject::getAnimationScale(VertexTransformer::Key key, wp::Vector2* scaleMin, wp::Vector2* scaleMax)
		{
			mVertexTransformer.getInterpolatorScale(key, scaleMin, scaleMax);
		}

		void VertexTransformerObject::setAnimationEasing(VertexTransformer::Key key, uint32_t segment, Easing easing)
		{
			mVertexTransformer.setInterpolatorEasing(key, segment, easing);
			invalidatePostTransform();
		}

		vector<Interpolator<float>::Segment> const& VertexTransformerObject::getAnimationSegments(VertexTransformer::Key key) const
		{
			return mVertexTransformer.getInterpolatorSegments(key);
		}

		vector<vector<Interpolator<float>::Point>> VertexTransformerObject::renderAnimation(VertexTransformer::Key key, float resolution) const
		{
			return mVertexTransformer.renderInterpolator(key, resolution);
		}
		
		void VertexTransformerObject::setScaleTransformFlow(tTransformFlow const& flow)
		{
			mVertexTransformer.setScaleTransformFlow(flow);
			invalidatePostTransform();
		}

		tTransformFlow& VertexTransformerObject::getScaleTransformFlow()
		{
			return mVertexTransformer.getScaleTransformFlow();
		}

		void VertexTransformerObject::setAngleTransformFlow(tTransformFlow const& flow)
		{
			mVertexTransformer.setAngleTransformFlow(flow);
			invalidatePostTransform();
		}

		tTransformFlow& VertexTransformerObject::getAngleTransformFlow()
		{
			return mVertexTransformer.getOrbitAngleTransformFlow();
		}

		void VertexTransformerObject::setOrbitAngleTransformFlow(tTransformFlow const& flow)
		{
			mVertexTransformer.setOrbitAngleTransformFlow(flow);
			invalidatePostTransform();
		}

		tTransformFlow& VertexTransformerObject::getOrbitAngleTransformFlow()
		{
			return mVertexTransformer.getOrbitAngleTransformFlow();
		}

		void VertexTransformerObject::setOrbitDistanceTransformFlow(tTransformFlow const& flow)
		{
			mVertexTransformer.setOrbitDistanceTransformFlow(flow);
			invalidatePostTransform();
		}

		tTransformFlow& VertexTransformerObject::getOrbitDistanceTransformFlow()
		{
			return mVertexTransformer.getOrbitAngleTransformFlow();
		}

		void VertexTransformerObject::setFollowOrbitAngle(bool follow)
		{
			mVertexTransformer.setFollowOrbitAngle(follow);
			invalidatePostTransform();
		}

		bool VertexTransformerObject::getFollowOrbitAngle() const
		{
			return mVertexTransformer.getFollowOrbitAngle();
		}

		void VertexTransformerObject::deltaT(VertexTransformer::Key key, float delta)
		{
			mVertexTransformer.deltaT(key, delta);
			invalidatePostTransform();
		}

		void VertexTransformerObject::setT(VertexTransformer::Key key, float value)
		{
			mVertexTransformer.setT(key, value);
			invalidatePostTransform();
		}

		float VertexTransformerObject::getT(VertexTransformer::Key key) const
		{
			return mVertexTransformer.getT(key);
		}

		void VertexTransformerObject::setInputs(wp::Vector2 const& entityPosition, float entityAngle)
		{
			auto const& influenceOrigin = getOrigin() + getInfluenceOriginOffset();

			auto infDist = entityPosition.distanceTo(influenceOrigin);
			auto infDistT = mInputInterpolators[(int)InputType::InfluenceEyeDistance].getValue(infDist);

			auto infAngle = influenceOrigin.anticlockwiseAngleTo(entityPosition);
			auto infAngleT = mInputInterpolators[(int)InputType::InfluenceEyeAngle].getValue(infAngle);

			auto entAngleT = mInputInterpolators[(int)InputType::PlayerAngle].getValue(entityAngle);

			mVertexTransformer.setInputs(infDistT, infAngleT, entAngleT);
		}

	} // core
} // bw