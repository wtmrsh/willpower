#pragma once

#include <vector>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>

#include "core/Platform.h"
#include "core/Primitive.h"


namespace bw
{
	namespace core
	{

		struct ClippedPolygon
		{
			bool isHole;
			std::vector<wp::Vector2> vertices;
			wp::BoundingBox bounds;
		};

		class BW_API Clipper
		{
		public:

			virtual std::vector<ClippedPolygon> clip(std::vector<Primitive*> const& primitives, wp::BoundingBox const& extents) = 0;
		};

	} // core
} // bw
