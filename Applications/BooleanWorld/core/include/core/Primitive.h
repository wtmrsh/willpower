#pragma once

#include <vector>
#include <array>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>

#include "core/VertexTransformerObject.h"
#include "core/Triangulation.h"


namespace bw
{
	namespace core
	{

		struct PrimitiveVertex
		{
			wp::Vector2 p;
			int64_t z;
		};


		class BW_API Primitive : public VertexTransformerObject
		{
			friend class PrimitiveGroup;
			
			friend class World;
			
		public:

			enum struct Operation
			{
				Union,
				Intersection,
				Difference,
				XOR
			};

			enum struct FillRule
			{
				NonZero,
				EvenOdd
			};

		protected:

			enum struct Index
			{
				Current = 0,
				Original,
				Min,
				Max
			};

		private:

			uint32_t mId;

			Operation mOperation;

			FillRule mFillRule;

			uint8_t mPriority;

			wp::BoundingBox mBounds;

			mutable bool mCacheInvalid;

			mutable std::vector<PrimitiveVertex> mCachedVertices;

		protected:

			std::vector<PrimitiveVertex> mVertices;

		private:

			void invalidate();

			virtual std::vector<PrimitiveVertex> generateTransformedVertices(wp::Vector2* minExtent = nullptr, wp::Vector2* maxExtent = nullptr) const;

			bool childrenModified() const override;

		protected:

			virtual void generateVertices();

			virtual std::vector<PrimitiveVertex> generateVerticesImpl() = 0;

			void invalidatePostTransform() override;

			void copyFrom(Primitive const& other);

			void setVertices(std::vector<PrimitiveVertex> vertices);

			void serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const override;

			bool deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) override;

			void calculateBounds();

		public:

			explicit Primitive();

			Primitive(Operation operation, FillRule fillType);

			Primitive(Primitive const& other);

			Primitive& operator=(Primitive const& other);

			virtual ~Primitive() = default;

			virtual Primitive* copy() const = 0;

			virtual std::string getType() const = 0;

			virtual std::string getName() const;

			uint32_t getId() const;

			void setOperation(Operation operation);

			Operation getOperation() const;

			void setFillRule(FillRule fillRule);

			FillRule getFillRule() const;

			void setPriority(uint8_t priority);

			uint8_t getPriority() const;

			wp::BoundingBox const& getBounds() const;

			virtual std::vector<PrimitiveVertex> getVertices() const;
	
			virtual uint32_t getNumVertices() const;

			Triangulation triangulate() const;
		};


	} // core
} // bw