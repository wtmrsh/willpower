#pragma once

#include "Platform.h"
#include "InputType.h"


namespace bw
{
	namespace core
	{

		struct BW_API tTransform
		{
			enum class OperandType
			{
				Input,
				Constant,
				TransformOutput
			};

			enum class Operation
			{
				Add,
				Mul,
				AbsDiff,
				Min,
				Max,
				Avg,
				Less,
				Greater,
				LessEq,
				GreaterEq
			};

			OperandType operands[2];
			float constants[2];
			InputType inputs[2];
			Operation operation;

		private:

			void copyFrom(tTransform const& other);

		public:

			tTransform();

			tTransform(
				OperandType operand0,
				OperandType operand1,
				float constant0,
				float constant1,
				InputType input0,
				InputType input1,
				Operation _operation);

			tTransform(tTransform const& other);

			tTransform& operator=(tTransform const& other);

			static tTransform makeConstant(float value);

			static tTransform makeZero();

			static tTransform makeOne();

			static tTransform makePassthroughPrevious();

			static tTransform makePassthroughInput(InputType inputType);
		};

	} // core
} // bw