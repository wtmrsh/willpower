#pragma once

#include <vector>
#include <array>
#include <cstdint>
#include <exception>
#include <algorithm>

#include <willpower/common/Globals.h>
#include <willpower/common/BezierSpline.h>

#include "core/Platform.h"
#include "core/Easing.h"
#include "core/Serializable.h"
#include "core/SerializationException.h"


namespace bw
{
	namespace core
	{

		template<typename T>
		class Interpolator : public Serializable
		{
		public:

			typedef std::pair<float, T> Point;

			static const uint32_t MaxPoints = 16;

		public:

			struct Segment
			{
				Easing easing;
			};

		private:

			std::vector<Point> mPoints;

			std::vector<Segment> mSegments;

			wp::Vector2 mScale[2];

		private:

			bool childrenModified() const override
			{
				return false;
			}

			T getValueInSegment(uint32_t segment, float time) const
			{
				auto p1 = mPoints[segment];
				auto p2 = mPoints[segment + 1];
				auto const& seg = mSegments[segment];

				// Convert to unit, ease, then convert back to scale
				auto p0 = mScale[0];
				auto dp = mScale[1] - mScale[0];

				p1.first -= p0.x;
				p1.second -= p0.y;

				p1.first /= dp.x;
				p1.second /= dp.y;

				p2.first -= p0.x;
				p2.second -= p0.y;

				p2.first /= dp.x;
				p2.second /= dp.y;

				time -= p0.x;
				time /= dp.x;

				float dt = ease(seg.easing, time);

				return mPoints[segment].second + (mPoints[segment + 1].second - mPoints[segment].second) * dt;
			}

		protected:

			void copyFrom(Interpolator const& other)
			{
				mPoints = other.mPoints;
				mSegments = other.mSegments;
				mScale[0] = other.mScale[0];
				mScale[1] = other.mScale[1];
			}

			void serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const override
			{
				BW_UNUSED(workData);

				serializer->beginArray("points");
				{
					uint32_t numPoints = (uint32_t)mPoints.size();
					for (uint32_t i = 0; i < numPoints; ++i)
					{
						auto const& point = mPoints[i];

						serializer->beginMap("point");
						{
							serializer->writeFloat("time", point.first);
							serializer->writeFloat("value", (float)point.second);

							serializer->endMap();
						}
					}

					serializer->endArray();
				}

				serializer->beginArray("segments");
				{
					uint32_t numSegments = (uint32_t)mSegments.size();
					for (uint32_t i = 0; i < numSegments; ++i)
					{
						auto const& segment = mSegments[i];

						serializer->beginMap("segment");
						{
							serializer->writeUint32("easing", (uint32_t)segment.easing);

							serializer->endMap();
						}
					}

					serializer->endArray();
				}

				serializer->writeVector2("scaleMin", mScale[0]);
				serializer->writeVector2("scaleMax", mScale[1]);
			}

			bool deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) override
			{
				BW_UNUSED(workData);

				std::vector<Point> points;
				std::vector<Segment> segments;
				wp::Vector2 scale[2];

				try
				{
					serializer->beginArray("points");
					{
						while (serializer->nextArrayItem())
						{
							serializer->beginMap("point");
							{
								float time = serializer->readFloat("time");
								T value = (T)serializer->readFloat("value");

								points.push_back({ time, value });

								serializer->endMap();
							}
						}

						serializer->endArray();
					}

					if (points.size() > MaxPoints)
					{
						throw SerializationException(std::format("More than {} points found in Interpolator.", MaxPoints));
					}

					serializer->beginArray("segments");
					{
						while (serializer->nextArrayItem())
						{
							Segment segment;

							serializer->beginMap("segment");
							{
								segment.easing = (Easing)serializer->readUint32("easing");

								serializer->endMap();
							}

							segments.push_back(segment);
						}

						serializer->endArray();
					}

					if ((int)segments.size() != (int)(points.size() - 1))
					{
						throw SerializationException("Number of segments in Interpolator does not match number of points");
					}

					scale[0] = serializer->readVector2("scaleMin");
					scale[1] = serializer->readVector2("scaleMax");
				}
				catch (std::exception& e)
				{
					addDeserializationError(e.what());
					return false;
				}

				// Commit
				mPoints = points;
				mSegments = segments;
				mScale[0] = scale[0];
				mScale[1] = scale[1];
				return true;
			}

		public:

			Interpolator()
				: mScale{{ 0, 0 }, { 1, 1 }}
			{
			}

			explicit Interpolator(T value)
				: mScale{ { 0, 0 }, { 1, 1 } }
			{
				setPoints({{0.0f, value }, { 1.0f, value }});
			}

			explicit Interpolator(std::vector<Point> const& points)
				: mScale{ { 0, 0 }, { 1, 1 } }
			{
				setPoints(points);
			}

			Interpolator(Interpolator const& other)
			{
				copyFrom(other);
			}

			Interpolator& operator=(Interpolator const& other)
			{
				copyFrom(other);
				return* this;
			}

			virtual ~Interpolator() = default;

			T getValue(float time) const
			{
				auto numPoints = (uint32_t)mPoints.size();

				float res{ -1.0f };
				if (numPoints == 1)
				{
					res = mPoints[0].second;
				}
				else
				{
					for (uint32_t i = 0; i < numPoints - 1; ++i)
					{
						if (mPoints[i].first <= time && mPoints[i + 1].first > time)
						{
							res = getValueInSegment(i, time);
							break;
						}
					}

					if (res < 0.0f)
					{
						auto const& point = mPoints.back();
						res = point.second;
					}
				}

				return res;
			}

			std::vector<std::vector<Point>> render(float resolution) const
			{
				// Render each segment individually.  Where there are discontinuities,
				// create separate vectors

				std::vector<std::vector<Point>> points;
				std::vector<Point> p;

				auto numSegments = (uint32_t)mSegments.size();
				for (uint32_t i = 0; i < numSegments; ++i)
				{
					auto const& p0 = mPoints[i + 0];
					auto const& p1 = mPoints[i + 1];

					float dx = (p1.first - p0.first) / (mScale[1].x - mScale[0].x);

					if (dx == 0.0f)
					{
						if (!p.empty())
						{
							points.push_back(p);
							p.clear();
						}

						continue;
					}

					wp::Vector2 v0 = { p0.first, p0.second };
					wp::Vector2 v1 = { p1.first, p1.second };

					v0 -= mScale[0];
					v1 -= mScale[0];

					v0 /= (mScale[1] - mScale[0]);
					v1 /= (mScale[1] - mScale[0]);

					auto np = (uint32_t)(resolution * dx);
					for (uint32_t j = 0; j < np; ++j)
					{
						float t = j / (float)(np - 1);
						float dt = ease(mSegments[i].easing, t);

						auto x = v0.x + (v1.x - v0.x) * t;
						auto y = v0.y + (v1.y - v0.y) * dt;
						p.push_back({ x, y });
					}
				}

				if (!p.empty())
				{
					points.push_back(p);
				}

				return points;
			}

			void setScale(wp::Vector2 const& scaleMin, wp::Vector2 const& scaleMax)
			{
				mScale[0] = scaleMin;
				mScale[1] = scaleMax;
			}

			void getScale(wp::Vector2* scaleMin, wp::Vector2* scaleMax) const
			{
				*scaleMin = mScale[0];
				*scaleMax = mScale[1];
			}

			void setPoints(std::vector<std::pair<float, T>> const& points)
			{
				auto numPoints = (uint32_t)points.size();

				if (numPoints > MaxPoints)
				{
					throw std::exception("Too many points");
				}

				for (uint32_t i = 0; i < numPoints; ++i)
				{
					if (i > 0 && points[i].first < points[i - 1].first)
					{
						throw std::exception("Interpolator points not ascending in time");
					}
				}

				mPoints = points;

				mSegments.clear();
				for (uint32_t i = 0; i < numPoints - 1; ++i)
				{
					mSegments.push_back({ Easing::RampUp });
				}
			}

			std::vector<Point> const& getPoints() const
			{
				return mPoints;
			}

			void updatePoint(uint32_t index, float time, T const& value)
			{
				mPoints[index] = { time, value };
			}

			void addPoint(float time, T value)
			{
				auto numPoints = (uint32_t)mPoints.size();

				if (numPoints == 0)
				{
					throw std::exception("Cannot add a single point to an empty interpolator");
				}
				else if (numPoints < MaxPoints)
				{
					mPoints.push_back({});
					mSegments.push_back({});

					uint32_t i = 0;
					for (; i < numPoints; ++i)
					{
						auto const& point = mPoints[i];

						if (point.first > time)
						{
							break;
						}
					}

					// Shift points up
					for (int j = (int)numPoints; j > (int)i; --j)
					{
						mPoints[j] = mPoints[j - 1];

						if (j < (int)numPoints)
						{
							mSegments[j] = mSegments[j - 1];
						}
					}

					numPoints++;
					mPoints[i] = std::make_pair(time, value);
					mSegments[i].easing = Easing::RampUp;
				}
				else
				{
					throw std::exception("Too many points");
				}
			}

			void removePoint(uint32_t index)
			{
				// Shift points down
				auto numPoints = (uint32_t)mPoints.size();

				if (numPoints == 2)
				{
					throw std::exception("Cannot have an interpolator with 1 point.");
				}

				numPoints--;

				for (uint32_t i = index; i < numPoints; ++i)
				{
					mPoints[i] = mPoints[i + 1];

					if (i < (numPoints - 1))
					{
						mSegments[i] = mSegments[i + 1];
					}
				}

				mPoints.pop_back();
				mSegments.pop_back();
			}

			void setEasing(uint32_t segment, Easing easing)
			{
				mSegments[segment].easing = easing;
			}

			Easing getEasing(uint32_t segment) const
			{
				return mSegments[segment].easing;
			}

			std::vector<Segment> const& getSegments() const
			{
				return mSegments;
			}

			Interpolator<T>::Segment const& getSegment(uint32_t index) const
			{
				return mSegments[index];
			}
		};

	} // core
} // bw