#pragma once

#include "Platform.h"


namespace bw
{
	namespace core
	{

		enum LoopBehaviour
		{
			Stop,
			PingPong,
			Loop
		};

		struct TValue
		{
			float value;
			LoopBehaviour loop;
			int dir;
		};

	} // bw
} // core
