#pragma once

#include <vector>
#include <set>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>
#include <willpower/common/AccelerationGrid.h>

#include "core/Platform.h"
#include "core/Serializable.h"
#include "core/Primitive.h"
#include "core/PrimitiveGroup.h"
#include "core/Clipper.h"
#include "core/Stats.h"


namespace bw
{
	namespace core
	{

		class BW_API World : public Serializable
		{
			struct SortPrimitivesByPriority
			{
				bool operator()(Primitive const* a, Primitive const* b)
				{
					return a->getPriority() < b->getPriority();
				}
			};

		private:

			std::string mName;

			wp::BoundingBox mExtents;

			std::vector<Primitive*> mPrimitives;

			wp::AccelerationGrid* mPrimitiveLookupGrid;

			mutable int64_t mClipNs, mTriangulateNs;

			mutable uint32_t mNumPrimitivesClipped, mNumPolygonsGenerated, mNumVerticesGenerated;

		private:

			bool childrenModified() const override;

			Primitive* instantiatePrimitive(std::string const& type) const;

			std::vector<Primitive*> sortPrimitiveIndicesByPriority(std::set<uint32_t> const& indices) const;

		protected:

			void copyFrom(World const& other);

			void serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const override;

			bool deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) override;

		public:

			World();

			World(float sizeX, float sizeY);

			World(World const& other);

			World& operator=(World const& other);

			virtual ~World();

			void createPrimitiveAccelerationGrid();

			void clear();

			void setName(std::string const& name);

			std::string const& getName() const;

			wp::BoundingBox const& getExtents() const;

			uint32_t addPrimitive(Primitive* primitive);

			void removePrimitive(Primitive* primitive, bool failIfNotFound = true);

			void removePrimitive(uint32_t index);

			Primitive* getPrimitive(uint32_t index);

			Primitive const* getPrimitive(uint32_t index) const;

			Primitive* findPrimitive(wp::Vector2 const& worldPos, bool exact) const;

			uint32_t findPrimitiveIndex(wp::Vector2 const& worldPos, bool exact) const;

			uint32_t getNumPrimitives() const;

			std::vector<Primitive*> const& getPrimitives() const;

			std::vector<Primitive*> getPrimitivesByPriority() const;

			void newFrame();

			void update(float frameTime, wp::Vector2 const& entityPos, float entityAngle);

			std::vector<ClippedPolygon> calculatePolygons(wp::BoundingBox const& extents, std::vector<Primitive*> const& primitives) const;

			std::vector <wp::Vector2> triangulate(wp::BoundingBox const& extents, std::set<uint32_t> const& primitiveIndices, std::vector<ClippedPolygon>* outClippedPolygons = nullptr) const;

			Stats getStats() const;
		};

	} // core
} // bw