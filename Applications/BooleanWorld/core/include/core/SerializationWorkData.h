#pragma once

#include <vector>
#include <map>


namespace bw
{
	namespace core
	{
		class VertexTransformer;

		struct SerializationWorkData
		{
			// Map VertexTransformer ids to their pointer
			std::map<uint32_t, VertexTransformer*> vtIdToVtMap;
			
			// Map VertexTransformer ids to their parent id
			std::map<uint32_t, int32_t> vtIdToParentMap;
		};

	} // core
} // bw