#pragma once

#include <algorithm>
#include <vector>

#include <willpower/common/Vector2.h>
#include <willpower/common/MathsUtils.h>

#include "core/Platform.h"
#include "core/tValue.h"
#include "core/Interpolator.h"
#include "core/tTransform.h"
#include "core/Serializable.h"
#include "core/SerializationException.h"


namespace bw
{
	namespace core
	{
		typedef std::vector<tTransform> tTransformFlow;

		class BW_API VertexTransformer : public Serializable
		{
			static uint32_t msIdGenerator;

		public:

			enum class Key
			{
				Scale,
				Angle,
				OrbitAngle,
				OrbitDistance,
				COUNT
			};

		private:

			uint32_t mId;

			VertexTransformer* mParent;

			wp::Vector2 mOrigin;

			Interpolator<float> mAnimationInterpolators[4];

			TValue mT[4];

			tTransformFlow mTransformFlows[4];

			bool mFollowOrbitAngle;

			// Inputs
			float mEntityInfluenceDistance, mEntityInfluenceAngle, mEntityAngle;

		private:

			bool childrenModified() const override;

			wp::Vector2 calculateOffset() const;

		protected:

			void copyFrom(VertexTransformer const& other);

			void serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const override;

			bool deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) override;

		public:

			explicit VertexTransformer(VertexTransformer* parent = nullptr);

			VertexTransformer(VertexTransformer const& other);

			VertexTransformer& operator=(VertexTransformer const& other);

			void setParent(VertexTransformer* parent);

			void deltaT(Key key, float delta);

			void setT(Key key, float value);

			float getT(Key key) const;

			void setLoopBehaviour(Key key, LoopBehaviour behaviour);

			LoopBehaviour getLoopBehaviour(Key key) const;

			void setOrigin(wp::Vector2 const& origin);

			wp::Vector2 const& getOrigin() const;

			Interpolator<float> const& getInterpolator(Key key) const;

			Interpolator<float>& getInterpolator(Key key);

			void setInterpolatorValues(Key key, std::vector<std::pair<float, float>> const& values);

			std::vector<Interpolator<float>::Point> const& getInterpolatorValues(Key key) const;

			void updateInterpolatorValue(Key key, uint32_t index, float time, float const& value);

			void addInterpolatorValue(Key key, float time, float value);

			void removeInterpolatorValue(Key key, uint32_t index);

			float getInterpolatorValue(Key key) const;

			float getInterpolatorValue(Key key, float t) const;

			void getInterpolatorScale(Key key, wp::Vector2* scaleMin, wp::Vector2* scaleMax);

			void setInterpolatorEasing(Key key, uint32_t segment, Easing easing);

			std::vector<Interpolator<float>::Segment> const& getInterpolatorSegments(Key key) const;

			std::vector<std::vector<Interpolator<float>::Point>> renderInterpolator(Key key, float resolution) const;

			//
			// Transform flows
			//
			void setOffsetTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getOffsetTransformFlow();

			void setScaleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getScaleTransformFlow();

			void setAngleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getAngleTransformFlow();

			void setOrbitAngleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getOrbitAngleTransformFlow();

			void setOrbitDistanceTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getOrbitDistanceTransformFlow();

			void setFollowOrbitAngle(bool follow);

			bool getFollowOrbitAngle() const;

			void setInputs(float entityInfluenceDistance, float entityInfluenceAngle, float entityAngle);

			float transformT(Key key) const;

			wp::Vector2 transform(wp::Vector2 const& vertex) const;
		};

	} // core
} // bw