#pragma once

#include "core/Platform.h"


namespace bw
{
	namespace core
	{

		enum struct Easing
		{
			RampUp,
			RampDown,
			EaseInSine,
			EaseInCubic,
			EaseInQuintic,
			EaseOutSine,
			EaseOutCubic,
			EaseOutQuintic,
			EaseInOutSine,
			EaseInOutCubic,
			EaseInOutQuintic,
			EaseInBack,
			EaseOutBack,
			EaseInOutBack,
			EaseInExpo,
			EaseOutExpo,
			EaseInOutExpo,
			EaseInElastic,
			EaseOutElastic,
			EaseInOutElastic,
			EaseInBounce,
			EaseOutBounce,
			EaseInOutBounce
		};

		float ease(Easing easing, float v);


	} // core
} // bw