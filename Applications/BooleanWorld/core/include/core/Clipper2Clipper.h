#pragma once

#include "core/Platform.h"
#include "core/Clipper.h"


namespace bw
{
	namespace core
	{

		class BW_API Clipper2Clipper : public Clipper
		{
		public:

			std::vector<ClippedPolygon> clip(std::vector<Primitive*> const& primitives, wp::BoundingBox const& extents) override;
		};

	} // core
} // bw