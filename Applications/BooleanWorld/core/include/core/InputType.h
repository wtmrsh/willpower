#pragma once

#include "Platform.h"


namespace bw
{
	namespace core
	{

		enum class InputType
		{
			InfluenceEyeDistance,
			InfluenceEyeAngle,
			PlayerAngle,
			COUNT
		};

	} // bw
} // core
