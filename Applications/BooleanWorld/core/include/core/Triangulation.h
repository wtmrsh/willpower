#pragma once

#include <vector>

#include <willpower/common/Vector2.h>
#include <willpower/common/BoundingBox.h>
#include <willpower/common/MathsUtils.h>

#include "core/Platform.h"


namespace bw
{
	namespace core
	{

		struct Triangulation
		{
			struct Triangle
			{
				wp::Vector2 v[3];
			};

			std::vector<Triangle> tris;
			wp::BoundingBox bounds;

			bool pointInside(wp::Vector2 const& point) const
			{
				if (!bounds.pointInside(point))
				{
					return false;
				}

				for (auto const& tri : tris)
				{
					if (wp::MathsUtils::pointInTriangle(point, tri.v[0], tri.v[1], tri.v[2]))
					{
						return true;
					}
				}

				return false;
			}
		};

	} // core
} // bw
