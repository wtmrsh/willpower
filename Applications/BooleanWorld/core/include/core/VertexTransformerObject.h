#pragma once

#include <vector>

#include <willpower/common/Vector2.h>

#include "core/Platform.h"
#include "core/VertexTransformer.h"
#include "core/tValue.h"
#include "core/Serializable.h"
#include "core/Interpolator.h"
#include "core/tTransform.h"
#include "core/tValue.h"
#include "core/InputType.h"


namespace bw
{
	namespace core
	{

		class BW_API VertexTransformerObject : public Serializable
		{
			VertexTransformer mVertexTransformer;

			std::array<Interpolator<float>, 3> mInputInterpolators;

			TValue mT[3];

			wp::Vector2 mInfluenceOriginOffset;

		private:

			bool childrenModified() const override;

		protected:

			virtual void invalidatePostTransform() = 0;

			virtual void copyFrom(VertexTransformerObject const& other);

			void serializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) const override;

			bool deserializeImpl(std::shared_ptr<Serializer> serializer, SerializationWorkData& workData) override;

			VertexTransformer* getVertexTransformer();

			wp::Vector2 transformVertex(wp::Vector2 const& v) const;

		public:

			VertexTransformerObject();

			VertexTransformerObject(VertexTransformerObject const& other);

			VertexTransformerObject& operator=(VertexTransformerObject const& other);

			virtual ~VertexTransformerObject() = default;

			void setTransformerParent(VertexTransformer* transformer);

			void setT(InputType type, float value);

			float getT(InputType key) const;

			void setOrigin(wp::Vector2 const& origin);

			wp::Vector2 const& getOrigin() const;

			void setInfluenceOriginOffset(wp::Vector2 const& originOffset);

			wp::Vector2 const& getInfluenceOriginOffset() const;

			Interpolator<float> const& getAnimationInterpolator(VertexTransformer::Key key) const;

			Interpolator<float>& getAnimationInterpolator(VertexTransformer::Key key);

			Interpolator<float> const& getInputInterpolator(InputType type) const;

			Interpolator<float>& getInputInterpolator(InputType type);

			void setAnimationValues(VertexTransformer::Key key, std::vector<std::pair<float, float>> const& values);

			std::vector<Interpolator<float>::Point> const& getAnimationValues(VertexTransformer::Key key) const;

			void updateAnimationValue(VertexTransformer::Key key, uint32_t index, float time, float const& value);

			void addAnimationValue(VertexTransformer::Key key, float time, float value);

			void removeAnimationValue(VertexTransformer::Key key, uint32_t index);

			float getAnimationValue(VertexTransformer::Key key) const;

			float getAnimationValue(VertexTransformer::Key key, float t) const;

			void getAnimationScale(VertexTransformer::Key key, wp::Vector2* scaleMin, wp::Vector2* scaleMax);

			void setAnimationEasing(VertexTransformer::Key key, uint32_t segment, Easing easing);

			std::vector<Interpolator<float>::Segment> const& getAnimationSegments(VertexTransformer::Key key) const;

			std::vector<std::vector<Interpolator<float>::Point>> renderAnimation(VertexTransformer::Key key, float resolution) const;

			//
			// Transform flows
			//
			void setScaleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getScaleTransformFlow();

			void setAngleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getAngleTransformFlow();

			void setOrbitAngleTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getOrbitAngleTransformFlow();

			void setOrbitDistanceTransformFlow(tTransformFlow const& flow);

			tTransformFlow& getOrbitDistanceTransformFlow();

			void setFollowOrbitAngle(bool follow);

			bool getFollowOrbitAngle() const;

			//
			// Utility
			//
			void deltaT(VertexTransformer::Key key, float delta);

			void setT(VertexTransformer::Key key, float value);

			float getT(VertexTransformer::Key key) const;

			void setInputs(wp::Vector2 const& entityPosition, float entityAngle);
		};

	} // core
} // bw