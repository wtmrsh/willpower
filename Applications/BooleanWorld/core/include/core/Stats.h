#pragma once

#include <cstdint>

#include "core/Platform.h"


namespace bw
{
	namespace core
	{

		struct Stats
		{
			uint32_t primitivesProcessed;

			uint32_t polygonsGenerated;

			uint32_t verticesGenerated;

			int64_t clipNs;
	
			int64_t triangulateNs;
		};

	} // core
} // bw