#define NOMINMAX

#include <string>
#include <vector>
#include <filesystem>
#include <format>
#include <Windows.h>

#include <nfd/nfd.h>

#include <core/RegularPolygon.h>
#include <core/CirclePolygon.h>
#include <core/RectanglePolygon.h>
#include <core/PathPolygon.h>
#include <core/SuperformulaPolygon.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_curve.hpp"
#include "IconsFontAwesome5.h"

#include "UI.h"
#include "Defines.h"
#include "Document.h"
#include "Undo.h"
#include "Actions.h"
#include "ExitApplicationException.h"


namespace editor
{
	using namespace std;

	static wp::Vector2 gViewOffset{ 0.0f, 0.0f };

	bool mouseInteractingWithBackground()
	{
		auto worldPos = getMouseWorldPosition();

		auto const& io = ImGui::GetIO();
		return !io.WantCaptureMouse;
	}

	wp::Vector2 getMouseWorldPosition()
	{
		auto mouseScreenPos = ImGui::GetMousePos();

		return {
			(mouseScreenPos.x - gViewOffset.x) - ED_WINDOW_WIDTH / 2.0f,
			((ED_WINDOW_HEIGHT - mouseScreenPos.y) - gViewOffset.y) - ED_WINDOW_HEIGHT / 2.0f
		};
	}

	uint32_t getHoveredPrimitiveIndex(editor::Document* doc)
	{
		if (!mouseInteractingWithBackground())
		{
			return ~0u;
		}

		auto worldPos = getMouseWorldPosition();

		if (doc->isActive())
		{
			auto world = doc->getWorld();
			return world->findPrimitiveIndex(worldPos, true);
		}
		else
		{
			return ~0u;
		}
	}

	void PushDisabled()
	{
		ImGuiContext& g = *GImGui;
		if ((g.CurrentItemFlags & ImGuiItemFlags_Disabled) == 0)
		{
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, g.Style.Alpha * 0.6f);
		}

		ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
	}

	void PopDisabled()
	{
		ImGui::PopItemFlag();

		ImGuiContext& g = *GImGui;
		if ((g.CurrentItemFlags & ImGuiItemFlags_Disabled) == 0)
		{
			ImGui::PopStyleVar();
		}
	}

	bool ToggleButton(const char* str_id, const char* title, bool v)
	{
		ImVec2 p = ImGui::GetCursorScreenPos();
		ImDrawList* draw_list = ImGui::GetWindowDrawList();

		float height = ImGui::GetFrameHeight();
		float width = height * 1.55f;
		float radius = height * 0.50f;

		ImGui::InvisibleButton(str_id, ImVec2(width, height));

		bool clicked = ImGui::IsItemClicked();

		float t = v ? 1.0f : 0.0f;

		ImGuiContext& g = *GImGui;
		float ANIM_SPEED = 0.08f;
		if (g.LastActiveId == g.CurrentWindow->GetID(str_id))// && g.LastActiveIdTimer < ANIM_SPEED)
		{
			float t_anim = ImSaturate(g.LastActiveIdTimer / ANIM_SPEED);
			t = v ? (t_anim) : (1.0f - t_anim);
		}

		ImU32 col_bg;
		if (ImGui::IsItemHovered())
			col_bg = ImGui::GetColorU32(ImLerp(ImVec4(0.78f, 0.78f, 0.78f, 1.0f), ImVec4(0.64f, 0.83f, 0.34f, 1.0f), t));
		else
			col_bg = ImGui::GetColorU32(ImLerp(ImVec4(0.85f, 0.85f, 0.85f, 1.0f), ImVec4(0.56f, 0.83f, 0.26f, 1.0f), t));

		draw_list->AddRectFilled(p, ImVec2(p.x + width, p.y + height), col_bg, height * 0.5f);
		draw_list->AddCircleFilled(ImVec2(p.x + radius + t * (width - radius * 2.0f), p.y + radius), radius - 1.5f, IM_COL32(255, 255, 255, 255));

		ImGui::SameLine();
		ImGui::Text(title);

		return clicked;
	}

	bool ToggleButton(const char* str_id, const char* title, bool* v)
	{
		auto clicked = ToggleButton(str_id, title, *v);

		if (clicked)
		{
			*v = !*v;
		}

		return clicked;
	}

	void newDocument(editor::Document* doc)
	{
		doc->newDoc();
	}

	void openDocument(editor::Document* doc)
	{
		std::vector<std::pair<std::string, std::string>> extensions = {
			std::make_pair("YAML", "yaml")
		};

		auto numExtensions = extensions.size();

		auto filters = new nfdfilteritem_t[numExtensions];
		for (uint32_t i = 0; i < numExtensions; ++i)
		{
			filters[i] = { extensions[i].first.c_str(), extensions[i].second.c_str() };
		}

		nfdchar_t* outPath;
		auto res = NFD_OpenDialog(&outPath, filters, (nfdfiltersize_t)numExtensions, nullptr);

		if (res == NFD_OKAY)
		{
			std::string filepath(outPath);
			bool ok = doc->openDoc(filepath);

			NFD_FreePath(outPath);

			if (!ok)
			{
				ImGui::OpenPopup("Open file failed");

				// Always center this window when appearing
				ImVec2 center = ImGui::GetMainViewport()->GetCenter();
				ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

				if (ImGui::BeginPopupModal("Open file failed", NULL, ImGuiWindowFlags_AlwaysAutoResize))
				{
					ImGui::Text("Load failed!  See error log.");
					
					if (ImGui::Button("OK", ImVec2(120, 0))) 
					{ 
						ImGui::CloseCurrentPopup(); 
					}

					ImGui::EndPopup();
				}
			}
		}

		delete[] filters;
	}

	void saveDocumentAs(editor::Document* doc)
	{
		std::vector<std::pair<std::string, std::string>> extensions = {
			std::make_pair("YAML", "yaml")
		};

		auto numExtensions = extensions.size();

		auto filters = new nfdfilteritem_t[numExtensions];
		for (uint32_t i = 0; i < numExtensions; ++i)
		{
			filters[i] = { extensions[i].first.c_str(), extensions[i].second.c_str() };
		}

		nfdchar_t* outPath;
		auto res = NFD_SaveDialog(&outPath, filters, (nfdfiltersize_t)numExtensions, nullptr, nullptr);

		if (res == NFD_OKAY)
		{
			std::string filepath(outPath);
			doc->saveDocAs(filepath);

			NFD_FreePath(outPath);
		}

		delete[] filters;
	}

	void saveDocument(editor::Document* doc)
	{
		if (!doc->hasFilepath())
		{
			saveDocumentAs(doc);
		}
		else
		{
			doc->saveDoc();
		}
	}

	void exitApp(editor::Document* doc)
	{
		throw ExitApplicationException(0, "Exit");
	}

	void checkModifiedOperation(editor::Document* doc, string const& title, DocumentHelperFunction func)
	{
		// Centre dialogue
		ImVec2 center = ImGui::GetMainViewport()->GetCenter();
		ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

		if (ImGui::BeginPopupModal(title.c_str(), nullptr, ImGuiWindowFlags_AlwaysAutoResize))
		{
			std::string saveText = "Do you want to save changes?";
			ImGui::Text(saveText.c_str());
			ImGui::Separator();

			if (ImGui::Button("Save", ImVec2(120, 0)))
			{
				saveDocument(doc);
				func(doc);
				ImGui::CloseCurrentPopup();
			}

			ImGui::SetItemDefaultFocus();

			ImGui::SameLine();
			if (ImGui::Button("Don't Save", ImVec2(120, 0)))
			{
				func(doc);
				ImGui::CloseCurrentPopup();
			}

			ImGui::SameLine();
			if (ImGui::Button("Cancel", ImVec2(120, 0)))
			{
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}
	}

	void handleModifiedDocument(editor::Document* doc, bool docAction, bool checkDocumentModified, std::string docText, DocumentHelperFunction helperFunc)
	{
		if (docAction)
		{
			if (checkDocumentModified && doc->isModified())
			{
				char const* docTextStr = docText.c_str();
				ImGui::OpenPopup(docTextStr);
			}
			else
			{
				helperFunc(doc);
			}
		}
	}

	ImVec2 gMainMenuWindowSize;

	void renderMenu(editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings)
	{
		bool docAction = false;
		bool checkDocumentModified = false;
		DocumentHelperFunction helperFunc;
		std::string docText;

		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("New", "Ctrl+N"))
				{
					docAction = true;
					checkDocumentModified = true;
					helperFunc = newDocument;
					docText = "New world";
				}

				if (ImGui::MenuItem("Open", "Ctrl+O"))
				{
					docAction = true;
					checkDocumentModified = true;
					helperFunc = openDocument;
					docText = "Open world";
				}

				auto world = doc->getWorld();
				bool saveDisabled = !world || !doc->isModified();
				bool saveAsDisabled = !world;

				if (saveDisabled)
				{
					PushDisabled();
				}

				if (ImGui::MenuItem("Save", "Ctrl+S"))
				{
					docAction = true;
					checkDocumentModified = false;
					helperFunc = saveDocument;
					docText = "Save world";
				}

				if (saveDisabled)
				{
					PopDisabled();
				}

				if (saveAsDisabled)
				{
					PushDisabled();
				}

				if (ImGui::MenuItem("Save as..."))
				{
					docAction = true;
					checkDocumentModified = false;
					helperFunc = saveDocumentAs;
					docText = "Save world";
				}

				if (saveAsDisabled)
				{
					PopDisabled();
				}

				ImGui::Separator();
				if (ImGui::MenuItem("Exit"))
				{
					docAction = true;
					checkDocumentModified = true;
					helperFunc = exitApp;
					docText = "Exit application";
				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Edit"))
			{
				bool canUndoAction = canUndo();
				bool canRedoAction = canRedo();
				bool hasSelection = doc->hasSelection();

				if (!canUndoAction)
				{
					PushDisabled();
				}

				if (ImGui::MenuItem("Undo", "Ctrl+Z"))
				{
					undo(doc);
				}

				if (!canUndoAction)
				{
					PopDisabled();
				}

				if (!canRedoAction)
				{
					PushDisabled();
				}

				if (ImGui::MenuItem("Redo", "Ctrl+Y"))
				{
					redo(doc);
				}

				if (!canRedoAction)
				{
					PopDisabled();
				}

				if (!hasSelection)
				{
					PushDisabled();
				}

				if (ImGui::MenuItem("Delete", "Del"))
				{
					auto index = *doc->getSelectedPrimitiveIndices().begin();
					transactUndoableAction(doc, format("Delete Primitive {}", index), bind(deletePrimitive, placeholders::_1, index));
				}

				if (ImGui::MenuItem("Deselect all"))
				{
					transactUndoableAction(doc, "Clear Selected Primitives", clearSelectedPrimitives);
				}

				if (!hasSelection)
				{
					PopDisabled();
				}

				ImGui::EndMenu();
			}
			
			if (ImGui::BeginMenu("View"))
			{
				ImGui::MenuItem("Grid", 0, &editorSettings.showGrid);

				ImGui::MenuItem("Triangulation border", 0, &editorSettings.renderTriangulationBorder);

				ImGui::MenuItem("Primitive borders", 0, &editorSettings.renderPrimitiveBorders);

				ImGui::MenuItem("Primitive bounds", 0, &editorSettings.renderPrimitiveBounds);

				ImGui::EndMenu();
			}

			gMainMenuWindowSize = ImGui::GetWindowSize();
			ImGui::EndMainMenuBar();
		}

		// Handle action
		auto docTextStr = docText.c_str();
		checkModifiedOperation(doc, docTextStr, helperFunc);
		handleModifiedDocument(doc, docAction, checkDocumentModified, docText, helperFunc);
	}

	void renderToolbar(Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		ImGuiIO& io = ImGui::GetIO();

		auto windowFlags = 0
			| ImGuiWindowFlags_NoDecoration;

		ImGui::SetNextWindowPos(ImVec2(0, gMainMenuWindowSize.y));
		ImGui::SetNextWindowSize(ImVec2(io.DisplaySize.x, 35));

		if (ImGui::Begin("Toolbar", nullptr, windowFlags))
		{
			bool docAction = false;
			bool checkDocumentModified = false;
			DocumentHelperFunction helperFunc;
			string docText;

			auto world = doc->getWorld();

			bool saveDisabled = !world || !doc->isModified();
			bool saveAsDisabled = !world;
			bool canUndoAction = canUndo();
			bool canRedoAction = canRedo();
			bool hasSelection = doc->hasSelection();

			//
			// File operations
			//
			if (ImGui::Button("New"))
			{
				docAction = true;
				checkDocumentModified = true;
				helperFunc = newDocument;
				docText = "New world";
			}

			ImGui::SameLine();

			if (ImGui::Button("Open"))
			{
				docAction = true;
				checkDocumentModified = true;
				helperFunc = openDocument;
				docText = "Open world";
			}

			ImGui::SameLine();
			
			if (saveDisabled)
			{
				PushDisabled();
			}

			if (ImGui::Button("Save"))
			{
				docAction = true;
				checkDocumentModified = false;
				helperFunc = saveDocument;
				docText = "Save world";
			}

			if (saveDisabled)
			{
				PopDisabled();
			}

			ImGui::SameLine();

			if (saveAsDisabled)
			{
				PushDisabled();
			}

			if (ImGui::Button("Save as"))
			{
				docAction = true;
				checkDocumentModified = false;
				helperFunc = saveDocumentAs;
				docText = "Save world";
			}

			if (saveAsDisabled)
			{
				PopDisabled();
			}

			//
			// Primitive operations
			//
			ImGui::SameLine();

			if (!hasSelection)
			{
				PushDisabled();
			}

			if (ImGui::Button("Delete"))
			{
				auto index = *doc->getSelectedPrimitiveIndices().begin();
				transactUndoableAction(doc, format("Delete Primitive {}", index), bind(deletePrimitive, placeholders::_1, index));
			}

			if (!hasSelection)
			{
				PopDisabled();
			}

			//
			// Undo/redo
			//
			ImGui::SameLine();
			
			if (!canUndoAction)
			{
				PushDisabled();
			}

			if (ImGui::Button("Undo"))
			{
				undo(doc);
			}

			if (!canUndoAction)
			{
				PopDisabled();
			}

			if (!canRedoAction)
			{
				PushDisabled();
			}

			ImGui::SameLine();

			if (ImGui::Button("Redo"))
			{
				redo(doc);
			}

			if (!canRedoAction)
			{
				PopDisabled();
			}

			//
			// Visual
			//
			ImGui::SameLine();

			if (!world)
			{
				PushDisabled();
			}

			ToggleButton("TogglePrimitiveBorders", "Prim. borders", &editorSettings.renderPrimitiveBorders);

			ImGui::SameLine();

			ToggleButton("TogglePrimitiveBounds", "Prim. bounds", &editorSettings.renderPrimitiveBounds);

			ImGui::SameLine();

			ToggleButton("ToggleTriangulationBorders", "Tri. borders", &editorSettings.renderTriangulationBorder);

			if (!world)
			{
				PopDisabled();
			}

			// Grid
			ImGui::SameLine();

			ToggleButton("ToggleGrid", "Grid", &editorSettings.showGrid);

			ImGui::SameLine();

			ImGui::SetNextItemWidth(100);

			static int gridSize = (int)(log((float)editorSettings.gridSize) / log(2.0f)) - 3;
			string gridSizeText = format("{}", (int)editorSettings.gridSize);
			if (ImGui::SliderInt("Size##GridSize", &gridSize, 0, 3, gridSizeText.c_str()))
			{
				editorSettings.gridSize = (float)(1 << (gridSize + 3));
			}

			// Handle action
			auto docTextStr = docText.c_str();
			checkModifiedOperation(doc, docTextStr, helperFunc);
			handleModifiedDocument(doc, docAction, checkDocumentModified, docText, helperFunc);

			ImGui::End();
		}
	}

	void renderStatusbar(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		ImGuiViewportP* viewport = (ImGuiViewportP*)(void*)ImGui::GetMainViewport();

		auto windowFlags =
			ImGuiWindowFlags_NoScrollbar |
			ImGuiWindowFlags_NoSavedSettings |
			ImGuiWindowFlags_MenuBar;

		float height = ImGui::GetFrameHeight();

		if (ImGui::BeginViewportSideBar("##MainStatusBar", viewport, ImGuiDir_Down, height, windowFlags))
		{
			if (ImGui::BeginMenuBar())
			{
				if (doc->isActive())
				{
					auto world = doc->getWorld();

					// Number of primitives
					auto numPrimitives = world->getNumPrimitives();
					string numPrimsText = format("{} total primitive(s)", numPrimitives);

					ImVec4 c = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
					ImGui::TextColored(c, numPrimsText.c_str());

					ImGui::SameLine();

					// Times
					auto const& stats = world->getStats();
					
					float clipMs = stats.clipNs / 1000000.0f;
					float triMs = stats.triangulateNs / 1000000.0f;
					string processData = format("| {} primitives(s) -> {} vertices & {} polygon(s) | clip: {:.1f} ms / triangulate: {:.1f} ms",
						stats.primitivesProcessed,
						stats.verticesGenerated,
						stats.polygonsGenerated,
						clipMs,
						triMs);

					ImGui::TextColored(c, processData.c_str());

					// Mouse
					auto mousePos = getMouseWorldPosition();
					string mouseData = format("| {:.1f}, {:.1f}", mousePos.x, mousePos.y);

					ImGui::SameLine();

					ImGui::TextColored(c, mouseData.c_str());
				}

				ImGui::EndMenuBar();
			}
			ImGui::End();
		}
	}

	void renderCreateRegularPolygon(editor::Document* doc, bw::core::Primitive::Operation op, bw::core::Primitive::FillRule fillRule, uint8_t priority, float scale, float angle, wp::Vector2 const& viewOrigin)
	{
		static int numSides = 3;

		ImGui::SetNextItemWidth(128);

		if (ImGui::InputInt("Sides##CreatePrimitive", &numSides, 1, 1))
		{
			numSides = max(0, min(numSides, 8));
		}

		if (ImGui::Button("Create##CreatePrimitive"))
		{

			transactUndoableAction(doc, format("Create Regular {}-Gon Primitive", numSides), bind(createRegularPolygonPrimitive, placeholders::_1,
				op,	fillRule, (uint32_t)numSides, priority, viewOrigin, scale, angle));
		}
	}

	void renderCreateCirclePolygon(editor::Document* doc, bw::core::Primitive::Operation op, bw::core::Primitive::FillRule fillRule, uint8_t priority, float scale, float angle, wp::Vector2 const& viewOrigin)
	{
		static float resolution = 0.5f;

		ImGui::SetNextItemWidth(128);

		if (ImGui::InputFloat("Res##CreatePrimitive", &resolution, 0.01f, 0.1f))
		{
			resolution = max(0.0f, min(resolution, 1.0f));
		}

		if (ImGui::Button("Create##CreatePrimitive"))
		{
			transactUndoableAction(doc, "Create Circle Primitive", bind(createCirclePrimitive, placeholders::_1, 
				op,	fillRule, resolution, priority, viewOrigin, scale, angle));
		}
	}
	
	void renderCreateRectanglePolygon(editor::Document* doc, bw::core::Primitive::Operation op, bw::core::Primitive::FillRule fillRule, uint8_t priority, float scale, float angle, wp::Vector2 const& viewOrigin)
	{
		static float xyRatio = 2.0f;

		ImGui::SetNextItemWidth(128);

		if (ImGui::InputFloat("Ratio##CreatePrimitive", &xyRatio, 0.01f, 0.1f))
		{
			xyRatio = max(1.0f, min(xyRatio, 4.0f));
		}

		if (ImGui::Button("Create##CreatePrimitive"))
		{
			transactUndoableAction(doc, "Create Rectangle Primitive", bind(createRectanglePrimitive, placeholders::_1,
				op, fillRule, xyRatio, priority, viewOrigin, scale, angle));
		}
	}
	
	void renderCreatePathPolygon(editor::Document* doc, bw::core::Primitive::Operation op, bw::core::Primitive::FillRule fillRule, uint8_t priority, float scale, float angle, wp::Vector2 const& viewOrigin)
	{
	}
	
	void renderCreateSuperformulaPolygon(editor::Document* doc, bw::core::Primitive::Operation op, bw::core::Primitive::FillRule fillRule, uint8_t priority, float scale, float angle, wp::Vector2 const& viewOrigin)
	{
		static float resolution = 0.5f;
		static float values[6] = { 1.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };

		ImGui::SetNextItemWidth(128);

		if (ImGui::InputFloat("Res##CreatePrimitive", &resolution, 0.01f, 0.1f))
		{
			resolution = max(0.0f, min(resolution, 1.0f));
		}

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("a##CreatePrimitive", &values[0], 0.1f, 2.0f);

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("b##CreatePrimitive", &values[1], 0.1f, 2.0f);

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("m##CreatePrimitive", &values[2], 0.1f, 10.0f);

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("n1##CreatePrimitive", &values[3], 0.1f, 10.0f);

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("n2##CreatePrimitive", &values[4], 0.1f, 10.0f);

		ImGui::SetNextItemWidth(128);
		ImGui::SliderFloat("n3##CreatePrimitive", &values[5], 0.1f, 10.0f);

		if (ImGui::Button("Create##CreatePrimitive"))
		{
			transactUndoableAction(doc, "Create Superformula Primitive", bind(createSuperformulaPrimitive, placeholders::_1,
				op, fillRule, values, resolution, priority, viewOrigin, scale, angle));
		}
	}

	bw::core::Primitive::Operation setOperationWidget(bw::core::Primitive* primitive, int mode)
	{
		int selectedOperation;
		bw::core::Primitive::Operation editOperation = primitive ? primitive->getOperation()
			: bw::core::Primitive::Operation::Union;

		string name;
		switch (mode)
		{
		case 0:
			name = "Operation##CreatePrimitive";
			break;

		case 1:
			name = "Operation##EditPrimitive";
			break;

		case 2:
			name = "Operation##OrderPrimitive";
			break;

		default:
			throw EditorException("Unknown widget mode");
		}
		switch (editOperation)
		{
		case bw::core::Primitive::Operation::Union:
			selectedOperation = 0;
			break;

		case bw::core::Primitive::Operation::Difference:
			selectedOperation = 1;
			break;

		case bw::core::Primitive::Operation::Intersection:
			selectedOperation = 2;
			break;

		case bw::core::Primitive::Operation::XOR:
			selectedOperation = 3;
			break;

		default:
			throw EditorException("Unknown operation");
		}

		vector<string> operations = {
			"Union",
			"Difference",
			"Intersection",
			"XOR"
		};

		string operationsStr;

		for (auto const& operation : operations)
		{
			operationsStr += operation;
			operationsStr += '\0';
		}

		if (ImGui::Combo(name.c_str(), &selectedOperation, operationsStr.c_str(), 6))
		{
			switch (selectedOperation)
			{
			case 0:
				editOperation = bw::core::Primitive::Operation::Union;
				break;

			case 1:
				editOperation = bw::core::Primitive::Operation::Difference;
				break;

			case 2:
				editOperation = bw::core::Primitive::Operation::Intersection;
				break;

			case 3:
				editOperation = bw::core::Primitive::Operation::XOR;
				break;

			default:
				throw EditorException("Unknown operation");
			}

			if (primitive)
			{
				primitive->setOperation(editOperation);
			}
		}

		return editOperation;
	}

	bw::core::Primitive::FillRule setFillRuleWidget(bw::core::Primitive* primitive, int mode)
	{
		int selectedFillRule;
		bw::core::Primitive::FillRule editFillRule = primitive ? primitive->getFillRule()
			: bw::core::Primitive::FillRule::NonZero;

		string name;
		switch (mode)
		{
		case 0:
			name = "FillRule##CreatePrimitive";
			break;

		case 1:
			name = "FillRule##EditPrimitive";
			break;

		case 2:
			name = "FillRule##OrderPrimitive";
			break;

		default:
			throw EditorException("Unknown widget mode");
		}

		switch (editFillRule)
		{
		case bw::core::Primitive::FillRule::NonZero:
			selectedFillRule = 0;
			break;

		case bw::core::Primitive::FillRule::EvenOdd:
			selectedFillRule = 1;
			break;

		default:
			throw EditorException("Unknown fill rule");
		}

		vector<string> fillRules = {
			"NonZero",
			"EvenOdd",
		};

		string fillRulesStr;

		for (auto const& fillRule : fillRules)
		{
			fillRulesStr += fillRule;
			fillRulesStr += '\0';
		}

		if (ImGui::Combo(name.c_str(), &selectedFillRule, fillRulesStr.c_str(), 6))
		{
			switch (selectedFillRule)
			{
			case 0:
				editFillRule = bw::core::Primitive::FillRule::NonZero;
				break;

			case 1:
				editFillRule = bw::core::Primitive::FillRule::EvenOdd;
				break;

			default:
				throw EditorException("Unknown fill rule");
			}

			if (primitive)
			{
				primitive->setFillRule(editFillRule);
			}
		}

		return editFillRule;
	}

	void renderCreateNewPrimitive(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		static int selectedPrimitiveType = 0;
		static bw::core::Primitive::Operation createOperation = bw::core::Primitive::Operation::Union;
		bw::core::Primitive::FillRule createFillRule = bw::core::Primitive::FillRule::NonZero;

		vector<string> primitiveTypes = {
			"Regular polygon",
			"Circle",
			"Rectangle",
			"Path",
			"Superformula"
		};

		string primitiveTypesStr;

		for (auto const& primitiveType : primitiveTypes)
		{
			primitiveTypesStr += primitiveType;
			primitiveTypesStr += '\0';
		}

		ImGui::Combo("Type##CreatePrimitive", &selectedPrimitiveType, primitiveTypesStr.c_str(), 6);

		createOperation = setOperationWidget(nullptr, 0);
		createFillRule = setFillRuleWidget(nullptr, 0);

		static int primitivePriority{ 0 };
		ImGui::SliderInt("Priority##CreatePrimitive", &primitivePriority, 0, 255);

		static float primitiveScale{ 100.0f };
		ImGui::SliderFloat("Size##CreatePrimitive", &primitiveScale, 50, 400);

		static float primitiveAngle{ 0.0f };
		if (ImGui::SliderAngle("Angle##CreatePrimitive", &primitiveAngle, 0.0f, 360.0f))
		{
			primitiveAngle = wp::MathsUtils::degrees(primitiveAngle);
		}

		switch (selectedPrimitiveType)
		{
		case 0:
			renderCreateRegularPolygon(doc, createOperation, createFillRule, (uint8_t)primitivePriority, primitiveScale, primitiveAngle, renderSettings.origin);
			break;

		case 1:
			renderCreateCirclePolygon(doc, createOperation, createFillRule, (uint8_t)primitivePriority, primitiveScale, primitiveAngle, renderSettings.origin);
			break;

		case 2:
			renderCreateRectanglePolygon(doc, createOperation, createFillRule, (uint8_t)primitivePriority, primitiveScale, primitiveAngle, renderSettings.origin);
			break;

		case 3:
			renderCreatePathPolygon(doc, createOperation, createFillRule, (uint8_t)primitivePriority, primitiveScale, primitiveAngle, renderSettings.origin);
			break;

		case 4:
			renderCreateSuperformulaPolygon(doc, createOperation, createFillRule, (uint8_t)primitivePriority, primitiveScale, primitiveAngle, renderSettings.origin);
			break;

		default:
			break;
		}
	}

	void renderCreatePrimitivePanel(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		auto windowFlags = 0;

		bool docIsActive = doc->isActive();

		if (ImGui::CollapsingHeader("Create Primitive", nullptr, windowFlags))
		{
			if (!docIsActive)
			{
				PushDisabled();
			}

			renderCreateNewPrimitive(doc, editorSettings, renderSettings);

			if (!docIsActive)
			{
				PopDisabled();
			}
		}
	}

	void renderEditRegularPolygon(bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
	}

	void renderEditCirclePolygon(bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		ImGui::SetNextItemWidth(128);

		auto circle = static_cast<bw::core::CirclePolygon*>(primitive);
		float resolution = circle->getResolution();

		if (ImGui::InputFloat("Res##EditPrimitive", &resolution, 0.01f, 0.1f))
		{
			resolution = max(0.0f, min(resolution, 1.0f));
			circle->setResolution(resolution);
		}
	}

	void renderEditRectanglePolygon(bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
	}

	void renderEditPathPolygon(bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
	}

	void renderEditSuperformulaPolygon(bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		ImGui::SetNextItemWidth(128);

		auto sf = static_cast<bw::core::SuperformulaPolygon*>(primitive);
		float resolution = sf->getResolution();

		if (ImGui::InputFloat("Res##EditPrimitive", &resolution, 0.01f, 0.1f))
		{
			resolution = max(0.0f, min(resolution, 1.0f));
			sf->setResolution(resolution);
		}

		float values[6];
		for (uint32_t i = 0; i < 6; ++i)
		{
			values[i] = sf->getValue(i);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("a##EditPrimitive", &values[0], 0.1f, 2.0f))
		{
			sf->setValue(0, values[0]);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("b##EditPrimitive", &values[1], 0.1f, 2.0f))
		{
			sf->setValue(1, values[1]);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("m##EditPrimitive", &values[2], 0.1f, 10.0f))
		{
			sf->setValue(2, values[2]);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("n1##EditPrimitive", &values[3], 0.1f, 10.0f))
		{
			sf->setValue(3, values[3]);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("n2##EditPrimitive", &values[4], 0.1f, 10.0f))
		{
			sf->setValue(4, values[4]);
		}

		ImGui::SetNextItemWidth(128);
		if (ImGui::SliderFloat("n3##EditPrimitive", &values[5], 0.1f, 10.0f))
		{
			sf->setValue(5, values[5]);
		}
	}

	void renderTransformFlow(editor::Document* doc, bw::core::Primitive* primitive, bw::core::tTransformFlow& flow, bw::core::VertexTransformer::Key key, string const& keyName)
	{
		if (ImGui::Button(ICON_FA_PLUS))
		{
			transactUndoableAction(doc, format("Add {} Transform", keyName),
				bind(addTransform, placeholders::_1, primitive, key));
		}

		char const* operandTypes[] = { "Input", "Constant", "Previous" };
		char const* inputTypes[] = { "Eye dist", "Eye angle", "Global angle" };

		for (uint32_t i = 0; i < (uint32_t)flow.size(); ++i)
		{
			ImGui::PushID(i);

			auto& transform = flow[i];

			int operand0 = (int)transform.operands[0];
			int operand1 = (int)transform.operands[1];
			float constant0 = transform.constants[0];
			float constant1 = transform.constants[1];
			int input0 = (int)transform.inputs[0];
			int input1 = (int)transform.inputs[1];
			int operation = (int)transform.operation;

			// Operand 1 type
			ImGui::SetNextItemWidth(96);
			if (ImGui::Combo("##Op1TransformFlow", &operand0, operandTypes, IM_ARRAYSIZE(operandTypes)))
			{
				transform.operands[0] = (bw::core::tTransform::OperandType)(operand0);
			}

			ImGui::SameLine();

			// Operand 1 value
			ImGui::SetNextItemWidth(112);
			switch ((bw::core::tTransform::OperandType)operand0)
			{
			case bw::core::tTransform::OperandType::Input:
				if (ImGui::Combo("##In1TransformFlow", &input0, inputTypes, IM_ARRAYSIZE(inputTypes)))
				{
					transform.inputs[0] = (bw::core::InputType)(input0);
				}
				break;

			case bw::core::tTransform::OperandType::Constant:
				if (ImGui::InputFloat("##Cn1TransformFlow", &constant0))
				{
					transform.constants[0] = constant0;
				}
				break;

			case bw::core::tTransform::OperandType::TransformOutput:
				ImGui::Text("<result>");
				break;
			}

			ImGui::SameLine();

			// Operation
			char const* opTypes[] = { "+", "*", "|-|", "Min", "Max", "Avg", "<", ">", "<=", ">=" };

			ImGui::SetNextItemWidth(64);
			if (ImGui::Combo("##OpTransformFlow", &operation, opTypes, IM_ARRAYSIZE(opTypes)))
			{
				transform.operation = (bw::core::tTransform::Operation)operation;
			}

			ImGui::SameLine();

			// Operand 2 type
			ImGui::SetNextItemWidth(96);
			if (ImGui::Combo("##Op2TransformFlow", &operand1, operandTypes, IM_ARRAYSIZE(operandTypes)))
			{
				transform.operands[1] = (bw::core::tTransform::OperandType)(operand1);
			}

			ImGui::SameLine();

			// Operand 2 value
			ImGui::SetNextItemWidth(112);
			switch ((bw::core::tTransform::OperandType)operand1)
			{
			case bw::core::tTransform::OperandType::Input:
				if (ImGui::Combo("##In2TransformFlow", &input1, inputTypes, IM_ARRAYSIZE(inputTypes)))
				{
					transform.inputs[1] = (bw::core::InputType)(input1);
				}
				break;

			case bw::core::tTransform::OperandType::Constant:
				if (ImGui::InputFloat("##Cn2TransformFlow", &constant1))
				{
					transform.constants[1] = constant1;
				}
				break;

			case bw::core::tTransform::OperandType::TransformOutput:
				ImGui::Text("<result>");
				break;
			}

			// Move up/down, delete
			ImGui::SameLine();

			int counter = 0;
			ImGui::PushButtonRepeat(false);

			if (i > 0)
			{
				ImGui::SameLine();
				if (ImGui::ArrowButton("##up", ImGuiDir_Up))
				{
					counter--;
				}
			}

			if (i < (uint32_t)(flow.size() - 1))
			{
				ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
				if (ImGui::ArrowButton("##down", ImGuiDir_Down))
				{
					counter++;
				}
			}

			ImGui::PopButtonRepeat();

			if (counter < 0)
			{
				transactUndoableAction(doc, format("Swap {} Transforms", keyName),
					bind(swapTransforms, placeholders::_1, primitive, key, i, i - 1));

			}
			else if (counter > 0)
			{
				transactUndoableAction(doc, format("Swap {} Transforms", keyName),
					bind(swapTransforms, placeholders::_1, primitive, key, i, i + 1));
			}

			ImGui::SameLine();

			if (ImGui::Button(ICON_FA_ERASER))
			{
				transactUndoableAction(doc, format("Remove {} Transform", keyName),
					bind(removeTransform, placeholders::_1, primitive, key, i));
			}

			// Next
			if (i != (flow.size() - 1))
			{
				ImGui::Separator();
			}

			ImGui::PopID();
		}
	}

	bool gShowFlows[4] = { true, true, true, true };

	void renderTransformFlow(editor::Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key)
	{
		string keyName;

		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			keyName = "Scale";
			break;

		case bw::core::VertexTransformer::Key::Angle:
			keyName = "Angle";
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			keyName = "Orbit Angle";
			break;
	
		case bw::core::VertexTransformer::Key::OrbitDistance:
			keyName = "Orbit Distance";
			break;
		}

		ImGui::PushID(keyName.c_str());

		if (ToggleButton("Toggle##TransformFlow", format("Transform {}", keyName).c_str(), &gShowFlows[(int)key]))
		{
			if (!gShowFlows[(int)key])
			{
				// Reset flow
				switch (key)
				{
				case bw::core::VertexTransformer::Key::Scale:
					primitive->setScaleTransformFlow({ bw::core::tTransform::makeZero() });
					break;

				case bw::core::VertexTransformer::Key::Angle:
					primitive->setAngleTransformFlow({ bw::core::tTransform::makeZero() });
					break;

				case bw::core::VertexTransformer::Key::OrbitAngle:
					primitive->setOrbitAngleTransformFlow({ bw::core::tTransform::makeZero() });
					break;

				case bw::core::VertexTransformer::Key::OrbitDistance:
					primitive->setOrbitDistanceTransformFlow({ bw::core::tTransform::makeZero() });
					break;
				}
			}
		}

		if (!gShowFlows[(int)key])
		{
			ImGui::PopID();
			return;
		}

		ImGui::SameLine();

		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			renderTransformFlow(doc, primitive, primitive->getScaleTransformFlow(), key, keyName);
			break;

		case bw::core::VertexTransformer::Key::Angle:
			renderTransformFlow(doc, primitive, primitive->getAngleTransformFlow(), key, keyName);
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			renderTransformFlow(doc, primitive, primitive->getOrbitAngleTransformFlow(), key, keyName);
			break;

		case bw::core::VertexTransformer::Key::OrbitDistance:
			renderTransformFlow(doc, primitive, primitive->getOrbitDistanceTransformFlow(), key, keyName);
			break;
		}

		ImGui::PopID();
	}

	vector<string> gEasingStrings = {
		"RampUp",
		"RampDown",
		"EaseInSine",
		"EaseInCubic",
		"EaseInQuintic",
		"EaseOutSine",
		"EaseOutCubic",
		"EaseOutQuintic",
		"EaseInOutSine",
		"EaseInOutCubic",
		"EaseInOutQuintic",
		"EaseInBack",
		"EaseOutBack",
		"EaseInOutBack",
		"EaseInExpo",
		"EaseOutExpo",
		"EaseInOutExpo",
		"EaseInElastic",
		"EaseOutElastic",
		"EaseInOutElastic",
		"EaseInBounce",
		"EaseOutBounce",
		"EaseInOutBounce"
	};

	bool gShowCurves[7] = { true, true, true, true, true, true, true };

	void renderInterpolator(editor::Document* doc, bw::core::Primitive* primitive, bw::core::Interpolator<float>& lerper, int curveIndex, string const& name, float* timelineT)
	{
		vector<bw::core::Interpolator<float>::Point> points = lerper.getPoints();
		vector<vector<bw::core::Interpolator<float>::Point>> renderValues = lerper.render(100.0f);

		wp::Vector2 scaleMin, scaleMax;
		lerper.getScale(&scaleMin, &scaleMax);

		ImGui::PushID(name.c_str());

		if (ToggleButton("Toggle##Interpolator", format("Animate {}", name).c_str(), &gShowCurves[curveIndex]))
		{
			if (!gShowCurves[curveIndex])
			{
				// Reset lerper
				float value = lerper.getValue(0.0f);
				lerper.setPoints({ { 0.0f, value }, { 1.0f, value } });
			}
		}

		if (!gShowCurves[curveIndex])
		{
			ImGui::PopID();
			return;
		}

		auto numPoints = (uint32_t)points.size();

		array<ImVec2, bw::core::Interpolator<float>::MaxPoints> imPoints;
		for (uint32_t i = 0; i < numPoints; ++i)
		{
			imPoints[i] = { points[i].first, points[i].second };
		}

		int nPoints = (int)numPoints, editPoint;
		int curveWidgetWidth = 0;

		if (ImGui::MultiCurve(name.c_str(),
			imPoints.data(),
			&nPoints,
			bw::core::Interpolator<float>::MaxPoints,
			&editPoint,
			false,
			scaleMin.x,
			scaleMin.y,
			scaleMax.x,
			scaleMax.y,
			renderValues,
			*timelineT,
			128,
			&curveWidgetWidth))
		{
			// We've either created, deleted or moved a point, depending on the new size.
			// So we may need to update the segments
			if (nPoints > (int)numPoints)
			{
				auto const& p = imPoints[editPoint];
				transactUndoableAction(doc, format("Add {} Point at {:.2f}", name, p.x), bind(addAnimationKeyToInterpolator, placeholders::_1, ref(lerper), p.x, p.y));
			}
			else if (nPoints < (int)numPoints)
			{
				transactUndoableAction(doc, format("Remove {} Point {}", name, editPoint), bind(removeAnimationKeyFromInterpolator, placeholders::_1, ref(lerper), editPoint));
			}
			else
			{
				// Moved
				lerper.updatePoint(editPoint, imPoints[editPoint].x, imPoints[editPoint].y);
			}
		}

		// Timeline
		ImGui::SetNextItemWidth((float)curveWidgetWidth);
		float scaledTimeline = *timelineT * (scaleMax.x - scaleMin.x) + scaleMin.x;
		
		if (ImGui::SliderFloat("##Timeline", &scaledTimeline, scaleMin.x, scaleMax.x))
		{
			*timelineT = (scaledTimeline - scaleMin.x) / (scaleMax.x - scaleMin.x);
		}

		// Segments
		string easingsStr;

		for (auto const& easing : gEasingStrings)
		{
			easingsStr += easing;
			easingsStr += '\0';
		}

		{
			points = lerper.getPoints();
			vector<bw::core::Interpolator<float>::Segment> const& segments = lerper.getSegments();

			auto numPoints = (uint32_t)points.size();
			auto numSegments = (uint32_t)segments.size();

			for (uint32_t i = 0; i < numSegments; ++i)
			{
				// Ignore segments where diff(x) is 0
				if (i < numSegments && points[i].first == points[i + 1].first)
				{
					continue;
				}

				auto& segment = segments[i];

				ImGui::PushID(i);

				ImGui::SetNextItemWidth(128);
				int curEasing = (int)segment.easing;
				if (ImGui::Combo("##Interpolator", &curEasing, easingsStr.c_str(), 6))
				{
					lerper.setEasing(i, (bw::core::Easing)curEasing);
				}

				ImGui::SameLine();
				ImGui::SetNextItemWidth((float)curveWidgetWidth - 128);
				float inputValues[4] = { points[i + 0].first, points[i + 0].second, points[i + 1].first, points[i + 1].second };
				if (ImGui::InputFloat4("##Interpolator", inputValues, "%.2f", ImGuiInputTextFlags_EnterReturnsTrue))
				{
					// Clamp to neighbours to ensure we don't end up with non-ascending values
					if (i > 0)
					{
						inputValues[0] = max(points[i - 1].first, inputValues[0]);
					}

					inputValues[0] = min(points[i + 1].first, inputValues[0]);
					inputValues[2] = max(inputValues[0], inputValues[2]);

					if (i < numSegments)
					{
						inputValues[2] = min(points[i + 2].first, inputValues[2]);
					}

					// Clamp to defined extents
					inputValues[0] = max(scaleMin.x, min(inputValues[0], scaleMax.x));
					inputValues[1] = max(scaleMin.y, min(inputValues[1], scaleMax.y));
					inputValues[2] = max(scaleMin.x, min(inputValues[2], scaleMax.x));
					inputValues[3] = max(scaleMin.y, min(inputValues[3], scaleMax.y));

					transactUndoableAction(doc, "Update Points", [&lerper, i, inputValues](Document* doc) {
						updateAnimationKeyInInterpolator(doc, lerper, i, inputValues[0], inputValues[1]);
						updateAnimationKeyInInterpolator(doc, lerper, i + 1, inputValues[2], inputValues[3]);
					});
				}

				ImGui::PopID();
			}
		}

		ImGui::PopID();
	}

	void renderInterpolator(editor::Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key)
	{
		string keyName;
		float t;

		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			keyName = "Scale";
			break;

		case bw::core::VertexTransformer::Key::Angle:
			keyName = "Angle";
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			keyName = "Orbit Angle";
			break;

		case bw::core::VertexTransformer::Key::OrbitDistance:
			keyName = "Orbit Distance";
			break;
		}

		t = primitive->getT(key);
		renderInterpolator(doc, primitive, primitive->getAnimationInterpolator(key), (int)key + 3, keyName, &t);
		primitive->setT(key, t);
	}

	void renderInputs(editor::Document* doc, bw::core::Primitive* primitive)
	{
		float t = primitive->getT(bw::core::InputType::InfluenceEyeDistance);
		renderInterpolator(doc, primitive, primitive->getInputInterpolator(bw::core::InputType::InfluenceEyeDistance), 0, "Eye distance", &t);
		primitive->setT(bw::core::InputType::InfluenceEyeDistance, t);

		t = primitive->getT(bw::core::InputType::InfluenceEyeAngle);
		renderInterpolator(doc, primitive, primitive->getInputInterpolator(bw::core::InputType::InfluenceEyeAngle), 1, "Eye angle", &t);
		primitive->setT(bw::core::InputType::InfluenceEyeAngle, t);

		t = primitive->getT(bw::core::InputType::PlayerAngle);
		renderInterpolator(doc, primitive, primitive->getInputInterpolator(bw::core::InputType::PlayerAngle), 2, "Player angle", &t);
		primitive->setT(bw::core::InputType::PlayerAngle, t);
	}

	void renderEditPrimitive(editor::Document* doc, bw::core::Primitive* primitive, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		setOperationWidget(primitive, 1);
		setFillRuleWidget(primitive, 1);

		int primitivePriority = (int)primitive->getPriority();
		if (ImGui::SliderInt("Priority##EditPrimitive", &primitivePriority, 0, 255))
		{
			transactUndoableAction(doc, format("Set Primitive Priority to {}", primitivePriority),
				bind(setPrimitivePriority, placeholders::_1, primitive, (uint8_t)primitivePriority));
		}

		wp::Vector2 primitiveOrigin = primitive->getOrigin();

		float pOrigin[2] = { primitiveOrigin.x, primitiveOrigin.y };
		if (ImGui::InputFloat2("Origin##EditPrimitive", pOrigin))
		{
			wp::Vector2 origin{ pOrigin[0], pOrigin[1] };

			transactUndoableAction(doc, "Set Primitive Origin",
				bind(setPrimitiveOrigin, placeholders::_1, primitive, origin));
		}

		wp::Vector2 primitiveInfluenceOriginOffset = primitive->getInfluenceOriginOffset();

		float pInfluenceOriginOffset[2] = { primitiveInfluenceOriginOffset.x, primitiveInfluenceOriginOffset.y };
		if (ImGui::InputFloat2("Influence Origin Offset##EditPrimitive", pInfluenceOriginOffset))
		{
			wp::Vector2 influenceOriginOffset{ pInfluenceOriginOffset[0], pInfluenceOriginOffset[1] };

			transactUndoableAction(doc, "Set Primitive Influence Origin Offset",
				bind(setPrimitiveInfluenceOriginOffset, placeholders::_1, primitive, influenceOriginOffset));
		}

		if (primitive->getType() == "Regular")
		{
			renderEditRegularPolygon(primitive, editorSettings, renderSettings);
		}
		else if (primitive->getType() == "Circle")
		{
			renderEditCirclePolygon(primitive, editorSettings, renderSettings);
		}
		else if (primitive->getType() == "Rectangle")
		{
			renderEditRectanglePolygon(primitive, editorSettings, renderSettings);
		}
		else if (primitive->getType() == "Path")
		{
			renderEditPathPolygon(primitive, editorSettings, renderSettings);
		}
		else if (primitive->getType() == "Superformula")
		{
			renderEditSuperformulaPolygon(primitive, editorSettings, renderSettings);
		}
		else
		{
			throw EditorException("Unknown primitive type: " + primitive->getType());
		}

		bool orientOrbitAngle = primitive->getFollowOrbitAngle();
		if (ImGui::Checkbox("Follow orbit angle", &orientOrbitAngle))
		{
			string action = orientOrbitAngle ? "Set Primitive angle to Orbit" : "Unset Primitive Angle to Orbit";

			transactUndoableAction(doc, action,
				bind(setPrimitiveFollowOrbitAngle, placeholders::_1, primitive, orientOrbitAngle));

			primitive->setFollowOrbitAngle(orientOrbitAngle);
		}

		// Inputs
		if (ImGui::CollapsingHeader("Inputs"))
		{
			renderInputs(doc, primitive);
		}

		// T-transforms
		if (ImGui::CollapsingHeader("Transforms"))
		{
			renderTransformFlow(doc, primitive, bw::core::VertexTransformer::Key::Scale);
			renderTransformFlow(doc, primitive, bw::core::VertexTransformer::Key::Angle);
			renderTransformFlow(doc, primitive, bw::core::VertexTransformer::Key::OrbitAngle);
			renderTransformFlow(doc, primitive, bw::core::VertexTransformer::Key::OrbitDistance);
		}

		// Interpolators
		if (ImGui::CollapsingHeader("Animation"))
		{
			renderInterpolator(doc, primitive, bw::core::VertexTransformer::Key::Scale);
			renderInterpolator(doc, primitive, bw::core::VertexTransformer::Key::Angle);
			renderInterpolator(doc, primitive, bw::core::VertexTransformer::Key::OrbitAngle);
			renderInterpolator(doc, primitive, bw::core::VertexTransformer::Key::OrbitDistance);
		}
	}

	void renderEditPrimitive(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		auto world = doc->getWorld();
		auto const& selectedIndices = doc->getSelectedPrimitiveIndices();

		switch (selectedIndices.size())
		{
		case 0:
			break;

		case 1:
			renderEditPrimitive(doc, world->getPrimitive(*selectedIndices.begin()), editorSettings, renderSettings);
			break;

		default:
			ImGui::Text("Multiple primitives selected.");
			break;
		}
	}

	void renderEditPrimitivePanel(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		auto windowFlags = 0;

		bool docIsActive = doc->isActive();

		auto const& selectedIndices = doc->getSelectedPrimitiveIndices();

		if (!selectedIndices.empty())
		{
			if (ImGui::CollapsingHeader("Edit Primitive", nullptr, windowFlags))
			{
				renderEditPrimitive(doc, editorSettings, renderSettings);
			}
		}
	}

	void renderPrimitiveOrderView(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		if (ImGui::Begin("Clip order"))
		{
			auto primitives = doc->getWorld()->getPrimitivesByPriority();
			auto numPrimitives = (uint32_t)primitives.size();

			for (uint32_t i = 0; i < numPrimitives; ++i)
			{
				ImGui::PushID(i);

				auto primitive = primitives[i];

				if (ImGui::Button(ICON_FA_HAND_POINTER))
				{
					auto selectId = primitive->getId();
					auto f = bind(editor::selectPrimitive, placeholders::_1, selectId);
					editor::transactUndoableAction(doc, format("Select Primitive {}", selectId), f);
				}

				ImGui::SameLine();

				int primitivePriority = (int)primitive->getPriority();
				ImGui::Text("%s :: Priority %d", primitive->getName().c_str(), primitivePriority);

				int counter = 0;
				ImGui::PushButtonRepeat(false);

				if (i > 0)
				{
					ImGui::SameLine();
					if (ImGui::ArrowButton("##up", ImGuiDir_Up))
					{
						counter--;
					}
				}

				if (i < (numPrimitives - 1))
				{
					ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
					if (ImGui::ArrowButton("##down", ImGuiDir_Down))
					{
						counter++;
					}
				}

				ImGui::PopButtonRepeat();

				if (counter < 0)
				{
					auto prevPriority = primitives[i - 1]->getPriority();
					primitives[i - 1]->setPriority(primitive->getPriority());
					primitive->setPriority(prevPriority);
				}
				else if (counter > 0)
				{
					auto nextPriority = primitives[i + 1]->getPriority();
					primitives[i + 1]->setPriority(primitive->getPriority());
					primitive->setPriority(nextPriority);
				}

				if (i != 0)
				{
					setOperationWidget(primitive, 2);
					setFillRuleWidget(primitive, 2);
				}

				ImGui::Separator();

				ImGui::PopID();
			}
		}
	
		ImGui::End();
	}

	void renderPrimitiveView(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		if (ImGui::Begin("Primitives"))
		{
			renderCreatePrimitivePanel(doc, editorSettings, renderSettings);
			renderEditPrimitivePanel(doc, editorSettings, renderSettings);
		}

		ImGui::End();
	}

	void renderHistoryView(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		if (ImGui::Begin("History"))
		{
			auto history = getActionHistory();

			if (history.empty())
			{
				ImGui::End();
				return;
			}

			// Get number of undos and redos ahead of time
			uint32_t undoCount = 0;
			for (; undoCount < (uint32_t)history.size(); ++undoCount)
			{
				if (!history[undoCount].isUndo)
				{
					break;
				}
			}

			uint32_t redoCount = (uint32_t)history.size() - undoCount;

			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1, 0.7f, 0.7f, 1));
			
			for (uint32_t i = 0; i < (uint32_t)history.size(); ++i)
			{
				auto const& item = history[i];
				bool isUndo = i < undoCount;

				ImGui::PushID(i);

				if (i == undoCount)
				{
					ImGui::Separator();

					ImGui::PopStyleColor();
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.7f, 1.0f, 0.7f, 1));
				}

				if (ImGui::Button(isUndo ? ICON_FA_UNDO : ICON_FA_REDO))
				{
					// Work out how many to go back or forward
					if (isUndo)
					{
						undo(doc, undoCount - i);
					}
					else
					{
						redo(doc, (i + 1) - undoCount);
					}
				}

				ImGui::SameLine();
				ImGui::Text(item.id.c_str());

				ImGui::PopID();
			}

			ImGui::PopStyleColor();
		}
	
		ImGui::End();
	}

	bool gMovePlayerProxy = false;

	void renderMainWindow(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings)
	{
		// Shortcuts
		if (ImGui::Shortcut(ImGuiKey_Delete, 0, ImGuiInputFlags_RouteGlobalLow))
		{
			if (!ImGui::IsAnyItemActive() && !ImGui::IsAnyItemFocused())
			{
				if (doc->hasSelection())
				{
					auto index = *doc->getSelectedPrimitiveIndices().begin();
					transactUndoableAction(doc, format("Delete Primitive {}", index), bind(deletePrimitive, placeholders::_1, index));
				}
			}
		}
		if (ImGui::Shortcut(ImGuiKey_Space, 0, ImGuiInputFlags_RouteGlobalLow))
		{
			if (!ImGui::IsAnyItemActive() && !ImGui::IsAnyItemFocused())
			{
				gMovePlayerProxy = !gMovePlayerProxy;
			}
		}

		if (gMovePlayerProxy)
		{
			auto worldPos = getMouseWorldPosition();
			doc->setPlayerProxyPosition(worldPos);
		}

		renderMenu(doc, editorSettings, renderSettings);
		renderToolbar(doc, editorSettings, renderSettings);
		renderStatusbar(doc, editorSettings, renderSettings);

		if (doc->isActive())
		{
			renderPrimitiveView(doc, editorSettings, renderSettings);
			renderPrimitiveOrderView(doc, editorSettings, renderSettings);
			renderHistoryView(doc, editorSettings, renderSettings);
		}
	}

} // editor