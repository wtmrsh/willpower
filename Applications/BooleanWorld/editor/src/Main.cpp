#define NOMINMAX

#include <Windows.h>

#include <glew/glew.h>
#include <nfd/nfd.h>

#pragma warning(push)
#pragma warning(disable: 4307)
#include <spdlog/spdlog.h>
#pragma warning(pop)
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <SDL/SDL.h>
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <SDL/SDL_opengles2.h>
#else
#include <SDL/SDL_opengl.h>
#endif

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_opengl3.h"
#include "imnodes.h"
#include "IconsFontAwesome5.h"

#include "Defines.h"
#include "Document.h"
#include "EditorSettings.h"
#include "RenderSettings.h"
#include "Render.h"
#include "UI.h"
#include "Undo.h"
#include "Actions.h"
#include "EditorException.h"
#include "ExitApplicationException.h"


spdlog::logger* gLogger{ nullptr };
SDL_Window* gWindow{ nullptr };
SDL_GLContext gContext;

static editor::EditorSettings gEditorSettings;
static editor::RenderSettings gRenderSettings;

using namespace std;

SDL_Window* createWindow()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
	{
		printf("Error: %s\n", SDL_GetError());
		return nullptr;
	}

	// GL 3.0 + GLSL 130
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	// From 2.0.18: Enable native IME.
#ifdef SDL_HINT_IME_SHOW_UI
	SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
#endif

	// Create window with graphics context
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
	SDL_Window* window = SDL_CreateWindow("Editor", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT, window_flags);

	gLogger->info("Window created");

	return window;
}

SDL_GLContext createContext(SDL_Window* window)
{
	SDL_GLContext gl_context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, gl_context);
	SDL_GL_SetSwapInterval(1); // Enable vsync

	gLogger->info("OpenGL context created");

	return gl_context;
}

void setupImGui(SDL_Window* window, SDL_GLContext context)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImNodes::CreateContext();
	ImNodes::PushAttributeFlag(ImNodesAttributeFlags_EnableLinkDetachWithDragClick);

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();

	// Setup Platform/Renderer backends
	ImGui_ImplSDL2_InitForOpenGL(window, context);

	const char* glsl_version = "#version 130";
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Use '#define IMGUI_ENABLE_FREETYPE' in your imconfig file to use Freetype for higher quality font rendering.
	// - Read 'docs/FONTS.md' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	// - Our Emscripten build process allows embedding fonts to be accessible at runtime from the "fonts/" folder. See Makefile.emscripten for details.
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);

	gLogger->info("ImGui set up");
}

void setupLogging()
{
	auto fileSink = make_shared<spdlog::sinks::basic_file_sink_mt>("logs/editor.log", true);

#ifdef _DEBUG
	auto consoleSink = make_shared<spdlog::sinks::stdout_color_sink_mt>();
	consoleSink->set_level(spdlog::level::debug);

	fileSink->set_level(spdlog::level::debug);

	gLogger = new spdlog::logger("editor", { consoleSink, fileSink });
#else
	fileSink->set_level(spdlog::level::info);

	gLogger = new spdlog::logger("editor", { fileSink });
#endif

	gLogger->set_level(spdlog::level::debug);
}

void initialise()
{
	setupLogging();

	//
	// Set up SDL
	//
	gWindow = createWindow();
	gContext = createContext(gWindow);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		throw exception("GLEW initialisation failed");
	}

	// Global settings
	gEditorSettings = {
		false,  // Show grid
		true,   // Render triangulation border 
		true,   // Render primitive borders
		false,  // Render primitive bounding boxes
		32.0f   // Grid size
	};

	gRenderSettings = { 
		wp::Vector2::ZERO  // View offset
	};

	// World rendering
	int frameBufferWidth, frameBufferHeight;
	SDL_GL_GetDrawableSize(gWindow, &frameBufferWidth, &frameBufferHeight);
	initialiseWorldRendering(frameBufferWidth, frameBufferHeight, editor::Document::instance(), gEditorSettings, gRenderSettings);

	//
	// Set up ImGui
	//
	setupImGui(gWindow, gContext);
}

void setup()
{
	// Set up NFD (file dialogs)
	NFD_Init();

	// ImGui extra twiddling
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	io.Fonts->AddFontDefault();

	float baseFontSize = 13.0f; // 13.0f is the size of the default font. Change to the font size you use.
	float iconFontSize = baseFontSize * 2.0f / 3.0f; // FontAwesome fonts need to have their sizes reduced by 2.0f/3.0f in order to align correctly

	static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_16_FA, 0 };
	ImFontConfig icons_config;
	icons_config.MergeMode = true;
	icons_config.PixelSnapH = true;
	icons_config.GlyphMinAdvanceX = iconFontSize;
	io.Fonts->AddFontFromFileTTF(FONT_ICON_FILE_NAME_FAS, iconFontSize, &icons_config, icons_ranges);
	io.Fonts->Build();
}

void shutdown()
{
	gLogger->info("Shutting down");

	delete gLogger;
	gLogger = nullptr;

	// ImGui
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	// Platform
	SDL_GL_DeleteContext(gContext);
	SDL_DestroyWindow(gWindow);
	SDL_Quit();

	NFD_Quit();
}

bool processEvents(SDL_Window* window)
{
	// Poll and handle events (inputs, window resize, etc.)
	// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
	// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
	// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
	// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
	bool done = false;
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		ImGui_ImplSDL2_ProcessEvent(&event);
		if (event.type == SDL_QUIT)
		{
			done = true;
		}
		if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
		{
			done = true;
		}
	}

	return done;
}

uint32_t handleSelections(editor::Document* doc)
{
	auto hoveredPrimitiveIndex = editor::getHoveredPrimitiveIndex(doc);

	// Select single primitive on left-mouse click
	if (ImGui::IsMouseClicked(0))
	{
		if (hoveredPrimitiveIndex != ~0u)
		{
			auto f = bind(editor::selectPrimitive, placeholders::_1, hoveredPrimitiveIndex);
			editor::transactUndoableAction(doc, format("Select Primitive {}", hoveredPrimitiveIndex), f);
		}
	}

	// Clear selection on right-mouse click on non-selected primitive
	if (ImGui::IsMouseClicked(1))
	{
		if (hoveredPrimitiveIndex == ~0u)
		{
			editor::transactUndoableAction(doc, "Clear Selected Primitives", editor::clearSelectedPrimitives);
		}
		else
		{
			// Context menu?
			// ...
		}
	}

	if (hoveredPrimitiveIndex != ~0u)
	{
		auto const& selectedPrimitiveIndices = editor::Document::instance()->getSelectedPrimitiveIndices();

		if (selectedPrimitiveIndices.find(hoveredPrimitiveIndex) != selectedPrimitiveIndices.end())
		{
			ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);
		}
	}

	return hoveredPrimitiveIndex;
}

void run()
{
	ImVec4 clearColour = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	ImGuiIO& io = ImGui::GetIO();

	//
	// Main loop
	//
	LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
	LARGE_INTEGER Frequency;

	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&StartingTime);

	bool done = false, showDemoWindow = false;
	while (!done)
	{
		// Get elapsed time
		QueryPerformanceCounter(&EndingTime);
		ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
		ElapsedMicroseconds.QuadPart *= 1000000;
		ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;

		StartingTime = EndingTime;

		auto updateTimeMicros = ElapsedMicroseconds.QuadPart;

		// Events
		done = processEvents(gWindow);

		// Logic
		if (editor::Document::instance()->isActive())
		{
			auto const& proxyPos = editor::Document::instance()->getPlayerProxyPosition();
			auto proxyAngle = editor::Document::instance()->getPlayerProxyAngle();

			editor::Document::instance()->getWorld()->update(updateTimeMicros / 1000000.0f, proxyPos, proxyAngle);
		}

		// Rendering
		resizeFramebuffer(ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT);
		glViewport(0, 0, ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT);
		glClearColor(clearColour.x * clearColour.w, clearColour.y * clearColour.w, clearColour.z * clearColour.w, clearColour.w);
		glClear(GL_COLOR_BUFFER_BIT);
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame();

		ImGui::NewFrame();

		// Render to imgui window
		resizeFramebuffer(ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT);
		glViewport(0, 0, ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT);

		/*
		ImGui::GetBackgroundDrawList()->AddImage(
			(void*)(size_t)getWorldTextureId(),
			ImVec2(0, 0),
			ImVec2(ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT),
			ImVec2(0, 1),
			ImVec2(1, 0)
		);
		*/

		// Set up selections, eg for logic on them
		if (!io.WantCaptureMouse)
		{
			auto hoveredPrimitiveIndex = handleSelections(editor::Document::instance());
		}

		editor::renderMainWindow(editor::Document::instance(), gEditorSettings, gRenderSettings);

		if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_F12)))
		{
			showDemoWindow = !showDemoWindow;
		}

		if (showDemoWindow)
		{
			ImGui::SetNextWindowFocus();
			ImGui::ShowDemoWindow();
		}

		renderWorld(editor::Document::instance(), gEditorSettings, gRenderSettings);

		// Rendering
		ImGui::Render();

		// Old, direct way of rendering world
		//renderWorldToTexture(editor::Document::instance(), gEditorSettings, gRenderSettings);

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(gWindow);
	}
}

void outputToDebugger(std::string const& msg)
{
#ifdef _DEBUG
	char const* msgc = msg.c_str();

	size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msgc, (int)strlen(msgc), 0, 0);
	std::wstring ret(reqLength, L'\0');

	::MultiByteToWideChar(CP_UTF8, 0, msgc, (int)strlen(msgc), &ret[0], (int)ret.length());
	OutputDebugString(ret.c_str());
#endif
}

void outputException(std::string const& msg)
{
	gLogger->critical(msg);
	outputToDebugger(msg);
}

//
// Entrypoint
//
int main(int, char**)
{
	int exitCode{ 0 };

	initialise();

	try
	{
		setup();
		run();
	}
	catch (ExitApplicationException& e)
	{
		exitCode = e.getExitCode();
		outputException(e.getMessage());
	}
	catch (EditorException& e)
	{
		exitCode = 1;
		outputException(e.getMessage());
	}
	catch (exception& e)
	{
		exitCode = 1;
		outputException(e.what());
	}

	shutdown();
	
	return exitCode;
}
