#define NOMINMAX

#include <Windows.h>
#include <algorithm>
#include <exception>

#include <glew/glew.h>

#include <gl/GL.h>

#include "imgui.h"

#include "Defines.h"
#include "Document.h"
#include "Render.h"

#define MAX_RENDER_TRIANGLES		4096
#define MAX_RENDER_LINES			4096
#define MAX_RENDER_VERTICES			4096

using namespace std;

static const uint32_t gVertexStride = 6;
static const uint32_t gLineStride = 6;
static const uint32_t gTriangleStride = 8;
static GLuint FBO;
static GLuint RBO;
static GLuint Texture;


/////////////////////////////////
// Vertex rendering
/////////////////////////////////
static GLuint VerticesVAO;
static GLuint VerticesVBO;
static GLuint VerticesProgram;

static char const* VertexVertexShaderCode = R"*(
#version 440

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec4 in_col;

layout (location = 1) out vec4 out_col;

uniform vec2 u_windowSize;

void main()
{
	vec2 halfSize = u_windowSize * 0.5;
	gl_Position = vec4(in_pos / halfSize, 0, 1);
	gl_PointSize = 4.0;
	out_col = in_col;
}
)*";

static char const* VertexFragmentShaderCode = R"*(
#version 440

layout (location = 1) in vec4 in_col;

layout (location = 0) out vec4 out_col;

void main()
{
	out_col = in_col;
}
)*";

/////////////////////////////////
// Line rendering
/////////////////////////////////
static GLuint GridVAO, PrimitiveBoundsVAO;
static GLuint GridVBO, PrimitiveBoundsVBO;
static GLuint LinesProgram;

static char const* LineVertexShaderCode = R"*(
#version 440

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec4 in_col;

layout (location = 1) out vec4 out_col;

uniform vec2 u_windowSize;

void main()
{
	vec2 halfSize = u_windowSize * 0.5;
	gl_Position = vec4(in_pos / halfSize, 0, 1);
	out_col = in_col;
}
)*";

static char const* LineFragmentShaderCode = R"*(
#version 440

layout (location = 1) in vec4 in_col;

layout (location = 0) out vec4 out_col;

void main()
{
	out_col = in_col;
}
)*";

/////////////////////////////////
// Triangle rendering
/////////////////////////////////
//static GLuint TrianglesVAO;
//static GLuint TrianglesVBO;
static GLuint WorldTrianglesVAO;
static GLuint WorldTrianglesVBO;
static GLuint TrianglesProgram;

static char const* TriangleVertexShaderCode = R"*(
#version 440

layout (location = 0) in vec2 in_pos;
layout (location = 1) in vec2 in_tex;
layout (location = 2) in vec4 in_col;

layout (location = 0) out vec2 out_tex;
layout (location = 1) out vec4 out_col;

uniform vec2 u_windowSize;

void main()
{
	vec2 halfSize = u_windowSize * 0.5;
	gl_Position = vec4(in_pos / halfSize, 0, 1);

	out_tex = in_tex;
	out_col = in_col;
}
)*";

static char const* TriangleFragmentShaderCode = R"*(
#version 440

layout (location = 0) in vec2 in_tex;
layout (location = 1) in vec4 in_col;

layout (location = 0) out vec4 out_col;

void main()
{
	out_col = in_col;
}
)*";

void addShader(GLuint program, const char* shaderCode, GLenum type)
{
	GLuint shader = glCreateShader(type);

	const GLchar* code[1];
	code[0] = shaderCode;

	GLint codeLength[1];
	codeLength[0] = (GLint)strlen(shaderCode);

	glShaderSource(shader, 1, code, codeLength);
	glCompileShader(shader);

	GLint result = 0;
	GLchar log[1024] = { 0 };

	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(shader, sizeof(log), NULL, log);
		throw exception(log);
	}

	glAttachShader(program, shader);
}

GLuint createProgram(char const* vertShaderCode, char const* fragmentShaderCode)
{
	auto program = glCreateProgram();

	addShader(program, vertShaderCode, GL_VERTEX_SHADER);
	addShader(program, fragmentShaderCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar log[1024] = { 0 };

	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(program, sizeof(log), NULL, log);
		throw exception(log);
	}

	glValidateProgram(program);
	glGetProgramiv(program, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(program, sizeof(log), NULL, log);
		throw exception(log);
	}

	return program;
}

void createPrograms()
{
	TrianglesProgram = createProgram(TriangleVertexShaderCode, TriangleFragmentShaderCode);
	VerticesProgram = createProgram(VertexVertexShaderCode, VertexFragmentShaderCode);
	LinesProgram = createProgram(LineVertexShaderCode, LineFragmentShaderCode);
}

void createFramebuffer(int width, int height)
{
	glGenFramebuffers(1, &FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);

	glGenTextures(1, &Texture);
	glBindTexture(GL_TEXTURE_2D, Texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Texture, 0);

	glGenRenderbuffers(1, &RBO);
	glBindRenderbuffer(GL_RENDERBUFFER, RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		throw exception("Framebuffer is not complete.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

void bindFramebuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
}

void unbindFramebuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void resizeFramebuffer(int width, int height)
{
	glBindTexture(GL_TEXTURE_2D, Texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Texture, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);
}

uint32_t getWorldTextureId()
{
	return (uint32_t)Texture;
}

void getGridVertexData(vector<float>& data, uint32_t& numLines, editor::EditorSettings const& settings, editor::RenderSettings const& renderSettings)
{
	const int linesX = (int)(ED_WINDOW_WIDTH / settings.gridSize) + 1;
	const int linesY = (int)(ED_WINDOW_HEIGHT / settings.gridSize) + 1;

	//data.reserve((linesX + linesY) * 2 * gLineStride * sizeof(float));

	// Vertical
	for (int i = 0; i < linesX; ++i)
	{
		float x = i * settings.gridSize - ED_WINDOW_WIDTH / 2;
		float y0 = -ED_WINDOW_HEIGHT / 2;
		float y1 = ED_WINDOW_HEIGHT / 2;

		// Position
		data.push_back(x);
		data.push_back(y0);

		// Colour
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);

		// Position
		data.push_back(x);
		data.push_back(y1);

		// Colour
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
	}

	// Horizontal
	for (int i = 0; i < linesY; ++i)
	{
		float y = i * settings.gridSize - ED_WINDOW_HEIGHT / 2;
		float x0 = -ED_WINDOW_WIDTH / 2;
		float x1 = ED_WINDOW_WIDTH / 2;

		// Position
		data.push_back(x0);
		data.push_back(y);

		// Colour
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);

		// Position
		data.push_back(x1);
		data.push_back(y);

		// Colour
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
		data.push_back(0.35f);
	}

	numLines = (uint32_t)data.size() / (gLineStride * 2);
}

void createGridData(editor::EditorSettings const& settings, editor::RenderSettings const& renderSettings)
{
	vector<float> vertData;
	uint32_t numLines;

	getGridVertexData(vertData, numLines, settings, renderSettings);

	// Lines
	glGenVertexArrays(1, &GridVAO);
	glBindVertexArray(GridVAO);

	glGenBuffers(1, &GridVBO);
	glBindBuffer(GL_ARRAY_BUFFER, GridVBO);
	glBufferData(GL_ARRAY_BUFFER, vertData.size() * sizeof(float), &(vertData[0]), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, gLineStride * sizeof(float), (const GLvoid*)(0 * sizeof(float)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, gLineStride * sizeof(float), (const GLvoid*)(2 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void updateGridData(editor::EditorSettings const& settings, editor::RenderSettings const& renderSettings, uint32_t& numLines)
{
	vector<float> vertData;

	getGridVertexData(vertData, numLines, settings, renderSettings);
	glBindBuffer(GL_ARRAY_BUFFER, GridVBO);
	auto bufferPtr = (int8_t*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	auto lineDataLength = numLines * 2 * gLineStride;
	memcpy(bufferPtr, &(vertData[0]), lineDataLength * sizeof(float));
	glUnmapBuffer(GL_ARRAY_BUFFER);
}

void getWorldData(editor::Document* doc,
	shared_ptr<bw::core::World> world,
	editor::EditorSettings const& editorSettings,
	editor::RenderSettings const& renderSettings,
	vector<float>& vertData,
	uint32_t& numVerts,
	vector<float>& primitiveBoundsData,
	uint32_t& numPrimitiveBoundsLines,
	vector<float>& worldTriData,
	uint32_t& numWorldTris)
{
	const uint32_t maxVertexData = MAX_RENDER_VERTICES * 1 * gVertexStride;
	const uint32_t maxLineData = MAX_RENDER_LINES * 2 * gLineStride;
	const uint32_t maxTriData = MAX_RENDER_TRIANGLES * 3 * gTriangleStride;

	vertData.resize(maxVertexData);
	primitiveBoundsData.resize(maxLineData);
	worldTriData.resize(maxTriData);

	if (!world)
	{
		numVerts = 0;
		numWorldTris = 0;
		return;
	}

	wp::BoundingBox viewBounds(renderSettings.origin, wp::Vector2(ED_WINDOW_WIDTH, ED_WINDOW_WIDTH));

	// Get all primitives for now
	uint32_t numPrimitives = world->getNumPrimitives();
	set<uint32_t> primitiveIndices;
	for (uint32_t i = 0; i < numPrimitives; ++i)
	{
		primitiveIndices.insert(i);
	}

	vector<bw::core::ClippedPolygon> clippedPolygons;

	auto triVertices = world->triangulate(viewBounds, primitiveIndices, &clippedPolygons);

	int wt = 0;
	uint32_t numWorldTriVertices = (uint32_t)triVertices.size();
	for (uint32_t i = 0; i < numWorldTriVertices; i += 3)
	{
		if (wt + 18 >= maxTriData)
		{
			goto tris_done;
		}

		worldTriData[wt++] = triVertices[i + 0].x;
		worldTriData[wt++] = triVertices[i + 0].y;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0.2f;
		worldTriData[wt++] = 0.3f;
		worldTriData[wt++] = 0.8f;
		worldTriData[wt++] = 1.0f;

		worldTriData[wt++] = triVertices[i + 1].x;
		worldTriData[wt++] = triVertices[i + 1].y;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0.2f;
		worldTriData[wt++] = 0.3f;
		worldTriData[wt++] = 0.8f;
		worldTriData[wt++] = 1.0f;

		worldTriData[wt++] = triVertices[i + 2].x;
		worldTriData[wt++] = triVertices[i + 2].y;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0;
		worldTriData[wt++] = 0.2f;
		worldTriData[wt++] = 0.3f;
		worldTriData[wt++] = 0.8f;
		worldTriData[wt++] = 1.0f;
	}

tris_done:;

	// Lines
	int l = 0, bl = 0;

	auto hoveredPrimitiveIndex = doc->getHoveredPrimitiveIndex();
	auto const& selectedPrimitiveIndices = doc->getSelectedPrimitiveIndices();

	auto primitives = world->getPrimitives();

	if (editorSettings.renderTriangulationBorder)
	{
		for (auto const& clippedPolygon : clippedPolygons)
		{
			auto numPolyVertices = (uint32_t)clippedPolygon.vertices.size();

			for (uint32_t i = 0; i < numPolyVertices; ++i)
			{
				if (l + 12 > maxLineData)
				{
					goto lines_done;
				}

				auto v0 = clippedPolygon.vertices[i];
				auto v1 = clippedPolygon.vertices[(i + 1) % numPolyVertices];

				/*
				lineData[l++] = v0.x;
				lineData[l++] = v0.y;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;

				lineData[l++] = v1.x;
				lineData[l++] = v1.y;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;
				lineData[l++] = 1.0f;
				*/
			}
		}
	}

	for (uint32_t i = 0; i < primitives.size(); ++i)
	{
		auto const* primitive = primitives[i];
		bool hovered = hoveredPrimitiveIndex == i;
		bool selected = selectedPrimitiveIndices.find(i) != selectedPrimitiveIndices.end();

		if (editorSettings.renderPrimitiveBorders || hovered || selected)
		{
			auto primVertices = primitive->getVertices();
			auto numPrimVertices = (uint32_t)primVertices.size();

			for (uint32_t j = 0; j < numPrimVertices; ++j)
			{
				if (l + 12 > maxLineData)
				{
					goto lines_done;
				}

				auto v0 = primVertices[j];
				auto v1 = primVertices[(j + 1) % numPrimVertices];

				float c[3];
				if (selected)
				{
					c[0] = 1.0f;
					c[1] = 0.0f;
					c[2] = 0.0f;
				}
				else if (hovered)
				{
					c[0] = 1.0f;
					c[1] = 0.5f;
					c[2] = 0.0f;
				}
				else
				{
					c[0] = 0.5f;
					c[1] = 0.5f;
					c[2] = 0.5f;
				}

				/*
				lineData[l++] = v0.p.x;
				lineData[l++] = v0.p.y;

				lineData[l++] = c[0];
				lineData[l++] = c[1];
				lineData[l++] = c[2];
				lineData[l++] = 1.0f;

				lineData[l++] = v1.p.x;
				lineData[l++] = v1.p.y;

				lineData[l++] = c[0];
				lineData[l++] = c[1];
				lineData[l++] = c[2];
				lineData[l++] = 1.0f;
				*/
			}
		}
		if (editorSettings.renderPrimitiveBounds)
		{
			auto const& bounds = primitive->getBounds();
				
			wp::Vector2 minExtent, maxExtent;
			bounds.getExtents(minExtent, maxExtent);

			if (bl + 48 > maxLineData)
			{
				goto lines_done;
			}

			primitiveBoundsData[bl++] = minExtent.x;
			primitiveBoundsData[bl++] = minExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = maxExtent.x;
			primitiveBoundsData[bl++] = minExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = maxExtent.x;
			primitiveBoundsData[bl++] = minExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = maxExtent.x;
			primitiveBoundsData[bl++] = maxExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = maxExtent.x;
			primitiveBoundsData[bl++] = maxExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = minExtent.x;
			primitiveBoundsData[bl++] = maxExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = minExtent.x;
			primitiveBoundsData[bl++] = maxExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;

			primitiveBoundsData[bl++] = minExtent.x;
			primitiveBoundsData[bl++] = minExtent.y;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
			primitiveBoundsData[bl++] = 0.0f;
			primitiveBoundsData[bl++] = 1.0f;
		}
	}

lines_done:;

	numVerts = 0;
	numPrimitiveBoundsLines = bl / (gLineStride * 2);
	numWorldTris = wt / (gTriangleStride * 3);
}

void createWorldData(editor::Document* doc, shared_ptr<bw::core::World> world, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings)
{
	vector<float> worldVertData, primitiveBoundsData, worldTriData;
	uint32_t numVerts, numPrimitiveBoundsLines, numWorldTris;

	getWorldData(doc, world, editorSettings, renderSettings, worldVertData, numVerts, primitiveBoundsData, numPrimitiveBoundsLines, worldTriData, numWorldTris);

	// Vertices
	glGenVertexArrays(1, &VerticesVAO);
	glBindVertexArray(VerticesVAO);

	glGenBuffers(1, &VerticesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, VerticesVBO);

	glBufferData(GL_ARRAY_BUFFER, worldVertData.size() * sizeof(float), &(worldVertData[0]), GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, gVertexStride * sizeof(float), (const GLvoid*)(0 * sizeof(float)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, gVertexStride * sizeof(float), (const GLvoid*)(2 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Primitive bounding boxes
	glGenVertexArrays(1, &PrimitiveBoundsVAO);
	glBindVertexArray(PrimitiveBoundsVAO);

	glGenBuffers(1, &PrimitiveBoundsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, PrimitiveBoundsVBO);

	glBufferData(GL_ARRAY_BUFFER, primitiveBoundsData.size() * sizeof(float), &(primitiveBoundsData[0]), GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, gLineStride * sizeof(float), (const GLvoid*)(0 * sizeof(float)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, gLineStride * sizeof(float), (const GLvoid*)(2 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Triangles
	/*
	glGenVertexArrays(1, &TrianglesVAO);
	glBindVertexArray(TrianglesVAO);

	glGenBuffers(1, &TrianglesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, TrianglesVBO);

	glBufferData(GL_ARRAY_BUFFER, worldTriData.size() * sizeof(float), &(worldTriData[0]), GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(0 * sizeof(float)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(2 * sizeof(float)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(4 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	*/

	// World Triangles
	glGenVertexArrays(1, &WorldTrianglesVAO);
	glBindVertexArray(WorldTrianglesVAO);

	glGenBuffers(1, &WorldTrianglesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, WorldTrianglesVBO);

	glBufferData(GL_ARRAY_BUFFER, worldTriData.size() * sizeof(float), &(worldTriData[0]), GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(0 * sizeof(float)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(2 * sizeof(float)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, gTriangleStride * sizeof(float), (const GLvoid*)(4 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void updateWorldData(editor::Document* doc, shared_ptr<bw::core::World> world, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings, uint32_t& numVerts, uint32_t& numPrimitiveBoundsLines, uint32_t& numWorldTris)
{
	vector<float> worldVertData, primitiveBoundsData, worldTriData;
	getWorldData(doc, world, editorSettings, renderSettings, worldVertData, numVerts, primitiveBoundsData, numPrimitiveBoundsLines, worldTriData, numWorldTris);

	// Vertices
	if (numVerts > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, VerticesVBO);
		auto bufferPtr = (int8_t*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		auto vertDataLength = numVerts * 1 * gVertexStride;
		memcpy(bufferPtr, &(worldVertData[0]), vertDataLength * sizeof(float));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}

	// Primitive bounding boxes
	if (numPrimitiveBoundsLines > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, PrimitiveBoundsVBO);
		auto bufferPtr = (int8_t*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		auto lineDataLength = numPrimitiveBoundsLines * 2 * gLineStride;
		memcpy(bufferPtr, &(primitiveBoundsData[0]), lineDataLength * sizeof(float));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}

	// World Triangles
	if (numWorldTris > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, WorldTrianglesVBO);
		auto bufferPtr = (int8_t*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

		auto triDataLength = numWorldTris * 3 * gTriangleStride;
		memcpy(bufferPtr, &(worldTriData[0]), triDataLength * sizeof(float));
		glUnmapBuffer(GL_ARRAY_BUFFER);
	}
}

void initialiseWorldRendering(int width, int height, editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings)
{
	createWorldData(doc, doc->getWorld(), editorSettings, renderSettings);
	createGridData(editorSettings, renderSettings);
	createPrograms();
	createFramebuffer(width, height);
}

void renderWorldToTexture(editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings)
{
	/*
	Split up different renders, rather than doing it by primitive type:
	1. World triangles (triangles)
	   - Special mode where the z coord stores primitive index, and we get the operation type from it,
	     and set the vertex colour based on the operation type.  (We will want to store the primitive index
		 in z anyway).
	2. World border (poly lines: see ImDrawList::AddPolyline)
	3. Selected primitive outline (poly lines)
	4. Primitive AABB (lines)
	5. Primitive influence origin (?)
	6. Primitive influence circles (polylines, special case as they're circles)
	7. Player proxy (?) and direction (line)
	*/


	uint32_t numWorldVerts = 0;
	uint32_t numPrimitiveBoundsLines = 0;
	uint32_t numWorldTris = 0;

	updateWorldData(doc, doc->getWorld(), editorSettings, renderSettings, numWorldVerts, numPrimitiveBoundsLines, numWorldTris);

	bindFramebuffer();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	GLuint windowSize = glGetUniformLocation(TrianglesProgram, "u_windowSize");

	// Triangles
	/*
	glUseProgram(TrianglesProgram);
	glUniform2f(windowSize, (float)ED_WINDOW_WIDTH, (float)ED_WINDOW_HEIGHT);

	glBindVertexArray(TrianglesVAO);
	glDrawArrays(GL_TRIANGLES, 0, numWorldTris * 3);
	glBindVertexArray(0);
	glUseProgram(0);
	*/

	// World triangles
	glUseProgram(TrianglesProgram);
	glUniform2f(windowSize, (float)ED_WINDOW_WIDTH, (float)ED_WINDOW_HEIGHT);

	glBindVertexArray(WorldTrianglesVAO);
	glDrawArrays(GL_TRIANGLES, 0, numWorldTris * 3);
	glBindVertexArray(0);
	glUseProgram(0);

	// Bounding boxes
	if (numPrimitiveBoundsLines > 0)
	{
		glUseProgram(LinesProgram);
		glUniform2f(windowSize, (float)ED_WINDOW_WIDTH, (float)ED_WINDOW_HEIGHT);

		glBindVertexArray(PrimitiveBoundsVAO);
		glDrawArrays(GL_LINES, 0, numPrimitiveBoundsLines * 2);
		glBindVertexArray(0);
		glUseProgram(0);
	}

	// Vertices
	if (false)
	{
		glUseProgram(VerticesProgram);
		glUniform2f(windowSize, (float)ED_WINDOW_WIDTH, (float)ED_WINDOW_HEIGHT);

		glBindVertexArray(VerticesVAO);
		glDrawArrays(GL_POINTS, 0, numWorldVerts);
		glBindVertexArray(0);
		glUseProgram(0);
	}

	// Grid
	if (editorSettings.showGrid)
	{
		uint32_t numGridLines = 0;

		updateGridData(editorSettings, renderSettings, numGridLines);

		glUseProgram(LinesProgram);

		windowSize = glGetUniformLocation(LinesProgram, "u_windowSize");
		glUniform2f(windowSize, (float)ED_WINDOW_WIDTH, (float)ED_WINDOW_HEIGHT);

		glBindVertexArray(GridVAO);
		glDrawArrays(GL_LINES, 0, numGridLines);
		glBindVertexArray(0);
		glUseProgram(0);
	}

	unbindFramebuffer();
}

void renderWorld(editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings)
{
	auto world = doc->getWorld();

	if (!world)
	{
		return;
	}

	// Triangulate, etc
	auto windowSize = wp::Vector2(ED_WINDOW_WIDTH, ED_WINDOW_HEIGHT);
	wp::BoundingBox viewBounds(renderSettings.origin - windowSize / 2, windowSize);

	// Get all primitives for now
	uint32_t numPrimitives = world->getNumPrimitives();

	if (numPrimitives == 0)
	{
		return;
	}

	set<uint32_t> primitiveIndices;
	for (uint32_t i = 0; i < numPrimitives; ++i)
	{
		primitiveIndices.insert(i);
	}

	vector<bw::core::ClippedPolygon> clippedPolygons;
	auto triVertices = world->triangulate(viewBounds, primitiveIndices, &clippedPolygons);

	// Render
	auto drawList = ImGui::GetBackgroundDrawList();
	auto offset = viewBounds.getMinExtent();
	
	ImColor backgroundColour(0.2f, 0.3f, 1.0f, 1.0f);
	ImColor borderColour(1.0f, 0.7f, 0.2f, 1.0f);
	ImColor boundsColour(0.2f, 1.0f, 0.3f, 1.0f);
	ImColor primitiveColour(0.5f, 0.5f, 0.5f, 1.0f);
	ImColor selectedPrimitiveColour(1.0f, 0.3f, 0.3f, 1.0f);
	ImColor hoveredPrimitiveColour(1.0f, 0.1f, 0.1f, 1.0f);
	ImColor influenceEyeColour(0.9f, 0.8f, 0.7f, 1.0f);
	ImColor influenceColours[3] = {
		{1.0f, 0.3f, 0.3f, 1.0f},
		{0.3f, 1.0f, 0.3f, 1.0f},
		{0.3f, 0.3f, 0.1f, 1.0f}
	};
	ImColor playerProxyColour(1.0f, 0.5f, 0.0f, 1.0f);

	auto hoveredPrimitiveIndex = doc->getHoveredPrimitiveIndex();
	auto const& selectedPrimitiveIndices = doc->getSelectedPrimitiveIndices();

	// Triangulation
	uint32_t numWorldTriVertices = (uint32_t)triVertices.size();

	if (numWorldTriVertices > 0)
	{
		drawList->AddDrawCmd();
		drawList->Flags &= ~ImDrawListFlags_AntiAliasedFill;

		for (uint32_t i = 0; i < numWorldTriVertices; i += 3)
		{
			drawList->AddTriangleFilled(
				{ triVertices[i + 0].x - offset.x, ED_WINDOW_HEIGHT - (triVertices[i + 0].y - offset.y) },
				{ triVertices[i + 1].x - offset.x, ED_WINDOW_HEIGHT - (triVertices[i + 1].y - offset.y) },
				{ triVertices[i + 2].x - offset.x, ED_WINDOW_HEIGHT - (triVertices[i + 2].y - offset.y) },
				backgroundColour);
		}

		// World border
		if (editorSettings.renderTriangulationBorder)
		{
			drawList->AddDrawCmd();

			for (auto const& clippedPolygon : clippedPolygons)
			{
				auto numPolyVertices = (int)clippedPolygon.vertices.size();
				vector<ImVec2> imPoints(numPolyVertices);

				for (int i = 0; i < numPolyVertices; ++i)
				{
					imPoints[i] = {
						clippedPolygon.vertices[i].x - offset.x,
						ED_WINDOW_HEIGHT - (clippedPolygon.vertices[i].y - offset.y)
					};
				}

				drawList->AddPolyline(imPoints.data(), numPolyVertices, borderColour, ImDrawFlags_Closed, 2.0f);
			}
		}
	}

	// Primitives
	if (!primitiveIndices.empty())
	{
		drawList->AddDrawCmd();

		for (auto primitiveIndex : primitiveIndices)
		{
			auto primitive = world->getPrimitive(primitiveIndex);
			bool hovered = hoveredPrimitiveIndex == primitiveIndex;
			bool selected = selectedPrimitiveIndices.find(primitiveIndex) != selectedPrimitiveIndices.end();

			if (editorSettings.renderPrimitiveBorders || hovered || selected)
			{
				auto primVertices = primitive->getVertices();
				auto numPrimVertices = (uint32_t)primVertices.size();

				auto numVertices = (int)primVertices.size();
				vector<ImVec2> imPoints(numVertices);

				for (int i = 0; i < numVertices; ++i)
				{
					imPoints[i] = {
						primVertices[i].p.x - offset.x,
						ED_WINDOW_HEIGHT - (primVertices[i].p.y - offset.y)
					};
				}

				if (selected)
				{
					drawList->AddPolyline(imPoints.data(), numVertices, selectedPrimitiveColour, ImDrawFlags_Closed, 2.5f);
				}
				else if (hovered)
				{
					drawList->AddPolyline(imPoints.data(), numVertices, hoveredPrimitiveColour, ImDrawFlags_Closed, 1.5f);
				}
				else
				{
					drawList->AddPolyline(imPoints.data(), numVertices, primitiveColour, ImDrawFlags_Closed, 1.5f);
				}
			}
		}

		// Influence eye
		drawList->AddDrawCmd();
		drawList->Flags |= ImDrawListFlags_AntiAliasedFill;

		for (auto primitiveIndex : primitiveIndices)
		{
			bool selected = selectedPrimitiveIndices.find(primitiveIndex) != selectedPrimitiveIndices.end();
			if (!selected)
			{
				continue;
			}

			auto primitive = world->getPrimitive(primitiveIndex);

			auto influenceCentre = primitive->getInfluenceOriginOffset();

			influenceCentre.x -= offset.x;
			influenceCentre.y = ED_WINDOW_HEIGHT - (influenceCentre.y - offset.y);

			drawList->AddCircleFilled({ influenceCentre.x, influenceCentre.y }, 6, influenceEyeColour);

			drawList->AddBezierQuadratic(
				{ influenceCentre.x - 17, influenceCentre.y },
				{ influenceCentre.x, influenceCentre.y + 11 },
				{ influenceCentre.x + 17, influenceCentre.y },
				influenceEyeColour,
				1.5f);

			drawList->AddBezierQuadratic(
				{ influenceCentre.x + 17, influenceCentre.y },
				{ influenceCentre.x, influenceCentre.y - 11 },
				{ influenceCentre.x - 17, influenceCentre.y },
				influenceEyeColour,
				1.5f);
		}
	}

	// Player proxy
	drawList->AddDrawCmd();

	auto playerProxyPos = doc->getPlayerProxyPosition();
	float playerProxyAngle = doc->getPlayerProxyAngle();

	playerProxyPos.x -= offset.x;
	playerProxyPos.y = ED_WINDOW_HEIGHT - (playerProxyPos.y - offset.y);

	drawList->AddCircleFilled({ playerProxyPos.x, playerProxyPos.y }, 16, playerProxyColour);

	// Influence circles
	if (!primitiveIndices.empty())
	{
		drawList->AddDrawCmd();

		for (auto primitiveIndex : primitiveIndices)
		{
			bool selected = selectedPrimitiveIndices.find(primitiveIndex) != selectedPrimitiveIndices.end();
			if (!selected)
			{
				continue;
			}

			auto primitive = world->getPrimitive(primitiveIndex);

			auto influenceCentre = primitive->getInfluenceOriginOffset();

			influenceCentre.x -= offset.x;
			influenceCentre.y = ED_WINDOW_HEIGHT - (influenceCentre.y - offset.y);

			for (int i = 0; i < (int)bw::core::InputType::COUNT; ++i)
			{
				auto& lerper = primitive->getInputInterpolator((bw::core::InputType)i);
				auto const& point = lerper.getPoints();

				auto colour = influenceColours[i];

				for (auto const& point : point)
				{
					int numSegs = (int)point.first;
					drawList->AddCircle({ influenceCentre.x, influenceCentre.y }, point.first, colour, numSegs, 1.5f);
				}
			}
		}
	}

	// Bounding boxes
	if (editorSettings.renderPrimitiveBounds && !primitiveIndices.empty())
	{
		drawList->AddDrawCmd();

		for (auto primitiveIndex : primitiveIndices)
		{
			auto primitive = world->getPrimitive(primitiveIndex);
			auto const& bounds = primitive->getBounds();

			wp::Vector2 minExtent, maxExtent;
			bounds.getExtents(minExtent, maxExtent);

			minExtent.x -= offset.x;
			minExtent.y = ED_WINDOW_HEIGHT - (minExtent.y - offset.y);

			maxExtent.x -= offset.x;
			maxExtent.y = ED_WINDOW_HEIGHT - (maxExtent.y - offset.y);

			drawList->AddRect({ minExtent.x, minExtent.y }, { maxExtent.x, maxExtent.y }, boundsColour);
		}
	}
}