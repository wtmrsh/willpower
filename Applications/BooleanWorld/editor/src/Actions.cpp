#include <algorithm>

#include <core/RegularPolygon.h>
#include <core/CirclePolygon.h>
#include <core/RectanglePolygon.h>
#include <core/SuperformulaPolygon.h>

#include "Defines.h"
#include "Actions.h"
#include "EditorException.h"


namespace editor
{
	using namespace std;

	void selectPrimitive(Document* doc, uint32_t primitiveIndex)
	{
		doc->setSelectedPrimitiveIndices({ primitiveIndex });
	}

	void clearSelectedPrimitives(Document* doc)
	{
		doc->clearSelectedPrimitiveIndices();
	}

	void deletePrimitive(Document* doc, uint32_t primitiveIndex)
	{
		doc->getWorld()->removePrimitive(primitiveIndex);
		doc->removeSelectedPrimitiveIndex(primitiveIndex);
	}

	void _setPrimitiveParameters(bw::core::Primitive* prim, uint8_t priority, wp::Vector2 const& origin, wp::Vector2 const& offset, float scale, float angle)
	{
		prim->setPriority(priority);
		prim->setOrigin(origin);

		prim->setAnimationValues(bw::core::VertexTransformer::Key::Scale, { { 0.0f, scale}, { 1.0f, scale} });
		prim->setAnimationValues(bw::core::VertexTransformer::Key::Angle, { { 0.0f, angle }, { 1.0f, angle } });
		prim->setAnimationValues(bw::core::VertexTransformer::Key::OrbitAngle, { { 0.0f, 0.0f }, { 1.0f, 0.0f } });
		prim->setAnimationValues(bw::core::VertexTransformer::Key::OrbitDistance, { { 0.0f, 0.0f }, { 1.0f, 0.0f } });
	}

	void createRegularPolygonPrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		uint32_t numSides,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle)
	{
		auto world = doc->getWorld();

		auto index = world->addPrimitive(new bw::core::RegularPolygon(op, fillRule, numSides));
		_setPrimitiveParameters(world->getPrimitive(index), priority, origin, wp::Vector2::ZERO, scale, angle);
	}

	void createCirclePrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float resolution,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle)
	{
		auto world = doc->getWorld();

		auto index = world->addPrimitive(new bw::core::CirclePolygon(op, fillRule, resolution));
		_setPrimitiveParameters(world->getPrimitive(index), priority, origin, wp::Vector2::ZERO, scale, angle);
	}

	void createRectanglePrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float xyRatio,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle)
	{
		auto world = doc->getWorld();

		auto index = world->addPrimitive(new bw::core::RectanglePolygon(op, fillRule, xyRatio));
		_setPrimitiveParameters(world->getPrimitive(index), priority, origin, wp::Vector2::ZERO, scale, angle);
	}

	void createSuperformulaPrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float values[6],
		float resolution,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle)
	{
		auto world = doc->getWorld();

		auto index = world->addPrimitive(new bw::core::SuperformulaPolygon(op, fillRule, resolution, values));
		_setPrimitiveParameters(world->getPrimitive(index), priority, origin, wp::Vector2::ZERO, scale, angle);
	}

	void setPrimitiveOrigin(Document* doc, bw::core::Primitive* primitive, wp::Vector2 const& origin)
	{
		primitive->setOrigin(origin);
	}

	void setPrimitiveInfluenceOriginOffset(Document* doc, bw::core::Primitive* primitive, wp::Vector2 const& influenceOriginOffset)
	{
		primitive->setInfluenceOriginOffset(influenceOriginOffset);
	}

	void setPrimitiveFollowOrbitAngle(Document* doc, bw::core::Primitive* primitive, bool orient)
	{
		primitive->setFollowOrbitAngle(orient);
	}

	void setPrimitivePriority(Document* doc, bw::core::Primitive* primitive, uint8_t priority)
	{
		primitive->setPriority(priority);
	}

	void addAnimationKeyToInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, float time, float value)
	{
		lerper.addPoint(time, value);
	}

	void removeAnimationKeyFromInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, uint32_t index)
	{
		lerper.removePoint(index);
	}

	void addAnimationKeyToPrimitive(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, float time, float value)
	{
		primitive->addAnimationValue(key, time, value);
	}

	void removeAnimationKeyFromPrimitive(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index)
	{
		primitive->removeAnimationValue(key, index);
	}

	void updateAnimationKeyInInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, uint32_t index, float time, float value)
	{
		lerper.updatePoint(index, time, value);
	}

	void _addTransform(bw::core::tTransformFlow& flow)
	{
		flow.push_back(bw::core::tTransform::makePassthroughPrevious());
	}

	void addTransform(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key)
	{
		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			_addTransform(primitive->getScaleTransformFlow());
			break;

		case bw::core::VertexTransformer::Key::Angle:
			_addTransform(primitive->getAngleTransformFlow());
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			_addTransform(primitive->getOrbitAngleTransformFlow());
			break;

		case bw::core::VertexTransformer::Key::OrbitDistance:
			_addTransform(primitive->getOrbitDistanceTransformFlow());
			break;
		}
	}

	void _removeTransform(bw::core::tTransformFlow& flow, uint32_t index)
	{
		auto numTransforms = (uint32_t)flow.size();

		for (uint32_t i = index; i < numTransforms - 1; ++i)
		{
			flow[i] = flow[i + 1];
		}

		flow.pop_back();
	}

	void removeTransform(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index)
	{
		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			_removeTransform(primitive->getScaleTransformFlow(), index);
			break;

		case bw::core::VertexTransformer::Key::Angle:
			_removeTransform(primitive->getAngleTransformFlow(), index);
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			_removeTransform(primitive->getOrbitAngleTransformFlow(), index);
			break;

		case bw::core::VertexTransformer::Key::OrbitDistance:
			_removeTransform(primitive->getOrbitDistanceTransformFlow(), index);
			break;
		}
	}

	void _swapTransforms(bw::core::tTransformFlow& flow, uint32_t index1, uint32_t index2)
	{
		swap(flow[index1], flow[index2]);
	}

	void swapTransforms(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index1, uint32_t index2)
	{
		switch (key)
		{
		case bw::core::VertexTransformer::Key::Scale:
			_swapTransforms(primitive->getScaleTransformFlow(), index1, index2);
			break;

		case bw::core::VertexTransformer::Key::Angle:
			_swapTransforms(primitive->getAngleTransformFlow(), index1, index2);
			break;

		case bw::core::VertexTransformer::Key::OrbitAngle:
			_swapTransforms(primitive->getOrbitAngleTransformFlow(), index1, index2);
			break;

		case bw::core::VertexTransformer::Key::OrbitDistance:
			_swapTransforms(primitive->getOrbitDistanceTransformFlow(), index1, index2);
			break;
		}
	}

} // editor