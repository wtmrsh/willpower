#include <deque>
#include <set>

#include <core/World.h>

#include "Undo.h"
#include "Document.h"

#define MAX_STACK_SIZE 20


namespace editor
{
	using namespace std;

	struct UndoData
	{
		bw::core::World world;
		set<uint32_t> selection;
	};

	struct UndoEntry
	{
		std::string id;
		UndoData data;
	};

	// Internal
	static std::deque<UndoEntry> gUndoStack, gRedoStack;
	static UndoData gTransactionalData;
	static std::string gTransactionalId;
	static UndoableActionFunction gTransactionalFunc;


	bool canUndo()
	{
		return !gUndoStack.empty();
	}

	bool canRedo()
	{
		return !gRedoStack.empty();
	}

	size_t getUndoLevels()
	{
		return gUndoStack.size();
	}

	size_t getRedoLevels()
	{
		return gRedoStack.size();
	}

	void beginUndoableAction(Document* doc, string const& id, UndoableActionFunction func)
	{
		gTransactionalId = id;
		gTransactionalData.world = *doc->getWorld();
		gTransactionalData.selection = doc->getSelectedPrimitiveIndices();

		gTransactionalFunc = func;
	}

	void commitUndoableAction(Document* doc)
	{
		while (gUndoStack.size() >= MAX_STACK_SIZE)
		{
			gUndoStack.pop_front();
		}

		gRedoStack.clear();

		UndoEntry data{ gTransactionalId, gTransactionalData };
		gUndoStack.push_back(data);

		gTransactionalFunc(doc);

		gTransactionalFunc = nullptr;
		gTransactionalId.clear();
		gTransactionalData.world.clear();
		gTransactionalData.selection.clear();

		doc->setModified();
	}

	void transactUndoableAction(Document* doc, string const& id, UndoableActionFunction func)
	{
		beginUndoableAction(doc, id, func);
		commitUndoableAction(doc);
	}

	void abandonUndoableAction(Document* doc)
	{
		gTransactionalId.clear();
		gTransactionalData.world.clear();

		gTransactionalFunc = nullptr;
	}

	void undo(Document* doc, int count)
	{
		auto world = *doc->getWorld();
		auto const& selection = doc->getSelectedPrimitiveIndices();

		for (int i = 0; i < count; ++i)
		{
			auto const& oldEntry = gUndoStack.back();

			gRedoStack.push_back({ oldEntry.id, { world, selection } });

			if (i == (count - 1))
			{
				doc->setWorld(oldEntry.data.world);
				doc->setSelectedPrimitiveIndices(oldEntry.data.selection);
			}

			gUndoStack.pop_back();
		}
	}

	void redo(Document* doc, int count)
	{
		auto curWorld = *doc->getWorld();
		auto const& curSelection = doc->getSelectedPrimitiveIndices();

		for (int i = 0; i < count; ++i)
		{
			auto const& oldEntry = gRedoStack.back();

			gUndoStack.push_back({ oldEntry.id, { curWorld, curSelection } });

			if (i == (count - 1))
			{
				doc->setWorld(oldEntry.data.world);
				doc->setSelectedPrimitiveIndices(oldEntry.data.selection);
			}

			gRedoStack.pop_back();
		}
	}

	vector<HistoryItem> getActionHistory()
	{
		vector<HistoryItem> entries;

		for (auto item : gUndoStack)
		{
			entries.push_back({ item.id, true });
		}

		for (auto item : gRedoStack)
		{
			entries.push_back({ item.id, false });
		}

		return entries;
	}

} // editor