#include <exception>
#include <filesystem>
#include <format>

#pragma warning(push)
#pragma warning(disable: 4307)
#include <spdlog/spdlog.h>
#pragma warning(pop)
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <yaml-cpp/yaml.h>

#include "core/YamlSerializer.h"

#include "Document.h"
#include "EditorException.h"

extern spdlog::logger* gLogger;


namespace editor
{
	using namespace std;

	Document* Document::msInstance = nullptr;

	Document::Document()
		: mModified(false)
		, mHoveredPrimitiveIndex(~0u)
		, mPlayerProxyPosition({0, 0})
		, mPlayerProxyAngle(0.0f)
	{
	}

	Document::~Document()
	{
	}

	Document* Document::instance()
	{
		if (!msInstance)
		{
			msInstance = new Document();
		}

		return msInstance;
	}

	bool Document::isActive() const
	{
		return mWorld != nullptr;
	}

	void Document::setModified()
	{
		mModified = true;
	}

	bool Document::isModified() const
	{
		return mModified;
	}

	string const& Document::getFilepath() const
	{
		return mFilepath;
	}

	bool Document::hasFilepath() const
	{
		return mFilepath != "";
	}

	void Document::setWorld(bw::core::World const& world)
	{
		mWorld = make_shared<bw::core::World>(world);
	}

	shared_ptr<bw::core::World> Document::getWorld()
	{
		return mWorld;
	}

	void Document::setHoveredPrimitiveIndex(uint32_t index)
	{
		mHoveredPrimitiveIndex = index;
	}

	void Document::clearHoveredPrimitiveIndex()
	{
		mHoveredPrimitiveIndex = ~0u;
	}

	void Document::setSelectedPrimitiveIndices(set<uint32_t> const& indices)
	{
		mSelectedPrimitiveIndices = indices;
	}

	void Document::addSelectedPrimitiveIndex(uint32_t index)
	{
		mSelectedPrimitiveIndices.insert(index);
	}

	void Document::addSelectedPrimitiveIndices(set<uint32_t> const& indices)
	{
		set_union(
			mSelectedPrimitiveIndices.begin(),
			mSelectedPrimitiveIndices.end(),
			indices.begin(),
			indices.end(),
			inserter(mSelectedPrimitiveIndices, mSelectedPrimitiveIndices.begin()));
	}

	void Document::removeSelectedPrimitiveIndex(uint32_t index)
	{
		mSelectedPrimitiveIndices.erase(index);
	}

	void Document::removeSelectedPrimitiveIndices(set<uint32_t> const& indices)
	{
		set_difference(
			mSelectedPrimitiveIndices.begin(),
			mSelectedPrimitiveIndices.end(),
			indices.begin(),
			indices.end(),
			inserter(mSelectedPrimitiveIndices, mSelectedPrimitiveIndices.begin()));
	}

	void Document::clearSelectedPrimitiveIndices()
	{
		mSelectedPrimitiveIndices.clear();
	}

	uint32_t Document::getHoveredPrimitiveIndex() const
	{
		return mHoveredPrimitiveIndex;
	}

	set<uint32_t> const& Document::getSelectedPrimitiveIndices() const
	{
		return mSelectedPrimitiveIndices;
	}

	bool Document::hasSelection() const
	{
		return !mSelectedPrimitiveIndices.empty();
	}

	void Document::setPlayerProxyPosition(wp::Vector2 const& pos)
	{
		mPlayerProxyPosition = pos;
	}

	wp::Vector2 const& Document::getPlayerProxyPosition() const
	{
		return mPlayerProxyPosition;
	}

	void Document::setPlayerProxyAngle(float angle)
	{
		mPlayerProxyAngle = angle;
	}

	float Document::getPlayerProxyAngle() const
	{
		return mPlayerProxyAngle;
	}

	void Document::newDoc()
	{
		if (mWorld)
		{
			throw EditorException("Tried to create a new document with an existing one.");
		}
		
		mWorld = make_shared<bw::core::World>(8192.0f, 8192.0f);
		mModified = false;
	}

	void Document::closeDoc()
	{
		mWorld.reset();
		mFilepath = "";
	}

	bool Document::openDoc(string const& filepath)
	{
		mFilepath = filepath;

		auto path = filesystem::path(mFilepath);
		auto ext = path.extension().string();
		transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

		if (ext == ".yaml")
		{
			auto ser = shared_ptr<bw::core::YamlSerializer>(bw::core::YamlSerializer::fromFile(mFilepath));

			ser->deserialize();

			if (mWorld)
			{
				throw EditorException("Tried to create a new document with an existing one.");
			}

			mWorld = make_shared<bw::core::World>(8192.0f, 8192.0f);

			auto workData = bw::core::SerializationWorkData{};

			if (mWorld->deserialize(ser, workData))
			{
				auto const& warnings = mWorld->getDeserializationWarnings();

				if (!warnings.empty())
				{
					for (auto const& warning : warnings)
					{
						gLogger->warn(warning);
					}
				}

				return true;
			}
			else
			{
				auto const& errors = mWorld->getDeserializationErrors();

				if (!errors.empty())
				{
					for (auto const& error : errors)
					{
						gLogger->error(error);
					}
				}

				return false;
			}
		}
		else
		{
			throw EditorException(format("Could not open {} (filetype not supported)", mFilepath));
		}
	}

	void Document::saveDoc()
	{
		if (mFilepath == "")
		{
			throw EditorException("Document has no filepath set.");
		}

		auto path = std::filesystem::path(mFilepath);
		auto ext = path.extension().string();
		std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

		if (ext == ".yaml")
		{
			auto ser = shared_ptr<bw::core::YamlSerializer>(bw::core::YamlSerializer::toFile(mFilepath));
			auto workData = bw::core::SerializationWorkData{};

			mWorld->serialize(ser, workData);
			ser->serialize();
		}
		else
		{
			throw EditorException(format("Could not save {} (filetype not supported)", mFilepath));
		}

		mModified = false;
	}

	void Document::saveDocAs(string const& filepath)
	{
		mFilepath = filepath;
		saveDoc();
	}

}