#pragma once

#include <cstdint>
#include <set>

#include "Document.h"
#include "EditorSettings.h"
#include "RenderSettings.h"


void resizeFramebuffer(int width, int height);

uint32_t getWorldTextureId();

void renderWorldToTexture(editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings);

void initialiseWorldRendering(int width, int height, editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings);

void renderWorld(editor::Document* doc, editor::EditorSettings const& editorSettings, editor::RenderSettings const& renderSettings);