#pragma once

#include <willpower/common/Vector2.h>


namespace editor
{

	struct RenderSettings
	{
		wp::Vector2 origin;
	};

}
