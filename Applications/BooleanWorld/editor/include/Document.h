#pragma once

#include <string>
#include <set>

#include <willpower/common/Vector2.h>

#include "core/World.h"


namespace editor
{

	class Document
	{
		bool mModified;

		std::string mFilepath;

		std::shared_ptr<bw::core::World> mWorld;

		uint32_t mHoveredPrimitiveIndex;

		std::set<uint32_t> mSelectedPrimitiveIndices;

		wp::Vector2 mPlayerProxyPosition;

		float mPlayerProxyAngle;

		static Document* msInstance;

	public:

		Document();

		virtual ~Document();

		static Document* instance();

		bool isActive() const;

		void setModified();

		bool isModified() const;

		std::string const& getFilepath() const;

		bool hasFilepath() const;

		void setWorld(bw::core::World const& world);

		std::shared_ptr<bw::core::World> getWorld();

		void setHoveredPrimitiveIndex(uint32_t index);

		void clearHoveredPrimitiveIndex();

		uint32_t getHoveredPrimitiveIndex() const;

		void setSelectedPrimitiveIndices(std::set<uint32_t> const& indices);

		void addSelectedPrimitiveIndex(uint32_t index);

		void addSelectedPrimitiveIndices(std::set<uint32_t> const& indices);

		void removeSelectedPrimitiveIndex(uint32_t index);

		void removeSelectedPrimitiveIndices(std::set<uint32_t> const& indices);

		void clearSelectedPrimitiveIndices();

		std::set<uint32_t> const& getSelectedPrimitiveIndices() const;

		bool hasSelection() const;

		void setPlayerProxyPosition(wp::Vector2 const& pos);

		wp::Vector2 const& getPlayerProxyPosition() const;

		void setPlayerProxyAngle(float angle);

		float getPlayerProxyAngle() const;

		void newDoc();

		void closeDoc();

		bool openDoc(std::string const& filepath);

		void saveDoc();

		void saveDocAs(std::string const& filepath);

	};

} // editor
