#pragma once

#include <functional>
#include <string>
#include <vector>

#include "Document.h"


namespace editor
{

	struct HistoryItem
	{
		std::string id;
		bool isUndo;
	};

	typedef std::function<void(Document*)> UndoableActionFunction;

	bool canUndo();

	bool canRedo();

	size_t getUndoLevels();

	size_t getRedoLevels();

	void beginUndoableAction(Document* doc, std::string const& id, UndoableActionFunction func);

	void commitUndoableAction(Document* doc);

	void transactUndoableAction(Document* doc, std::string const& id, UndoableActionFunction func);

	void abandonUndoableAction(Document* doc);

	void undo(Document* doc, int count = 1);

	void redo(Document* doc, int count = 1);

	std::vector<HistoryItem> getActionHistory();

} // editor