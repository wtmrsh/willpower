#pragma once

#include <functional>
#include <string>

#include <willpower/common/Vector2.h>

#include "Document.h"
#include "EditorSettings.h"
#include "RenderSettings.h"


namespace editor
{
	typedef std::function<void(Document*)> DocumentHelperFunction;

	wp::Vector2 getMouseWorldPosition();

	uint32_t getHoveredPrimitiveIndex(editor::Document* doc);

	void newDocument(editor::Document* doc);

	void openDocument(editor::Document* doc);

	void saveDocument(editor::Document* doc);

	void exitApp(editor::Document* doc);

	void checkModifiedOperation(editor::Document* doc, std::string const& title, DocumentHelperFunction func);

	void handleModifiedDocument(editor::Document* doc, bool docAction, bool checkDocumentModified, std::string docText, DocumentHelperFunction helperFunc);

	void renderMainWindow(editor::Document* doc, editor::EditorSettings& editorSettings, editor::RenderSettings& renderSettings);

}