#pragma once

namespace editor
{

	struct EditorSettings
	{
		bool showGrid{ false };

		bool renderTriangulationBorder{ true };

		bool renderPrimitiveBorders{ true };

		bool renderPrimitiveBounds{ false };

		float gridSize{ 32 };
	};

} // editor
