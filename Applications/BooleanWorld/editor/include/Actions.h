#pragma once

#include <cstdint>

#include "Undo.h"
#include "Document.h"


namespace editor
{

	void selectPrimitive(Document* doc, uint32_t primitiveIndex);

	void clearSelectedPrimitives(Document* doc);

	void deletePrimitive(Document* doc, uint32_t primitiveIndex);

	void createRegularPolygonPrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		uint32_t numSides,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle);

	void createCirclePrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float resolution,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle);

	void createRectanglePrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float xyRatio,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle);

	void createSuperformulaPrimitive(Document* doc,
		bw::core::Primitive::Operation op,
		bw::core::Primitive::FillRule fillRule,
		float values[6],
		float resolution,
		uint8_t priority,
		wp::Vector2 const& origin,
		float scale,
		float angle);

	void setPrimitiveOrigin(Document* doc, bw::core::Primitive* primitive, wp::Vector2 const& origin);

	void setPrimitiveInfluenceOriginOffset(Document* doc, bw::core::Primitive* primitive, wp::Vector2 const& influenceOriginOffset);

	void setPrimitiveFollowOrbitAngle(Document* doc, bw::core::Primitive* primitive, bool orient);

	void setPrimitivePriority(Document* doc, bw::core::Primitive* primitive, uint8_t priority);

	void addAnimationKeyToPrimitive(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, float time, float value);

	void removeAnimationKeyFromPrimitive(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index);

	void addAnimationKeyToInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, float time, float value);

	void removeAnimationKeyFromInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, uint32_t index);

	void updateAnimationKeyInInterpolator(Document* doc, bw::core::Interpolator<float>& lerper, uint32_t index, float time, float value);

	void addTransform(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key);

	void removeTransform(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index);

	void swapTransforms(Document* doc, bw::core::Primitive* primitive, bw::core::VertexTransformer::Key key, uint32_t index1, uint32_t index2);
} // editor