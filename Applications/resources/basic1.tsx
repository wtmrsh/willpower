<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="Basic" tilewidth="32" tileheight="32" tilecount="64" columns="8">
 <image source="tiles.png" width="256" height="256"/>
 <tile id="0" type="CornerNW_nw"/>
 <tile id="1" type="CornerNE_ne"/>
 <tile id="2" type="Vertical_w"/>
 <tile id="7" type="Blank"/>
 <tile id="8" type="CornerSW_sw"/>
 <tile id="9" type="CornerSE_se"/>
 <tile id="10" type="Horizontal_s"/>
 <tile id="16" type="CornerNW_se"/>
 <tile id="17" type="CornerNE_sw"/>
 <tile id="18" type="Vertical_e"/>
 <tile id="24" type="CornerSW_ne"/>
 <tile id="25" type="CornerSE_nw"/>
 <tile id="26" type="Horizontal_n"/>
</tileset>
