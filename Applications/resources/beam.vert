@@Version

@@In pos = vec2
@@Passthrough uv = vec2
@@Passthrough colour = vec4

void main()
{
	vec4 transVertex = @MCPMatrix * vec4(@In(pos).xy, 0, 1);
	vec2 centredPos = vec2(transVertex.x - @HalfWindowSize.x, transVertex.y - @HalfWindowSize.y);
	gl_Position = vec4(centredPos / @HalfWindowSize, 0, 1);
}