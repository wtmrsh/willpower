REM utils
cd ext\utils
git pull origin master
call RebuildAll.bat

cd ..\..

REM MassivePolyPusher
cd ext\MassivePolyPusher
git pull origin master
call RebuildAll.bat

cd ..\..

REM Willpower
devenv Willpower/build/vs2017/Willpower.sln /Rebuild "Release|Win32"
devenv Willpower/build/vs2017/Willpower.sln /Rebuild "Debug|Win32"

devenv Applib/build/vs2017/Applib.sln /Rebuild "Release|Win32"
devenv Applib/build/vs2017/Applib.sln /Rebuild "Debug|Win32"

devenv Applications/build/vs2017/Applications.sln /Rebuild "Release|Win32"
devenv Applications/build/vs2017/Applications.sln /Rebuild "Debug|Win32"

devenv Launcher/build/vs2017/Launcher.sln /Rebuild "Release|Win32"
devenv Launcher/build/vs2017/Launcher.sln /Rebuild "Debug|Win32"

