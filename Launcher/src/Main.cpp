#include <iostream>

#include "utils/StringUtils.h"

#include "Platform.h"

#if APP_PLATFORM == APP_PLATFORM_WINDOWS
#   include <windows.h>
#	include <psapi.h>
#endif

#include <willpower/common/Exceptions.h>
#include <willpower/common/Logger.h>
#include <willpower/common/MathsUtils.h>

#include <willpower/application/ServiceLocator.h>
#include <willpower/application/ApplicationSettings.h>
#include <willpower/application/resourcesystem/ResourceManager.h>
#include <willpower/application/resourcesystem/ResourceExceptions.h>

#include <mpp/RenderSystem.h>
#include <mpp/ResourceManager.h>
#include <mpp/StaticLogger.h>
#include <mpp/Logger.h>

#include "ProgramOptions.h"
#include "ApplicationDLL.h"
#include "StateManager.h"
#include "ExitApplicationException.h"
#include "DirectoryResourceLocation.h"
#include "ZipResourceLocation.h"

#include "sdl/WindowSDL.h"
#include "sdl/TimerSDL.h"

using namespace std;
using namespace wp;

// Logging
Logger* gLogger = nullptr;
mpp::Logger* gMppLogger = nullptr;

// Platform objects
static ApplicationDLL* gDLL = nullptr;
static StateManager* gStateMgr = nullptr;
static WindowSDL* gWindow = nullptr;
static TimerSDL* gTimer = nullptr;

// Application objects
static application::ApplicationSettings* gAppSettings = nullptr;
static application::resourcesystem::ResourceManager* gResourceMgr = nullptr;

// Rendering objects
static mpp::RenderSystem* gRenderSystem = nullptr;
static mpp::ResourceManager* gRenderSystemResourceMgr = nullptr;

//
// Initialise all systems
//
ProgramOptions startup(string const& configFile)
{
	// Create loggers
	gLogger = new Logger();
	gLogger->open("LauncherLog.html");

	gMppLogger = new mpp::Logger();
	if (!gMppLogger->initialise("mpp.log", mpp::Logger::Level::Debug))
	{
		throw exception("Could not create MPP logger!");
	}

	// Read in program options
	ProgramOptions options = parseProgramOptions(configFile);
	logProgramOptions(options, gLogger);

	// Set up application settings
	gAppSettings = new application::ApplicationSettings();
	gAppSettings->VideoWidth = options.screenWidth;
	gAppSettings->VideoHeight = options.screenHeight;
	gAppSettings->Fullscreen = options.fullScreen;

	application::ServiceLocator::provideApplicatonSettings(gAppSettings);

	// Initialise SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		throw exception("Could not initialise SDL video!");
	}

	// Create timer
	gTimer = new TimerSDL();

	// Create window
	gWindow = new WindowSDL("Window", options);
	gWindow->create();

	// Create render system
	//mpp::enable_static_log(MPP_RESOURCE_LOGFILE, true);

	gRenderSystem = new mpp::RenderSystem(gWindow->getWidth(), gWindow->getHeight(), gMppLogger);
	gRenderSystemResourceMgr = new mpp::ResourceManager(gRenderSystem, gMppLogger);
	gRenderSystem->createCoreResources(gRenderSystemResourceMgr);

	// Resource manager
	gResourceMgr = new application::resourcesystem::ResourceManager(gRenderSystem, gRenderSystemResourceMgr, gLogger);

	// Add resource location factories
	gResourceMgr->addResourceLocationFactory("Directory", [](string const& location, string const& definitionFile)
		-> application::resourcesystem::ResourceLocation*
	{
		return new DirectoryResourceLocation(gLogger, location, definitionFile);
	});

	gResourceMgr->addResourceLocationFactory("ZipFile", [](string const& location, string const& definitionFile)
		-> application::resourcesystem::ResourceLocation*
	{
		return new ZipResourceLocation(gLogger, location, definitionFile);
	});

	// Add resource locations
	for (auto const& rl: options.resourceLocations)
	{
		gResourceMgr->addResourceLocation(rl.type, rl.path, rl.definitionFile);
	}

	// Load application DLL
	gDLL = new ApplicationDLL();
	gDLL->load(options.dll, options.arguments, gLogger, gResourceMgr);

	// Create state manager and get state factories
	gStateMgr = new StateManager(gResourceMgr, gRenderSystem, gRenderSystemResourceMgr);
	gDLL->registerStateFactories(gStateMgr);

	return options;
}

//
// Destroy all systems
//
void shutdown()
{
	// Destroy state manager
	delete gStateMgr;
	gStateMgr = nullptr;

	// Destroy resource manager
	delete gResourceMgr;
	gResourceMgr = nullptr;

	// Destroy render system
	gRenderSystem->destroyCoreResources();
	delete gRenderSystem;
	gRenderSystem = nullptr;

	gRenderSystemResourceMgr->dumpResources("final-resources.csv");
	delete gRenderSystemResourceMgr;
	gRenderSystemResourceMgr = nullptr;

	delete gMppLogger;
	gMppLogger = nullptr;

	// Destroy window
	delete gWindow;
	gWindow = nullptr;

	// Destroy timer
	delete gTimer;
	gTimer = nullptr;

	// Shut down SDL
	SDL_Quit();

	// Destroy application
	delete gAppSettings;
	gAppSettings = nullptr;
	application::ServiceLocator::provideApplicatonSettings(nullptr);

	// Destroy application DLL
	delete gDLL;
	gDLL = nullptr;

	// Destroy logger
	delete gLogger;
	gLogger = nullptr;
}

//
// Helper to set up debug text to display
//

void setupDebugPanel()
{
	string fpsColour;
	float fps = gTimer->getFPS();
	if (fps < 30)
	{
		fpsColour = "[#FF0000FF]";
	}
	else if (fps < 55)
	{
		fpsColour = "[#FFFF00FF]";
	}
	else
	{
		fpsColour = "[#00FF00FF]";
	}

	string fpsDisplay = STR_FORMAT("FPS: {}{}", fpsColour, (int)fps);
	gRenderSystem->setDebugPreMessages({ fpsDisplay });

	gRenderSystem->setDebugPostMessages(gStateMgr->getDebuggingText());

	gRenderSystem->showDebugPanel(gWindow->displayDebugging(),
		mpp::RenderSystem::TimeUnit::Milliseconds,
		mpp::RenderSystem::SizeUnit::Megabytes);
}

//
// Entry point
//
#if APP_PLATFORM == APP_PLATFORM_WINDOWS
int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	string configFile = "Launcher.cfg";

	if (__argc > 1)
	{
		configFile = string(__argv[1]);
	}
#else
#  undef main // SDL_main is included via SDL.h
int main(int argc, char** argv)
{
	string configFile = "Launcher.cfg";
	if (argc > 1)
	{
		configFile = string(argv[1]);
	}
#endif

	int exitCode = 0;
	try
	{
		auto options = startup(configFile);
		
		// Update timer
		TimerSDL frameTimer;
		uint64_t updateCount;

		// Main loop
		float accum = 0.0f;
		const float updateFreq = 1.0f / 60.0f;

		mpp::RenderInfo renderInfo;

		gStateMgr->enterInitialState(options.gameResource);

		gTimer->reset();
		while (true)
		{
			// Get frame time
			float frameTime = gTimer->getDeltaTime();
			gTimer->addFrameToCounter(frameTime);
			
			accum += frameTime;

			// Frame timing
			frameTimer.reset();
			updateCount = 0;

			// Process window messages
			gWindow->processEvents(gStateMgr);

			// Update current state
			while (accum >= updateFreq)
			{
				accum -= updateFreq;
				gStateMgr->update(updateFreq);
				updateCount++;
			}

			// Render
			setupDebugPanel();

			gRenderSystem->startStatsCollection();

			gStateMgr->render(gRenderSystem, gRenderSystemResourceMgr);

			auto ri = gRenderSystem->finishStatsCollection();

			// Flip to screen
			gWindow->show();
		}
	}
	catch (ExitApplicationException& e)
	{
		gLogger->info(e.getMessage());
		exitCode = e.getExitCode();
	}
	catch (application::resourcesystem::ResourceException& e)
	{
		auto res = e.getResource();
		gLogger->error("Error in resource: " + res->getQualifiedName());
		gLogger->error(e.what());
		exitCode = 1;

#ifdef _DEBUG
		char const* msg = e.what();

		size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), 0, 0);
		wstring ret(reqLength, L'\0');

		::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), &ret[0], (int)ret.length());
		OutputDebugString(ret.c_str());
#endif
	}
	catch (application::resourcesystem::ResourceSystemException& e)
	{
		gLogger->error(e.what());
		exitCode = 1;

#ifdef _DEBUG
		char const* msg = e.what();

		size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), 0, 0);
		wstring ret(reqLength, L'\0');

		::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), &ret[0], (int)ret.length());
		OutputDebugString(ret.c_str());
#endif
	}
	catch (Exception& e)
	{
		gLogger->error(e.what());
		exitCode = 1;

#ifdef _DEBUG
		char const* msg = e.what();

		size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), 0, 0);
		wstring ret(reqLength, L'\0');

		::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), &ret[0], (int)ret.length());
		OutputDebugString(ret.c_str());
#endif
	}
	catch (exception& e)
	{
		gLogger->error(e.what());
		exitCode = 1;

#ifdef _DEBUG
		char const* msg = e.what();

		size_t reqLength = ::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), 0, 0);
		wstring ret(reqLength, L'\0');

		::MultiByteToWideChar(CP_UTF8, 0, msg, (int)strlen(msg), &ret[0], (int)ret.length());
		OutputDebugString(ret.c_str());
#endif
	}

	shutdown();
	return exitCode;
}