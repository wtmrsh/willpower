#pragma once

#define VER_PRODUCTNAME_STR         "Willpower Engine"

#define VER_FILEVERSION             0,0,0,0
#define VER_FILEVERSION_STR         "0.0.0.0 dev-build"

#define VER_PRODUCTVERSION          0,0,0,0
#define VER_PRODUCTVERSION_STR      "0.0.0.0 dev-build"

#define VER_COMPANYNAME_STR         ""
#define VER_LEGALCOPYRIGHT_STR      "2016 wtmrsh@gmail.com"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR

#define VER_COMPANYDOMAIN_STR       ""
