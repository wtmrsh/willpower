#include <willpower/application/StateExceptions.h>
#include <willpower/application/ServiceLocator.h>
#include <willpower/application/ApplicationSettings.h>

#include "StateMapLoad.h"
#include "PlayObjectCreators.h"
#include "Map.h"

namespace applib
{

	using namespace std;
	using namespace wp;

	StateMapLoad::StateMapLoad(GeometryMeshRendererFactory* factory, bool useThreading)
		: ThreadableLoadState("MapLoad", Action::Load, useThreading)
		, mFactory(factory)
	{
	}

	ThreadableLoadState::LoadFunction StateMapLoad::getWorkFunction(wp::application::resourcesystem::ResourceManager* resourceMgr, mpp::RenderSystem* renderSystem, mpp::ResourceManager* renderResourceMgr, void* args)
	{
		WP_UNUSED(renderSystem);
		WP_UNUSED(renderResourceMgr);
		WP_UNUSED(args);

		auto transitionData = static_cast<StateTransitionData*>(args);
		return bind(&StateMapLoad::loadResources, this, resourceMgr, &transitionData->mapData);
	}

	void StateMapLoad::loadResources(application::resourcesystem::ResourceManager* resourceMgr, MapTransitionData* transitionData)
	{
		string mapName = transitionData->nextMapName;
		string mapNamespace = mapName;

		// Set next map in transition data
		auto mapResource = resourceMgr->getResource(mapName, mapNamespace);
		mTransitionData.mapData.nextMap.map = mapResource;

		// Initialise resource loading
		mWillpowerResourcesToLoad = { mapResource };
		mWillpowerResourcesToUnload.clear();

		if (usingThreading())
		{
			for (auto res : mWillpowerResourcesToLoad)
			{
				addPendingResourceName(res->getName());
			}

			auto loadCallbackFn = bind(&StateMapLoad::loadResourceCallback,
				this,
				placeholders::_1,
				placeholders::_2,
				placeholders::_3);

			for (auto resource : mWillpowerResourcesToLoad)
			{
				resourceMgr->createResource(resource, loadCallbackFn);
				resourceMgr->loadResource(resource, loadCallbackFn);
				resourceMgr->acquireResource(resource);
			}

			mWillpowerResourcesToLoad.clear();
		}

		// Create objects
		auto mapRendererFn = [this, mapResource](bool useThreading)
		{
			this->addText("Creating map renderer");
			auto renderer = createMapRenderer(mapResource, this->mwRenderSystem, this->mwRenderResourceMgr, mFactory, useThreading);
			this->mTransitionData.mapData.nextMap.mapRenderer = renderer ? unique_ptr<viz::GeometryMeshRenderer>(renderer) : nullptr;
		};

		auto mapCollSimFn = [this, mapResource](bool useThreading)
		{
			this->addText("Creating player collision sim");
			auto collisionSim = createMapCollisionSim(mapResource, this->mwRenderSystem, this->mwRenderResourceMgr, useThreading);
			this->mTransitionData.mapData.nextMap.mapCollisionSim = collisionSim ? unique_ptr<collide::Simulation>(collisionSim) : nullptr;
		};

		auto mapMeshCollisionMgrFn = [this, mapResource](bool useThreading)
		{
			this->addText("Creating dynamic collision manager");
			auto meshCollisionMgr = createMeshCollisionManager(mapResource, this->mwRenderSystem, this->mwRenderResourceMgr, useThreading);
			this->mTransitionData.mapData.nextMap.meshCollisionMgr = meshCollisionMgr ? unique_ptr<firepower::MeshCollisionManager>(meshCollisionMgr) : nullptr;
		};

		processPostWork({ mapRendererFn, mapCollSimFn, mapMeshCollisionMgrFn });
	}


} // applib